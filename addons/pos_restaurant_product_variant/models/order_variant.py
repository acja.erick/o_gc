from odoo import fields, api, models


class OrderVariant(models.Model):
    _name = "order.variant"
    product_tmpl_id = fields.Many2one('product.template', 'Combo', required=1,
                                      domain=[('available_in_pos', '=', True)])
    attribute_id = fields.Many2one('product.attribute', 'Attribute', required=1)
    value_id = fields.Many2one('product.attribute.value', string='Value', required=1)
    price_extra = fields.Float('Price extra', help='Price extra will included to public price of product', required=1)

    product_id = fields.Many2one('product.product', 'Product link stock',
                                 help='If choice, will made stock move, automatic compute on hand of this product')
    order_id = fields.Many2one('pos.order.id', 'Order id')
    quantity = fields.Float('quantity', help='Quantity Of variant')
    uom_id = fields.Many2one('uom.uom', 'Unit link stock')
    selected = fields.Boolean('selected', default=1)
    order_product_id = fields.Many2one('product.product', 'Product link stock')
    name = fields.Char('name')

    def write_variant(self, variant, order):
        uom_id_temp = variant['uom_id']
        if uom_id_temp:
            uom_id_temp = variant['uom_id'][0]
        selected_temp = 0
        if 'selected' in variant:
            selected_temp = 1
        variant_json = {
            'product_tmpl_id': variant['product_tmpl_id'][0],
            'attribute_id': variant['attribute_id'][0],
            'value_id' : variant['value_id'][0],
            'price_extra' : variant['price_extra'],
            'product_id' : variant['product_id'][0],
            'order_id' : order.id,
            'quantity' : variant['quantity'],
            'uom_id' : uom_id_temp,
            'selected' : selected_temp,
            'name' : order.name
        }
        self.create(variant_json)



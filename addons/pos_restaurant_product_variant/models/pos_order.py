# -*- coding: utf-8 -*-
from odoo import api, fields, models, tools, _
from odoo.tools import float_is_zero
import logging
import psycopg2
from functools import partial

from odoo.exceptions import UserError
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import json

_logger = logging.getLogger(__name__)


class pos_order(models.Model):
    _inherit = "pos.order"

    picking_ids = fields.One2many('stock.picking', 'pos_order_id', 'Delivery Orders')

    @api.model
    def create_from_ui(self, orders, draft=False):
        _logger.info('begin create_from_ui')
        order_ref = super(pos_order, self).create_from_ui(orders, draft)
        _logger.info(order_ref)
        order_ids = [order.get('id', False) for order in order_ref]
        orders_object = self.browse(order_ids)
        for order in orders_object:
            self.create_picking_with_multi_variant(orders, order)
            self.create_bom_with_multi_variant(orders, order)
        _logger.info('end create_from_ui')
        return order_ref


    @api.model
    def _order_fields(self, ui_order):
        print('ui_order get called in super', ui_order)
        process_line = partial(self.env['pos.order.line']._order_line_fields, session_id=ui_order['pos_session_id'])
        return {
            'user_id': ui_order['user_id'] or False,
            'session_id': ui_order['pos_session_id'],
            'lines': [process_line(l) for l in ui_order['lines']] if ui_order['lines'] else False,
            'pos_reference': ui_order['name'],
            'sequence_number': ui_order['sequence_number'],
            'partner_id': ui_order['partner_id'] or False,
            'date_order': ui_order['creation_date'].replace('T', ' ')[:19],
            'fiscal_position_id': ui_order['fiscal_position_id'],
            'pricelist_id': ui_order['pricelist_id'],
            'amount_paid': ui_order['amount_paid'],
            'amount_total': ui_order['amount_total'],
            'amount_tax': ui_order['amount_tax'],
            'amount_return': ui_order['amount_return'],
            'company_id': self.env['pos.session'].browse(ui_order['pos_session_id']).company_id.id,
            'to_invoice': ui_order['to_invoice'] if "to_invoice" in ui_order else False,
            'foodbot_order_id' : ui_order['foodbot_order_id'] if "foodbot_order_id" in ui_order else False
        }

    def create_bom_with_multi_variant(self,orders, order):
        print ("create bom variant",orders, order)
        _logger.info('create_bom_with_multi_variant')
        # print ("aaaaaaaaaaaaa",a)

    def create_picking_with_multi_variant(self, orders, order):
        _logger.info('begin create_picking_with_multi_variant')
        print ("before callllllllll multi variant from mrp")
        _logger.info('before callllllllll multi variant')
        self.create_bom_with_multi_variant(orders, order)
        print ("after callllllllllll")
        for o in orders:
            warehouse_obj = self.env['stock.warehouse']
            move_object = self.env['stock.move']
            moves = move_object
            picking_obj = self.env['stock.picking']
            product_obj = self.env['product.product']
            variants = []
            if o['data']['name'] == order.pos_reference:
                picking_type = order.picking_type_id
                if not picking_type:
                    continue
                location_id = order.location_id.id
                address = order.partner_id.address_get(['delivery']) or {}
                if order.partner_id:
                    destination_id = order.partner_id.property_stock_customer.id
                else:
                    if (not picking_type) or (not picking_type.default_location_dest_id):
                        customerloc, supplierloc = warehouse_obj._get_partner_locations()
                        destination_id = customerloc.id
                    else:
                        destination_id = picking_type.default_location_dest_id.id
                if o['data'] and o['data'].get('lines', False):
                    for line in o['data']['lines']:
                        if line[2] and line[2].get('variants', False):
                            for var in line[2]['variants']:
                                print('var', var)
                                if var.get('product_id'):
                                    variants.append(var)
                            del line[2]['variants']
                if variants:
                    _logger.info('Processing Order have variant items')
                    picking_vals = {
                        'name': order.name + '/Variant',
                        'origin': order.name,
                        'partner_id': address.get('delivery', False),
                        'date_done': order.date_order,
                        'picking_type_id': picking_type.id,
                        'company_id': order.company_id.id,
                        'move_type': 'direct',
                        'note': order.note or "",
                        'location_id': location_id,
                        'location_dest_id': destination_id,
                        'pos_order_id': order.id,
                    }
                    _logger.info('{0}'.format(picking_vals))
                    order_picking = picking_obj.create(picking_vals)
                    for variant in variants:
                        product = product_obj.browse(variant.get('product_id')[0])
                        print('variant and product', product, variant)
                        move = move_object.create({
                            'name': order.name,
                            'product_uom': variant['uom_id'] and variant['uom_id'][0] if variant.get('uom_id', []) else product.uom_id.id,
                            'picking_id': order_picking.id,
                            'picking_type_id': picking_type.id,
                            'product_id': product.id,
                            'product_uom_qty': abs(variant['quantity']),
                            'state': 'draft',
                            'location_id': location_id,
                            'location_dest_id': destination_id,
                        })
                        print('variant', variant)
                        self.env['order.variant'].write_variant(variant, order)
                        moves |= move
                    order_picking.action_assign()
                    #order_picking.force_assign()
                    # wiz = self.env['stock.immediate.transfer'].create({'pick_ids': [(4, order_picking.id)]})
                    # wiz.process()
                    if order_picking:
                        order_picking.action_done()
                    _logger.info('Delivery Picking Variant : %s' % order_picking.name)
        _logger.info('end create_picking_with_multi_variant')
        return True

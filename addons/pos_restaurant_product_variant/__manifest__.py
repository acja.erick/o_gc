{
    'name': "POS Product Multi variant",
    'version': '3.2.1',
    'category': 'Point of Sale',
    'author': 'TL Technology',
    'sequence': 0,
    'summary': 'Define attributes and variants on one product \n'
               'Cashier can choice variant from request of customer\n',
    'description': 'POS Product multi vatiant',
    'depends': ['pos_restaurant'],
    'data': [
        'security/ir.model.access.csv',
        'import/template.xml',
        'views/product.xml',
        'views/pos_order.xml',
    ],
    'qweb': [
        'static/src/xml/*.xml'
    ],
    'price': '100',
    'website': 'http://posodoo.com',
    'application': True,
    'images': ['static/description/icon.png'],
    'support': 'thanhchatvn@gmail.com',
    "currency": 'EUR',
    'live_test_url': 'http://posodoo.com/web/signup',
    "license": "OPL-1"
}

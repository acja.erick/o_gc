odoo.define('pos_restaurant_product_variant', function (require) {
    var models = require('point_of_sale.models');
    var screens = require('point_of_sale.screens');
    var core = require('web.core');
    
    var gui = require('point_of_sale.gui');
    var PopupWidget = require('point_of_sale.popups');
    
    var multiprint = require('pos_restaurant.multiprint');
    var utils = require('web.utils');

    var QWeb = core.qweb;
    var _t = core._t;
    var round_di = utils.round_decimals;

    models.load_models([
        {
            model: 'product.variant',
            fields: ['product_tmpl_id', 'attribute_id', 'value_id', 'price_extra', 'product_id', 'quantity', 'uom_id'],
            domain: function (self) {
                return [['active', '=', true]];
            },
            loaded: function (self, variants) {
                self.variants = variants;
                self.variant_by_product_tmpl_id = {};
                self.variant_by_id = {};
                for (var i = 0; i < variants.length; i++) {
                    var variant = variants[i];
                    self.variant_by_id[variant.id] = variant;
                    if (!self.variant_by_product_tmpl_id[variant['product_tmpl_id'][0]]) {
                        self.variant_by_product_tmpl_id[variant['product_tmpl_id'][0]] = [variant]
                    } else {
                        self.variant_by_product_tmpl_id[variant['product_tmpl_id'][0]].push(variant)
                    }
                }
            }
        },
        {
            model: 'product.attribute',
            fields: ['name', 'multi_choice'],
            domain: function (self) {
                return [];
            },
            loaded: function (self, attributes) {
                self.product_attributes = attributes;
                self.product_attribute_by_id = {};
                for (var i = 0; i < attributes.length; i++) {
                    var attribute = attributes[i];
                    self.product_attribute_by_id[attribute.id] = attribute;
                }
            }
        }
    ]);


    var pop_up_select_variants = PopupWidget.extend({
        template: 'pop_up_select_variants',
        get_attribute_by_id: function (attribute_id) {
            return this.pos.product_attribute_by_id[attribute_id];
        },
        show: function (options) {
            var self = this;
            this._super(options);
            this.variants_selected = {};
            var variants = options.variants;
            var selected_orderline = options.selected_orderline;
            var variants_selected = selected_orderline['variants'];
            var variants_by_product_attribute_id = {};
            var attribute_ids = [];

            // if (reward['discount_product_ids'].indexOf(curr_line['product']['id']) != -1) {
            for (var i = 0; i < variants.length; i++) {
                var variant = variants[i];
                var attribute_id = variant['attribute_id'][0];
                var attribute = this.pos.product_attribute_by_id[attribute_id];
                if (attribute_ids.indexOf(attribute_id) == -1) {
                    attribute_ids.push(attribute_id)
                }
                if (attribute) {
                    if (!variants_by_product_attribute_id[attribute_id]) {
                        variants_by_product_attribute_id[attribute_id] = [variant];
                    } else {
                        variants_by_product_attribute_id[attribute_id].push(variant);
                    }
                }
            }
            if (variants_selected.length != 0) {
                for (var i = 0; i < variants.length; i++) {
                    var variant = _.findWhere(variants_selected, {id: variants[i].id});
                    if (variant) {
                        self.variants_selected[variant.id] = variant;
                        variants[i]['selected'] = true
                    } else {
                        variants[i]['selected'] = false
                    }
                }
            }
            var image_url = window.location.origin + '/web/image?model=product.template&field=image_medium&id=';
            self.$el.find('div.body').html(QWeb.render('attribute_variant_list', {
                attribute_ids: attribute_ids,
                variants_by_product_attribute_id: variants_by_product_attribute_id,
                image_url: image_url,
                widget: self
            }));

            this.$('.variant').click(function () {
                var variant_id = parseInt($(this).data('id'));
                var variant = self.pos.variant_by_id[variant_id];
                if (variant) {
                    if ($(this).closest('.variant').hasClass("item-selected") == true) {
                        $(this).closest('.variant').toggleClass("item-selected");
                        delete self.variants_selected[variant.id];
                    } else {
                        $(this).closest('.variant').toggleClass("item-selected");
                        self.variants_selected[variant.id] = variant;

                    }
                }
            });
            this.$('.radio-check').click(function () {
                var variant_id = parseInt($(this).data('id'));
                var variant = self.pos.variant_by_id[variant_id];
                if (variant) {
                    if ($(this).closest('.radio-check').hasClass("radio-selected") == true) {
                        $(this).closest('.radio-check').toggleClass("radio-selected");
                        delete self.variants_selected[variant.id];
                    } else {
                        $(this).closest('.radio-check').toggleClass("radio-selected");
                        for (var index_variant in self.variants_selected) {
                            if (self.variants_selected[index_variant].attribute_id[0] == variant.attribute_id[0]) {
                                delete self.variants_selected[index_variant];
                            }
                        }
                        self.variants_selected[variant.id] = variant;

                    }
                }
            });
            this.$('.confirm-variant').click(function () {
                var variants_selected = self.variants_selected;
                var variants = _.map(variants_selected, function (variant) {
                    return variant;
                });
                selected_orderline['variants'] = variants;
                selected_orderline.trigger('change', selected_orderline);
                //self.pos.get_order().trigger('change');
                // if (variants.length == 0) {
                //     return selected_orderline.set_unit_price(selected_orderline.product.lst_price);
                // } else {
                //     var price_extra_total = selected_orderline.product.lst_price;
                //     for (var i = 0; i < variants.length; i++) {
                //         price_extra_total += variants[i].price_extra;
                //     }
                //     selected_orderline.set_unit_price(price_extra_total);
                //     selected_orderline.trigger('update:OrderLine');
                //     self.pos.get_order().trigger('change');
                // }
            });
        }
    });
    gui.define_popup({name: 'pop_up_select_variants', widget: pop_up_select_variants});

    var button_add_variants = screens.ActionButtonWidget.extend({
        template: 'button_add_variants',
        init: function (parent, options) {
            this._super(parent, options);

            this.pos.get('orders').bind('add remove change', function () {
                this.renderElement();
            }, this);

            this.pos.bind('change:selectedOrder', function () {
                this.renderElement();
            }, this);
        },
        button_click: function () {
            var selected_orderline = this.pos.get_order().selected_orderline;
            if (!selected_orderline) {
                return this.gui.show_popup('error', {
                    message: _t('Please select line before choice a variants'),
                });
            } else {
                if (this.pos.variant_by_product_tmpl_id[selected_orderline.product.product_tmpl_id]) {
                    return this.gui.show_popup('pop_up_select_variants', {
                        variants: this.pos.variant_by_product_tmpl_id[selected_orderline.product.product_tmpl_id],
                        selected_orderline: selected_orderline,
                    });
                }
            }
        },
    });

    screens.define_action_button({
        'name': 'button_add_variants',
        'widget': button_add_variants,
        'condition': function () {
            return true;
        },
    });

    var button_remove_variants = screens.ActionButtonWidget.extend({
        template: 'button_remove_variants',
        init: function (parent, options) {
            this._super(parent, options);

            this.pos.get('orders').bind('add remove change', function () {
                this.renderElement();
            }, this);

            this.pos.bind('change:selectedOrder', function () {
                this.renderElement();
            }, this);
        },
        button_click: function () {
            var selected_orderline = this.pos.get_order().selected_orderline;
            if (!selected_orderline) {
                this.gui.show_popup('error', {
                    message: _t('Please select line'),
                });
                return;
            } else {
                selected_orderline['variants'] = [];
                selected_orderline.set_unit_price(selected_orderline.product.lst_price);
            }
        },
    });

    screens.define_action_button({
        'name': 'button_remove_variants',
        'widget': button_remove_variants,
        'condition': function () {
            return true;
        },
    });

    var _super_Order = models.Order.prototype;
    models.Order = models.Order.extend({
        build_line_resume: function () {
            var resume = _super_Order.build_line_resume.apply(this, arguments);
            var new_resume = {};
            var multi_variant = false;
            this.orderlines.each(function (line) {
                if (line.mp_skip) {
                    return;
                }
                var line_hash = line.get_line_diff_hash();
                var qty = Number(line.get_quantity());
                var note = line.get_note();
                var product_id = line.get_product().id;

                if (typeof new_resume[line_hash] === 'undefined') {
                    new_resume[line_hash] = {
                        qty: qty,
                        note: note,
                        product_id: product_id,
                        product_name_wrapped: line.generate_wrapped_product_name(),
                    };
                    if (line['variants']) {
                        multi_variant = true;
                        new_resume[line_hash]['variants'] = line['variants'];
                    }
                } else {
                    new_resume[line_hash].qty += qty;
                }
            });
            if (multi_variant) {
                return new_resume
            } else {
                return resume;
            }
        },
        computeChanges: function (categories) {
            _super_Order.computeChanges.apply(this, arguments);
            var current_res = this.build_line_resume();
            var old_res = this.saved_resume || {};
            var json = this.export_as_JSON();
            var add = [];
            var rem = [];
            var line_hash;
            for (line_hash in current_res) {
                var curr = current_res[line_hash];
                var old = old_res[line_hash];
                if (typeof old === 'undefined') {
                    var value = {
                        'id': curr.product_id,
                        'name': this.pos.db.get_product_by_id(curr.product_id).display_name,
                        'name_wrapped': curr.product_name_wrapped,
                        'note': curr.note,
                        'qty': curr.qty,
                    }
                    if (curr['variants']) {
                        value['variants'] = curr['variants'];
                    }
                    add.push(value);

                } else if (old.qty < curr.qty) {
                    var value = {
                        'id': curr.product_id,
                        'name': this.pos.db.get_product_by_id(curr.product_id).display_name,
                        'name_wrapped': curr.product_name_wrapped,
                        'note': curr.note,
                        'qty': curr.qty - old.qty,
                    }
                    if (curr['variants']) {
                        value['variants'] = curr['variants'];
                    }
                    add.push(value);
                } else if (old.qty > curr.qty) {
                    var value = {
                        'id': curr.product_id,
                        'name': this.pos.db.get_product_by_id(curr.product_id).display_name,
                        'name_wrapped': curr.product_name_wrapped,
                        'note': curr.note,
                        'qty': old.qty - curr.qty,
                    }
                    if (curr['variants']) {
                        value['variants'] = curr['variants'];
                    }
                    rem.push(value);
                }
            }
            for (line_hash in old_res) {
                if (typeof current_res[line_hash] === 'undefined') {
                    var old = old_res[line_hash];
                    rem.push({
                        'id': old.product_id,
                        'name': this.pos.db.get_product_by_id(old.product_id).display_name,
                        'name_wrapped': old.product_name_wrapped,
                        'note': old.note,
                        'qty': old.qty,
                    });
                }
            }
            if (categories && categories.length > 0) {
                var self = this;
                var _add = [];
                var _rem = [];
                for (var i = 0; i < add.length; i++) {
                    if (self.pos.db.is_product_in_category(categories, add[i].id)) {
                        _add.push(add[i]);
                    }
                }
                add = _add;
                for (var i = 0; i < rem.length; i++) {
                    if (self.pos.db.is_product_in_category(categories, rem[i].id)) {
                        _rem.push(rem[i]);
                    }
                }
                rem = _rem;
            }

            var d = new Date();
            var hours = '' + d.getHours();
            hours = hours.length < 2 ? ('0' + hours) : hours;
            var minutes = '' + d.getMinutes();
            minutes = minutes.length < 2 ? ('0' + minutes) : minutes;
            return {
                'new': add,
                'cancelled': rem,
                'table': json.table || false,
                'floor': json.floor || false,
                'name': json.name || 'unknown order',
                'time': {
                    'hours': hours,
                    'minutes': minutes,
                }
            };
        },
    });

    var _super_order_line = models.Orderline.prototype;
    models.Orderline = models.Orderline.extend({
        initialize: function (attributes, options) {
            _super_order_line.initialize.apply(this, arguments);
            if (!this.variants) {
                this.variants = [];
            }
        },
        init_from_JSON: function (json) {
            if (json.variants) {
                this.variants = json.variants
            }
            return _super_order_line.init_from_JSON.apply(this, arguments);
        },
        export_as_JSON: function () {
            var json = _super_order_line.export_as_JSON.apply(this, arguments);
            if (this.variants) {
                json.variants = this.variants;
            }
            return json
        },
        export_for_printing: function () {
            var datas = _super_order_line.export_for_printing.apply(this, arguments);
            if (this.variants) {
                datas['variants'] = this.variants;
            }
            return datas;
        },
        is_multi_variant: function () {
            var variants = this.pos.variant_by_product_tmpl_id[this.product.product_tmpl_id]
            if (!variants) {
                return false;
            }
            if (variants.length > 0) {
                return true;
            } else {
                return false;
            }
        },
        has_variants: function () {
            if (this.variants && this.variants.length && this.variants.length > 0) {
                return true
            } else {
                return false
            }
        },
        get_unit_price: function(){
            var digits = this.pos.dp['Product Price'];
            // round and truncate to mimic _symbol_set behavior

            let price = this.price;

            price += this.get_variant_price();

            return parseFloat(round_di(price || 0, digits).toFixed(digits));
        },
        get_variant_price(){
            var self = this;
            var price_extra = 0;
            if (self.variants.length > 0) {
                for (var i = 0; i < self.variants.length; i++) {
                    price_extra += self.variants[i].price_extra;
                }
            
            }
            return price_extra;
        }
    });

    screens.OrderWidget.include({
        update_summary: function () {
            this._super();
            var selected_order = this.pos.get_order();
            if (selected_order && selected_order.selected_orderline) {
                var check = selected_order.selected_orderline.is_multi_variant();
                var buttons = this.getParent().action_buttons;
                if (buttons && buttons.button_add_variants) {
                    buttons.button_add_variants.highlight(check);
                }
                var has_variants = selected_order.selected_orderline.has_variants();
                if (buttons && buttons.button_remove_variants) {
                    buttons.button_remove_variants.highlight(has_variants);
                }
            }
        }
    })

});

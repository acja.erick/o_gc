odoo.define('pos_restaurant_product_variant', function (require) {
"use strict";

    var PosBaseWidget = require('point_of_sale.BaseWidget');
    var models = require('point_of_sale.models');
    var screens = require('point_of_sale.screens');
    var core = require('web.core');
    var ajax = require('web.ajax');
    var rpc = require('web.rpc');


    var gui = require('point_of_sale.gui');
    var PopupWidget = require('point_of_sale.popups');

    var multiprint = require('pos_restaurant.multiprint');
    var utils = require('web.utils');

    var QWeb = core.qweb;
    var _t = core._t;
    var round_di = utils.round_decimals;

    var BarcodeEvents = require('barcodes.BarcodeEvents').BarcodeEvents;
    var Printer = require('point_of_sale.Printer').Printer;

    var _super_posmodel = models.PosModel.prototype;

    models.PosModel = models.PosModel.extend({
        init_from_JSON: function (json) {
            if (json.foodbot_order_id) {
                this.foodbot_order_id = json.foodbot_order_id
            }
            return _super_posmodel.init_from_JSON.apply(this, arguments);
        },
        export_as_JSON: function () {
            var json = _super_posmodel.export_as_JSON.apply(this, arguments);
            if (this.foodbot_order_id) {
                json.foodbot_order_id = this.foodbot_order_id;
            }
            console.log('Returing json', json)
            return json
        }
    });


    var foodbotOrderSet = new Set();

    models.load_models([
        {
            model: 'product.variant',
            fields: ['product_tmpl_id', 'attribute_id', 'value_id', 'price_extra', 'product_id', 'quantity', 'uom_id'],
            domain: function (self) {
                return [['active', '=', true]];
            },
            loaded: function (self, variants) {
                self.variants = variants;
                self.variant_by_product_tmpl_id = {};
                self.variant_by_id = {};
                for (var i = 0; i < variants.length; i++) {
                    var variant = variants[i];
                    self.variant_by_id[variant.id] = variant;
                    if (!self.variant_by_product_tmpl_id[variant['product_tmpl_id'][0]]) {
                        self.variant_by_product_tmpl_id[variant['product_tmpl_id'][0]] = [variant]
                    } else {
                        self.variant_by_product_tmpl_id[variant['product_tmpl_id'][0]].push(variant)
                    }
                }
            }
        },
        {
            model: 'product.attribute',
            fields: ['name', 'multi_choice', 'is_promotion'],
            domain: function (self) {
                return [];
            },
            loaded: function (self, attributes) {
                console.log('Self and attributes', self, attributes)
                self.product_attributes = attributes;
                self.product_attribute_by_id = {};
                for (var i = 0; i < attributes.length; i++) {
                    var attribute = attributes[i];
                    self.product_attribute_by_id[attribute.id] = attribute;
                }



                var posConfig  = self.config;
                console.log('Config got', self)
                var ablyKey = self.company.foodbot_ably_key;
                if(posConfig.accept_foodbot_orders) {
                        console.log('Ably started', ablyKey)
                         const ably = new Ably.Realtime(ablyKey);
                         var channel = ably.channels.get('odoo-onlineorders');
                         channel.subscribe(function(message) {
                             console.log('Order got', message)
                             processOrderPacket(message.data, self)
                         });

                        function processOrderPacket(orderEventString, selfObj) {
                            var orderEvent = orderEventString;

                            try {
                                orderEvent = JSON.parse(orderEventString);
                            } catch (e) {
                                console.log('Exception got')
                            }

                            //Condition for checking location id and restaurant id
                            if(posConfig.branch_id == orderEvent.branch_id) {
                                var orderDetails = getOrderDetails(orderEvent, selfObj)
                            }
                            console.log('orderEvent', orderEvent)
                        }

            function getOrderDetails(orderEvent, selfObj) {
                        let dataPack = { "restaurant_id": orderEvent.restaurant_id, "branch_id": orderEvent.branch_id};
                        console.log('Sending data',  JSON.stringify(dataPack))
                        console.log('Got data', selfObj.company.foodbot_url)
                        var companyConfig = selfObj.company;
                        $.ajax({
                            type: "POST",
                            url: companyConfig.foodbot_url + "/pos/odoo-orderpull",
                            data: JSON.stringify(dataPack),
                            headers : {
                                "Content-Type" : "application/json",
                                "Authorization" : companyConfig.foodbot_auth_key,
                                "UUID" : companyConfig.foodbot_uuid
                            },
                            success: function(response){
                                console.log('Response got', response);
                                var orderDetails = response.data;
                                //var res = {"status":"success","status_code":"200","message":"Order found","data":{"orders":[{"order_id":400500720,"customer_name":"Vishal Pawar","phone_number":"7972279044","sender_id":"7972279044","sub_total":"82.00","discount":"0.00","total":"82.00","net_cost":"82.00","net_tax":"","order_timestamp":"2020-11-12 18:12:48","promotion_details":[{"foodbot_promotion_id":500104,"pos_promotion_id":1,"promotion_title":"1° Bebida Staff","discount":81}],"payment_details":[{"foodbot_payment_method_id":3,"pos_payment_method_id":1,"payment_method_name":"Cash","payment_amount":"82.00"}],"order_items":[{"foodbot_item_id":5829,"pos_item_id":"23","quantity":2,"item_cost":60,"tax_amount":0,"total_after_tax":60,"item_name":"Oolong Milk Tea ","modifiers":[]}]}]}}
                                //var orderDetails = res.data;

                                if(orderDetails !== undefined) {
                                var orders = orderDetails.orders;
                                for(var i = 0 ; i < orders.length ; i++) {
                                     var foodBotOrder = orders[i];
                                     console.log('foodbotOrderSet', foodbotOrderSet)

                                     if(!foodbotOrderSet.has(foodBotOrder.order_id)) {

                                     foodbotOrderSet.add(foodBotOrder.order_id)
                                     var orderGot = _super_posmodel.add_new_order.call(self);
                                     orderGot.foodbot_order_id = foodBotOrder.order_id;
                                     console.log('New Order created', {orderGot, foodBotOrder})
                                     var foodBotOrderItems = foodBotOrder.order_items;
                                     for(let j = 0 ; j < foodBotOrderItems.length; j++) {
                                         let foodBotItem = foodBotOrderItems[j];
                                         let product = orderGot.pos.db.product_by_id[foodBotItem.pos_item_id]
                                         if(product !== undefined) {
                                             let newProduct = $.extend(true, {}, product);
                                             console.log('product got', {product, newProduct, orderGot})
                                             newProduct.taxes_id = product.taxes_id;
                                             var line = new models.Orderline({}, {pos: orderGot.pos, order: orderGot, product: newProduct});
                                             line.price_manually_set = true; // no need pricelist change, price of promotion change the same, i blocked
                                             line.set_quantity(foodBotItem.quantity);
                                             line.set_unit_price(foodBotItem.item_cost);

                                             if(j == 0) {
                                                line.set_note('FoodBot Order #' + foodBotOrder.order_id);
                                             }
                                             var foodBotMod = foodBotItem.modifiers;
                                             let variants_selected = {}
                                             if(foodBotMod !== undefined && foodBotMod.length > 0) {
                                                    for(let z = 0 ; z < foodBotMod.length; z++) {
                                                        let productTemplId = product.product_tmpl_id;
                                                        let variantArr = orderGot.pos.variant_by_product_tmpl_id[productTemplId]
                                                        if(variantArr != undefined && variantArr.length > 0) {
                                                               for(let teVar = 0 ; teVar < variantArr.length; teVar++) {
                                                                      if(variantArr[teVar].value_id[0] == foodBotMod[z].pos_modifier_id) {
                                                                            variantArr[teVar]['foodbot_var_price'] = foodBotMod[z].modifier_cost
                                                                            variants_selected[variantArr[teVar].id] = variantArr[teVar]
                                                                            break
                                                                      }
                                                               }
                                                        }
                                                    }


                                                     var variants = _.map(variants_selected, function (variant) {
                                                            let newVariant = $.extend(true, {}, variant);
                                                            newVariant.price_extra = variant.foodbot_var_price;
                                                            delete variant.foodbot_var_price;
                                                            return newVariant;
                                                     });
                                                     console.log('variants selected', variants_selected, variants)
                                                     line['variants'] = variants;
                                                     //selected_orderline.trigger('change', selected_orderline);
                                                     //self.pos.get_order().trigger('change');

                                                     var price_extra_total = foodBotItem.item_cost;
                                                     console.log('Product lst price', price_extra_total)
                                                     for (var i = 0; i < variants.length; i++) {
                                                            price_extra_total += variants[i].price_extra;
                                                            console.log('variant added', price_extra_total)
                                                     }
                                                     line.set_unit_price(price_extra_total);
                                                     line.trigger('update:OrderLine');
                                             }

                                             console.log('posPromotion', self)
                                             orderGot.orderlines.add(line);
                                             //addPromotions(product, orderGot, foodBotOrder)
                                         } else {
                                            alert('Error : Order could not be placed. Please check product mapping for.' + foodBotItem.pos_item_id)
                                            continue
                                         }
                                     }
                                     let paymentDetails = foodBotOrder.payment_details;
                                     for(let k = 0 ; k < paymentDetails.length; k++) {
                                        console.log('Payment detail', paymentDetails[k])
                                        let payment = paymentDetails[k];
                                        let paymentLine = orderGot.pos.payment_methods_by_id[payment.pos_payment_method_id];
                                        if(paymentLine !== undefined) {
                                            paymentLine.paid = true;
                                            paymentLine.amount = payment.payment_amount;
                                            //paymentLine.set_amount(payment.payment_amount)
                                            console.log('paymentLine created', paymentLine)
                                            orderGot.add_paymentline(paymentLine);
                                            orderGot.selected_paymentline.paid = true;
                                            orderGot.selected_paymentline.set_amount(payment.payment_amount);
                                        } else {
                                            alert('Payment method not found')
                                        }
                                     }

                                     let promotions = [];
                                     let promotions_selected = {}
                                     console.log('order lines ', line)
                                     let foodBotPromotions = foodBotOrder.promotion_details;
                                     for(let n = 0; n < foodBotPromotions.length; n++) {
                                          let promotionIdTemp = foodBotPromotions[n].pos_promotion_id;
                                          if(promotionIdTemp !== undefined) {
                                                    let promotionTmp = orderGot.pos.promotion_by_id[promotionIdTemp];
                                                    if(promotionTmp === undefined) {
                                                         alert('Error : Promotion not found. Please check promotion mapping for.' + promotionIdTemp)
                                                    } else {
                                                        console.log('promotionTmp', promotionTmp)
                                                        // promotionTmp.foodbot_promotion_price = foodBotPromotions[n].discount
                                                        promotions_selected[promotionTmp.id] = promotionTmp;
                                                }
                                        } else
                                            console.log('Promotion not found for', foodBotPromotions)
                                     }

                                     for (var k in promotions_selected) {
                                             promotions.push(promotions_selected[k]);
                                     }
                                     console.log('promotions got', promotions)
                                     if (promotions.length) {
                                             orderGot.manual_compute_promotion(promotions, true)
//                                             for(var t = 0 ;  t < promotions.length ; t++) {
//                                             var p = promotions[t];
//                                             console.log('we got p here', p)
//                                             var options = {};
//                                             //options.promotion_discount_total_order = true;
//                                             //options.promotion = true;
//                                             var promoProduct = orderGot.pos.db.get_product_by_id(p['product_id'][0]);
//                                             options.promotion_reason = "% discount applied.";
//                                             options['applied_promotion_id'] = p['id'];
//                                             options['type'] = p['type'];
//                                             orderGot.add_promotion(promoProduct, p.foodbot_promotion_price, 1, options)
//                                             delete p.foodbot_promotion_price;
//                                         }
                                     }

                                     addCustomer(orderGot, foodBotOrder)
                                     console.log('Order at last', orderGot)
                                     orderGot.trigger('change', self);
                                     acknowledgeOrder(foodBotOrder, orderEvent, selfObj)

                                 } else {
                                    acknowledgeOrder(foodBotOrder, orderEvent, selfObj)
                                    alert('Warning : Duplicate foodbot order.')
                                 }

                                }

                                } else {
                                    console.log('No orders from Foodbot')
                                }

                            },
                            fail: function(response) {
                                console.log('fail response got from api', response)
                            }
                        });
           }




                }
            }
        }
    ]);

    function addCustomer(orderGot, foodBotOrder) {
       let partner = orderGot.pos.db.get_partner_by_barcode(foodBotOrder.sender_id)
       console.log('partner got', partner)
        if(partner !== undefined) {
             orderGot.set_client(partner)
        } else {

            let fields = {
                name : foodBotOrder.customer_name,
                phone : foodBotOrder.phone_number,
                barcode : foodBotOrder.sender_id,
                is_ably : true
            }

            rpc.query({
                model: 'res.partner',
                method: 'create_from_ui',
                args: [fields],
            })
            .then(function(partner_id){
                console.log('Partner id got', partner_id, fields)
                orderGot.pos.load_new_partners();
                orderGot.set_client(orderGot.pos.db.get_partner_by_id(partner_id))
                //self.saved_client_details(partner_id);
            }).catch(function(error){
                error.event.preventDefault();
                var error_body = _t('Your Internet connection is probably down.');
                if (error.message.data) {
                    var except = error.message.data;
                    error_body = except.arguments && except.arguments[0] || except.message || error_body;
                }
                orderGot.gui.show_popup('error',{
                    'title': _t('Error: Could not Save Changes'),
                    'body': error_body,
                });
                //contents.on('click','.button.save',function(){ self.save_client_details(partner); });
            });

        }
    }

    function addPromotions(product,orderGot, foodBotOrder) {
            let foodBotPromotions = foodBotOrder.promotion_details;
            console.log('foodBotPromotions', foodBotPromotions)
            for(let i = 0; foodBotPromotions.length; i++) {
                let newProduct = $.extend(true, {}, product);
                var line = new models.Orderline({}, {pos: orderGot.pos, order: orderGot, product: newProduct});
                line.price_manually_set = true; // no need pricelist change, price of promotion change the same, i blocked
                line.set_quantity(1);
                //line.set_unit_price(price);
                line.set_unit_price(foodBotPromotions[i].item_discount);
                orderGot.orderlines.add(line);
            }
    }

    function acknowledgeOrder(foodBotOrder, orderEvent, selfObj) {
          let dataPack = { "restaurant_id": orderEvent.restaurant_id, "branch_id": orderEvent.branch_id, "order_id" : foodBotOrder.order_id};
          let companyConfig = selfObj.company;
          $.ajax({
                            type: "POST",
                            url: companyConfig.foodbot_url + "/pos/odoo-orderack",
                            data: JSON.stringify(dataPack),
                            headers : {
                                "Content-Type" : "application/json",
                                "Authorization" : companyConfig.foodbot_auth_key,
                                "UUID" : companyConfig.foodbot_uuid
                            },
                            success: function(response){
                                console.log('Success response got', response)
                            },
                            fail: function(response) {
                                console.log('Error response', response)
                            }
        })
    }


    var pop_up_select_variants = PopupWidget.extend({
        template: 'pop_up_select_variants',
        get_attribute_by_id: function (attribute_id) {
            return this.pos.product_attribute_by_id[attribute_id];
        },
        show: function (options) {
            var self = this;
            this._super(options);
            this.variants_selected = {};
            var variants = [];
            variants = Object.assign([], options.variants);
            var selected_orderline = options.selected_orderline;
            var variants_selected = Object.assign([], selected_orderline['variants']);
            var variants_by_product_attribute_id = {};
            var attribute_ids = [];

            // if (reward['discount_product_ids'].indexOf(curr_line['product']['id']) != -1) {
            for (var i = 0; i < variants.length; i++) {
                var variant = Object.assign({},variants[i]);
                variant.selected = false;
                var attribute_id = variant['attribute_id'][0];
                var attribute = this.pos.product_attribute_by_id[attribute_id];
                if (attribute_ids.indexOf(attribute_id) == -1) {
                    attribute_ids.push(attribute_id)
                }
                if (attribute) {
                    if (!variants_by_product_attribute_id[attribute_id]) {
                        variants_by_product_attribute_id[attribute_id] = [variant];
                    } else {
                        variants_by_product_attribute_id[attribute_id].push(variant);
                    }
                }
            }
            if (variants_selected.length != 0) {
                let atributeKeys = Object.keys(variants_by_product_attribute_id);
                for(var j=0; j<atributeKeys.length; j++){
                    let tempVariants = variants_by_product_attribute_id[atributeKeys[j]];
                    for (var i = 0; i < tempVariants.length; i++) {
                        var variant = _.findWhere(variants_selected, {id: tempVariants[i].id});
                        if (variant) {
                            self.variants_selected[variant.id] = variant;
                            tempVariants[i]['selected'] = true
                        } else {
                            tempVariants[i]['selected'] = false
                        }
                    }
                }
            }
            var image_url = window.location.origin + '/web/image?model=product.template&field=image_medium&id=';
            self.$el.find('div.body').html(QWeb.render('attribute_variant_list', {
                attribute_ids: attribute_ids,
                variants_by_product_attribute_id: variants_by_product_attribute_id,
                image_url: image_url,
                widget: self
            }));

            this.$('.variant').click(function () {
                var variant_id = parseInt($(this).data('id'));
                var variant = self.pos.variant_by_id[variant_id];
                if (variant) {
                    if ($(this).closest('.variant').hasClass("item-selected") == true) {
                        $(this).closest('.variant').toggleClass("item-selected");
                        delete self.variants_selected[variant.id];
                    } else {
                        $(this).closest('.variant').toggleClass("item-selected");
                        self.variants_selected[variant.id] = variant;

                    }
                }
            });
            this.$('.radio-check').click(function () {
                var variant_id = parseInt($(this).data('id'));
                var variant_name = $(this).attr('name');
                var variant = self.pos.variant_by_id[variant_id];
                function resetCheckbox($chkBox, type){
                    let radioSelector = "input.radio-check[name='"+variant_name+"']";
                    $($(radioSelector)).removeClass('radio-selected');
                    $($(radioSelector)).prop('checked', false);
                    if(type === 'add'){
                        $chkBox.addClass('radio-selected');
                        $chkBox.prop('checked', true);
                    }

                }
                if (variant) {
                    let $radioCheck = $(this).closest('.radio-check');
                    if ($radioCheck.hasClass("radio-selected")) {
                        resetCheckbox($radioCheck, 'remove');
                        variant.selected = false;
                        delete self.variants_selected[variant.id];
                    } else {
                        resetCheckbox($radioCheck, 'add');
                        for (var index_variant in self.variants_selected) {
                            if (self.variants_selected[index_variant].attribute_id[0] == variant.attribute_id[0]) {
                                delete self.variants_selected[index_variant];
                            }
                        }
                        variant.selected = true;
                        self.variants_selected[variant.id] = variant;

                    }
                }
            });
            this.$('.confirm-variant').click(function () {
                var variants_selected = self.variants_selected;
                var variants = _.map(variants_selected, function (variant) {
                    return variant;
                });
                console.log('variants selected', variants_selected, variants)
                selected_orderline['variants'] = variants;
                //selected_orderline.trigger('change', selected_orderline);
                //self.pos.get_order().trigger('change');
                if (variants.length == 0) {
                    let unitPrice =  selected_orderline.set_unit_price(selected_orderline.product.lst_price);
                    selected_orderline.trigger('update:OrderLine');
                    self.pos.get_order().trigger('change');
                    return unitPrice
                } else {
                    var price_extra_total = selected_orderline.product.lst_price;
                    for (var i = 0; i < variants.length; i++) {
                        price_extra_total += variants[i].price_extra;
                    }
                    selected_orderline.set_unit_price(price_extra_total);
                    selected_orderline.price_manually_set = true;
                    selected_orderline.trigger('update:OrderLine');
                    self.pos.get_order().trigger('change');
                }
            });
        }
    });
    gui.define_popup({name: 'pop_up_select_variants', widget: pop_up_select_variants});

    var button_add_variants = screens.ActionButtonWidget.extend({
        template: 'button_add_variants',
        init: function (parent, options) {
            this._super(parent, options);

            this.pos.get('orders').bind('add remove change', function () {
                this.renderElement();
            }, this);

            this.pos.bind('change:selectedOrder', function () {
                this.renderElement();
            }, this);
        },
        button_click: function () {
            var selected_orderline = this.pos.get_order().selected_orderline;
            if (!selected_orderline) {
                return this.gui.show_popup('error', {
                    message: _t('Please select line before choice a variants'),
                });
            } else {
                if (this.pos.variant_by_product_tmpl_id[selected_orderline.product.product_tmpl_id]) {
                    return this.gui.show_popup('pop_up_select_variants', {
                        variants: this.pos.variant_by_product_tmpl_id[selected_orderline.product.product_tmpl_id],
                        selected_orderline: selected_orderline,
                    });
                }
            }
        },
    });

    screens.define_action_button({
        'name': 'button_add_variants',
        'widget': button_add_variants,
        'condition': function () {
            return true;
        },
    });

    var button_remove_variants = screens.ActionButtonWidget.extend({
        template: 'button_remove_variants',
        init: function (parent, options) {
            this._super(parent, options);

            this.pos.get('orders').bind('add remove change', function () {
                this.renderElement();
            }, this);

            this.pos.bind('change:selectedOrder', function () {
                this.renderElement();
            }, this);
        },
        button_click: function () {
            var selected_orderline = this.pos.get_order().selected_orderline;
            if (!selected_orderline) {
                this.gui.show_popup('error', {
                    message: _t('Please select line'),
                });
                return;
            } else {
                selected_orderline['variants'] = [];
                selected_orderline.set_unit_price(selected_orderline.product.lst_price);
            }
        },
    });

    screens.define_action_button({
        'name': 'button_remove_variants',
        'widget': button_remove_variants,
        'condition': function () {
            return true;
        },
    });

    var _super_Order = models.Order.prototype;
    models.Order = models.Order.extend({
        build_line_resume: function () {
            var resume = _super_Order.build_line_resume.apply(this, arguments);
            var new_resume = {};
            var multi_variant = false;
            this.orderlines.each(function (line) {
                if (line.mp_skip) {
                    return;
                }
                var line_hash = line.get_line_diff_hash();
                var qty = Number(line.get_quantity());
                var note = line.get_note();
                var product_id = line.get_product().id;

                if (typeof new_resume[line_hash] === 'undefined') {
                    new_resume[line_hash] = {
                        qty: qty,
                        note: note,
                        product_id: product_id,
                        product_name_wrapped: line.generate_wrapped_product_name(),
                    };
                    if (line['variants']) {
                        multi_variant = true;
                        new_resume[line_hash]['variants'] = line['variants'];
                    }
                } else {
                    new_resume[line_hash].qty += qty;
                }
            });
            if (multi_variant) {
                return new_resume
            } else {
                return resume;
            }
        },
        computeChanges: function(categories){
            _super_Order.computeChanges.apply(this, arguments);
            var current_res = this.build_line_resume();
            var old_res     = this.saved_resume || {};
            var json        = this.export_as_JSON();
            var add = [];
            var rem = [];
            var line_hash;

            for ( line_hash in current_res) {
                var curr = current_res[line_hash];
                var old  = {};
                var found = false;
                for(var id in old_res) {
                    if(old_res[id].product_id === curr.product_id){
                        found = true;
                        old = old_res[id];
                        break;
                    }
                }

                if (!found) {
                    let rec = {
                        'id':       curr.product_id,
                        'name':     this.pos.db.get_product_by_id(curr.product_id).display_name,
                        'name_wrapped': curr.product_name_wrapped,
                        'note':     curr.note,
                        'qty':      curr.qty,
                    }
                    if (curr['variants']) {
                        rec['variants'] = curr['variants'];
                    }
                    add.push(rec);
                } else if (old.qty < curr.qty) {
                    
                    let rec = {
                        'id':       curr.product_id,
                        'name':     this.pos.db.get_product_by_id(curr.product_id).display_name,
                        'name_wrapped': curr.product_name_wrapped,
                        'note':     curr.note,
                        'qty':      curr.qty - old.qty,
                    };
                    if (curr['variants']) {
                        rec['variants'] = curr['variants'];
                    }
                    add.push(rec);
                } else if (old.qty > curr.qty) {
                    
                    let rec = {
                        'id':       curr.product_id,
                        'name':     this.pos.db.get_product_by_id(curr.product_id).display_name,
                        'name_wrapped': curr.product_name_wrapped,
                        'note':     curr.note,
                        'qty':      old.qty - curr.qty,
                    };
                    if (curr['variants']) {
                        rec['variants'] = curr['variants'];
                    }
                    rem.push(rec);
                }
            }

            for (line_hash in old_res) {
                var found = false;
                for(var id in current_res) {
                    if(current_res[id].product_id === old_res[line_hash].product_id)
                        found = true;
                }
                if (!found) {
                    var old = old_res[line_hash];
                    rem.push({
                        'id':       old.product_id,
                        'name':     this.pos.db.get_product_by_id(old.product_id).display_name,
                        'name_wrapped': old.product_name_wrapped,
                        'note':     old.note,
                        'qty':      old.qty,
                    });
                }
            }

            if(categories && categories.length > 0){
                // filter the added and removed orders to only contains
                // products that belong to one of the categories supplied as a parameter

                var self = this;

                var _add = [];
                var _rem = [];

                for(var i = 0; i < add.length; i++){
                    if(self.pos.db.is_product_in_category(categories,add[i].id)){
                        _add.push(add[i]);
                    }
                }
                add = _add;

                for(var i = 0; i < rem.length; i++){
                    if(self.pos.db.is_product_in_category(categories,rem[i].id)){
                        _rem.push(rem[i]);
                    }
                }
                rem = _rem;
            }

            var d = new Date();
            var hours   = '' + d.getHours();
                hours   = hours.length < 2 ? ('0' + hours) : hours;
            var minutes = '' + d.getMinutes();
                minutes = minutes.length < 2 ? ('0' + minutes) : minutes;

            return {
                'new': add,
                'cancelled': rem,
                'table': json.table || false,
                'floor': json.floor || false,
                'name': json.name  || 'unknown order',
                'time': {
                    'hours':   hours,
                    'minutes': minutes,
                },
            };

        },
        /*
        computeChanges: function (categories) {
            _super_Order.computeChanges.apply(this, arguments);
            var current_res = this.build_line_resume();
            var old_res = this.saved_resume || {};
            var json = this.export_as_JSON();
            var add = [];
            var rem = [];
            var line_hash;
            for (line_hash in current_res) {
                var curr = current_res[line_hash];
                var old = old_res[line_hash];
                if (typeof old === 'undefined') {
                    var value = {
                        'id': curr.product_id,
                        'name': this.pos.db.get_product_by_id(curr.product_id).display_name,
                        'name_wrapped': curr.product_name_wrapped,
                        'note': curr.note,
                        'qty': curr.qty,
                    }
                    if (curr['variants']) {
                        value['variants'] = curr['variants'];
                    }
                    add.push(value);

                } else if (old.qty < curr.qty) {
                    var value = {
                        'id': curr.product_id,
                        'name': this.pos.db.get_product_by_id(curr.product_id).display_name,
                        'name_wrapped': curr.product_name_wrapped,
                        'note': curr.note,
                        'qty': curr.qty - old.qty,
                    }
                    if (curr['variants']) {
                        value['variants'] = curr['variants'];
                    }
                    add.push(value);
                } else if (old.qty > curr.qty) {
                    var value = {
                        'id': curr.product_id,
                        'name': this.pos.db.get_product_by_id(curr.product_id).display_name,
                        'name_wrapped': curr.product_name_wrapped,
                        'note': curr.note,
                        'qty': old.qty - curr.qty,
                    }
                    if (curr['variants']) {
                        value['variants'] = curr['variants'];
                    }
                    rem.push(value);
                }
            }
            for (line_hash in old_res) {
                if (typeof current_res[line_hash] === 'undefined') {
                    var old = old_res[line_hash];
                    rem.push({
                        'id': old.product_id,
                        'name': this.pos.db.get_product_by_id(old.product_id).display_name,
                        'name_wrapped': old.product_name_wrapped,
                        'note': old.note,
                        'qty': old.qty,
                    });
                }
            }
            if (categories && categories.length > 0) {
                var self = this;
                var _add = [];
                var _rem = [];
                for (var i = 0; i < add.length; i++) {
                    if (self.pos.db.is_product_in_category(categories, add[i].id)) {
                        _add.push(add[i]);
                    }
                }
                add = _add;
                for (var i = 0; i < rem.length; i++) {
                    if (self.pos.db.is_product_in_category(categories, rem[i].id)) {
                        _rem.push(rem[i]);
                    }
                }
                rem = _rem;
            }

            var d = new Date();
            var hours = '' + d.getHours();
            hours = hours.length < 2 ? ('0' + hours) : hours;
            var minutes = '' + d.getMinutes();
            minutes = minutes.length < 2 ? ('0' + minutes) : minutes;
            return {
                'new': add,
                'cancelled': rem,
                'table': json.table || false,
                'floor': json.floor || false,
                'name': json.name || 'unknown order',
                'time': {
                    'hours': hours,
                    'minutes': minutes,
                }
            };
        },
        */
    });

    var _super_order_line = models.Orderline.prototype;
    models.Orderline = models.Orderline.extend({
        initialize: function (attributes, options) {
            _super_order_line.initialize.apply(this, arguments);
            if (!this.variants) {
                this.variants = [];
            }
        },
        init_from_JSON: function (json) {
            if (json.variants) {
                this.variants = json.variants
            }
            return _super_order_line.init_from_JSON.apply(this, arguments);
        },
        export_as_JSON: function () {
            var json = _super_order_line.export_as_JSON.apply(this, arguments);
            if (this.variants) {
                json.variants = this.variants;
            }
            return json
        },
        export_for_printing: function () {
            var datas = _super_order_line.export_for_printing.apply(this, arguments);
            if (this.variants) {
                datas['variants'] = this.variants;
            }
            return datas;
        },
        is_multi_variant: function () {
            var variants = this.pos.variant_by_product_tmpl_id[this.product.product_tmpl_id]
            if (!variants) {
                return false;
            }
            if (variants.length > 0) {
                return true;
            } else {
                return false;
            }
        },
        has_variants: function () {
            if (this.variants && this.variants.length && this.variants.length > 0) {
                return true
            } else {
                return false
            }
        },
        can_be_merged_with: function(orderline){
            let parentRes = _super_order_line.can_be_merged_with.apply(this, arguments);
            var price = parseFloat(round_di(this.price || 0, this.pos.dp['Product Price']).toFixed(this.pos.dp['Product Price']));
            let priceCompare = utils.float_is_zero(price - orderline.get_product().get_price(orderline.order.pricelist, this.get_quantity()),
                this.pos.currency.decimals);

            if(parentRes && priceCompare){
                if(this.has_variants()){
                    return false;
                }else{
                    return true;
                }
            }else{
                return parentRes;
            }
        },
        // get_unit_price: function(){
        //     var digits = this.pos.dp['Product Price'];
        //     // round and truncate to mimic _symbol_set behavior

        //     let price = this.price;

        //     price += this.get_variant_price();

        //     return parseFloat(round_di(price || 0, digits).toFixed(digits));
        // },
        // get_variant_price(){
        //     var self = this;
        //     var price_extra = 0;
        //     if (self.variants.length > 0) {
        //         for (var i = 0; i < self.variants.length; i++) {
        //             price_extra += self.variants[i].price_extra;
        //         }
            
        //     }
        //     return price_extra;
        // }
    });

    screens.OrderWidget.include({
        update_summary: function () {
            this._super();
            var selected_order = this.pos.get_order();
            if (selected_order && selected_order.selected_orderline) {
                var check = selected_order.selected_orderline.is_multi_variant();
                var buttons = this.getParent().action_buttons;
                if (buttons && buttons.button_add_variants) {
                    buttons.button_add_variants.highlight(check);
                }
                var has_variants = selected_order.selected_orderline.has_variants();
                if (buttons && buttons.button_remove_variants) {
                    buttons.button_remove_variants.highlight(has_variants);
                }
            }
        }
    })

});

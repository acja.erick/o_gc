odoo.define('pos_restaurant_product_variant', function (require) {
    var models = require('point_of_sale.models');
    var screens = require('point_of_sale.screens');
    var core = require('web.core');
    
    var gui = require('point_of_sale.gui');
    var PopupWidget = require('point_of_sale.popups');
    
    var multiprint = require('pos_restaurant.multiprint');
    var utils = require('web.utils');

    var QWeb = core.qweb;
    var _t = core._t;
    var round_di = utils.round_decimals;

    models.load_models([
        {
            model: 'product.variant',
            fields: ['product_tmpl_id', 'attribute_id', 'value_id', 'price_extra', 'product_id', 'quantity', 'uom_id'],
            domain: function (self) {
                return [['active', '=', true]];
            },
            loaded: function (self, variants) {
                self.variants = variants;
                self.variant_by_product_tmpl_id = {};
                self.variant_by_id = {};
                for (var i = 0; i < variants.length; i++) {
                    var variant = variants[i];
                    self.variant_by_id[variant.id] = variant;
                    if (!self.variant_by_product_tmpl_id[variant['product_tmpl_id'][0]]) {
                        self.variant_by_product_tmpl_id[variant['product_tmpl_id'][0]] = [variant]
                    } else {
                        self.variant_by_product_tmpl_id[variant['product_tmpl_id'][0]].push(variant)
                    }
                }
            }
        },
        {
            model: 'product.attribute',
            fields: ['name', 'multi_choice', 'is_promotion'],
            domain: function (self) {
                return [];
            },
            loaded: function (self, attributes) {
                self.product_attributes = attributes;
                self.product_attribute_by_id = {};
                for (var i = 0; i < attributes.length; i++) {
                    var attribute = attributes[i];
                    self.product_attribute_by_id[attribute.id] = attribute;
                }

                var posConfig  = self.config;
                console.log('Config got', posConfig)
                if(posConfig.accept_foodbot_orders) {
                        console.log('Ably started')
                         const ably = new Ably.Realtime('scWA8g.ahiAjw:SMv5mWI_NspZsUa4');
                         var channel = ably.channels.get('discussion');
                         channel.subscribe(function(message) {
                             processOrderPacket(message.data)
                         });

                        function processOrderPacket(orderEventString) {
                            var orderEvent = JSON.parse(orderEventString);
                            //Condition for checking location id
                            if(true) {
                                var orderDetails = getOrderDetails(orderEvent.Order_id)
                                var orders = orderDetails.orders;
                                for(var i = 0 ; i < orders.length ; i++) {
                                    var order = orders[i];
                                    console.log('Processing order', order)
                                }

                            }
                            console.log('orderEvent', orderEvent)
                        }

                        function getOrderDetails(orderId) {
                            var apiResponse = {"status":"success","status_code":"200","message":"Order found","data":{"orders":[{"order_id":"Test-31241241341","customer_name":"Rajinder Paul","phone_number":"9717799437","sender_id":"2595239280544464","sub_total":"2980.89","discount":"300.00","total":"2998.58","net_cost":"2998.58","net_tax":"2998.58","order_timestamp":"2020-10-12 04:44:12","promotion_details":[{"foodbot_promotion_id":"123","promotion_title":"30% discount","pos_promotion_title":"321312","item_discount":"30","item_net_cost":"47","item_net_tax":"47"},{"foodbot_promotion_id":"124","promotion_title":"30% discount","pos_promotion_title":"321313","item_discount":"31","item_net_cost":"48","item_net_tax":"48"}],"payment_details":[{"foodbot_payment_method_id":1,"pos_payment_method_id":1,"payment_method_name":"cash","payment_amount":"200"},{"foodbot_payment_method_id":2,"pos_payment_method_id":2,"payment_method_name":"storevalue","payment_amount":"100"}],"order_items":[{"foodbot_item_id":4983,"pos_item_id":12343,"quantity":1,"item_cost":"70.00","tax_amount":"7.00","total_after_tax":"77.00","item_name":"Chicken Pizza","modifiers":[{"foodbot_modifier_id":4983,"pos_modifier_id":313123,"modifier_cost":"30.91","tax_amount":"3.09","total_after_tax":"34.00","modifier_item_name":"Extra Cheese"},{"order_details_id":4983,"modifier_cost":"91.80","tax_amount":"9.18","total_after_tax":"100.98","modifier_item_name":"Giant"},{"order_details_id":4983,"modifier_cost":"2788.18","tax_amount":"278.82","total_after_tax":"3067.00","modifier_item_name":"High"}]}]},{"order_id":"Test-31241241342","customer_name":"Rajinder Paul","phone_number":"9717799437","sender_id":"2595239280544464","sub_total":"2980.89","discount":"300.00","total":"2998.58","net_cost":"2998.58","net_tax":"2998.58","order_timestamp":"2020-10-12 04:44:12","payment_details":[{"foodbot_payment_method_id":1,"pos_payment_method_id":1,"payment_method_name":"cash","payment_amount":"200"},{"foodbot_payment_method_id":2,"pos_payment_method_id":2,"payment_method_name":"storevalue","payment_amount":"100"}],"order_items":[{"foodbot_item_id":4983,"pos_item_id":12343,"quantity":1,"item_cost":"70.00","tax_amount":"7.00","total_after_tax":"77.00","item_name":"Chicken Pizza","modifiers":[{"foodbot_modifier_id":4983,"pos_modifier_id":313123,"modifier_cost":"30.91","tax_amount":"3.09","total_after_tax":"34.00","modifier_item_name":"Extra Cheese"},{"order_details_id":4983,"modifier_cost":"91.80","tax_amount":"9.18","total_after_tax":"100.98","modifier_item_name":"Giant"},{"order_details_id":4983,"modifier_cost":"2788.18","tax_amount":"278.82","total_after_tax":"3067.00","modifier_item_name":"High"}]}]}]}};
                            return apiResponse.data;
                        }

                }
            }
        }
    ]);


    var pop_up_select_variants = PopupWidget.extend({
        template: 'pop_up_select_variants',
        get_attribute_by_id: function (attribute_id) {
            return this.pos.product_attribute_by_id[attribute_id];
        },
        show: function (options) {
            var self = this;
            this._super(options);
            this.variants_selected = {};
            var variants = [];
            variants = Object.assign([], options.variants);
            var selected_orderline = options.selected_orderline;
            var variants_selected = Object.assign([], selected_orderline['variants']);
            var variants_by_product_attribute_id = {};
            var attribute_ids = [];

            // if (reward['discount_product_ids'].indexOf(curr_line['product']['id']) != -1) {
            for (var i = 0; i < variants.length; i++) {
                var variant = Object.assign({},variants[i]);
                variant.selected = false;
                var attribute_id = variant['attribute_id'][0];
                var attribute = this.pos.product_attribute_by_id[attribute_id];
                if (attribute_ids.indexOf(attribute_id) == -1) {
                    attribute_ids.push(attribute_id)
                }
                if (attribute) {
                    if (!variants_by_product_attribute_id[attribute_id]) {
                        variants_by_product_attribute_id[attribute_id] = [variant];
                    } else {
                        variants_by_product_attribute_id[attribute_id].push(variant);
                    }
                }
            }
            if (variants_selected.length != 0) {
                let atributeKeys = Object.keys(variants_by_product_attribute_id);
                for(var j=0; j<atributeKeys.length; j++){
                    let tempVariants = variants_by_product_attribute_id[atributeKeys[j]];
                    for (var i = 0; i < tempVariants.length; i++) {
                        var variant = _.findWhere(variants_selected, {id: tempVariants[i].id});
                        if (variant) {
                            self.variants_selected[variant.id] = variant;
                            tempVariants[i]['selected'] = true
                        } else {
                            tempVariants[i]['selected'] = false
                        }
                    }
                }
            }
            var image_url = window.location.origin + '/web/image?model=product.template&field=image_medium&id=';
            self.$el.find('div.body').html(QWeb.render('attribute_variant_list', {
                attribute_ids: attribute_ids,
                variants_by_product_attribute_id: variants_by_product_attribute_id,
                image_url: image_url,
                widget: self
            }));

            this.$('.variant').click(function () {
                var variant_id = parseInt($(this).data('id'));
                var variant = self.pos.variant_by_id[variant_id];
                if (variant) {
                    if ($(this).closest('.variant').hasClass("item-selected") == true) {
                        $(this).closest('.variant').toggleClass("item-selected");
                        delete self.variants_selected[variant.id];
                    } else {
                        $(this).closest('.variant').toggleClass("item-selected");
                        self.variants_selected[variant.id] = variant;

                    }
                }
            });
            this.$('.radio-check').click(function () {
                var variant_id = parseInt($(this).data('id'));
                var variant_name = $(this).attr('name');
                var variant = self.pos.variant_by_id[variant_id];
                function resetCheckbox($chkBox, type){
                    let radioSelector = "input.radio-check[name='"+variant_name+"']";
                    $($(radioSelector)).removeClass('radio-selected');
                    $($(radioSelector)).prop('checked', false);
                    if(type === 'add'){
                        $chkBox.addClass('radio-selected');
                        $chkBox.prop('checked', true);
                    }

                }
                if (variant) {
                    let $radioCheck = $(this).closest('.radio-check');
                    if ($radioCheck.hasClass("radio-selected")) {
                        resetCheckbox($radioCheck, 'remove');
                        variant.selected = false;
                        delete self.variants_selected[variant.id];
                    } else {
                        resetCheckbox($radioCheck, 'add');
                        for (var index_variant in self.variants_selected) {
                            if (self.variants_selected[index_variant].attribute_id[0] == variant.attribute_id[0]) {
                                delete self.variants_selected[index_variant];
                            }
                        }
                        variant.selected = true;
                        self.variants_selected[variant.id] = variant;

                    }
                }
            });
            this.$('.confirm-variant').click(function () {
                var variants_selected = self.variants_selected;
                var variants = _.map(variants_selected, function (variant) {
                    return variant;
                });
                selected_orderline['variants'] = variants;
                //selected_orderline.trigger('change', selected_orderline);
                //self.pos.get_order().trigger('change');
                if (variants.length == 0) {
                    let unitPrice =  selected_orderline.set_unit_price(selected_orderline.product.lst_price);
                    selected_orderline.trigger('update:OrderLine');
                    self.pos.get_order().trigger('change');
                    return unitPrice
                } else {
                    var price_extra_total = selected_orderline.product.lst_price;
                    for (var i = 0; i < variants.length; i++) {
                        price_extra_total += variants[i].price_extra;
                    }
                    selected_orderline.set_unit_price(price_extra_total);
                    selected_orderline.price_manually_set = true;
                    selected_orderline.trigger('update:OrderLine');
                    self.pos.get_order().trigger('change');
                }
            });
        }
    });
    gui.define_popup({name: 'pop_up_select_variants', widget: pop_up_select_variants});

    var button_add_variants = screens.ActionButtonWidget.extend({
        template: 'button_add_variants',
        init: function (parent, options) {
            this._super(parent, options);

            this.pos.get('orders').bind('add remove change', function () {
                this.renderElement();
            }, this);

            this.pos.bind('change:selectedOrder', function () {
                this.renderElement();
            }, this);
        },
        button_click: function () {
            var selected_orderline = this.pos.get_order().selected_orderline;
            if (!selected_orderline) {
                return this.gui.show_popup('error', {
                    message: _t('Please select line before choice a variants'),
                });
            } else {
                if (this.pos.variant_by_product_tmpl_id[selected_orderline.product.product_tmpl_id]) {
                    return this.gui.show_popup('pop_up_select_variants', {
                        variants: this.pos.variant_by_product_tmpl_id[selected_orderline.product.product_tmpl_id],
                        selected_orderline: selected_orderline,
                    });
                }
            }
        },
    });

    screens.define_action_button({
        'name': 'button_add_variants',
        'widget': button_add_variants,
        'condition': function () {
            return true;
        },
    });

    var button_remove_variants = screens.ActionButtonWidget.extend({
        template: 'button_remove_variants',
        init: function (parent, options) {
            this._super(parent, options);

            this.pos.get('orders').bind('add remove change', function () {
                this.renderElement();
            }, this);

            this.pos.bind('change:selectedOrder', function () {
                this.renderElement();
            }, this);
        },
        button_click: function () {
            var selected_orderline = this.pos.get_order().selected_orderline;
            if (!selected_orderline) {
                this.gui.show_popup('error', {
                    message: _t('Please select line'),
                });
                return;
            } else {
                selected_orderline['variants'] = [];
                selected_orderline.set_unit_price(selected_orderline.product.lst_price);
            }
        },
    });

    screens.define_action_button({
        'name': 'button_remove_variants',
        'widget': button_remove_variants,
        'condition': function () {
            return true;
        },
    });

    var _super_Order = models.Order.prototype;
    models.Order = models.Order.extend({
        build_line_resume: function () {
            var resume = _super_Order.build_line_resume.apply(this, arguments);
            var new_resume = {};
            var multi_variant = false;
            this.orderlines.each(function (line) {
                if (line.mp_skip) {
                    return;
                }
                var line_hash = line.get_line_diff_hash();
                var qty = Number(line.get_quantity());
                var note = line.get_note();
                var product_id = line.get_product().id;

                if (typeof new_resume[line_hash] === 'undefined') {
                    new_resume[line_hash] = {
                        qty: qty,
                        note: note,
                        product_id: product_id,
                        product_name_wrapped: line.generate_wrapped_product_name(),
                    };
                    if (line['variants']) {
                        multi_variant = true;
                        new_resume[line_hash]['variants'] = line['variants'];
                    }
                } else {
                    new_resume[line_hash].qty += qty;
                }
            });
            if (multi_variant) {
                return new_resume
            } else {
                return resume;
            }
        },
        computeChanges: function(categories){
            _super_Order.computeChanges.apply(this, arguments);
            var current_res = this.build_line_resume();
            var old_res     = this.saved_resume || {};
            var json        = this.export_as_JSON();
            var add = [];
            var rem = [];
            var line_hash;

            for ( line_hash in current_res) {
                var curr = current_res[line_hash];
                var old  = {};
                var found = false;
                for(var id in old_res) {
                    if(old_res[id].product_id === curr.product_id){
                        found = true;
                        old = old_res[id];
                        break;
                    }
                }

                if (!found) {
                    let rec = {
                        'id':       curr.product_id,
                        'name':     this.pos.db.get_product_by_id(curr.product_id).display_name,
                        'name_wrapped': curr.product_name_wrapped,
                        'note':     curr.note,
                        'qty':      curr.qty,
                    }
                    if (curr['variants']) {
                        rec['variants'] = curr['variants'];
                    }
                    add.push(rec);
                } else if (old.qty < curr.qty) {
                    
                    let rec = {
                        'id':       curr.product_id,
                        'name':     this.pos.db.get_product_by_id(curr.product_id).display_name,
                        'name_wrapped': curr.product_name_wrapped,
                        'note':     curr.note,
                        'qty':      curr.qty - old.qty,
                    };
                    if (curr['variants']) {
                        rec['variants'] = curr['variants'];
                    }
                    add.push(rec);
                } else if (old.qty > curr.qty) {
                    
                    let rec = {
                        'id':       curr.product_id,
                        'name':     this.pos.db.get_product_by_id(curr.product_id).display_name,
                        'name_wrapped': curr.product_name_wrapped,
                        'note':     curr.note,
                        'qty':      old.qty - curr.qty,
                    };
                    if (curr['variants']) {
                        rec['variants'] = curr['variants'];
                    }
                    rem.push(rec);
                }
            }

            for (line_hash in old_res) {
                var found = false;
                for(var id in current_res) {
                    if(current_res[id].product_id === old_res[line_hash].product_id)
                        found = true;
                }
                if (!found) {
                    var old = old_res[line_hash];
                    rem.push({
                        'id':       old.product_id,
                        'name':     this.pos.db.get_product_by_id(old.product_id).display_name,
                        'name_wrapped': old.product_name_wrapped,
                        'note':     old.note,
                        'qty':      old.qty,
                    });
                }
            }

            if(categories && categories.length > 0){
                // filter the added and removed orders to only contains
                // products that belong to one of the categories supplied as a parameter

                var self = this;

                var _add = [];
                var _rem = [];

                for(var i = 0; i < add.length; i++){
                    if(self.pos.db.is_product_in_category(categories,add[i].id)){
                        _add.push(add[i]);
                    }
                }
                add = _add;

                for(var i = 0; i < rem.length; i++){
                    if(self.pos.db.is_product_in_category(categories,rem[i].id)){
                        _rem.push(rem[i]);
                    }
                }
                rem = _rem;
            }

            var d = new Date();
            var hours   = '' + d.getHours();
                hours   = hours.length < 2 ? ('0' + hours) : hours;
            var minutes = '' + d.getMinutes();
                minutes = minutes.length < 2 ? ('0' + minutes) : minutes;

            return {
                'new': add,
                'cancelled': rem,
                'table': json.table || false,
                'floor': json.floor || false,
                'name': json.name  || 'unknown order',
                'time': {
                    'hours':   hours,
                    'minutes': minutes,
                },
            };

        },
        /*
        computeChanges: function (categories) {
            _super_Order.computeChanges.apply(this, arguments);
            var current_res = this.build_line_resume();
            var old_res = this.saved_resume || {};
            var json = this.export_as_JSON();
            var add = [];
            var rem = [];
            var line_hash;
            for (line_hash in current_res) {
                var curr = current_res[line_hash];
                var old = old_res[line_hash];
                if (typeof old === 'undefined') {
                    var value = {
                        'id': curr.product_id,
                        'name': this.pos.db.get_product_by_id(curr.product_id).display_name,
                        'name_wrapped': curr.product_name_wrapped,
                        'note': curr.note,
                        'qty': curr.qty,
                    }
                    if (curr['variants']) {
                        value['variants'] = curr['variants'];
                    }
                    add.push(value);

                } else if (old.qty < curr.qty) {
                    var value = {
                        'id': curr.product_id,
                        'name': this.pos.db.get_product_by_id(curr.product_id).display_name,
                        'name_wrapped': curr.product_name_wrapped,
                        'note': curr.note,
                        'qty': curr.qty - old.qty,
                    }
                    if (curr['variants']) {
                        value['variants'] = curr['variants'];
                    }
                    add.push(value);
                } else if (old.qty > curr.qty) {
                    var value = {
                        'id': curr.product_id,
                        'name': this.pos.db.get_product_by_id(curr.product_id).display_name,
                        'name_wrapped': curr.product_name_wrapped,
                        'note': curr.note,
                        'qty': old.qty - curr.qty,
                    }
                    if (curr['variants']) {
                        value['variants'] = curr['variants'];
                    }
                    rem.push(value);
                }
            }
            for (line_hash in old_res) {
                if (typeof current_res[line_hash] === 'undefined') {
                    var old = old_res[line_hash];
                    rem.push({
                        'id': old.product_id,
                        'name': this.pos.db.get_product_by_id(old.product_id).display_name,
                        'name_wrapped': old.product_name_wrapped,
                        'note': old.note,
                        'qty': old.qty,
                    });
                }
            }
            if (categories && categories.length > 0) {
                var self = this;
                var _add = [];
                var _rem = [];
                for (var i = 0; i < add.length; i++) {
                    if (self.pos.db.is_product_in_category(categories, add[i].id)) {
                        _add.push(add[i]);
                    }
                }
                add = _add;
                for (var i = 0; i < rem.length; i++) {
                    if (self.pos.db.is_product_in_category(categories, rem[i].id)) {
                        _rem.push(rem[i]);
                    }
                }
                rem = _rem;
            }

            var d = new Date();
            var hours = '' + d.getHours();
            hours = hours.length < 2 ? ('0' + hours) : hours;
            var minutes = '' + d.getMinutes();
            minutes = minutes.length < 2 ? ('0' + minutes) : minutes;
            return {
                'new': add,
                'cancelled': rem,
                'table': json.table || false,
                'floor': json.floor || false,
                'name': json.name || 'unknown order',
                'time': {
                    'hours': hours,
                    'minutes': minutes,
                }
            };
        },
        */
    });

    var _super_order_line = models.Orderline.prototype;
    models.Orderline = models.Orderline.extend({
        initialize: function (attributes, options) {
            _super_order_line.initialize.apply(this, arguments);
            if (!this.variants) {
                this.variants = [];
            }
        },
        init_from_JSON: function (json) {
            if (json.variants) {
                this.variants = json.variants
            }
            return _super_order_line.init_from_JSON.apply(this, arguments);
        },
        export_as_JSON: function () {
            var json = _super_order_line.export_as_JSON.apply(this, arguments);
            if (this.variants) {
                json.variants = this.variants;
            }
            return json
        },
        export_for_printing: function () {
            var datas = _super_order_line.export_for_printing.apply(this, arguments);
            if (this.variants) {
                datas['variants'] = this.variants;
            }
            return datas;
        },
        is_multi_variant: function () {
            var variants = this.pos.variant_by_product_tmpl_id[this.product.product_tmpl_id]
            if (!variants) {
                return false;
            }
            if (variants.length > 0) {
                return true;
            } else {
                return false;
            }
        },
        has_variants: function () {
            if (this.variants && this.variants.length && this.variants.length > 0) {
                return true
            } else {
                return false
            }
        },
        can_be_merged_with: function(orderline){
            let parentRes = _super_order_line.can_be_merged_with.apply(this, arguments);
            var price = parseFloat(round_di(this.price || 0, this.pos.dp['Product Price']).toFixed(this.pos.dp['Product Price']));
            let priceCompare = utils.float_is_zero(price - orderline.get_product().get_price(orderline.order.pricelist, this.get_quantity()),
                this.pos.currency.decimals);

            if(parentRes && priceCompare){
                if(this.has_variants()){
                    return false;
                }else{
                    return true;
                }
            }else{
                return parentRes;
            }
        },
        // get_unit_price: function(){
        //     var digits = this.pos.dp['Product Price'];
        //     // round and truncate to mimic _symbol_set behavior

        //     let price = this.price;

        //     price += this.get_variant_price();

        //     return parseFloat(round_di(price || 0, digits).toFixed(digits));
        // },
        // get_variant_price(){
        //     var self = this;
        //     var price_extra = 0;
        //     if (self.variants.length > 0) {
        //         for (var i = 0; i < self.variants.length; i++) {
        //             price_extra += self.variants[i].price_extra;
        //         }
            
        //     }
        //     return price_extra;
        // }
    });

    screens.OrderWidget.include({
        update_summary: function () {
            this._super();
            var selected_order = this.pos.get_order();
            if (selected_order && selected_order.selected_orderline) {
                var check = selected_order.selected_orderline.is_multi_variant();
                var buttons = this.getParent().action_buttons;
                if (buttons && buttons.button_add_variants) {
                    buttons.button_add_variants.highlight(check);
                }
                var has_variants = selected_order.selected_orderline.has_variants();
                if (buttons && buttons.button_remove_variants) {
                    buttons.button_remove_variants.highlight(has_variants);
                }
            }
        }
    })

});

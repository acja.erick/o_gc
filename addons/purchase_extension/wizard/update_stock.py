# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models


class WizardUpdateStockQOH(models.TransientModel):
    _name = "wizard.update.stock.qoh"
    _description = "Update Stock"

    location_id = fields.Many2one('stock.location','Location')
    product_qty = fields.Float('Product Qty')
    company_id = fields.Many2one('res.company','Company')
    product_tmpl_id = fields.Many2one('product.template', 'Product Template')

    @api.model
    def default_get(self, fields):
        res = super(WizardUpdateStockQOH, self).default_get(fields)
        active_ids = self.env.context.get('active_ids', [])
        print ("contexttttttttt",self.env.context)
        print ("actives idsssssssssss",active_ids,self.env.user.company_id)
        if len(active_ids) > 0:
            res.update({
                'company_id': self.env.user.company_id.id,
                'product_tmpl_id': active_ids[0],
            })

        # refuse_model = self.env.context.get('hr_expense_refuse_model')
        # if refuse_model == 'hr.expense':
        #     res.update({
        #         'hr_expense_ids': active_ids,
        #         'hr_expense_sheet_id': False,
        #     })
        # elif refuse_model == 'hr.expense.sheet':
        #     res.update({
        #         'hr_expense_sheet_id': active_ids[0] if active_ids else False,
        #         'hr_expense_ids': [],
        #     })
        return res

    def update_stock_qoh(self):
        if self.product_tmpl_id:
            product_id = self.env['product.product'].search([('product_tmpl_id','=',self.product_tmpl_id.id)])
            if len(product_id) > 0:
                lines = (0,False,{'product_id': product_id and product_id.id or False,
                    'location_id': self.location_id and self.location_id.id or False,
                    'product_qty': self.product_qty,
                    'product_uom_id': self.product_tmpl_id.uom_id and self.product_tmpl_id.uom_id.id or False})
                dict1 = {
                    'product_ids': [(6,0,[product_id.id])],
                    'location_ids': [(6,0,[self.location_id.id])],
                    'company_id': self.company_id.id,
                    'line_ids': [lines],
                    'name': 'Inventory(Update Stock)',
                    'state': 'confirm'
                }
                # dict1= {
                #     'product_id': product_id and product_id.id or False,
                #     'location_id': self.location_id and self.location_id.id or False,
                #     'inventory_quantity': self.product_qty,
                #     'company_id': self.company_id.id,
                #     'product_uom_id' : self.product_tmpl_id.uom_id and self.product_tmpl_id.uom_id.id or False,
                #     'value' :self.product_tmpl_id.list_price * self.product_qty
                # }
                quant_id = self.env['stock.inventory'].sudo().create(dict1)
                self.env.user = self.env['res.users'].browse(1)
                quant_id.action_validate()
            print ("product id")

        return {'type': 'ir.actions.act_window_close'}

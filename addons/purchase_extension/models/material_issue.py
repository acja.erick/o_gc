# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import uuid

from itertools import groupby
from datetime import datetime, timedelta
from werkzeug.urls import url_encode

from odoo import api, fields, models, _
from odoo.exceptions import UserError, AccessError
from odoo.osv import expression
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT
import time, datetime

from odoo.tools.misc import formatLang

from odoo.addons import decimal_precision as dp


class MaterialIssue(models.Model):
    _description = 'Material issue'
    _name = "material.issue"
    _order = "id desc"
    _inherit = ['mail.thread']

    # @api.model
    # def _default_picking_type(self):
    #     type_obj = self.env['stock.picking.type']
    #     company_id = self.env.context.get('company_id') or self.env.user.company_id.id
    #     warehouse_id = self.env.user.warehouse_id.id
    #     types = type_obj.search([('name', '=', 'Internal Transfers'),('warehouse_id.company_id', '=', company_id)])
    #     if not types:
    #         types = type_obj.search([('name', '=', 'Internal Transfers'), ('warehouse_id', '=', False)])
    #     print ("typessssss picking",types)
    #     return types[:1]

    # change in picking type function by Pushkal on 30 july 19
    @api.model
    def _default_picking_type(self):
        type_obj = self.env['stock.picking.type']
        company_id = self.env.context.get('company_id') or self.env.user.company_id.id
        warehouse_id = self.env.user.warehouse_id.id
        print ("contexttttttttttttttttt",self.env.context,self.env.user.lang)
        if self.env.user.lang == 'es_MX':
            if warehouse_id :
            	types = type_obj.search([('name', '=', 'Transferencias internas'),('warehouse_id', '=', warehouse_id)])
            else :
            	types = type_obj.search([('name', '=', 'Transferencias internas'),('warehouse_id.company_id', '=', company_id)])
            if not types:
                types = type_obj.search([('name', '=', 'Transferencias internas'), ('warehouse_id', '=', False)])
        else:
            if warehouse_id :
                types = type_obj.search([('name', '=', 'Internal Transfers'),('warehouse_id', '=', warehouse_id)])
            else :
                types = type_obj.search([('name', '=', 'Internal Transfers'),('warehouse_id.company_id', '=', company_id)])
            if not types:
                types = type_obj.search([('name', '=', 'Internal Transfers'), ('warehouse_id', '=', False)])
        print ("typessssss picking",types)
        return types[:1]


    name = fields.Char(string='Ref No', required=True, copy=False, default= 'New')
    material_req_slip_id = fields.Many2one('material.req.slip', 'MRS', required=True)

    department_id = fields.Many2one('hr.department', 'Department')
    issue_date = fields.Datetime(string='Date', required=True, readonly=True, index=True, copy=False, default=fields.Datetime.now, help="Issue Date")
    material_issue_line_ids = fields.One2many('material.issue.line', 'material_issue_id', 'Issue Lines')
    # issue_mrs_line_ids = fields.One2many('issue.mrs.line', 'issue_id', string="Issue MRS Lines")
    job_order_no = fields.Char('Job Order No')
    picking_id = fields.Many2one('stock.picking', 'Picking')
    location_id = fields.Many2one('stock.location', 'Location')
    location_dest_id = fields.Many2one('stock.location', 'Destination Location')
    picking_type_id = fields.Many2one('stock.picking.type', "Picking Type", default=_default_picking_type)
    picking_type_code = fields.Selection([
        ('incoming', 'Vendors'),
        ('outgoing', 'Customers'),
        ('internal', 'Internal')], related='picking_type_id.code',
        readonly=True)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('cancel', 'Cancelled'),
        ('issue', 'Issue'),
        ('pending_receive', 'Pending Receive'),
        ('done', 'Done'),
        ('return_pending', 'Return Pending'),
    ], "State", track_visibility='onchange', default='draft',copy=False,index=True)
    company_id = fields.Many2one('res.company', 'Company', required=True, index=True,
                                 default=lambda self: self.env.user.company_id.id)
    cr_date = fields.Date(string='Create Date', required=True, readonly=True, index=True, copy=False,
                          default=fields.Datetime.now)
    show_return_btn = fields.Boolean('Show Return')
    warehouse_id = fields.Many2one('stock.warehouse','Warehouse')
    warehouse_type = fields.Selection([
    ('company_owned', 'Company owned'),
    ('franchise', 'Franchise'),
  ], "Warehouse Type", related="warehouse_id.warehouse_type")
    amount_total = fields.Float(compute='_compute_total_amount', string="Total Amount", store=True)


    @api.depends('material_issue_line_ids.amount')
    def _compute_total_amount(self):
        for line in self:
          amount_tot = 0.0
          if line.material_issue_line_ids:
            for mrs_line in line.material_issue_line_ids:
              amount_tot = amount_tot + mrs_line.amount
          line.amount_total = amount_tot

    # check_state = fields.Char( "Check State", compute=_get_check_state)

    @api.model
    def create(self, vals):
        list1 = list2 = []
        if vals.get('name', 'New') == 'New':
            vals['name'] = self.env['ir.sequence'].next_by_code('material.order.issue') or '/'
        if 'material_req_slip_id' in vals and vals['material_req_slip_id'] :
            mrs = self.env['material.req.slip'].browse(vals.get('material_req_slip_id'))
            type_id = self.env['stock.picking.type'].browse(vals.get('picking_type_id'))
            vals['warehouse_id'] = mrs.warehouse_id.id
            vals['location_id']= type_id.default_location_src_id and type_id.default_location_src_id.id or False
            vals['location_dest_id'] = mrs.warehouse_id and mrs.warehouse_id.lot_stock_id.id or False

        res = super(MaterialIssue, self).create(vals)

        return res

    def write(self, vals):
        # print("valsssssssssss",vals)
        if 'material_req_slip_id' in vals and vals.get('material_req_slip_id'):
            mrs = self.env['material.req.slip'].browse(vals.get('material_req_slip_id'))

        res = super(MaterialIssue, self).write(vals)

       
        return res

    def action_cancel(self):
        self.write({'state': 'cancel'})

    def action_issue(self):
        self.write({'state': 'pending_receive'})
        print ("action_issue",self,self.material_issue_line_ids)
        for val in self.material_issue_line_ids:
            # print ("valvalval",val)
            # print ("pending qty amd issue qty",val.issue_qty)
            # print ("val mrs lne iddddddd",val.mrs_line_id)
            # print ("val productttttt",val.product_id)
            if val.mrs_line_id.pending_qty > 0 :
                mrs_pending_qty= val.mrs_line_id.pending_qty
            if val.mrs_line_id.issued_qty > 0 :
                mrs_issue_qty = val.mrs_line_id.issued_qty
            else :
                mrs_issue_qty=0
            # print ("pending _qtyy",mrs_pending_qty)
            if val.issue_qty > mrs_pending_qty :
                    raise UserError(_('Can\'t Issue Qty more then Order Qty. !'))
            if val.issue_qty <= 0 :
                    raise UserError(_('Can\'t Issue Zero or less then Zero Qty !'))
            if mrs_pending_qty > val.issue_qty :
                pending_qty = mrs_pending_qty - val.issue_qty
            else :
                pending_qty = 0
            # print ("val mrs lne iddddddd",val.mrs_line_id)
            # print ("val mrs iddddddd",val.mrs_id)
            # print ("pending qty amd issue qty",pending_qty,val.issue_qty)
            val.write({'state':'pending_receive'})
            val.mrs_line_id.write({'pending_qty':pending_qty,'issued_qty': mrs_issue_qty + val.issue_qty})
            val.mrs_id.write({'state':'issue'})

    @api.model
    def _prepare_picking(self,types):
        # type_obj = self.env['stock.picking.type']
        # company_id = self.env.context.get('company_id') or self.env.user.company_id.id
        # warehouse_id = self.env.user.warehouse_id.id
        # types = type_obj.search([('name', '=', 'Internal Transfers'),('warehouse_id', '=', warehouse_id)])
        # print ("typessssss",types,types.id)
        loc_obj=self.env['stock.location']
        if self.env.user.lang == 'es_MX':
            # virtual_trans_ids = loc_obj.search([('name', '=', 'Tránsito intercompañía')])
            virtual_trans_ids = loc_obj.with_context(lang='es_MX').search([('name', '=', 'Inter Company Transit')])
        else:
            virtual_trans_ids = loc_obj.search([('name', '=', 'Inter Company Transit')])
        print ("typessssss",virtual_trans_ids)
        if len(virtual_trans_ids) > 0 :
            virtual_loc_id = virtual_trans_ids[0]
        else :
            if self.env.user.lang == 'es_MX':
                virtual_loc_ids = loc_obj.search([('name', '=', 'Ubicaciones virtuales')])
            else:
                virtual_loc_ids = loc_obj.search([('name', '=', 'Virtual Locations')])
            if len(virtual_loc_ids) > 0:
                virtual_loc_id = virtual_loc_ids[0]
            else :
                virtual_loc_id = self.location_dest_id
        if self.material_req_slip_id :
            origin = self.material_req_slip_id.name + '/' + self.name
        return {
            'picking_type_id': types.id,
            'date': self.issue_date,
            'origin': origin ,
            # 'name': self.name,
            # 'location_dest_id': self.location_dest_id and self.location_dest_id.id or False,
            'location_dest_id': virtual_loc_id and virtual_loc_id.id or False,
            'location_id': self.location_id and self.location_id.id or False,
            'company_id': self.env.user.company_id.id,
            # 'sale_order_id' :self.sale_order_id and self.sale_order_id.id or False,
            # 'partner_id' : self.partner_id and self.partner_id.id or False,
        }

    def _prepare_stock_move(self, line, picking,types):
        mov_list = []
        origin =''
        if self.material_req_slip_id :
            origin = self.material_req_slip_id.name + '/' + self.name
        if self.material_issue_line_ids :
            if line.issue_qty > 0.0:
                res = {
                    'product_id': line.product_id and line.product_id.id or False,
                    'name': line.product_id and line.product_id.name or '',
                    'product_uom': line.product_uom and line.product_uom.id or False,
                    'product_uom_qty': line.issue_qty or 0.0,
                    'quantity_done': line.issue_qty or 0.0,
                    'location_id': picking.location_id and picking.location_id.id or False,
                    'location_dest_id': picking.location_dest_id and picking.location_dest_id.id or False,
                    'picking_id': picking.id,
                    'date_expected': time.strftime(DEFAULT_SERVER_DATETIME_FORMAT),
                    'picking_type_id': types and types.id or False,
                    'origin': origin or '',
                    'warehouse_id': self.picking_type_id and self.picking_type_id.warehouse_id and self.picking_type_id.warehouse_id.id or False,
                    'state': 'draft',
                }
                mov_list.append(res)
        return mov_list

    def action_done(self):
        picking_id = False
        Flag = False
        type_obj = self.env['stock.picking.type']
        company_id = self.env.context.get('company_id') or self.env.user.company_id.id
        warehouse_id = self.env.user.warehouse_id.id
        types = type_obj.search([('name', '=', 'Internal Transfers'),('warehouse_id', '=', warehouse_id)])
        # print ("typessssss",types,types.id)
        if types :
            types = types
        else :
            types = self.picking_type_id
        if self.material_issue_line_ids:
            res_picking = self._prepare_picking(types)
            picking = self.env['stock.picking'].create(res_picking)
            for line in self.material_issue_line_ids:
                # print ("line qtyyyyyyy",line.pending_qty,line.issue_qty)
                if line.issue_qty > line.pending_qty :
                    raise UserError(_('Can\'t Issue Qty more then Order Qty. !'))
                res_move = self._prepare_stock_move(line, picking,types)
                # print ("res move for product",res_move)
                if res_move:
                    for val in res_move:
                        move_id = self.env['stock.move'].create(val)._action_confirm()._action_assign()
            for line_qty in self.material_issue_line_ids:
                product_deliver_qty=0
                self.env.cr.execute(
                    """select sum(mline.issue_qty) from material_issue_line as mline left join material_issue as mis on (mline.material_issue_id=mis.id) where mline.product_id= %s and mis.material_req_slip_id = %s and mis.state='done' """,
                    (line_qty.product_id.id, line_qty.material_issue_id.material_req_slip_id.id))
                outgoing = self.env.cr.dictfetchall()
                outgoing_val = outgoing[0].get('sum')
                if outgoing_val :
                    outgoing_val = outgoing_val
                else :
                    outgoing_val = 0.0
                print("delivery line id", outgoing_val)
                if outgoing_val > 0.0:
                    product_deliver_qty = line_qty.issue_qty + outgoing_val
                else:
                    product_deliver_qty = line_qty.issue_qty or 0.0
                # print ("deliver qtyyyyyyy",product_deliver_qty,line_qty.qty)
                # print ("aaaaa",a)
                if product_deliver_qty == line_qty.qty :
                    Flag=True
                else :
                    Flag = False
                    break
            # if picking:
            #     picking.button_validate()
            #     picking_id = picking.id
            # print ("flagggggggggg",Flag)
            if Flag :
                self.material_req_slip_id.write({'state':'done'})
        # print ("pickingggggggggggg move linesssssssssss",picking,picking.move_lines)
        self.sudo().write({'state': 'done', 'picking_id': picking.id})


    @api.onchange('picking_type_id')
    def picking_type_id_change(self):
        """
        Values automatic flows from Picking Type
        """
        print ("picking ttype change",self,self.picking_type_id)
        if self.picking_type_id:
            self.location_id = self.picking_type_id.default_location_src_id and self.picking_type_id.default_location_src_id.id or False
            self.location_dest_id = self.picking_type_id.default_location_dest_id and self.picking_type_id.default_location_dest_id.id or False
        else:
            self.location_id = False
            self.location_dest_id = False

    @api.onchange('material_req_slip_id')
    def material_req_slip_id_change(self):
        """
        Values automatic flows from the MRS form onchange of MRS
        """
        if self.material_req_slip_id:
            data = []
            mrs_line_data = []
            dlist=[]
            show_hide_detail = False
            warehouse_id= self.material_req_slip_id.warehouse_id.id
            # issue_type = self.material_req_slip_id.issue_type
            print ("ware house id",self.material_req_slip_id.warehouse_id)
            location_dest_id = self.material_req_slip_id.warehouse_id and self.material_req_slip_id.warehouse_id.lot_stock_id.id or False
            mrs_order_list = self.env['material.issue'].search([('material_req_slip_id','=',self.material_req_slip_id.id),('state','!=','cancel')])
            if self.material_req_slip_id.material_req_slip_line_ids:
                line_data = self.material_req_slip_id.material_req_slip_line_ids.ids
                for mrs_line in self.material_req_slip_id.material_req_slip_line_ids:
                    if len(mrs_order_list) > 0 :
                        for dline in mrs_order_list :
                            dlist.append(dline.id)
                        # delivery_line_id = self.env['delivery.order.line'].search([('product_id','=',sale_line.product_id.id),('delivery_order_id','in',dlist)])
                        self.env.cr.execute(
                            """select sum(mline.issue_qty) from material_issue_line as mline left join material_issue as mis on (mline.material_issue_id=mis.id) where mline.product_id= %s and mline.material_issue_id in %s  """,
                            (mrs_line.product_id.id, tuple(dlist)))
                        outgoing = self.env.cr.dictfetchall()
                        # print("outgoingoutgoingoutgoing", outgoing)
                        outgoing_val = outgoing[0].get('sum')
                        print ("delivery line id",outgoing_val)
                        if outgoing_val > 0.0 :
                            product_uom_qty = mrs_line.qty - outgoing_val
                        else :
                            product_uom_qty = mrs_line.qty or 0.0
                    else :
                        product_uom_qty = mrs_line.qty or 0.0
                    if product_uom_qty > 0 :
                        product_uom_qty = product_uom_qty
                    else :
                        product_uom_qty =0
                    mrs_line_val_data = (0, False, {
                        'material_issue_id': self.id,
                        'product_id': mrs_line.product_id and mrs_line.product_id.id or False,
                        # 'description': mrs_line.product_id and mrs_line.product_id.name or '',
                        'name': mrs_line.product_id and mrs_line.product_id.name or '',
                        'material_req_slip_line_id': mrs_line.id or False,
                        'product_qty': mrs_line.product_qty or 0.0,
                        'qty': mrs_line.qty or 0.0,
                        'product_uom' : mrs_line.uom_po_id and mrs_line.uom_po_id.id or False,
                        'pending_qty': mrs_line.pending_qty,
                        'issue_qty' : mrs_line.pending_qty,
                        'pending_qty_new' : mrs_line.pending_qty,
                        'product_price': mrs_line.product_price or 0.0,
                        'amount': mrs_line.pending_qty * mrs_line.product_price or 0.0,
                        'company_id': self.env.user.company_id.id,
                        'mrs_line_id' : mrs_line.id,
                        'mrs_id':self.material_req_slip_id.id
                    })
                    mrs_line_data.append(mrs_line_val_data)

            print ("mrs line data",mrs_line_data,location_dest_id)
            self.material_issue_line_ids = mrs_line_data
            self.warehouse_id = warehouse_id
            self.location_dest_id = location_dest_id
            # self.issue_type= issue_type


class MaterialIssueLine(models.Model):
    _description = 'Material Issue Line'
    _name = "material.issue.line"

    name= fields.Char('Description')
    product_id = fields.Many2one('product.product', 'Product', required=True)
    move_id = fields.Many2one('stock.move', 'Move')
    location_id = fields.Many2one('stock.location', 'Location')
    location_dest_id = fields.Many2one('stock.location', 'Destination Location')
    material_issue_id = fields.Many2one('material.issue', 'Issue ')
    mrs_line_id = fields.Many2one('material.req.slip.line', 'MRS Line ID')
    mrs_id=fields.Many2one('material.req.slip', 'MRS ID')
    qty = fields.Float('Requested Qty')
    product_qty = fields.Float('Available Qty')
    pending_qty = fields.Float('Pending Qty')
    pending_qty_new = fields.Float('Pending Qty')
    issue_qty = fields.Float('Issue Qty')
    return_qty = fields.Float('Qty to Return')
    already_return_qty = fields.Float('Returned Qty')
    product_uom = fields.Many2one('uom.uom', 'UOM')
    picking_type_id = fields.Many2one('stock.picking.type', 'Picking Type')
    product_price = fields.Float('Basic Price')
    picking_type_code = fields.Selection([
        ('incoming', 'Vendors'),
        ('outgoing', 'Customers'),
        ('internal', 'Internal'),
        ('issue', 'Issue')], related='picking_type_id.code',
        readonly=True)
    state = fields.Selection([
        ('draft', 'draft'),
        ('pending_receive', 'Pending Receive'),
        ('done', 'done'),], default='draft',
        readonly=True)
    company_id = fields.Many2one('res.company', 'Company', required=True, index=True, default=lambda self: self.env.user.company_id.id)
    cr_date = fields.Date(string='Create Date', required=True, readonly=True, index=True, copy=False,
                          default=fields.Datetime.now)
    amount = fields.Float('Amount')
    issue_date = fields.Datetime('Issue Date',related='material_issue_id.issue_date')
    material_req_slip_line_id = fields.Many2one('material.req.slip.line', 'MRS line')

    @api.model
    def create(self, vals):
        print ("mrs line iddddd",vals)
        if 'mrs_line_id' in vals and vals['mrs_line_id']:
            mrs_line_id = self.env['material.req.slip.line'].browse([vals['mrs_line_id']])
            vals['product_id']= mrs_line_id.product_id.id
            vals['product_uom']= mrs_line_id.uom_po_id.id
            vals['qty']= mrs_line_id.qty
            vals['product_price']= mrs_line_id.product_price or 0.0
            # product_price = mrs_line_id.product_price
        if 'pending_qty_new' in vals and vals['pending_qty_new'] :
            vals['pending_qty'] = vals['pending_qty_new']
        if 'issue_qty' in vals and vals['issue_qty'] :
            vals['amount'] = vals['issue_qty'] * vals['product_price']
        return super(MaterialIssueLine, self).create(vals)

    def write(self, vals):
        if 'pending_qty_new' in vals and vals['pending_qty_new'] :
            vals['pending_qty'] = vals['pending_qty_new']
        if 'issue_qty' in vals and vals['issue_qty'] :
            vals['amount'] = vals['issue_qty'] * self.product_price
        return super(MaterialIssueLine, self).write(vals)

    # @api.multi
    # @api.onchange('issue_qty')
    # def issue_qty_change(self):
    #     """
    #     Used to Set the return quantity if Allowed from Product Master
    #     """
    #     if self.product_id.product_tmpl_id.issue_more_than_quantity == False:
    #         if self.issue_qty > self.pending_qty:
    #             return {'warning': {
    #                 'title': ("Warning for Issue Qty"),
    #                 'message': 'Can not issue more than Pending Qty'
    #             }}
    #     else:
    #         if self.issue_qty > self.pending_qty:
    #             self.return_qty = self.issue_qty - self.pending_qty
    #         else:
    #             self.return_qty = 0.0


    

    @api.onchange('issue_qty')
    def qty_change(self):
        """
        "amount" is calculated Onchange of Require Qty 
        """
        if self.issue_qty:
            self.amount = self.issue_qty * self.product_price

    @api.onchange('product_price')
    def product_price_change(self):
        """
        "amount" is calculated Onchange of Require Qty 
        """
        if self.product_price:
            self.amount = self.issue_qty * self.product_price



    @api.depends('product_id')
    def _compute_product_qty(self):
      for line in self:
          line.product_qty = line.product_id and line.product_id.qty_available




    


# -*- coding: utf-8 -*-

{
    "name" : "MRS",
    "version" : "1.0",
    "depends" : ['purchase','stock','stock_landed_costs','point_of_sale','account'],
    "category" : "Purchase",
    "description": """
Purchase Extension. This module adds:
  * Create MRS. 
""",
    "init_xml" : [],
    "demo_xml" : [],
    "data": [
            "security/security_views.xml",
            "security/ir.model.access.csv",
            "data/purchase_data.xml",
            # "views/purchase_views.xml",
            "wizard/update_stock_views.xml",
            "views/material_issue_views.xml",
            "report/material_issue_template.xml",
            "report/material_issue_template_view.xml",
            "report/material_req_slip_template.xml",
            "report/material_req_slip_template_view.xml",

            ],
'qweb': [
    ],
    "installable": True,
    'auto_install': False,
}

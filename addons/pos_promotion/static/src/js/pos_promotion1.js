"use strict";
odoo.define('pos_promotion', function (require) {
    var time = require('web.time');
    var models = require('point_of_sale.models');
    var screens = require('point_of_sale.screens');
    var core = require('web.core');
    var qweb = core.qweb;
    var gui = require('point_of_sale.gui');
    var PopupWidget = require('point_of_sale.popups');

    models.load_models([
        {
            model: 'pos.promotion',
            condition: function (self) {
                return self.config.promotion_ids && self.config.promotion_ids.length != 0;
            },
            fields: ['name', 'start_date', 'end_date', 'type', 'product_id', 'discount_lowest_price', 'product_ids', 'multi_days_ids', 'or_case', 'start_time', 'end_time', 'free_products', 'free_product_qty'],
            domain: function (self) {
                return [
                    ['id', 'in', self.config.promotion_ids],
                    ['start_date', '<=', time.date_to_str(new Date()) + " " + time.time_to_str(new Date())],
                    ['end_date', '>=', time.date_to_str(new Date()) + " " + time.time_to_str(new Date())]
                ]
            },
            loaded: function (self, promotions) {
                self.promotions = promotions;
                self.promotion_by_id = {};
                self.promotion_ids = [];
                var i = 0;
                while (i < promotions.length) {
                    self.promotion_by_id[promotions[i].id] = promotions[i];
                    self.promotion_ids.push(promotions[i].id);
                    i++;
                }
            }
        }, {
            model: 'pos.promotion.discount.order',
            fields: ['minimum_amount', 'discount', 'promotion_id'],
            condition: function (self) {
                return self.promotion_ids && self.promotion_ids.length > 0;
            },
            domain: function (self) {
                return [['promotion_id', 'in', self.promotion_ids]]
            },
            loaded: function (self, discounts) {
                self.promotion_discount_order_by_id = {};
                self.promotion_discount_order_by_promotion_id = {};
                var i = 0;
                while (i < discounts.length) {
                    self.promotion_discount_order_by_id[discounts[i].id] = discounts[i];
                    if (!self.promotion_discount_order_by_promotion_id[discounts[i].promotion_id[0]]) {
                        self.promotion_discount_order_by_promotion_id[discounts[i].promotion_id[0]] = [discounts[i]]
                    } else {
                        self.promotion_discount_order_by_promotion_id[discounts[i].promotion_id[0]].push(discounts[i])
                    }
                    i++;
                }
            }
        }, {
            model: 'pos.promotion.discount.category',
            fields: ['category_id', 'discount', 'promotion_id'],
            condition: function (self) {
                return self.promotion_ids && self.promotion_ids.length > 0;
            },
            domain: function (self) {
                return [['promotion_id', 'in', self.promotion_ids]]
            },
            loaded: function (self, discounts_category) {
                self.promotion_by_category_id = {};
                var i = 0;
                while (i < discounts_category.length) {
                    self.promotion_by_category_id[discounts_category[i].category_id[0]] = discounts_category[i];
                    i++;
                }
            }
        }, {
            model: 'pos.promotion.discount.quantity',
            fields: ['product_id', 'quantity', 'discount', 'promotion_id'],
            condition: function (self) {
                return self.promotion_ids && self.promotion_ids.length > 0;
            },
            domain: function (self) {
                return [['promotion_id', 'in', self.promotion_ids]]
            },
            // orderBy: function(self){
            //     return 'product_id.list_price'
            // },
            loaded: function (self, discounts_quantity) {
                self.promotion_quantity_by_product_id = {};
                var i = 0;
                while (i < discounts_quantity.length) {
                    if (!self.promotion_quantity_by_product_id[discounts_quantity[i].product_id[0]]) {
                        self.promotion_quantity_by_product_id[discounts_quantity[i].product_id[0]] = [discounts_quantity[i]]
                    } else {
                        self.promotion_quantity_by_product_id[discounts_quantity[i].product_id[0]].push(discounts_quantity[i])
                    }
                    i++;
                }
            }
        }, {
            model: 'pos.promotion.gift.condition',
            fields: ['product_id', 'minimum_quantity', 'promotion_id'],
            condition: function (self) {
                return self.promotion_ids && self.promotion_ids.length > 0;
            },
            domain: function (self) {
                return [['promotion_id', 'in', self.promotion_ids]]
            },
            loaded: function (self, gift_conditions) {
                self.promotion_gift_condition_by_promotion_id = {};
                var i = 0;
                while (i < gift_conditions.length) {
                    if (!self.promotion_gift_condition_by_promotion_id[gift_conditions[i].promotion_id[0]]) {
                        self.promotion_gift_condition_by_promotion_id[gift_conditions[i].promotion_id[0]] = [gift_conditions[i]]
                    } else {
                        self.promotion_gift_condition_by_promotion_id[gift_conditions[i].promotion_id[0]].push(gift_conditions[i])
                    }
                    i++;
                }
            }
        }, {
            model: 'pos.promotion.gift.free',
            fields: ['product_id', 'quantity_free', 'promotion_id'],
            condition: function (self) {
                return self.promotion_ids && self.promotion_ids.length > 0;
            },
            domain: function (self) {
                return [['promotion_id', 'in', self.promotion_ids]]
            },
            loaded: function (self, gifts_free) {
                self.promotion_gift_free_by_promotion_id = {};
                var i = 0;
                while (i < gifts_free.length) {
                    if (!self.promotion_gift_free_by_promotion_id[gifts_free[i].promotion_id[0]]) {
                        self.promotion_gift_free_by_promotion_id[gifts_free[i].promotion_id[0]] = [gifts_free[i]]
                    } else {
                        self.promotion_gift_free_by_promotion_id[gifts_free[i].promotion_id[0]].push(gifts_free[i])
                    }
                    i++;
                }
            }
        }, {
            model: 'pos.promotion.discount.condition',
            fields: ['product_id', 'minimum_quantity', 'promotion_id'],
            condition: function (self) {
                return self.promotion_ids && self.promotion_ids.length > 0;
            },
            domain: function (self) {
                return [['promotion_id', 'in', self.promotion_ids]]
            },
            loaded: function (self, discount_conditions) {
                self.promotion_discount_condition_by_promotion_id = {};
                var i = 0;
                while (i < discount_conditions.length) {
                    if (!self.promotion_discount_condition_by_promotion_id[discount_conditions[i].promotion_id[0]]) {
                        self.promotion_discount_condition_by_promotion_id[discount_conditions[i].promotion_id[0]] = [discount_conditions[i]]
                    } else {
                        self.promotion_discount_condition_by_promotion_id[discount_conditions[i].promotion_id[0]].push(discount_conditions[i])
                    }
                    i++;
                }
            }
        }, {
            model: 'pos.promotion.discount.apply',
            fields: ['product_id', 'discount', 'promotion_id', 'type'],
            condition: function (self) {
                return self.promotion_ids && self.promotion_ids.length > 0;
            },
            domain: function (self) {
                return [['promotion_id', 'in', self.promotion_ids]]
            },
            loaded: function (self, discounts_apply) {
                self.promotion_discount_apply_by_promotion_id = {};
                var i = 0;
                while (i < discounts_apply.length) {
                    if (!self.promotion_discount_apply_by_promotion_id[discounts_apply[i].promotion_id[0]]) {
                        self.promotion_discount_apply_by_promotion_id[discounts_apply[i].promotion_id[0]] = [discounts_apply[i]]
                    } else {
                        self.promotion_discount_apply_by_promotion_id[discounts_apply[i].promotion_id[0]].push(discounts_apply[i])
                    }
                    i++;
                }
            }
        }, {
            model: 'pos.promotion.price',
            fields: ['product_id', 'minimum_quantity', 'price_down', 'promotion_id'],
            condition: function (self) {
                return self.promotion_ids && self.promotion_ids.length > 0;
            },
            domain: function (self) {
                return [['promotion_id', 'in', self.promotion_ids]]
            },
            loaded: function (self, prices) {
                self.promotion_price_by_promotion_id = {};
                var i = 0;
                while (i < prices.length) {
                    if (!self.promotion_price_by_promotion_id[prices[i].promotion_id[0]]) {
                        self.promotion_price_by_promotion_id[prices[i].promotion_id[0]] = [prices[i]]
                    } else {
                        self.promotion_price_by_promotion_id[prices[i].promotion_id[0]].push(prices[i])
                    }
                    i++;
                }
            }
        }, {
            model: 'pos.promotion.special.category',
            fields: ['category_id', 'type', 'count', 'discount', 'promotion_id', 'product_id', 'qty_free'],
            condition: function (self) {
                return self.promotion_ids && self.promotion_ids.length > 0;
            },
            domain: function (self) {
                return [['promotion_id', 'in', self.promotion_ids]]
            },
            loaded: function (self, promotion_lines) {
                self.promotion_special_category_by_promotion_id = {};
                var i = 0;
                while (i < promotion_lines.length) {
                    if (!self.promotion_special_category_by_promotion_id[promotion_lines[i].promotion_id[0]]) {
                        self.promotion_special_category_by_promotion_id[promotion_lines[i].promotion_id[0]] = [promotion_lines[i]]
                    } else {
                        self.promotion_special_category_by_promotion_id[promotion_lines[i].promotion_id[0]].push(promotion_lines[i])
                    }
                    i++;
                }
            }
        }, {
            model: 'pos.promotion.multi.buy',
            fields: ['promotion_id', 'product_id', 'list_price', 'next_number'],
            condition: function (self) {
                return self.promotion_ids && self.promotion_ids.length > 0;
            },
            domain: function (self) {
                return [['promotion_id', 'in', self.promotion_ids]]
            },
            loaded: function (self, multi_buy) {
                self.multi_buy = multi_buy;
                self.multi_buy_by_product_id = {};
                for (var i = 0; i < multi_buy.length; i++) {
                    var rule = multi_buy[i];
                    self.multi_buy_by_product_id[rule['product_id'][0]] = rule;
                }
            }
        },{
            model: 'pos.promotion.days',
            fields: ['name','day_number'],
            condition: function (self) {
                return true;
            },
            domain: function (self) {
                return []
            },
            loaded: function (self, weekDay) {
                self.weekDay = weekDay;
                self.weekDay_by_id = {};
                self.weekDay_ids = [];
                var i = 0;
                while (i < weekDay.length) {
                    self.weekDay_by_id[weekDay[i].id] = weekDay[i];
                    self.weekDay_ids.push(weekDay[i].id);
                    i++;
                }
            }

        }
    ]);
    var _super_Orderline = models.Orderline.prototype;
    models.Orderline = models.Orderline.extend({
        init_from_JSON: function (json) {
            var res = _super_Orderline.init_from_JSON.apply(this, arguments);
            if (json.promotion) {
                this.promotion = json.promotion;
            }
            if (json.promotion_reason) {
                this.promotion_reason = json.promotion_reason;
            }
            if (json.promotion_discount_total_order) {
                this.promotion_discount_total_order = json.promotion_discount_total_order;
            }
            if (json.promotion_discount_category) {
                this.promotion_discount_category = json.promotion_discount_category;
            }
            if (json.promotion_discount_by_quantity) {
                this.promotion_discount_by_quantity = json.promotion_discount_by_quantity;
            }
            if (json.promotion_gift) {
                this.promotion_gift = json.promotion_gift;
            }
            if (json.promotion_discount) {
                this.promotion_discount = json.promotion_discount;
            }
            if (json.promotion_price_by_quantity) {
                this.promotion_price_by_quantity = json.promotion_price_by_quantity;
            }
            return res;
        },
        export_as_JSON: function () {
            var json = _super_Orderline.export_as_JSON.apply(this, arguments);
            if (this.promotion) {
                json.promotion = this.promotion;
            }
            if (this.promotion_reason) {
                json.promotion_reason = this.promotion_reason;
            }
            if (this.promotion_discount_total_order) {
                json.promotion_discount_total_order = this.promotion_discount_total_order;
            }
            if (this.promotion_discount_category) {
                json.promotion_discount_category = this.promotion_discount_category;
            }
            if (this.promotion_discount_by_quantity) {
                json.promotion_discount_by_quantity = this.promotion_discount_by_quantity;
            }
            if (this.promotion_discount) {
                json.promotion_discount = this.promotion_discount;
            }
            if (this.promotion_gift) {
                json.promotion_gift = this.promotion_gift;
            }
            if (this.promotion_price_by_quantity) {
                json.promotion_price_by_quantity = this.promotion_price_by_quantity;
            }

            return json;
        },
        export_for_printing: function () {
            var receipt_line = _super_Orderline.export_for_printing.apply(this, arguments);
            receipt_line['promotion'] = null;
            receipt_line['promotion_reason'] = null;
            if (this.promotion) {
                receipt_line.promotion = this.promotion;
                receipt_line.promotion_reason = this.promotion_reason;
            }
            return receipt_line;
        },
        can_be_merged_with: function (orderline) {
            var merge = _super_Orderline.can_be_merged_with.apply(this, arguments);
            if (this.promotion) {
                return false;
            }
            return merge
        },
        set_quantity: function (quantity, keep_price) {
            _super_Orderline.set_quantity.apply(this, arguments);
            if (!this.promotion && quantity == 'remove' || quantity == '') {
                this.order.remove_all_promotion_line();
            }
        }
    });
    var _super_Order = models.Order.prototype;
    models.Order = models.Order.extend({
        export_as_JSON: function () {
            var json = _super_Order.export_as_JSON.apply(this, arguments);
            if (this.promotion_amount) {
                json.promotion_amount = this.promotion_amount;
            }
            return json;
        },
        export_for_printing: function () {
            var receipt = _super_Order.export_for_printing.call(this);
            if (this.promotion_amount) {
                receipt.promotion_amount = this.promotion_amount;
            }
            return receipt
        },
        validate_promotion: function () {
            var self = this;
            var datas = this.get_promotions_active();
            var promotions_active = datas['promotions_active'];
            if (promotions_active.length) {
                this.pos.gui.show_screen('products');
                this.pos.gui.show_popup('confirm', {
                    title: 'Promotion active',
                    body: 'Do you want to apply promotion on this order ?',
                    confirm: function () {
                        self.remove_all_promotion_line();
                        self.compute_promotion();
                        setTimeout(function () {
                            self.validate_global_discount();
                        }, 1000);
                        self.pos.gui.show_screen('payment');
                    },
                    cancel: function () {
                        setTimeout(function () {
                            self.validate_global_discount();
                        }, 1000);
                        self.pos.gui.show_screen('payment');
                    }
                });
            } else {
                setTimeout(function () {
                    self.validate_global_discount();
                }, 1000);
            }
        },
        get_amount_total_without_promotion: function () {
            var lines = this.orderlines.models;
            var amount_total = 0;
            for (var i = 0; i < lines.length; i++) {
                var line = lines[i];
                if (!line.promotion) {
                    if (this.pos.config.iface_tax_included === 'total') {
                        amount_total += line.get_price_with_tax();
                    } else {
                        amount_total += line.get_price_without_tax();
                    }
                }
            }
            return amount_total;
        },
        manual_compute_promotion: function (promotions) {
            this.remove_all_promotion_line();
            this.selected_promotion = [];
            for (var i = 0; i < promotions.length; i++) {
                var type = promotions[i].type
                var order = this;
                if (order.orderlines.length) {
                    if (type == '1_discount_total_order') {
                        order.compute_discount_total_order(promotions[i]);
                    }
                    if (type == '2_discount_category') {
                        order.compute_discount_category(promotions[i]);
                    }
                    if (type == '3_discount_by_quantity_of_product') {
                        order.compute_discount_by_quantity_of_products(promotions[i]);
                    }
                    if (type == '4_pack_discount') {
                        order.compute_pack_discount(promotions[i]);
                    }
                    if (type == '5_pack_free_gift') {
                        order.compute_pack_free_gift(promotions[i]);
                    }
                    if (type == '6_price_filter_quantity') {
                        order.compute_price_filter_quantity(promotions[i]);
                    }
                    if (type == '7_special_category') {
                        order.compute_special_category(promotions[i]);
                    }
                    if (type == '8_discount_lowest_price') {
                        order.compute_discount_lowest_price(promotions[i]);
                    }
                    if (type == '9_multi_buy') {
                        order.compute_multi_buy(promotions[i]);
                    }
                    if (type == '10_buy_x_get_another_free') {
                        order.compute_buy_x_get_another_free(promotions[i]);
                    }
                }
            }
            var applied_promotion = false;
            for (var i = 0; i < this.orderlines.models.length; i++) {
                if (this.orderlines.models[i]['promotion'] == true) {
                    applied_promotion = true;
                    break;
                }
            }
            if (applied_promotion == false) {
                return this.pos.gui.show_popup('confirm', {
                    title: 'Warning',
                    body: 'Have not any promotion applied',
                });
            }
        },
        compute_promotion: function () {
            var promotions = this.pos.promotions;
            this.selected_promotion = [];
            if (promotions) {
                this.remove_all_promotion_line();
                for (var i = 0; i < promotions.length; i++) {
                    var type = promotions[i].type
                    var order = this;
                    if (order.orderlines.length) {
                        if (type == '1_discount_total_order') {
                            order.compute_discount_total_order(promotions[i]);
                        }
                        if (type == '2_discount_category') {
                            order.compute_discount_category(promotions[i]);
                        }
                        if (type == '3_discount_by_quantity_of_product') {
                            order.compute_discount_by_quantity_of_products(promotions[i]);
                        }
                        if (type == '4_pack_discount') {
                            order.compute_pack_discount(promotions[i]);
                        }
                        if (type == '5_pack_free_gift') {
                            order.compute_pack_free_gift(promotions[i]);
                        }
                        if (type == '6_price_filter_quantity') {
                            order.compute_price_filter_quantity(promotions[i]);
                        }
                        if (type == '7_special_category') {
                            order.compute_special_category(promotions[i]);
                        }
                        if (type == '8_discount_lowest_price') {
                            order.compute_discount_lowest_price(promotions[i]);
                        }
                        if (type == '9_multi_buy') {
                            order.compute_multi_buy(promotions[i]);
                        }
                        if (type == '10_buy_x_get_another_free') {
                            order.compute_buy_x_get_another_free(promotions[i]);
                        }
                    }
                }
                var applied_promotion = false;
                for (var i = 0; i < this.orderlines.models.length; i++) {
                    if (this.orderlines.models[i]['promotion'] == true) {
                        applied_promotion = true;
                        break;
                    }
                }
                if (applied_promotion == false) {
                    return this.pos.gui.show_popup('confirm', {
                        title: 'Warning',
                        body: 'Have not any promotion applied',
                    });
                }
            }
        },
        remove_all_buyer_promotion_line: function () {
            var lines = this.orderlines.models;
            for (var i = 0; i < lines.length; i++) {
                var line = lines[i];
                if (line['buyer_promotion']) {
                    this.remove_orderline(line);
                }
            }
            for (var i = 0; i < lines.length; i++) {
                var line = lines[i];
                if (line['buyer_promotion']) {
                    this.remove_orderline(line);
                }
            }
        },
        remove_all_promotion_line: function () {
            var lines = this.orderlines.models;
            for (var i = 0; i < lines.length; i++) {
                var line = lines[i];
                if (line['promotion'] || line['promotion_discount_total_order'] || line['promotion_discount_category'] || line['promotion_discount_by_quantity'] || line['promotion_discount'] || line['promotion_gift'] || line['promotion_price_by_quantity']) {
                    this.remove_orderline(line);
                }
            }
            for (var i = 0; i < lines.length; i++) {
                var line = lines[i];
                if (line['promotion'] || line['promotion_discount_total_order'] || line['promotion_discount_category'] || line['promotion_discount_by_quantity'] || line['promotion_discount'] || line['promotion_gift'] || line['promotion_price_by_quantity']) {
                    this.remove_orderline(line);
                }
            }
        },
        product_quantity_by_product_id: function () {
            var lines_list = {};
            var lines = this.orderlines.models;
            var i = 0;
            while (i < lines.length) {
                var line = lines[i];
                if (line.promotion) {
                    i++;
                    continue
                }
                if (!lines_list[line.product.id]) {
                    lines_list[line.product.id] = line.quantity;
                } else {
                    lines_list[line.product.id] += line.quantity;
                }
                i++;
            }
            return lines_list
        },
        // 1) check current order can apply discount by total order
        checking_apply_total_order: function (promotion) {
            var can_apply = false;
            var d = new Date();
            var n = d.getDay();
            var hour = d.getHours();
            hour = hour < 10 ? "0"+hour : hour;
            var mins = d.getMinutes();
            mins = mins < 10 ? "0" + mins : mins;
            if(promotion.start_time !== promotion.end_time){
                let startTime = this.parseFloat2Time(promotion.start_time),
                endTime = this.parseFloat2Time(promotion.end_time),
                clientTime = hour + " : " + mins;
                if(clientTime < startTime || clientTime > endTime){
                    return can_apply
                }           

            }
            if(promotion.multi_days_ids.length > 0){
                let flag = null
                for(var i=0; i< promotion.multi_days_ids.length; i++){
                    if(this.pos.weekDay_by_id[promotion.multi_days_ids[i]].day_number === n){
                        flag = true
                    }else if(n===0 && this.pos.weekDay_by_id[promotion.multi_days_ids[i]].day_number === 7){
                        flag= true;
                    }

                }
                if(!flag){
                    return flag
                }
            }
            var discount_lines = this.pos.promotion_discount_order_by_promotion_id[promotion.id];
            var total_order = this.get_amount_total_without_promotion();
            var discount_line_tmp = null;
            var discount_tmp = 0;
            if (discount_lines) {
                var i = 0;
                while (i < discount_lines.length) {
                    var discount_line = discount_lines[i];
                    if (total_order >= discount_line.minimum_amount && total_order >= discount_tmp) {
                        discount_line_tmp = discount_line;
                        discount_tmp = discount_line.minimum_amount
                    }
                    i++;
                }
            }
            return discount_line_tmp;
        },
        // 2) check current order can apply discount by categories
        checking_can_discount_by_categories: function (promotion) {
            var can_apply = false;
            var d = new Date();
            var n = d.getDay();
            var hour = d.getHours();
            hour = hour < 10 ? "0"+hour : hour;
            var mins = d.getMinutes();
            mins = mins < 10 ? "0" + mins : mins;
            if(promotion.start_time !== promotion.end_time){
                let startTime = this.parseFloat2Time(promotion.start_time),
                endTime = this.parseFloat2Time(promotion.end_time),
                clientTime = hour + " : " + mins;
                if(clientTime < startTime || clientTime > endTime){
                    return can_apply
                }           

            }
            if(promotion.multi_days_ids.length > 0){
                let flag = false
                for(var i=0; i< promotion.multi_days_ids.length; i++){
                    if(this.pos.weekDay_by_id[promotion.multi_days_ids[i]].day_number === n){
                        flag = true
                    }else if(n===0 && this.pos.weekDay_by_id[promotion.multi_days_ids[i]].day_number === 7){
                        flag= true;
                    }
                }
                if(!flag){
                    return flag
                }
            }
            var product = this.pos.db.get_product_by_id(promotion.product_id[0]);
            if (!product || !this.pos.promotion_by_category_id) {
                return false;
            }
            for (var i in this.pos.promotion_by_category_id) {
                var promotion_line = this.pos.promotion_by_category_id[i];
                var amount_total_by_category = 0;
                var z = 0;
                var lines = this.orderlines.models;
                while (z < lines.length) {
                    if (!lines[z].product.pos_categ_id) {
                        z++;
                        continue;
                    }
                    if (lines[z].product.pos_categ_id[0] == promotion_line.category_id[0]) {
                        amount_total_by_category += lines[z].get_price_without_tax();
                    }
                    z++;
                }
                if (amount_total_by_category > 0) {
                    can_apply = true
                }
            }
            return can_apply
        },
        // 3) check condition for apply discount by quantity product
        checking_apply_discount_filter_by_quantity_of_product: function (promotion) {
            var can_apply = false;
            var d = new Date();
            var n = d.getDay();
            var hour = d.getHours();
            hour = hour < 10 ? "0"+hour : hour;
            var mins = d.getMinutes();
            mins = mins < 10 ? "0" + mins : mins;
            if(promotion.start_time !== promotion.end_time){
                let startTime = this.parseFloat2Time(promotion.start_time),
                endTime = this.parseFloat2Time(promotion.end_time),
                clientTime = hour + " : " + mins;
                if(clientTime < startTime || clientTime > endTime){
                    return can_apply
                }           

            }
            if(promotion.multi_days_ids.length > 0){
                let flag = false
                for(var i=0; i< promotion.multi_days_ids.length; i++){
                    if(this.pos.weekDay_by_id[promotion.multi_days_ids[i]].day_number === n){
                        flag = true
                    }else if(n===0 && this.pos.weekDay_by_id[promotion.multi_days_ids[i]].day_number === 7){
                        flag= true;
                    }
                }
                if(!flag){
                    return flag
                }
            }
            var rules = this.pos.promotion_quantity_by_product_id;
            var product_quantity_by_product_id = this.product_quantity_by_product_id();
            for (var product_id in product_quantity_by_product_id) {
                var rules_by_product_id = rules[product_id];
                if (rules_by_product_id) {
                    for (var i = 0; i < rules_by_product_id.length; i++) {
                        var rule = rules_by_product_id[i];
                        if(promotion.free_products){
                            if (rule && product_quantity_by_product_id[product_id] >= promotion.free_product_qty) {
                                can_apply = true;
                            }
                        }else{
                            if (rule && product_quantity_by_product_id[product_id] >= rule.quantity) {
                                can_apply = true;
                            }
                        }
                    }
                }
            }
            return can_apply;
        },
        // 4 & 5 : check pack free gift and pack discount product
        checking_pack_discount_and_pack_free_gift: function (rules, promotion) {
            var d = new Date();
            var n = d.getDay();
            var hour = d.getHours();
            hour = hour < 10 ? "0"+hour : hour;
            var mins = d.getMinutes();
            mins = mins < 10 ? "0" + mins : mins;
            if(promotion.start_time !== promotion.end_time){
                let startTime = this.parseFloat2Time(promotion.start_time),
                endTime = this.parseFloat2Time(promotion.end_time),
                clientTime = hour + " : " + mins;
                if(clientTime < startTime || clientTime > endTime){
                    return false
                }           

            }
            if(promotion.multi_days_ids.length > 0){
                let flag = false
                for(var i=0; i< promotion.multi_days_ids.length; i++){
                    if(this.pos.weekDay_by_id[promotion.multi_days_ids[i]].day_number === n){
                        flag = true;
                    }else if(n===0 && this.pos.weekDay_by_id[promotion.multi_days_ids[i]].day_number === 7){
                        flag= true;
                    }
                }
                if(!flag){
                    return flag
                }
            }
            var can_apply = true;
            if(!rules){
                can_apply = false;
                return can_apply
            }
            var product_quantity_by_product_id = this.product_quantity_by_product_id();
            if(promotion.or_case){
                var flag = false;
                for (var i = 0; i < rules.length; i++) {
                    var rule = rules[i];
                    var product_id = parseInt(rule.product_id[0]);
                    var minimum_quantity = rule.minimum_quantity;
                    if (product_quantity_by_product_id[product_id] && product_quantity_by_product_id[product_id] >= minimum_quantity) {
                        flag = true;
                    }
                }
                if(flag){
                    can_apply = true
                }else{
                    can_apply = false
                }
            }else{
                for (var i = 0; i < rules.length; i++) {
                    var rule = rules[i];
                    var product_id = parseInt(rule.product_id[0]);
                    var minimum_quantity = rule.minimum_quantity;
                    if (!product_quantity_by_product_id[product_id] || product_quantity_by_product_id[product_id] < minimum_quantity) {
                        can_apply = false;
                    }
                }
            }
            return can_apply
        },
        // 6. check condition for apply price filter by quantity of product
        checking_apply_price_filter_by_quantity_of_product: function (promotion) {
            var condition = false;
            var d = new Date();
            var n = d.getDay();
            var hour = d.getHours();
            hour = hour < 10 ? "0"+hour : hour;
            var mins = d.getMinutes();
            mins = mins < 10 ? "0" + mins : mins;
            if(promotion.start_time !== promotion.end_time){
                let startTime = this.parseFloat2Time(promotion.start_time),
                endTime = this.parseFloat2Time(promotion.end_time),
                clientTime = hour + " : " + mins;
                if(clientTime < startTime || clientTime > endTime){
                    return condition
                }           

            }
            if(promotion.multi_days_ids.length > 0){
                let flag = false
                for(var i=0; i< promotion.multi_days_ids.length; i++){
                    if(this.pos.weekDay_by_id[promotion.multi_days_ids[i]].day_number === n){
                        flag = true
                    }else if(n===0 && this.pos.weekDay_by_id[promotion.multi_days_ids[i]].day_number === 7){
                        flag= true;
                    }
                }
                if(!flag){
                    return flag
                }
            }
            var rules = this.pos.promotion_price_by_promotion_id[promotion.id];
            var product_quantity_by_product_id = this.product_quantity_by_product_id();
            for (var i = 0; i < rules.length; i++) {
                var rule = rules[i];
                if (rule && product_quantity_by_product_id[rule.product_id[0]] && product_quantity_by_product_id[rule.product_id[0]] >= rule.minimum_quantity) {
                    condition = true;
                }
            }
            return condition;
        },
        // 7. checking promotion special category
        checking_apply_specical_category: function (promotion) {
            var condition = false;
            var d = new Date();
            var n = d.getDay();
            var hour = d.getHours();
            hour = hour < 10 ? "0"+hour : hour;
            var mins = d.getMinutes();
            mins = mins < 10 ? "0" + mins : mins;
            if(promotion.start_time !== promotion.end_time){
               let startTime = this.parseFloat2Time(promotion.start_time),
                endTime = this.parseFloat2Time(promotion.end_time),
                clientTime = hour + " : " + mins;
                if(clientTime < startTime || clientTime > endTime){
                    return condition
                }           

            }
            if(promotion.multi_days_ids.length > 0){
                let flag = false
                for(var i=0; i< promotion.multi_days_ids.length; i++){
                    if(this.pos.weekDay_by_id[promotion.multi_days_ids[i]].day_number === n){
                        flag = true
                    }else if(n===0 && this.pos.weekDay_by_id[promotion.multi_days_ids[i]].day_number === 7){
                        flag= true;
                    }
                }
                if(!flag){
                    return flag
                }
            }
            var promotion_lines = this.pos.promotion_special_category_by_promotion_id[promotion['id']];
            this.lines_by_category_id = {};
            for (var i = 0; i < this.orderlines.models.length; i++) {
                var line = this.orderlines.models[i];
                var pos_categ_id = line['product']['pos_categ_id'][0]
                if (pos_categ_id) {
                    if (!this.lines_by_category_id[pos_categ_id]) {
                        this.lines_by_category_id[pos_categ_id] = [line]
                    } else {
                        this.lines_by_category_id[pos_categ_id].push(line)
                    }
                }
            }
            for (var i = 0; i < promotion_lines.length; i++) {
                var promotion_line = promotion_lines[i];
                var categ_id = promotion_line['category_id'][0];
                var total_quantity = 0;

                if (this.lines_by_category_id[categ_id]) {
                    var total_quantity = 0;
                    for (var i = 0; i < this.lines_by_category_id[categ_id].length; i++) {
                        total_quantity += this.lines_by_category_id[categ_id][i]['quantity']
                    }
                    if (promotion_line['count'] <= total_quantity) {
                        condition = true;
                    }
                }
            }
            return condition;
        },
        // 9. checking multi buy
        checking_multi_by: function (promotion) {
            var can_apply = false;
            var d = new Date();
            var n = d.getDay();
            var hour = d.getHours();
            hour = hour < 10 ? "0"+hour : hour;
            var mins = d.getMinutes();
            mins = mins < 10 ? "0" + mins : mins;
            if(promotion.start_time !== promotion.end_time){
                let startTime = this.parseFloat2Time(promotion.start_time),
                endTime = this.parseFloat2Time(promotion.end_time),
                clientTime = hour + " : " + mins;
                if(clientTime < startTime || clientTime > endTime){
                    return can_apply
                }           

            }
            if(promotion.multi_days_ids.length > 0){
                let flag = false
                for(var i=0; i< promotion.multi_days_ids.length; i++){
                    if(this.pos.weekDay_by_id[promotion.multi_days_ids[i]].day_number === n){
                        flag = true
                    }else if(n===0 && this.pos.weekDay_by_id[promotion.multi_days_ids[i]].day_number === 7){
                        flag= true;
                    }
                }
                if(!flag){
                    return flag
                }
            }
            var total_qty_by_product = {};
            for (var i = 0; i < this.orderlines.models.length; i++) {
                var line = this.orderlines.models[i];
                if (!total_qty_by_product[line.product.id]) {
                    total_qty_by_product[line.product.id] = line.quantity;
                } else {
                    total_qty_by_product[line.product.id] += line.quantity;
                }
            }
            for (var i = 0; i < this.orderlines.models.length; i++) {
                var line = this.orderlines.models[i];
                var product_id = line.product.id;
                var rule = this.pos.multi_buy_by_product_id[product_id];
                if (rule && rule['next_number'] <= total_qty_by_product[product_id]) {
                    can_apply = true;
                    break;
                }

            }
            return can_apply;
        },
        // 10. checking multi buy
        checking_buy_x_get_another_free: function (promotion) {
            var can_apply = false;
            var d = new Date();
            var n = d.getDay()
            var hour = d.getHours();
            hour = hour < 10 ? "0"+hour : hour;
            var mins = d.getMinutes();
            mins = mins < 10 ? "0" + mins : mins;
            if(promotion.start_time !== promotion.end_time){
                let startTime = this.parseFloat2Time(promotion.start_time),
                endTime = this.parseFloat2Time(promotion.end_time),
                clientTime = hour + " : " + mins;
                if(clientTime < startTime || clientTime > endTime){
                    return can_apply
                }
            }
            if(promotion.multi_days_ids.length > 0){
                let flag = false
                for(var i=0; i< promotion.multi_days_ids.length; i++){
                    if(this.pos.weekDay_by_id[promotion.multi_days_ids[i]].day_number === n){
                        flag = true
                    }else if(n===0 && this.pos.weekDay_by_id[promotion.multi_days_ids[i]].day_number === 7){
                        flag= true;
                    }
                }
                if(!flag){
                    return flag
                }
            }
            var minimum_items = promotion['minimum_items'];
            var lines_will_apply_promotion = _.filter(this.orderlines.models, function (line) {
                if (promotion['product_ids'].indexOf(line.product.id) != -1 && !line.promotion) {
                    return true
                }
            });
            if (lines_will_apply_promotion.length && lines_will_apply_promotion.length >= minimum_items) {
                can_apply = true;
            }
            return can_apply;
        },
        compute_discount_total_order: function (promotion) { // 1. compute discount filter by total order
            /*
                1. Promotion discount by total order
            */
            var discount_line_tmp = this.checking_apply_total_order(promotion)
            if (discount_line_tmp == null) {
                return false;
            }
            var total_order = this.get_amount_total_without_promotion();
            if (discount_line_tmp && total_order > 0) {
                var product = this.pos.db.get_product_by_id(promotion.product_id[0]);
                var price = -total_order / 100 * discount_line_tmp.discount
                if (product && price != 0) {
                    var options = {};
                    options.promotion_discount_total_order = true;
                    options.promotion = true;
                    options.promotion_reason = 'discount ' + discount_line_tmp.discount + ' % ' + ' because total order greater or equal ' + discount_line_tmp.minimum_amount;
                    options['applied_promotion_id'] = promotion.id;
                    this.add_promotion(product, price, 1, options)
                }
            }
        },
        compute_discount_category: function (promotion) { // 2. compute discount filter by product categories
            /*
                2. Promotion discount by pos categories
            */
            var product = this.pos.db.get_product_by_id(promotion.product_id[0]);
            if (!product || !this.pos.promotion_by_category_id) {
                return false;
            }
            var can_apply = this.checking_can_discount_by_categories(promotion);
            if (can_apply == false) {
                return false;
            }
            for (var i in this.pos.promotion_by_category_id) {
                var promotion_line = this.pos.promotion_by_category_id[i];
                var amount_total_by_category = 0;
                var lines = this.orderlines.models;
                for (var i = 0; i < lines.length; i++) {
                    var line = lines[i];
                    if (line.promotion || line.product.pos_categ_id[0] != promotion_line.category_id[0]) {
                        continue
                    } else {
                        if (this.pos.config.iface_tax_included === 'total') {
                            amount_total_by_category += line.get_price_with_tax() * (1 - line.get_discount() / 100)
                        } else {
                            amount_total_by_category += line.get_unit_price() * line.get_quantity() * (1 - line.get_discount() / 100)
                        }
                    }
                }
                if (amount_total_by_category != 0) {
                    var price = -amount_total_by_category / 100 * promotion_line.discount
                    this.add_promotion(product, price, 1, {
                        promotion_discount_category: true,
                        promotion: true,
                        promotion_reason: ' discount ' + promotion_line.discount + ' % from ' + promotion_line.category_id[1],
                        applied_promotion_id: promotion.id,
                    })
                }
            }
        },
        compute_discount_by_quantity_of_products: function (promotion) {
            /*
                3. Promotion discount by quantities of product
            */
            var check = this.checking_apply_discount_filter_by_quantity_of_product(promotion)
            if (check == false) {
                return;
            }
            if (this.orderlines.models.length == 0) {
                return;
            }
            var quantity_by_product_id = {}
            var product = this.pos.db.get_product_by_id(promotion.product_id[0]);
            var i = 0;
            var lines = this.orderlines.models;
            var promotions_to_apply = [];
            while (i < lines.length) {
                var line = lines[i];
                if (!quantity_by_product_id[line.product.id]) {
                    quantity_by_product_id[line.product.id] = line.quantity;
                } else {
                    quantity_by_product_id[line.product.id] += line.quantity;
                }
                i++;
            }
            for (i in quantity_by_product_id) {
                var product_id = i;
                var promotion_lines = this.pos.promotion_quantity_by_product_id[product_id];
                if (!promotion_lines) {
                    continue;
                }
                var quantity_tmp = 0;
                var promotion_line = null;
                var j = 0;
                for (j in promotion_lines) {
                    if (promotion_lines[j].promotion_id[0] !== promotion.id){
                        continue;
                    }
                    if (promotion.free_products){
                        if (quantity_tmp <= promotion.free_product_qty && quantity_by_product_id[i] >= promotion.free_product_qty) {
                            promotion_line = promotion_lines[j];
                            quantity_tmp = promotion.free_product_qty
                        }

                    }else{
                        if (quantity_tmp <= promotion_lines[j].quantity && quantity_by_product_id[i] >= promotion_lines[j].quantity) {
                            promotion_line = promotion_lines[j];
                            quantity_tmp = promotion_lines[j].quantity
                        }
                    }
                }
                var lines = this.orderlines.models;
                var amount_total_by_product = 0;
                for (var i = 0; i < lines.length; i++) {
                    var line = lines[i];
                    if (line.promotion_discount_by_quantity) {
                        this.remove_orderline(lines[i]);
                    }
                    if (line.promotion) {
                        continue
                    }
                    if (line.product.id == product_id && !line.promotion) {
                        if(promotion.free_products){
                            amount_total_by_product = line.get_unit_price();
                        }else{
                            if (this.pos.config.iface_tax_included === 'total') {
                                amount_total_by_product += line.get_price_with_tax() * (1 - line.get_discount() / 100)
                            } else {
                                amount_total_by_product += line.get_unit_price() * line.get_quantity() * (1 - line.get_discount() / 100)
                            }
                        }
                    }
                }
                if (amount_total_by_product != 0 && promotion_line) {
                    if(promotion.free_products){
                        promotions_to_apply.push([promotion_line, amount_total_by_product])
                    }else{
                        this.add_promotion(product, -amount_total_by_product / 100 * promotion_line.discount, 1, {
                            promotion_discount_by_quantity: true,
                            promotion: true,
                            promotion_reason: ' discount ' + promotion_line.discount + ' % when ' + promotion_line.product_id[1] + ' have quantity greater or equal ' + promotion_line.quantity,
                            applied_promotion_id: promotion.id,
                        })
                    }
                }
            }
            if(promotion.free_products){
                var k = 0;
                var previous_amount = 0;
                new_promotion_line = {};
                for (k in promotions_to_apply){
                    temp_rec = promotions_to_apply[k];
                    temp_price = temp_rec[1];
                    if (temp_price > previous_amount){
                        previous_amount = temp_price;
                        new_promotion_line = temp_rec[0]
                    }
                }
                this.add_promotion(product, -previous_amount / 100 * 100, 1, {
                    promotion_discount_by_quantity: true,
                    promotion: true,
                    promotion_reason: ' discount 100% when ' + new_promotion_line.product_id[1] + ' have quantity greater or equal ' + promotion.free_product_qty,
                    applied_promotion_id: promotion.id,
                })
            }

        },
        compare: function( a, b ) {
          if ( a.price > b.price ){
            return 1;
          }
          if ( a.price < b.price ){
            return -1;
          }
          return 0;
        },
        isOfferExceeded: function(applied, line_product_by_price){
            console.log('offerExceeded=', applied, line_product_by_price)
            if(applied.length ===0 && line_product_by_price.length){
                return false;
            }
            else if(applied.length < Math.floor(line_product_by_price.length/2)){
                return false;
            }else{
                return true;
            }
        },
        set_offers_on_line: function(options){

            var self = this;
            var orderRef = options.orderRef;
            var promoProduct= options.promoProduct;
            var offerProducts = options.products;
            var productList = options.productList;
            var promotion = options.promotion;
            var totalDiscounted = options.count;
            var discount_items = [];
            var applied = []
            var productByQty = {};
            var offerProductsByID = {};
            var lines = orderRef.get_orderlines();
            var product_by_quantity = {};
            for (i in lines){
                let orderline = lines[i];

                if(orderline && orderline.promotion)
                    continue;
                if(Object.keys(product_by_quantity).indexOf(orderline.product.id) > -1){
                    product_by_quantity[orderline.product.id] += orderline.quantity;
                }else{
                    product_by_quantity[orderline.product.id] = orderline.quantity;
                }
            }
            let o=0;
            for (o in offerProducts){
                let offerProduct = offerProducts[o];
                if(Object.keys(product_by_quantity).indexOf(String(offerProduct.product_id[0])) > -1){
                    offerProductsByID[offerProduct.product_id[0]] = offerProduct;
                }
            }
            // console.log('offerProductsByID=',offerProductsByID);
            var line_product_by_price = [];

            var k = 0;
            for (k in lines){
                var orderline = lines[k]
                if(orderline.pr_apply)
                    line_product_by_price.push({'price': orderline.price, 'product_id': orderline.product, 'minimum_quantity': orderline.min_quantity, 'quantity': orderline.quantity, 'line': orderline, 'discountedQty': 0})
            }
            
            
            var offeredLineByProductId = {}
            for(pbp in line_product_by_price){
                let offerLine  = line_product_by_price[pbp];
                if(Object.keys(offeredLineByProductId).indexOf(offerLine.product_id.id + '') > -1){
                    temp = offeredLineByProductId[offerLine.product_id.id];
                    temp['quantity'] = temp.quantity + offerLine.quantity;
                    offeredLineByProductId[offerLine.product_id.id] = temp;
                }else{
                    offeredLineByProductId[offerLine.product_id.id] = offerLine;
                }
            }
            line_product_by_price = Object.values(offeredLineByProductId);
            
            var lineDiscountedQty = 0,
            totalLines = 0;
            for(pbp in line_product_by_price){

                let offerLine  = line_product_by_price[pbp];
                if(Object.keys(offerProductsByID).indexOf(offerLine.product_id.id) && offerLine.quantity >= offerLine.minimum_quantity){
                    
                    lineDiscountedQty += offerLine.quantity;
                    totalLines+=1;
                    //console.log('offerLine.quantity----',offerLine.quantity, lineDiscountedQty);
                }
            }
            lineDiscountedQty = Math.floor(lineDiscountedQty/2);
            //console.log('lineDiscountedQty=', lineDiscountedQty, totalLines);
            line_product_by_price.sort(self.compare);
            // console.log('line_product_by_price sorted===',line_product_by_price);

            for(pbp in line_product_by_price){

                offerLine  = line_product_by_price[pbp];
                if(Object.keys(offerProductsByID).indexOf(offerLine.product_id.id) && offerLine.quantity >= offerLine.minimum_quantity){
                    if(lineDiscountedQty && offerLine.quantity < lineDiscountedQty){
                        line_product_by_price[pbp]['discountedQty'] = offerLine.quantity;
                        lineDiscountedQty -= (offerLine.quantity)
                    }else{
                        if(lineDiscountedQty && offerLine.quantity >= lineDiscountedQty && offerLine.quantity >= offerLine.minimum_quantity){
                            line_product_by_price[pbp]['discountedQty'] = lineDiscountedQty
                            lineDiscountedQty = 0;

                        }else{
                            console.log('may be error no match found');
                        }
                    }
                }
            }
            console.log('line_product_by_price with discount',line_product_by_price);

            var discount = 0;
            var line = {};
            var discounted_qty = 0;
            for(pbp in line_product_by_price){
                let offerLine  = line_product_by_price[pbp];
                line = offerLine.line;
                // console.log('price with tax==', line.get_price_with_tax(), line.get_discount())
                if(Object.keys(offerProductsByID).indexOf(offerLine.product_id.id) && offerLine.quantity >= offerLine.minimum_quantity){
                    discounted_qty = offerLine['discountedQty'];
                    // console.log('discounted_qty=', discounted_qty);
                    if(discounted_qty <= 0){
                        continue;
                    }
                    discount_item = offerProductsByID[offerLine.product_id.id]
                    line = offerLine.line;
                    if (orderRef.pos.config.iface_tax_included === 'total') {
                        discount = (line.get_price_with_tax() * (1 - line.get_discount() / 100)/line.quantity);
                    } else {
                        discount = line.get_unit_price() * (1 - line.get_discount() / 100)
                    }
                    
                    orderRef.add_promotion(promoProduct, -discount / 100 * discount_item.discount, discounted_qty, {
                        promotion: true,
                        promotion_discount: true,
                        promotion_reason: 'Applied ' + discount_item.promotion_id[1] + ', discount ' + discount_item.product_id[1],
                        applied_promotion_id: discount_item.promotion_id[0],
                    }); 
                }  
            }
        },
        
        compute_pack_discount: function (promotion) {
            /*
                4. Promotion discount by pack products
            */
            var self=this;
            var promotion_condition_items = this.pos.promotion_discount_condition_by_promotion_id[promotion.id];
            // console.log('promotion_condition_items=',promotion_condition_items);
            var product = this.pos.db.get_product_by_id(promotion.product_id[0]);
            var check = this.checking_pack_discount_and_pack_free_gift(promotion_condition_items, promotion); //psb passed promotion as parameter
            if (check == true) {
                var discount_items = this.pos.promotion_discount_apply_by_promotion_id[promotion.id]
                // console.log('discount_items==',discount_items);
                if (!discount_items) {
                    return;
                }
                if(promotion.or_case){
                    var minPrice;
                    lines = this.orderlines.models;
                    //console.log('line=',lines);
                    var discProducts = 0;
                    for (var j = 0; j < lines.length; j++) {
                        line = lines[j];
                        if (line.promotion) {
                            continue;
                        }
                        
                        for (var i=0; i < promotion_condition_items.length; i++){
                            var pr_item = promotion_condition_items[i]
                            //console.log('pr_item==',pr_item.product_id,this.pos.db.get_product_by_id(line.product.id),line.product.id, line.product.product_tmpl_id)
                            if(pr_item.product_id[0] === line.product.id && line.quantity >= pr_item.minimum_quantity){
                                discProducts +=1
                                line.pr_apply = true;
                                line.min_quantity = pr_item.minimum_quantity
                                if(!minPrice){
                                    minPrice = line.price;
                                }else if(line.price < minPrice){
                                    minPrice = line.price;
                                }
                            }
                        }
                    }
                    var newDiscountedItems = [];
                    var discItemsIds = [];
                    
                    // console.log('discProducts===',discProducts,discount_items);
                    k = 0
                    while(k < discProducts){
                        var temp = false
                        for(var l=0; l < discount_items.length; l++){
                            var d_item = discount_items[l];
                            if(discItemsIds.indexOf(d_item.product_id[0]) > -1){
                                continue;
                            }
                            
                                discItemsIds.push(d_item.product_id[0])
                                newDiscountedItems.push(d_item)

                        }
                        k+=1

                    }
                    discount_items = newDiscountedItems;
                    productList = []
                    for(let i=0; i<discProducts; i++ ){
                        productList.push(discount_items)
                    }
                    setTimeout(function(){
                        self.set_offers_on_line({
                            productList: productList,
                            count : discProducts,
                            products: discount_items,
                            promotion: promotion,
                            promoProduct:product,
                            orderRef: self
                            });
                        // return self.pos.gui.show_popup('freeProducts', {
                        //     title: 'Offer Products',
                        //     body: 'Please choose free products',
                        //     productList: productList,
                        //     count : discProducts,
                        //     products: discount_items,
                        //     promotion: promotion,
                        //     promoProduct:product,
                        //     orderRef: self
                        //     })
                    },800)
                return
                

                }
                var i = 0;
                while (i < discount_items.length) {
                    var discount_item = discount_items[i];
                    var discount = 0;
                    var total_qty = 0;
                    var lines = this.orderlines.models;
                    for (var j = 0; j < lines.length; j++) {
                        if (lines[j].promotion) {
                            continue;
                        }
                        var line = lines[j];
                        if (line.product.id == discount_item.product_id[0]) {
                            total_qty += line.quantity
                            if (this.pos.config.iface_tax_included === 'total') {
                                discount += line.get_price_with_tax() * (1 - line.get_discount() / 100)
                            } else {
                                discount += line.get_unit_price() * line.get_quantity() * (1 - line.get_discount() / 100)
                            }
                        }
                    }
                    if (discount_item.type == 'one') {
                        discount = discount / total_qty;
                    }
                    if (product && discount != 0) {
                        this.add_promotion(product, -discount / 100 * discount_item.discount, 1, {
                            promotion: true,
                            promotion_discount: true,
                            promotion_reason: 'Applied ' + promotion['name'] + ', discount ' + discount_item.product_id[1],
                            applied_promotion_id: promotion.id,
                        })
                    }
                    i++;
                }
            }
        },
        count_quantity_by_product: function (product) {
            /*
                Function return total qty filter by product of order
            */
            var qty = 0;
            for (var i = 0; i < this.orderlines.models.length; i++) {
                var line = this.orderlines.models[i];
                if (line.product['id'] == product['id']) {
                    qty += line['quantity'];
                }
            }
            return qty;
        },
        compute_pack_free_gift: function (promotion) {
            /*
                5. Promotion free gift by pack products
            */
            var promotion_condition_items = this.pos.promotion_gift_condition_by_promotion_id[promotion.id];
            var check = this.checking_pack_discount_and_pack_free_gift(promotion_condition_items, promotion);//psb passed promotion as parameter
            if (check == true) {
                var gifts = this.pos.promotion_gift_free_by_promotion_id[promotion.id]
                if (!gifts) {
                    return;
                }
                var products_condition = {};
                for (var i = 0; i < promotion_condition_items.length; i++) {
                    var condition = promotion_condition_items[i];
                    var product = this.pos.db.get_product_by_id(condition.product_id[0]);
                    products_condition[product['id']] = this.count_quantity_by_product(product)
                }
                var can_continue = true;
                var temp = 1;
                for (var i = 1; i < 100; i++) {
                    for (var j = 0; j < promotion_condition_items.length; j++) {
                        var condition = promotion_condition_items[j];
                        var condition_qty = condition.minimum_quantity;
                        var product = this.pos.db.get_product_by_id(condition.product_id[0]);
                        var total_qty = this.count_quantity_by_product(product);
                        if (i * condition_qty <= total_qty) {
                            can_continue = true;
                        } else {
                            can_continue = false
                        }
                    }
                    if (can_continue == true) {
                        temp = i;
                    } else {
                        break;
                    }
                }
                var i = 0;
                while (i < gifts.length) {
                    var product = this.pos.db.get_product_by_id(gifts[i].product_id[0]);
                    if (product) {
                        this.add_promotion(product, 0, -(gifts[i].quantity_free * temp), {
                            promotion: true,
                            promotion_gift: true,
                            promotion_reason: 'Applied  ' + promotion['name'] + ', give ' + (gifts[i].quantity_free * temp) + ' ' + product['display_name'],
                            applied_promotion_id: promotion.id,
                        })
                    }
                    i++;
                }
            }
        },
        compute_price_filter_quantity: function (promotion) {
            /*
                6. Promotion set price filter by quantities of product
            */
            var promotion_prices = this.pos.promotion_price_by_promotion_id[promotion.id]
            var product = this.pos.db.get_product_by_id(promotion.product_id[0]);
            var lines = this.orderlines.models;
            if (promotion_prices) {
                var prices_item_by_product_id = {};
                for (var i = 0; i < promotion_prices.length; i++) {
                    var item = promotion_prices[i];
                    if (!prices_item_by_product_id[item.product_id[0]]) {
                        prices_item_by_product_id[item.product_id[0]] = [item]
                    } else {
                        prices_item_by_product_id[item.product_id[0]].push(item)
                    }
                }
                var quantity_by_product_id = this.product_quantity_by_product_id()
                var price_discount = 0;
                for (i in quantity_by_product_id) {
                    if (prices_item_by_product_id[i]) {
                        var quantity_tmp = 0
                        var price_item_tmp = null
                        // root: quantity line, we'll compare this with 2 variable quantity line greater minimum quantity of item and greater quantity temp
                        for (var j = 0; j < prices_item_by_product_id[i].length; j++) {
                            var price_item = prices_item_by_product_id[i][j];
                            if (quantity_by_product_id[i] >= price_item.minimum_quantity && quantity_by_product_id[i] >= quantity_tmp) {
                                quantity_tmp = price_item.minimum_quantity;
                                price_item_tmp = price_item;
                            }
                        }
                        if (price_item_tmp) {
                            var price_discount = 0;
                            var z = 0;
                            while (z < lines.length) {
                                var line = lines[z];
                                if (line.product.id == price_item_tmp.product_id[0]) {
                                    if (this.pos.config.iface_tax_included === 'total') {
                                        price_discount += line.get_price_with_tax() - (price_item_tmp['price_down'] * line.quantity)
                                    } else {
                                        price_discount += line.get_price_without_tax() - (price_item_tmp['price_down'] * line.quantity)
                                    }
                                }
                                z++;
                            }
                            if (price_discount != 0) {
                                this.add_promotion(product, price_discount, -1, {
                                    promotion: true,
                                    promotion_price_by_quantity: true,
                                    promotion_reason: 'Applied ' + promotion['name'] + ', By greater or equal ' + price_item_tmp.minimum_quantity + ' ' + price_item_tmp.product_id[1] + ' price down  ' + price_item_tmp['price_down'] + ' / unit.',
                                    applied_promotion_id: promotion.id,
                                })
                            }
                        }
                    }
                }
            }
        },
        compute_special_category: function (promotion) {
            /*
                7. Promotion filter by special category
            */
            var product_service = this.pos.db.product_by_id[promotion['product_id'][0]];
            var promotion_lines = this.pos.promotion_special_category_by_promotion_id[promotion['id']];
            this.lines_by_category_id = {};
            for (var i = 0; i < this.orderlines.models.length; i++) {
                var line = this.orderlines.models[i];
                if (line.promotion) {
                    continue;
                }
                var pos_categ_id = line['product']['pos_categ_id'][0]
                if (pos_categ_id) {
                    if (!this.lines_by_category_id[pos_categ_id]) {
                        this.lines_by_category_id[pos_categ_id] = [line]
                    } else {
                        this.lines_by_category_id[pos_categ_id].push(line)
                    }
                }
            }
            for (var i = 0; i < promotion_lines.length; i++) {
                var promotion_line = promotion_lines[i];
                var categ_id = promotion_line['category_id'][0];
                if (this.lines_by_category_id[categ_id]) {
                    var total_quantity = 0;
                    for (var i = 0; i < this.lines_by_category_id[categ_id].length; i++) {
                        total_quantity += this.lines_by_category_id[categ_id][i]['quantity']
                    }
                    if (promotion_line['count'] <= total_quantity) {
                        var promotion_type = promotion_line['type'];
                        if (promotion_type == 'discount') {
                            var discount = 0;
                            var quantity = 0;
                            var lines = this.lines_by_category_id[categ_id];
                            for (var j = 0; j < lines.length; j++) {
                                quantity += lines[j]['quantity'];
                                var line = lines[j];
                                if (quantity >= promotion_line['count']) {
                                    if (this.pos.config.iface_tax_included === 'total') {
                                        discount += line.get_price_with_tax() / 100 / line['quantity'] * promotion_line['discount']
                                    } else {
                                        discount += line.get_price_without_tax() / 100 / line['quantity'] * promotion_line['discount']
                                    }
                                }
                            }
                            if (discount != 0) {
                                this.add_promotion(product_service, -discount, 1, {
                                    promotion: true,
                                    promotion_special_category: true,
                                    promotion_reason: 'Applied ' + promotion['name'] + ',Buy bigger than or equal ' + promotion_line['count'] + ' product of ' + promotion_line['category_id'][1] + ' discount ' + promotion_line['discount'] + ' %',
                                    applied_promotion_id: promotion.id,
                                })
                            }
                        }
                        if (promotion_type == 'free') {
                            var product_free = this.pos.db.product_by_id[promotion_line['product_id'][0]];
                            if (product_free) {
                                this.add_promotion(product_free, 0, promotion_line['qty_free'], {
                                    promotion: true,
                                    promotion_special_category: true,
                                    promotion_reason: 'Applied ' + promotion['name'] + ', Buy bigger than or equal ' + promotion_line['count'] + ' product of ' + promotion_line['category_id'][1] + ' free ' + promotion_line['qty_free'] + ' ' + product_free['display_name'],
                                    applied_promotion_id: promotion.id,
                                })
                            }
                        }
                    }
                }
            }
        },
        compute_discount_lowest_price: function (promotion) { // compute discount lowest price
            var orderlines = this.orderlines.models;
            var line_apply = null;
            for (var i = 0; i < orderlines.length; i++) {
                var line = orderlines[i];
                if (!line_apply) {
                    line_apply = line
                } else {
                    if (line.get_price_with_tax() < line_apply.get_price_with_tax()) {
                        line_apply = line;
                    }
                }
            }
            var product_discount = this.pos.db.product_by_id[promotion.product_id[0]];
            if (line_apply && product_discount) {
                var price;
                if (this.pos.config.iface_tax_included === 'total') {
                    price = line_apply.get_price_with_tax()
                } else {
                    price = line_apply.get_price_without_tax()
                }
                this.add_promotion(product_discount, price, -1, {
                    promotion: true,
                    promotion_discount_lowest_price: true,
                    promotion_reason: 'Applied ' + promotion['name'] + ', discount ' + promotion.discount_lowest_price + ' % on product ' + line_apply.product.display_name,
                    applied_promotion_id: promotion.id,
                })
            }
        },
        compute_multi_buy: function (promotion) { // compute multi by
            var rule_applied = {};
            var total_qty_by_product = {};
            var total_price_by_product = {};
            for (var i = 0; i < this.orderlines.models.length; i++) {
                var line = this.orderlines.models[i];
                var product_id = line.product.id;
                var rule = this.pos.multi_buy_by_product_id[product_id];
                if (!total_qty_by_product[line.product.id]) {
                    total_qty_by_product[line.product.id] = line.quantity;
                } else {
                    total_qty_by_product[line.product.id] += line.quantity;
                }
                if (rule) {
                    rule_applied[rule['product_id'][0]] = rule;
                }
                if (this.pos.config.iface_tax_included === 'total') {
                    if (!total_price_by_product[line.product.id]) {
                        total_price_by_product[line.product.id] = line.get_price_with_tax();
                    } else {
                        total_price_by_product[line.product.id] += line.get_price_with_tax();
                    }
                } else {
                    if (!total_price_by_product[line.product.id]) {
                        total_price_by_product[line.product.id] = line.get_price_without_tax();
                    } else {
                        total_price_by_product[line.product.id] += line.get_price_without_tax();
                    }
                }
            }
            var product_discount = this.pos.db.product_by_id[promotion.product_id[0]];
            if (rule_applied && product_discount) {
                var price_discount = 0;
                for (var product_id in rule_applied) {
                    var product = this.pos.db.get_product_by_id(product_id);
                    var total_qty = total_qty_by_product[product_id];
                    var sub_price;
                    var price_discount;
                    if (total_qty >= rule_applied[product_id]['next_number'] && line.quantity > 0) {
                        if (total_qty % rule_applied[product_id]['next_number'] == 0) {
                            price_discount = total_price_by_product[product_id] - rule_applied[product_id]['list_price'] * total_qty;
                        } else {
                            var qty_temp = total_qty % rule_applied[product_id]['next_number'];
                            sub_price = qty_temp * total_price_by_product[product_id] / total_qty_by_product[product_id];
                            sub_price += (total_qty_by_product[product_id] - qty_temp) * rule_applied[product_id]['list_price'];
                            price_discount = total_price_by_product[product_id] - sub_price;
                        }
                    }
                    if (price_discount) {
                        if (price_discount < 0) {
                            price_discount = -price_discount
                        }
                        this.add_promotion(product_discount, price_discount, -1, {
                            promotion: true,
                            promotion_multi_buy: true,
                            promotion_reason: 'Applied ' + promotion['name'] + ', multi buy ' + product['display_name'],
                            applied_promotion_id: promotion.id,
                        })
                    }
                }
            }
        },
        compute_buy_x_get_another_free: function (promotion) {
            var lines_will_apply_promotion = _.filter(this.orderlines.models, function (line) {
                if (promotion['product_ids'].indexOf(line.product.id) != -1 && !line.promotion) {
                    return true
                }
            });
            var minimum_items = promotion['minimum_items'];
            if (lines_will_apply_promotion.length && lines_will_apply_promotion.length >= minimum_items) {
                var line_lowest_price = null;
                for (var i = 0; i < lines_will_apply_promotion.length; i++) {
                    var line = lines_will_apply_promotion[i];
                    if (line.promotion) {
                        continue;
                    }
                    if (!line_lowest_price) {
                        line_lowest_price = line;
                        continue
                    }
                    if (line_lowest_price.get_price_with_tax() > line.get_price_with_tax()) {
                        line_lowest_price = line;
                    }
                }
                var promotion_service = this.pos.db.get_product_by_id(promotion.product_id[0]);
                var price_discount;
                if (this.pos.config.iface_tax_included === 'total') {
                    price_discount = line_lowest_price.get_price_with_tax();
                } else {
                    price_discount = line_lowest_price.get_price_without_tax();
                }
                this.add_promotion(promotion_service, price_discount, -1, {
                    promotion: true,
                    promotion_multi_buy: true,
                    promotion_reason: 'Applied ' + promotion['name'] + ', free product ' + line_lowest_price.product['display_name'],
                    applied_promotion_id: promotion.id,
                })
            }
        },
        add_promotion: function (product, price, quantity, options) {
            /*
                    Function apply promotions to current order
             */
            var line = new models.Orderline({}, {pos: this.pos, order: this.pos.get_order(), product: product});
            order_promotions = this.pos.get_order().promotion_ids;
            if (order_promotions.length > 0){
                if (order_promotions[0][2].indexOf(options.applied_promotion_id) < 0){
                    order_promotions[0][2].push(options.applied_promotion_id)
                }
            }else{
                var tmp = [6, 0]
                tmp.push([options.applied_promotion_id])
                order_promotions.push(tmp)
            }
            if (options.promotion) {
                line.promotion = options.promotion;
            }
            if (options.buyer_promotion) {
                line.promotion = options.buyer_promotion;
            }
            if (options.frequent_buyer_id) {
                line.frequent_buyer_id = options.frequent_buyer_id;
            }
            if (options.promotion_reason) {
                line.promotion_reason = options.promotion_reason;
            }
            if (options.promotion_discount_total_order) {
                line.promotion_discount_total_order = options.promotion_discount_total_order;
            }
            if (options.promotion_discount_category) {
                line.promotion_discount_category = options.promotion_discount_category;
            }
            if (options.promotion_discount_by_quantity) {
                line.promotion_discount_by_quantity = options.promotion_discount_by_quantity;
            }
            if (options.promotion_discount) {
                line.promotion_discount = options.promotion_discount;
            }
            if (options.promotion_gift) {
                line.promotion_gift = options.promotion_gift;
            }
            if (options.promotion_price_by_quantity) {
                line.promotion_price_by_quantity = options.promotion_price_by_quantity;
            }
            if (options.promotion_special_category) {
                line.promotion_special_category = options.promotion_special_category;
            }
            if (options.promotion_discount_lowest_price) {
                line.promotion_discount_lowest_price = options.promotion_discount_lowest_price;
            }
            line.price_manually_set = true; // no need pricelist change, price of promotion change the same, i blocked
            line.set_quantity(quantity);
            line.set_unit_price(price);
            this.orderlines.add(line);
            this.trigger('change', this);
        },
        get_promotions_active: function () {
            var can_apply = null;
            var promotions_active = [];
            if (!this.pos.promotions) {
                return {
                    can_apply: can_apply,
                    promotions_active: []
                };
            }
            for (var i = 0; i < this.pos.promotions.length; i++) {
                var promotion = this.pos.promotions[i];
                if (promotion['type'] == '1_discount_total_order' && this.checking_apply_total_order(promotion)) {
                    can_apply = true;
                    promotions_active.push(promotion);
                }
                else if (promotion['type'] == '2_discount_category' && this.checking_can_discount_by_categories(promotion)) {
                    can_apply = true;
                    promotions_active.push(promotion);
                }
                else if (promotion['type'] == '3_discount_by_quantity_of_product' && this.checking_apply_discount_filter_by_quantity_of_product(promotion)) {
                    can_apply = true;
                    promotions_active.push(promotion);
                }
                else if (promotion['type'] == '4_pack_discount') {
                    var promotion_condition_items = this.pos.promotion_discount_condition_by_promotion_id[promotion.id];
                    var check = this.checking_pack_discount_and_pack_free_gift(promotion_condition_items);
                    if (check) {
                        can_apply = true;
                        promotions_active.push(promotion);
                    }
                }
                else if (promotion['type'] == '5_pack_free_gift') {
                    var promotion_condition_items = this.pos.promotion_gift_condition_by_promotion_id[promotion.id];
                    var check = this.checking_pack_discount_and_pack_free_gift(promotion_condition_items);
                    if (check) {
                        can_apply = true;
                        promotions_active.push(promotion);
                    }
                }
                else if (promotion['type'] == '6_price_filter_quantity' && this.checking_apply_price_filter_by_quantity_of_product(promotion)) {
                    can_apply = true;
                    promotions_active.push(promotion);
                }
                else if (promotion['type'] == '7_special_category' && this.checking_apply_specical_category(promotion)) {
                    can_apply = true;
                    promotions_active.push(promotion);
                }
                else if (promotion['type'] == '8_discount_lowest_price') {
                    can_apply = true;
                    promotions_active.push(promotion);
                }
                else if (promotion['type'] == '9_multi_buy') {
                    if (this.checking_multi_by(promotion)) {
                        can_apply = true;
                        promotions_active.push(promotion);
                    }
                }
                else if (promotion['type'] == '10_buy_x_get_another_free') {
                    if (this.checking_buy_x_get_another_free(promotion)) {
                        can_apply = true;
                        promotions_active.push(promotion);
                    }
                }
            }
            return {
                can_apply: can_apply,
                promotions_active: promotions_active
            };
        }
    });
    screens.OrderWidget.include({
        active_promotion: function (buttons, selected_order) {
            if (selected_order.orderlines && selected_order.orderlines.length > 0 && this.pos.config.promotion_ids.length > 0) {
                var lines = selected_order.orderlines.models;
                var promotion_amount = 0;
                for (var i = 0; i < lines.length; i++) {
                    var line = lines[i]
                    if (line.promotion) {
                        promotion_amount += line.get_price_without_tax()
                    }
                }
                var promotion_datas = selected_order.get_promotions_active();
                var can_apply = promotion_datas['can_apply'];
                if (buttons && buttons.button_promotion) {
                    buttons.button_promotion.highlight(can_apply);
                }
                var promotions_active = promotion_datas['promotions_active'];
                if (promotions_active.length) {
                    var promotion_recommend_customer_html = qweb.render('promotion_recommend_customer', {
                        promotions: promotions_active
                    });
                    $('.promotion_recommend_customer').html(promotion_recommend_customer_html);
                } else {
                    $('.promotion_recommend_customer').html("");
                }
            }
        },
        promotion_added: function (buttons, selected_order) {
            var promotion_added = false;
            if (selected_order.orderlines && selected_order.orderlines.length > 0 && this.pos.config.promotion_ids.length > 0) {
                var lines = selected_order.orderlines.models;
                for (var i = 0; i < lines.length; i++) {
                    var line = lines[i]
                    if (line.promotion) {
                        promotion_added = true;
                        break
                    }
                }
                if (buttons && buttons.button_remove_promotion) {
                    buttons.button_remove_promotion.highlight(promotion_added);
                }
            }
            return promotion_added;
        },
        active_buyers_promotion(buttons, selected_order) {
            if (buttons.button_buyer_promotion) {
                var check = false;
                if (selected_order.orderlines.length == 0) {
                    check = false;
                }
                var customer = selected_order.get_client();
                if (!customer) {
                    check = false;
                } else {
                    var buyers = this.pos.buyer_by_partner_id[customer.id];
                    if (buyers && buyers.length > 0) {
                        for (var i = 0; i < buyers.length; i++) {
                            var buyer = buyers[i];
                            var promotion_id = buyer['promotion_id'][0];
                            var promotion = this.pos.buyer_by_promotion_id[promotion_id];
                            if (promotion) {
                                var buyer_group_id = promotion['buyers_group'][0];
                                var buyer_group = this.pos.buyer_group_by_id[buyer_group_id];
                                if (buyer_group) {
                                    var product_ids = buyer_group['products_ids'];
                                    if (product_ids.length > 0) {
                                        var number_of_sales = buyer_group['number_of_sales'];
                                        var lines = _.filter(selected_order.orderlines.models, function (line) {
                                            return product_ids.indexOf(line.product.id) != -1
                                        });
                                        var products_add_promotion = {};
                                        for (var j = 0; j < lines.length; j++) {
                                            var line = lines[j];
                                            if (!products_add_promotion[line.product.id]) {
                                                var qty_total = selected_order.count_quantity_by_product(line.product);
                                                if (qty_total >= number_of_sales) {
                                                    check = true;
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }
                }
                buttons.button_buyer_promotion.highlight(check);
            }
        },
        update_summary: function () {
            this._super();
            var selected_order = this.pos.get_order();
            var buttons = this.getParent().action_buttons;
            if (selected_order && buttons) {
                this.active_promotion(buttons, selected_order);
                this.promotion_added(buttons, selected_order);
                this.active_buyers_promotion(buttons, selected_order);
            }
        }
    });

    var button_promotion = screens.ActionButtonWidget.extend({// promotion button
        template: 'button_promotion',
        button_click: function () {
            var order = this.pos.get('selectedOrder');
            // changed code: PSB
            var customer = order.changed.client;
            var promotion_manual_select = this.pos.config.promotion_manual_select;
            var self = this;
            
            if (!promotion_manual_select) {
                order.compute_promotion()
            } else {
                order.get_promotions_active().done(function(promotion_datas){
                    var promotions_active = promotion_datas['promotions_active'];
                    if (promotions_active.length) {
                        return self.pos.gui.show_popup('popup_selection_promotions', {
                            title: 'Promotions',
                            body: 'Please choice promotions and confirm',
                            promotions_active: promotions_active
                        })
                    } else {
                        return self.pos.gui.show_popup('confirm', {
                            title: 'Warning',
                            body: 'Nothing promotions active',
                        })
                    }
                });

            
            }
        }
    });
    screens.define_action_button({
        'name': 'button_promotion',
        'widget': button_promotion,
        'condition': function () {
            return this.pos.promotion_ids && this.pos.promotion_ids.length >= 1;
        }
    });

    var button_remove_promotion = screens.ActionButtonWidget.extend({
        template: 'button_remove_promotion',
        button_click: function () {
            var order = this.pos.get('selectedOrder');
            if (order) {
                order.remove_all_promotion_line();
                order.remove_all_promotion_line();
                this.pos.get('selectedOrder').promotion_ids = [];
            }
        }
    });
    screens.define_action_button({
        'name': 'button_remove_promotion',
        'widget': button_remove_promotion,
        'condition': function () {
            return this.pos.promotion_ids && this.pos.promotion_ids.length >= 1;
        }
    });
    

    var popup_selection_promotions = PopupWidget.extend({ // add tags
        template: 'popup_selection_promotions',
        show: function (options) {
            var self = this;
            this._super(options);
            this.promotions_selected = {};
            var promotions = options.promotions_active;
            this.promotions = promotions;
            var promotion_popup_el = self.$el.find('.body').html(qweb.render('promotion_list', {
                promotions: promotions,
                widget: self
            }));
            this.$('.product').click(function () {
                var promotion_id = parseInt($(this).data('id'));
                var promotion = self.pos.promotion_by_id[promotion_id];
                if (promotion) {
                    if ($(this).closest('.product').hasClass("item-selected") == true) {
                        $(this).closest('.product').toggleClass("item-selected");
                        delete self.promotions_selected[promotion.id];
                    } else {
                        var $el_popup_products = $(promotion_popup_el[0]).find('.product');
                        for(var i = 0; i < $el_popup_products.length; i++){
                            var $el = $el_popup_products[i];
                            if($($el).hasClass('item-selected') == true){
                            
                                var prm_id = parseInt($($el).data('id'));
                                $($el).toggleClass("item-selected");
                                delete self.promotions_selected[prm_id];
                            }
                        }
                        
                        $(this).closest('.product').toggleClass("item-selected");
                        self.promotions_selected[promotion.id] = promotion;
                    }
                }
            });
            this.$('.cancel').click(function () {
                self.pos.gui.close_popup();
            });
            this.$('.confirm').click(function () {
                self.pos.gui.close_popup();
                var promotions = [];
                for (var i in self.promotions_selected) {
                    promotions.push(self.promotions_selected[i]);
                }
                if (promotions.length) {
                    self.pos.get_order().manual_compute_promotion(promotions)
                }
            });
            this.$('.add_all').click(function () {
                self.pos.get_order().manual_compute_promotion(self.promotions);
                self.pos.gui.close_popup();
            });
        },
    });
    gui.define_popup({name: 'popup_selection_promotions', widget: popup_selection_promotions});

    var button_buyer_promotion = screens.ActionButtonWidget.extend({
        template: 'button_buyer_promotion',
        init: function (parent, options) {
            this._super(parent, options);
        },
        button_click: function () {
            var order = this.pos.get_order();
            if (!order) {
                return null
            }
            if (order.orderlines.length == 0) {
                return this.pos.gui.show_popup('confirm', {
                    title: 'Warning',
                    body: 'Your order is blank',
                })
            }
            var customer = order.get_client();
            order.remove_all_buyer_promotion_line();
            if (!customer) {
                return this.pos.gui.show_popup('confirm', {
                    title: 'Warning',
                    body: 'Please choice customer, customer of order is blank',
                })
            } else {
                var buyers = this.pos.buyer_by_partner_id[customer.id];
                if (buyers && buyers.length > 0) {
                    for (var i = 0; i < buyers.length; i++) {
                        var buyer = buyers[i];
                        var promotion_id = buyer['promotion_id'][0];
                        var promotion = this.pos.buyer_by_promotion_id[promotion_id];
                        if (promotion) {
                            var buyer_group_id = promotion['buyers_group'][0];
                            var buyer_group = this.pos.buyer_group_by_id[buyer_group_id];
                            if (buyer_group) {
                                var product_ids = buyer_group['products_ids'];
                                if (product_ids.length > 0) {
                                    var number_of_sales = buyer_group['number_of_sales'];
                                    var lines = _.filter(order.orderlines.models, function (line) {
                                        return product_ids.indexOf(line.product.id) != -1
                                    });
                                    var products_add_promotion = {};
                                    for (var j = 0; j < lines.length; j++) {
                                        var line = lines[j];
                                        if (!products_add_promotion[line.product.id]) {
                                            var qty_total = order.count_quantity_by_product(line.product);
                                            if (qty_total >= number_of_sales) {
                                                var qty_free = parseInt(qty_total / number_of_sales);
                                                var product_discount = this.pos.db.product_by_id[line.product.id];
                                                order.add_promotion(product_discount, 0, 1, {
                                                    'promotion': true,
                                                    'promotion_reason': 'By smaller than ' + number_of_sales + ' ' + line.product['display_name'] + ' free ' + qty_free,
                                                    'promotion_gift': true,
                                                    'frequent_buyer_id': buyer['id'],
                                                    'buyer_promotion': true
                                                });
                                                products_add_promotion[line.product.id] = line.product.id;
                                            }
                                        }
                                    }
                                }
                            } else {
                                return this.pos.gui.show_popup('confirm', {
                                    title: 'Warning',
                                    body: 'Could not find buyer group',
                                })
                            }

                        } else {
                            return this.pos.gui.show_popup('confirm', {
                                title: 'Warning',
                                body: 'Could not find promotion',
                            })
                        }
                    }
                } else {
                    return this.pos.gui.show_popup('confirm', {
                        title: 'Warning',
                        body: 'Customer have not promotion',
                    })
                }
            }
        }
    });
    screens.define_action_button({
        'name': 'button_buyer_promotion',
        'widget': button_buyer_promotion,
        'condition': function () {
            return this.pos.buyers_promotion && this.pos.buyers_promotion.length;
        }
    });




});

# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import itertools
import time
from odoo import api, fields, models, tools, _
from odoo.addons import decimal_precision as dp
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from odoo.exceptions import ValidationError, RedirectWarning, except_orm




class WizardPosPromotionAddProduct(models.TransientModel):
    _name = "wizard.pos.promotion.add.product"
    _description = "Pos Promotion add product"


    name = fields.Char('Name')
    pos_product_lines = fields.One2many('pos.add.product.line','wiz_pos_prom_id','Pos Product Line')
    type = fields.Selection([
        ('1_discount_total_order', '1. Discount each amount total order'),
        ('2_discount_category', '2. Discount each category'),
        ('3_discount_by_quantity_of_product', '3. Discount each quantity of product'),
        ('4_pack_discount', '4. Buy pack products discount products'),
        ('5_pack_free_gift', '5. Buy pack products free products'),
        ('6_price_filter_quantity', '6. Sale off products filter by quantity'),
        ('7_special_category', '7. Discount each special category'),
        ('8_discount_lowest_price', '8. Discount lowest price'),
        ('9_multi_buy', '9. Multi buy - By X for price'),
        ('10_buy_x_get_another_free', '10. Buy x get anothor free'),
    ], 'Type', default='1_discount_total_order', required=1)
    first_product_ids = fields.Many2many('product.product','first_product_wiz_rel','wizard_promtion_id1','product_id1','First Product')
    second_product_ids = fields.Many2many('product.product','second_product_wiz_rel','wizard_promtion_id2','product_id2', 'Second Product')
    category_ids = fields.Many2many('pos.category','prom_categ_wiz_rel','wizard_promtion_id3','categ_id','Catgeory')
    # pos_config_id = fields.Many2one('pos.config','POS')
    # type = fields.Selection([('direct', 'Direct'), ('arc', 'ARC'), ('open_order', 'Open Order')], string="Type",
    #                         help='ARC: When flow running from MRS to PO.\n'
    #                              ' Direct: When creted for ARC Item and Flow running from MRS to PR and then PO created directly from PR')
    # session_id = fields.Many2one('pos.session', 'Vendor')
    # session_ids = fields.Many2many('pos.session',string='Session ids')
    # pr_or_po = fields.Selection([('pr','PR'), ('po', 'PO')], string='Create PR or PO', help="PR: Pr created, PO: Adhoc Order created if type is Direct or ARC Order created if type is ARC")

    @api.model
    def default_get(self, fields):
        active_ids = self._context['active_ids']
        # pr_or_po = self._context['pr_or_po']

        type = ''

        res = super(WizardPosPromotionAddProduct, self).default_get(fields)
        if active_ids:
            browseable_active_ids = self.env['pos.promotion'].browse(active_ids)
            print ("browsesableeeeeeeeeeeeee",browseable_active_ids,browseable_active_ids.type)
            print ("fi;edsssssssss",fields,res)
            if 'type' in fields:
                res['type'] = browseable_active_ids.type


        return res
    #

    def get_add_products(self):
        """Call when button 'Add product' clicked.
        """
        print ("selffffffffff",self,self.pos_product_lines)
        if self :
            cond_list=[]
            apply_list=[]
            second_list=[]
            third_list=[]
            four_list1 = []
            four_list2 = []
            five_list1 = []
            five_list2 = []
            six_list = []
            nine_list = []
            pos_promotion_ids = self._context.get('active_id')
            promotion_ids = self.env['pos.promotion'].browse(pos_promotion_ids)
            print ("promotttttttttt",pos_promotion_ids,promotion_ids)
            # if self.pos_product_lines:
            #     for val in self.pos_product_lines:
            #         dict_cond = (0, False, {'product_id': val.product_id.id, 'minimum_quantity': 1.0})
            #         cond_list.append(dict_cond)
            #         dict_apply = (0, False, {'product_id': val.product_id.id, 'discount': 1, 'type': 'one'})
            #         apply_list.append(dict_apply)
            if promotion_ids.type == '3_discount_by_quantity_of_product' :
                if self.first_product_ids :
                    for val in self.first_product_ids :
                        dict_cond = (0, False, {'product_id': val.id, 'quantity': 1.0,'discount':0,'promotion_id':promotion_ids.id})
                        third_list.append(dict_cond)
                    # promotion_ids.discount_quantity_ids = third_list
            elif promotion_ids.type == '4_pack_discount' :
                if self.first_product_ids :
                    for val in self.first_product_ids :
                        dict_cond = (0, False, {'product_id': val.id, 'minimum_quantity': 1.0,'promotion_id':promotion_ids.id})
                        four_list1.append(dict_cond)
                    # promotion_ids.discount_condition_ids = four_list1
                if self.second_product_ids :
                    for val1 in self.second_product_ids :
                        dict_apply = (0, False, {'product_id': val1.id, 'discount': 1, 'type': 'one','promotion_id':promotion_ids.id})
                        four_list2.append(dict_apply)
                    # promotion_ids.discount_apply_ids = four_list2
            elif promotion_ids.type == '5_pack_free_gift' :
                if self.first_product_ids :
                    for val in self.first_product_ids :
                        dict_cond = (0, False, {'product_id': val.id, 'minimum_quantity': 1.0,'promotion_id':promotion_ids.id})
                        five_list1.append(dict_cond)
                    # promotion_ids.gift_condition_ids = five_list1
                if self.second_product_ids :
                    for val1 in self.second_product_ids :
                        dict_apply = (0, False, {'product_id': val1.id, 'quantity_free': 1.0,'promotion_id':promotion_ids.id})
                        five_list2.append(dict_apply)
                    # promotion_ids.gift_free_ids = five_list2
            # elif promotion_ids.type == '6_price_filter_quantity' :
            #     if self.first_product_ids :
            #         for val in self.first_product_ids :
            #             dict_cond = (0, False, {'product_id': val.id, 'minimum_quantity': 1.0,'list_price':0})
            #             six_list.append(dict_cond)
            #         # promotion_ids.price_ids = six_list
            # # elif promotion_ids.type == '7_special_category' :
            # #     if self.first_product_ids :
            # #         for val in self.first_product_ids :
            # #             dict_cond = (0, False, {'product_id': val.id, 'minimum_quantity': 1.0})
            # #             third_list.append(dict_cond)
            # #         promotion_ids.discount_quantity_ids = third_list
            # # elif promotion_ids.type == '8_discount_lowest_price' :
            # #     if self.first_product_ids :
            # #         for val in self.first_product_ids :
            # #             dict_cond = (0, False, {'product_id': val.id, 'minimum_quantity': 1.0})
            # #             third_list.append(dict_cond)
            # #         promotion_ids.discount_quantity_ids = third_list
            # elif promotion_ids.type == '9_multi_buy' :
            #     if self.first_product_ids :
            #         for val in self.first_product_ids :
            #             dict_cond = (0, False, {'product_id': val.id, 'list_price': 0,'next_number':0})
            #             nine_list.append(dict_cond)
            #         # promotion_ids.multi_buy_ids = nine_list
            # # elif promotion_ids.type == '10_buy_x_get_another_free' :
            # #     if self.first_product_ids :
            # #         for val in self.first_product_ids :
            # #             dict_cond = (0, False, {'product_id': val.id, 'minimum_quantity': 1.0})
            # #             third_list.append(dict_cond)
            # #         promotion_ids.discount_quantity_ids = third_list

            print ("condition listtttttttttt",cond_list)
            print ("apply_list listtttttttttt", apply_list)
            if promotion_ids :
                if len(third_list) > 0:
                    promotion_ids.discount_category_ids = [(6,0,[])]
                    promotion_ids.discount_quantity_ids = third_list
                    promotion_ids.discount_condition_ids = [(6,0,[])]
                    promotion_ids.discount_apply_ids = [(6,0,[])]
                    promotion_ids.gift_condition_ids = [(6,0,[])]
                    promotion_ids.gift_free_ids = [(6,0,[])]
                    promotion_ids.price_ids = [(6,0,[])]
                    promotion_ids.multi_buy_ids = [(6,0,[])]
                    promotion_ids.discount_order_ids =[(6,0,[])]
                    promotion_ids.special_category_ids = [(6,0,[])]
                    promotion_ids.product_ids =[(6,0,[])]
                elif len(four_list1) > 0 and len(four_list2) > 0:
                    promotion_ids.discount_category_ids = [(6,0,[])]
                    promotion_ids.discount_quantity_ids = [(6,0,[])]
                    promotion_ids.discount_condition_ids = four_list1
                    promotion_ids.discount_apply_ids = four_list2
                    promotion_ids.gift_condition_ids = [(6,0,[])]
                    promotion_ids.gift_free_ids = [(6,0,[])]
                    promotion_ids.price_ids = [(6,0,[])]
                    promotion_ids.multi_buy_ids = [(6,0,[])]
                    promotion_ids.discount_order_ids =[(6,0,[])]
                    promotion_ids.special_category_ids = [(6,0,[])]
                    promotion_ids.product_ids =[(6,0,[])]
                elif len(five_list1) > 0 and len(five_list1) > 0:
                    promotion_ids.discount_category_ids = [(6,0,[])]
                    promotion_ids.discount_quantity_ids = [(6,0,[])]
                    promotion_ids.discount_condition_ids = [(6,0,[])]
                    promotion_ids.discount_apply_ids = [(6,0,[])]
                    promotion_ids.gift_condition_ids = five_list1
                    promotion_ids.gift_free_ids = five_list2
                    promotion_ids.price_ids = [(6,0,[])]
                    promotion_ids.multi_buy_ids = [(6,0,[])]
                    promotion_ids.discount_order_ids =[(6,0,[])]
                    promotion_ids.special_category_ids = [(6,0,[])]
                    promotion_ids.product_ids =[(6,0,[])]

        return True


class PosAddProductLine(models.TransientModel):
    _name = "pos.add.product.line"
    _description = "Pos Promotion add product line"

    wiz_pos_prom_id = fields.Many2one('wizard.pos.promotion.add.product','Wizard Pos ID')
    product_id = fields.Many2one('product.product','Product')

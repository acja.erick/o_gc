odoo.define('pos_customer_api.pos_promotion', function (require) {
"use strict";

var core = require('web.core');
var pos_model = require('point_of_sale.models');
var screens = require('point_of_sale.screens');

var _t = core._t;

pos_model.load_fields("pos.promotion", ['id', 'customer_specific']);
    
});
odoo.define('pos_customer_api.pos_model', function (require) {
"use strict";

var core = require('web.core');
var pos_model = require('point_of_sale.models');

var _t = core._t;
    
    pos_model.load_fields("res.company", ['foodbot_api_key', 'restaurant_id', 'foodbot_url', 'crypto_expiration_time', 'qr_code_url', 'is_sale_points', 'point_product_id', 'foodbot_uuid', 'foodbot_auth_key', 'foodbot_ably_key']);
    pos_model.load_fields("pos.order", ['store_value_used', 'promotion_ids', 'unique_id']);

    var _super_Order = pos_model.Order.prototype;
    pos_model.Order = pos_model.Order.extend({

        initialize: function(){
            _super_Order.initialize.apply(this,arguments);
            this.store_value_used = 0;
            this.promotion_ids = [];
        },

        init_from_JSON: function (json) {
            _super_Order.init_from_JSON.apply(this, arguments);
            if (json.promotion_ids) {
                this.promotion_ids = json.promotion_ids;
            }
            if (json.store_value_used){
                this.store_value_used = json.store_value_used;
            }
            if (json.unique_id){
                this.unique_id = json.unique_id;
            }
        },
        export_as_JSON: function () {
            var json = _super_Order.export_as_JSON.apply(this, arguments);
            json.promotion_ids = this.promotion_ids ? this.promotion_ids : [];
            json.store_value_used = this.store_value_used ? this.store_value_used : 0;
            json.unique_id = this.unique_id ? this.unique_id : '';
            return json;
        },
        add_paymentline: function(payment_method) {
            this.assert_editable();
            var newPaymentline = new pos_model.Paymentline({},{order: this, payment_method:payment_method, pos: this.pos});
            if(!payment_method.is_cash_count || this.pos.config.iface_precompute_cash){
                if (payment_method.name.indexOf('FoodBot') > -1){
                    let due = this.get_due(),
                    set_due = 0;
                    let remainingBalance = 0;
                    if(this.store_value_used < this.store_value){
                        remainingBalance =  this.store_value - this.store_value_used;
                        if (remainingBalance >= due){
                            set_due = due;
                            this.store_value_used += due
                        }else if(remainingBalance < due){
                            set_due = remainingBalance;
                            this.store_value_used += remainingBalance;
                        }
                    }
                    this.set_store_value(set_due);
                    newPaymentline.set_amount( Math.max(set_due,0) );
                }else{
                    newPaymentline.set_amount( Math.max(this.get_due(),0) );
                }
            }
            this.paymentlines.add(newPaymentline);
            this.select_paymentline(newPaymentline);

        },
        set_store_value: function(storeValue){
            this.store_value_used = storeValue;
        },
        get_store_value: function(){
            return this.store_value_used;
        },
        set_unique_id: function(uniqueID){
            this.unique_id = uniqueID;
        },
        get_unique_id: function(){
            return this.unique_id;
        },

        parseFloat2Time: function(float_time){

            let hour = Math.floor(float_time),
            mins = (float_time - hour) * 100;
            mins = Math.floor(mins*60/100);
            if(hour < 10 ){
                hour = '0'+hour;
            }
            if(mins < 10){
                mins = '0'+mins;
            }
            let time = hour +' : '+mins;
            console.log('time==',time)
            return time
        },

        //Promotion related
        validate_promotion: function () {
            var self = this;
            self.get_promotions_active().done(function(datas){
                var promotions_active = datas['promotions_active'];
                if (promotions_active.length) {
                    self.pos.gui.show_screen('products');
                    self.pos.gui.show_popup('confirm', {
                        title: 'Promotion active',
                        body: 'Do you want to apply promotion on this order ?',
                        confirm: function () {
                            self.remove_all_promotion_line();
                            self.compute_promotion();
                            setTimeout(function () {
                                self.validate_global_discount();
                            }, 1000);
                            self.pos.gui.show_screen('payment');
                        },
                        cancel: function () {
                            setTimeout(function () {
                                self.validate_global_discount();
                            }, 1000);
                            self.pos.gui.show_screen('payment');
                        }
                    });
                } else {
                    setTimeout(function () {
                        self.validate_global_discount();
                    }, 1000);
                }
            });
        },
        get_promotions_active: function () {
            var can_apply = null;
            var promotions_active = [];
            if (!this.pos.promotions) {
                return {
                    can_apply: can_apply,
                    promotions_active: []
                };
            }
            for (var i = 0; i < this.pos.promotions.length; i++) {
                var promotion = this.pos.promotions[i];
                if (promotion['type'] == '1_discount_total_order' && this.checking_apply_total_order(promotion)) {
                    can_apply = true;
                    promotions_active.push(promotion);
                }
                else if (promotion['type'] == '2_discount_category' && this.checking_can_discount_by_categories(promotion)) {
                    can_apply = true;
                    promotions_active.push(promotion);
                }
                else if (promotion['type'] == '3_discount_by_quantity_of_product' && this.checking_apply_discount_filter_by_quantity_of_product(promotion)) {
                    can_apply = true;
                    promotions_active.push(promotion);
                }
                else if (promotion['type'] == '4_pack_discount') {
                    var promotion_condition_items = this.pos.promotion_discount_condition_by_promotion_id[promotion.id];
                    var check = this.checking_pack_discount_and_pack_free_gift(promotion_condition_items, promotion);
                    if (check) {
                        can_apply = true;
                        promotions_active.push(promotion);
                    }
                }
                else if (promotion['type'] == '5_pack_free_gift') {
                    var promotion_condition_items = this.pos.promotion_gift_condition_by_promotion_id[promotion.id];
                    var check = this.checking_pack_discount_and_pack_free_gift(promotion_condition_items, promotion);
                    if (check) {
                        can_apply = true;
                        promotions_active.push(promotion);
                    }
                }
                else if (promotion['type'] == '6_price_filter_quantity' && this.checking_apply_price_filter_by_quantity_of_product(promotion)) {
                    can_apply = true;
                    promotions_active.push(promotion);
                }
                else if (promotion['type'] == '7_special_category' && this.checking_apply_specical_category(promotion)) {
                    can_apply = true;
                    promotions_active.push(promotion);
                }
                else if (promotion['type'] == '8_discount_lowest_price') {
                    can_apply = true;
                    promotions_active.push(promotion);
                }
                else if (promotion['type'] == '9_multi_buy') {
                    if (this.checking_multi_by(promotion)) {
                        can_apply = true;
                        promotions_active.push(promotion);
                    }
                }
                else if (promotion['type'] == '10_buy_x_get_another_free') {
                    if (this.checking_buy_x_get_another_free(promotion)) {
                        can_apply = true;
                        promotions_active.push(promotion);
                    }
                }
            }
            //new code :PSB
            var $deferred = new $.Deferred();
            var self = this;
            var customer_promotions = []
            var active_promotions = {
                    can_apply: can_apply,
                    promotions_active: []
                };
            var rewards = [];
            self.get_foodbot_promotions().done(function(){
                rewards = self.pos.get_order().rewards;
                promotions_active.forEach(function(prActive){
                    if(prActive.customer_specific){
                        rewards.forEach(function(reward){
                            if (reward && reward.reward_id === prActive.id){
                                customer_promotions.push(prActive);
                            }
                        });
                    }else{
                        customer_promotions.push(prActive);
                    }                    
                })
                }).pipe(function(){
                    active_promotions['promotions_active'] = customer_promotions;
                    $deferred.resolve(active_promotions);
                });
            return $deferred.promise(active_promotions);
        },
        get_foodbot_promotions: function(){
            var  $deferred = new $.Deferred();
            var self = this;
            self.pos.get_order().rewards = [];
            let pos_order = self.pos.get_order();
	    
            let client = pos_order.changed.client;
            let branch_id = self.pos.config.branch_id;
            //console.log('promotions==',client,branch_id,client);
	        if (client && branch_id){
                if (client.barcode.length > 0){
                    let foodbot_url = self.pos.company.foodbot_url;
                    let api_key = self.pos.company.foodbot_api_key;
                    let req_data = {
                        sender_id : client.barcode,
                        restaurant_id : self.pos.company.restaurant_id,
                        branch_id : branch_id
                    }
                    //console.log('calling ajax');
                    $.ajax({
                        type: 'POST',
                        url: foodbot_url+'/pos/getrewards',
                        headers: {api_key:  api_key },
                        data: req_data,
                        dataType: 'json',
                        timeout:10000,
                        success: function (data) {
                           //console.log('data=',data);
                           // console.info(data.responseJSON.response);
                            if (data.response && data.response.rewards.status === 'success'){
                                self.pos.get_order().rewards = data.response.rewards.rewarddetails;
                                $deferred.resolve()
                            }else if(data.response && data.response.rewards.status !== 'success'){
                                console.info('Error in fetching Promotions')
                				console.error(data.response.rewards.status, ' Message: ' + data.response.rewards.message);
                                
                                $deferred.resolve()
                            }else{
            				console.info('Error in fetching Promotions')
            				console.error(data)
            				$deferred.resolve()
            			    }
                        },
            			fail: function(data){
            			   console.error('Error in response',data);
            			   if(data && data.message){
                                         alert("Foodbot error: "+ data.message);
                                       }
            			   $deferred.resolve()
            			},
                        error: function(xmlhttprequest, textstatus, message) {
                            if(textstatus==="timeout") {
                                console.error("got timeout");
                                $deferred.resolve()
                            } else {
                                console.log(textstatus, message);
                                $deferred.resolve()
                            }
                        }
                    });
                }else{
                    $deferred.resolve();
                }
            }else{
                $deferred.resolve();
            }
        return $deferred
        },

    });

});

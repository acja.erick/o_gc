odoo.define('pos_customer_api.pos_screen', function (require) {
"use strict";

    var core = require('web.core');
    var screens = require('point_of_sale.screens');
    var gui = require('point_of_sale.gui');
    var PopupWidget = require('point_of_sale.popups');
    var models = require('point_of_sale.models');
    var qweb = core.qweb;

    var _t = core._t;

    

    screens.NumpadWidget.include({
        applyAccessRights: function() {
            var has_price_control_rights = this.pos.config.restrict_price_control;
            if(has_price_control_rights && this.pos.get_cashier().role == 'manager'){
                this.$el.find('.mode-button[data-mode="price"]')
                    .toggleClass('disabled-mode', !has_price_control_rights)
                    .prop('disabled', !has_price_control_rights);
                this.$el.find('.mode-button[data-mode="discount"]')
                    .toggleClass('disabled-mode', !has_price_control_rights)
                    .prop('disabled', !has_price_control_rights);
            }else{
                this.$el.find('.mode-button[data-mode="price"]')
                .toggleClass('disabled-mode', true)
                .prop('disabled', true);
                this.$el.find('.mode-button[data-mode="discount"]')
                .toggleClass('disabled-mode', true)
                .prop('disabled', true);
            }
        },

    });

    screens.ActionpadWidget.include({
        renderElement: function() {
            var self = this;
            this._super();
            
//            this.$('.set-customer').unbind('click');
            
            this.$('.pay').click(function(){
                var order = self.pos.get_order();
                var has_valid_product_lot = _.every(order.orderlines.models, function(line){
                    return line.has_valid_product_lot();
                });
                var zero_product_qty = _.every(order.orderlines.models, function(line){
                    return line.quantity;
                });
                if(!has_valid_product_lot){
                    self.gui.show_popup('confirm',{
                        'title': _t('Empty Serial/Lot Number'),
                        'body':  _t('One or more product(s) required serial/lot number.'),
                        confirm: function(){
                            self.gui.show_screen('payment');
                        },
                    });
                }else if(!zero_product_qty){
                    self.gui.show_screen('products');
                    self.gui.show_popup('error',{
                        'title': _t('Zero Quantity Line'),
                        'body':  _t('Product line with zero quantity is not allowed.'),
                    });
                }
                else{
                    self.gui.show_screen('payment');
                }
            });

        },
    });

    screens.PaymentScreenWidget.include({
        show: function(){
            this._super();
            this.update_foodbot_wallet();
            var lines = this.pos.get_order().get_paymentlines();
            for ( var i = 0; i < lines.length; i++ ) {
                
                if(lines[i].payment_method.name.indexOf('FoodBot') > -1){
                    this.pos.get_order().store_value_used += lines[i].get_amount();
                }
            }
            this.pos.get_order().unique_id = '';
            this.pos.get_order().qrCode = '';
            if(this.pos.config.integrate_foodbot && !this.pos.get_order().get_client()){
                var hashids = new Hashids("foodbotodoointegration", 8, "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890");
                var uuid = this.pos.get_order().uid;
                uuid = parseInt(uuid.replace(/[-]/g, '').trim());
                var branch_id = this.pos.config.branch_id;
                this.pos.get_order().set_unique_id(branch_id +''+hashids.encode(uuid));
                var qr = new QRious({
                  value: this.pos.company.qr_code_url+branch_id +''+hashids.encode(uuid)
                });
                qr.size = 200;
                this.pos.get_order().qrCode = qr.toDataURL();

            }
            
        },
        hide: function(){
            this._super();
            console.log('this.pos.get_order()',this.pos.get_order())
            if(this.pos.get_order()){
                this.pos.get_order().store_value = 0;
                this.pos.get_order().store_value_used = 0;
            }
        },
        click_numpad: function(button) {
            var paymentlines = this.pos.get_order().get_paymentlines();
            var open_paymentline = false;

            for (var i = 0; i < paymentlines.length; i++) {
                if (! paymentlines[i].paid) {
                open_paymentline = true;
                }
            }

            if (! open_paymentline) {
                    this.pos.get_order().add_paymentline( this.pos.payment_methods[0]);
                    this.render_paymentlines();
                }

                let selectedLine = this.pos.get_order().selected_paymentline;
                if (selectedLine && selectedLine.payment_method.name.indexOf('Cryptocurrency') > -1){
                    alert('You can not modify the amount in this line.')
                    return;
                }
                if (selectedLine && selectedLine.payment_method.name.indexOf('FoodBot') < 0){
                    this.payment_input(button.data('action'));
                }
        },
        click_delete_paymentline: function(cid){
            var lines = this.pos.get_order().get_paymentlines();
            for ( var i = 0; i < lines.length; i++ ) {
                if (lines[i].cid === cid) {
                    if(lines[i].payment_method.name.indexOf('FoodBot') > -1){
                        this.pos.get_order().store_value_used -= lines[i].get_amount();
                        this.pos.get_order().set_store_value(this.pos.get_order().store_value_used);
                    }
                    if(lines[i].payment_method.name.indexOf('Cryptocurrency') > -1){
                        alert('You can not delete this line');
                        return;
                    }
                    this.pos.get_order().remove_paymentline(lines[i]);
                    this.reset_input();
                    this.render_paymentlines();
                    return;
                }
            }
        },
        click_paymentmethods: function(id) {
            var payment_method = this.pos.payment_methods_by_id[id];
            var order = this.pos.get_order();

            let foodBot = false;
            let is_fbwLine = false;

            if (order.electronic_payment_in_progress()) {
                this.gui.show_popup('error',{
                    'title': _t('Error'),
                    'body':  _t('There is already an electronic payment in progress.'),
                });
            } else {

                if (payment_method.name.indexOf('FoodBot') > -1){
                        foodBot = true;
                }
                else if (payment_method.name.indexOf('Cryptocurrency') > -1){
                    let dueAmount = order.get_due();
                    return this.pos.gui.show_popup('popupInitiateCrypto', {
                        title: 'Initiate Crypto Payment',
                        body: 'Please click on Initiate Button to make the payment of '+ dueAmount + ' by cryptocurrency',
                        cashregister: payment_method,
                        payment_obj : this,
                        dueAmount: dueAmount
                    });
                }

                let selLines = order.get_paymentlines();

                for ( var j=0; j < selLines.length; j++){
                    if (selLines[j].payment_method.name.indexOf('FoodBot') > -1){
                            is_fbwLine = true;
                    }
                }
                
                let store_value = this.pos.get_order().store_value;
                if ((foodBot && store_value > 0 && !is_fbwLine) || (!foodBot)){
                    order.add_paymentline(payment_method);
                    this.reset_input();

                    this.payment_interface = payment_method.payment_terminal;
                    if (this.payment_interface) {
                        order.selected_paymentline.set_payment_status('pending');
                    }

                    this.render_paymentlines();
                }
            }
        },
        // click_paymentmethods: function(id) {
        //     let foodBot = false;
        //     let is_fbwLine = false;
        //     var cashregister = null;
        //     for ( var i = 0; i < this.pos.cashregisters.length; i++ ) {
        //         if ( this.pos.cashregisters[i].journal_id[0] === id ){
        //             cashregister = this.pos.cashregisters[i];
        //             if (cashregister.journal_id[1].indexOf('FoodBot') > -1){
        //                 foodBot = true;
        //             }
        //             else if (cashregister.journal_id[1].indexOf('Cryptocurrency') > -1){
        //                 let dueAmount = this.pos.get_order().get_due();
        //                 return this.pos.gui.show_popup('popupInitiateCrypto', {
        //                     title: 'Initiate Crypto Payment',
        //                     body: 'Please click on Initiate Button to make the payment of '+ dueAmount + cashregister.currency_id[1] +' by cryptocurrency',
        //                     cashregister: cashregister,
        //                     payment_obj : this,
        //                     dueAmount: dueAmount
        //                 });
        //             }
        //         }
        //     }
            
        //     let selLines = this.pos.get_order().get_paymentlines();
        //     for ( var j=0; j < selLines.length; j++){
        //         if (selLines[j].name.indexOf('FoodBot') > -1){
        //                 is_fbwLine = true;
        //         }
        //     }
            
        //     let store_value = this.pos.get_order().store_value;
        //     if ((foodBot && store_value > 0 && !is_fbwLine) || (!foodBot)){
        //         this.pos.get_order().add_paymentline( cashregister );
        //         this.reset_input();
        //         this.render_paymentlines();
        //     }
        // },
        update_foodbot_wallet: function(){
            var self = this;
            let pos_order = self.pos.get_order();
            let client = pos_order.changed.client;
            let branch_id = self.pos.config.branch_id;
            this.pos.get_order().store_value = 0;
    	    if(!$('.paymentmethods #fdw_storeValue')[0]){
                  console.info('Foodbot Wallet not Exist');
    	      return;
    	    }
            if (client && branch_id){
                if (client.barcode.length > 0){
                    let foodbot_url = self.pos.company.foodbot_url;
                    let api_key = self.pos.company.foodbot_api_key;
                    let req_data = {
                        sender_id : client.barcode,
                        restaurant_id : self.pos.company.restaurant_id,
                        branch_id : branch_id
                    }
                    $.ajax({
                        type: 'POST',
                        url: foodbot_url+'/pos/getrewards',
                        headers: {api_key:  api_key },
                        data: req_data,
                        dataType: 'json',
                        success: function (data) {
                            //console.log('data===',data);
                            //console.info(data.responseJSON);
			                if (data.response && data.response.storevalue.status === 'success'){
                                self.pos.get_order().store_value = data.response.storevalue.store_value;
                                $('.paymentmethods #fdw_storeValue')[0].innerText = '(' + self.pos.get_order().store_value + ')'

                            }else{
                                console.info('Error in fetching store value', data.status);
                                if(data && data.response){
                                  console.error("Foodbot error: "+ data.response.storevalue.message);
                           	}
                            }
                        },
            			fail: function(data){
            			   console.error('Error in fetching storevalue');
            			   if(data && data.message){
            				alert("Foodbot error: "+ data.message);
            			   }
            			},
                        error: function(xmlhttprequest, textstatus, message) {
                            if(textstatus==="timeout") {
                                console.error("got timeout");
                            } else {
                                console.log(textstatus, message);
                            }
                        }
                    });
                }
            }
        },
        renderElement: function(){
            this._super();
    //        this.$('.js_set_customer').unbind('click');
        },

    });
    // To remove promotions if customer changed
    screens.ClientListScreenWidget.include({
        save_changes: function(){
            var order = this.pos.get_order();
            if( this.has_client_changed() ){
                order.remove_all_promotion_line();
                var default_fiscal_position_id = _.findWhere(this.pos.fiscal_positions, {'id': this.pos.config.default_fiscal_position_id[0]});
                if ( this.new_client ) {
                    var client_fiscal_position_id;
                    if (this.new_client.property_account_position_id ){
                        client_fiscal_position_id = _.findWhere(this.pos.fiscal_positions, {'id': this.new_client.property_account_position_id[0]});
                    }
                    order.fiscal_position = client_fiscal_position_id || default_fiscal_position_id;
                    order.set_pricelist(_.findWhere(this.pos.pricelists, {'id': this.new_client.property_product_pricelist[0]}) || this.pos.default_pricelist);
                } else {
                    order.fiscal_position = default_fiscal_position_id;
                    order.set_pricelist(this.pos.default_pricelist);
                }

                order.set_client(this.new_client);
            }
        }
    });
    screens.OrderWidget.include({
        active_promotion: function (buttons, selected_order) {
            if (selected_order.orderlines && selected_order.orderlines.length > 0 && this.pos.config.promotion_ids.length > 0) {
                var lines = selected_order.orderlines.models;
                var promotion_amount = 0;
                for (var i = 0; i < lines.length; i++) {
                    var line = lines[i]
                    if (line.promotion) {
                        promotion_amount += line.get_price_without_tax()
                    }
                }
                selected_order.get_promotions_active().done(function(promotion_datas){
                    var can_apply = promotion_datas['can_apply'];
                    if (buttons && buttons.button_promotion) {
                        buttons.button_promotion.highlight(can_apply);
                    }
                    var promotions_active = promotion_datas['promotions_active'];
                    if (promotions_active.length) {
                        var promotion_recommend_customer_html = qweb.render('promotion_recommend_customer', {
                            promotions: promotions_active
                        });
                        $('.promotion_recommend_customer').html(promotion_recommend_customer_html);
                    } else {
                        $('.promotion_recommend_customer').html("");
                    }
                });
            }
        },
    });
    
    var popupChooseProducts = PopupWidget.extend({
        template: 'popupFreeProducts',
        show: function (options) {
            var self = this;
            self._super(options);
            var orderRef = options.orderRef;
            var promoProduct= options.promoProduct;
            var offerProducts = options.products;
            var productList = options.productList;
            var promotion = options.promotion;
            var discount_items = [];
            self.$el.find('.body').append(qweb.render('select_promotion_product', {
                productList: productList,
                widget: self
            }));
            
            self.$('.cancel').click(function () {
                self.pos.gui.close_popup();
            });
            let selectedID;
            self.$('.addFreeProducts').click(function () {
                self.pos.gui.close_popup();
                $.each(self.$el.find('select'), function(){
                    selectedID = parseInt($(this).children("option:selected").val());
                    $.each(offerProducts, function(idx,product){
                        if(selectedID === product.id){
                            discount_items.push(product);
                        }
                    });
                });
            
            
                
                var i = 0;
                while (i < discount_items.length) {
                    var discount_item = discount_items[i];
                    var discount = 0;
                    var total_qty = 0;
                    var lines = orderRef.orderlines.models;
                    for (var j = 0; j < lines.length; j++) {
                        if (lines[j].promotion) {
                            continue;
                        }
                        var line = lines[j];
                        if (line.product.id == discount_item.product_id[0]) {
                            total_qty += line.quantity
                            if (orderRef.pos.config.iface_tax_included === 'total') {
                                discount += line.get_price_with_tax() * (1 - line.get_discount() / 100)
                            } else {
                                discount += line.get_unit_price() * line.get_quantity() * (1 - line.get_discount() / 100)
                            }
                        }
                    }
                    if (discount_item.type == 'one') {
                        discount = discount / total_qty;
                    }
                    if (promoProduct && discount != 0) {
                        orderRef.add_promotion(promoProduct, -discount / 100 * discount_item.discount, 1, {
                            promotion: true,
                            promotion_discount: true,
                            promotion_reason: 'Applied ' + promotion['name'] + ', discount ' + discount_item.product_id[1],
                            applied_promotion_id: promotion.id,
                        })
                    }
                    i++;
                }
            });
        },
    });
    gui.define_popup({name: 'freeProducts', widget: popupChooseProducts});

    var popupInitiateCrypto = PopupWidget.extend({
        template: 'popupCryptoCurrency',
        uiBlock: function(msg){
            $.blockUI({ css: { 
                border: 'none', 
                padding: '15px', 
                background: '#000', 
                '-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px', 
                opacity: .5, 
                color: '#fff',
                top:'55%',
                },
                message: msg
            });

        },
        hookThread: function(count, cashregister, payment_obj, maxWait){
            var self = this;
            var pos_order = self.pos.get_order();
            var invoice_ref = pos_order.get_name();
            var dueAmount = pos_order.get_due();

            function retryRequest(){
                self.uiBlock('waiting for payment to credit...');
                $.ajax({
                    type: 'GET',
                    url: location.origin+ '/pos/webhook_response',
                    data: {'invoice_ref':invoice_ref},
                    dataType: 'json',
                    success: function(data){
                        if(data.status === 'no'){
                            if(count < maxWait){
                                console.log('retrying to check payment');
                                setTimeout(function(){
                                    $.unblockUI();
                                    count +=1;
                                    retryRequest();
                                },5000)
                            }else{
                                $.unblockUI();
                                self.$('label#cryptoErrorMessage')[0].innerText = "Max retries reached no response recieved from crypto.";
                                self.$('div#initiateCryptoPayment').addClass('oe_hidden');
                                self.$('div#recheckCryptoPayment').removeClass('oe_hidden');
                            }
                            
                        }else if(data.status === 'success'){
                            $.unblockUI();
                            pos_order.add_paymentline( cashregister );
                            if(pos_order.selected_paymentline){
                                pos_order.selected_paymentline.set_amount( Math.max(dueAmount,0) )
                            }
                            payment_obj.reset_input();
                            payment_obj.render_paymentlines();
                            $('.paymentline.selected > td.col-tendered').removeClass('edit');
                            self.pos.gui.close_popup();
                            return
                        }else if(data.status === 'fail'){
                            $.unblockUI();
                            self.$('label#cryptoErrorMessage')[0].innerText = data.message;
                            self.$('div#initiateCryptoPayment').addClass('oe_hidden');
                            self.$('div#recheckCryptoPayment').removeClass('oe_hidden');
                        }
                    },
                    fail: function(data){
                        $.unblockUI();
                        if(data && data.message){
                            self.$('label#cryptoErrorMessage')[0].innerText = data.message;
                            self.$('div#initiateCryptoPayment').addClass('oe_hidden');
                            self.$('div#recheckCryptoPayment').removeClass('oe_hidden');
                        }
                    },
                    error: function(xmlhttprequest, textstatus, message) {
                        $.unblockUI();
                        self.$('label#cryptoErrorMessage')[0].innerText = textstatus;
                    }
                });
            }
            retryRequest();
        },
        show: function (options) {
            var self = this;
            self._super(options);
            var cashregister = options.cashregister;
            var payment_obj = options.payment_obj;
            var dueAmount = options.dueAmount;
            let pos_order = self.pos.get_order();
            let client = pos_order.changed.client;
            
            let invoice_ref = pos_order.get_name();
            let branch_id = self.pos.config.branch_id;
            let crypto_url = self.pos.company.foodbot_url;;
            let api_key = self.pos.company.foodbot_api_key;
            let maxWait = self.pos.company.crypto_expiration_time;
            let sender_id = "";
            if (client){
                sender_id = client.barcode;
            }

            console.log('order name',pos_order.get_name());
            
            self.$('div#initiateCryptoPayment').click(function () {
                self.$('label#cryptoSuccessMessage')[0].innerText= '';
                self.$('label#cryptoErrorMessage')[0].innerText = '';
                if (branch_id){
                    var requestInterval;      
                    let req_data={
                        "restaurant_id":self.pos.company.restaurant_id,    
                        "sender_id": sender_id,
                        "branch_id":branch_id,
                        "invoice_id":invoice_ref,
                        "amount":dueAmount,
                        "currency":cashregister.currency_id[1]
                    }
                    self.uiBlock('Initiating Payment...');
                    $.ajax({
                        type: 'POST',
                        url: crypto_url+'/pos/crypto/payment',
                        headers: {api_key: api_key},
                        data: req_data,
                        dataType: 'json',
                        complete: function (data) {
                            console.log('data===',data);
                            if (data.responseJSON && data.responseJSON.status.toLowerCase() === "success"){
                                let msg="Request "+ data.responseJSON.message;
                                self.$('label#cryptoSuccessMessage')[0].innerText = msg;
                                $.unblockUI();
                                self.uiBlock('Request '+data.responseJSON.status);
                                setTimeout(function(){
                                    $.unblockUI();
                                    self.$('label#cryptoSuccessMessage')[0].innerText= '';
                                    self.$('label#cryptoErrorMessage')[0].innerText = '';
                                    self.hookThread(0, cashregister, payment_obj, maxWait);
                                },10000);

                            }else{
                                $.unblockUI();
                                if(data.responseJSON){
                                    console.info('Error in initiating cryptocurrency', data.responseJSON.message);
                                    self.$('label#cryptoErrorMessage')[0].innerText = data.responseJSON.message;
                                }else{
                                    let msg = 'Unknown Error! Error in initiating cryptocurrency, error_code='+data.status
                                    console.info(msg);
                                    self.$('label#cryptoErrorMessage')[0].innerText = msg
                                }
                             
                            }
                        },
                        // error: function(data){
                        //     if(data.status === 404){
                        //         console.error('Error 404: in initiating cryptocurrency');
                        //     }else{
                        //         console.error('Error '+data.status+ ': in initiating cryptocurrency');
                        //     }
                        // }
                    });
                    
                }else{
                    self.$('label#cryptoErrorMessage')[0].innerText = "branch_id is missing"
                }
            });
            self.$('div#recheckCryptoPayment').click(function(){
                self.$('label#cryptoSuccessMessage')[0].innerText= '';
                self.$('label#cryptoErrorMessage')[0].innerText = '';
                self.uiBlock('Checking payment status...');
                let req_data={
                    "restaurant_id":self.pos.company.restaurant_id,    
                    "branch_id":branch_id,
                    "invoice_id":invoice_ref,
                }
                $.ajax({
                    type: 'POST',
                    url: crypto_url+'/pos/crypto/payment-status',
                    headers: {api_key: api_key},
                    data: req_data,
                    dataType: 'json',
                    complete: function (data) {
                        //console.info(data.responseJSON);
                        if (data.responseJSON && data.responseJSON.status.toLowerCase() === "success"){
                            if(data.responseJSON.message.toLowerCase() === 'payment_received'){
                                let msg="Request "+ data.responseJSON.message;
                                self.$('label#cryptoSuccessMessage')[0].innerText = msg;
                                $.unblockUI();
                                self.pos.get_order().add_paymentline( cashregister );
                                if(pos_order.selected_paymentline){
                                    pos_order.selected_paymentline.set_amount( Math.max(dueAmount,0) );
                                }
                                payment_obj.reset_input();
                                payment_obj.render_paymentlines();
                                $('.paymentline.selected > td.col-tendered').removeClass('edit');
                                self.pos.gui.close_popup();
                                return
                            }else if (data.responseJSON.message.toLowerCase() === "waiting_payment"){
                                $.unblockUI();
                                self.$('label#cryptoSuccessMessage')[0].innerText = 'Payment is in progress please wait and re-check after sometime by clicking on Re-check button';
                            }

                        }
                        else{
                            $.unblockUI();
                            if(data.responseJSON){
                                console.info(data.responseJSON.message);
                                self.$('label#cryptoErrorMessage')[0].innerText = data.responseJSON.message;
                            }else{
                                let msg = 'Unknown Error! While checkking status, error_code='+data.status
                                console.info(msg);
                                self.$('label#cryptoErrorMessage')[0].innerText = msg
                            }
                         
                        }
                    },
                });

            })
        }
    });
    gui.define_popup({name: 'popupInitiateCrypto', widget: popupInitiateCrypto});


    //Points
    var button_points = screens.ActionButtonWidget.extend({// points button
        template: 'button_points',
        button_click: function () {
            var order = this.pos.get('selectedOrder');
            var self = this;
            self.gui.show_popup('number',{
              'title': _t('Enter Amount for Points'),
              'confirm': function(value) {
                    if(self.pos.company.is_sale_points && value > 0){
                        var point_product = self.pos.company.point_product_id;
                        for (var i = 0; i < order.orderlines.models.length; i++) {
                            var oLine = order.orderlines.models[i];
                            if (oLine.product.id === point_product[0]) {
                                order.remove_orderline(oLine);
                            }
                        }
                        var product = this.pos.db.get_product_by_id(point_product[0]);
                        var line = new models.Orderline({}, {pos: self.pos, order: order, product: product});
                        line.price_manually_set = true; // no need pricelist change, price of promotion change the same, i blocked
                        line.set_quantity(value);
                        line.set_unit_price(1);
                        order.orderlines.add(line);
                        self.trigger('change', self);
                    }
                }
            });
            
        }
    });
    screens.define_action_button({
        'name': 'button_points',
        'widget': button_points,
        'condition': function () {
            return this.pos.company.is_sale_points;
        }
        
    });

});

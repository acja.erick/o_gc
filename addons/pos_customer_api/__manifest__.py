# -*- coding: utf-8 -*-

{
    "name" : "Customer New Fields For API",
    "version" : "1.0",
    "depends" :["base","point_of_sale", "pos_promotion"],
    "category" :  'Point of Sale',
    "description": """
Customer API Fields
""",
    "init_xml" : [],
    "demo_xml" : [],
    "data": [
            "security/ir.model.access.csv",
            "views/pos_template.xml",
            "views/res_partner_view.xml",
            "views/res_customer_view.xml",
            "views/pos_config_view.xml",
            "data/pos_order_cron_job_data.xml",
            "views/pos_promotion_view.xml",
            ],
    "qweb": ['static/src/xml/*.xml'],
    "active": False,
    "installable": True
}

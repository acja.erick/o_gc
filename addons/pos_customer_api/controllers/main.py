import json
import logging
import base64
import requests
import odoo
from odoo import http, _
from odoo.http import request
from odoo.exceptions import AccessError, UserError
import werkzeug
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import json


class PosSessionController(http.Controller):
    @http.route('/pos/addcustomer', type='json', auth="none")
    def customer_add(self, **kw):
        """
        error_codes:
            101: Invalid name
            102: Invalid phone number
            103: No Instance found for Branch id
            104: Invalid sender id
            105: Invalid API Key
            107: Invalid branch id. Must be integer
            108: Invalid restaurant id. Must be integer
            109: Invalid restaurant_id. No active setting found for this id.
            110: Internal Server Error
            
            Sample responses:
            {
                "result":{
                "message": "Customer already exist.",
                "status": "success",
                "id": 53
                },
                "jsonrpc": "2.0",
                "id": null
            }


            ODOO ERROR IF any EXCEPTION:
            {
                "error":{
                    "data":{"message": "'int' object has no attribute 'strip'", "name": "builtins.AttributeError", "exception_type": "internal_error",…},
                "message": "Odoo Server Error",
                "code": 200
                },
                "jsonrpc": "2.0",
                "id": null
            }
        """

        headers = request.httprequest.headers
        print('kw= %s, headers=%s'%(kw, headers.get('api_key')))
        name = kw.get('name')
        phone = kw.get('phone')
        branch_id = kw.get('branch_id')
        senderid = kw.get('senderid')
        img_id = kw.get('profile_pic_url')
        restaurant_id = kw.get('restaurant_id')
        company = False

        # if not isinstance(branch_id, int):
        #   return {
        #       'status': 'Fail',
        #       'message': "Invalid branch id. Must be integer",
        #       'error_code': '107'
        #   }
        if not isinstance(restaurant_id, int):
            return {
                'status': 'Fail',
                'message': "Invalid restaurant id. Must be integer",
                'error_code': '108'
            }
        if restaurant_id :
            #pos_config = request.env['pos.config'].sudo().search([('branch_id', '=', branch_id)])
            company = request.env['res.company'].sudo().search([('restaurant_id', '=', restaurant_id)])
            print('ompany=%s, apiKey=%s'%(company, company.api_key))
            
            if company:
                if headers.get('api_key') != company.api_key:
                    return {
                        'status' : 'Fail',
                        'message' : 'Invalid API Key.',
                        'error_code' : '105'
                    }
                if company.restaurant_id != restaurant_id:
                    return {
                        'status' : 'Fail',
                        'message' : 'Invalid restaurant_id. No active setting found for this id.',
                        'error_code' : '109'
                    }

            else:
                return {
                    'status': 'Fail',
                    'message': "No Instance found for restaurant id",
                    'error_code': '103'
                }

        else:
            return {
                'status': 'Fail',
                'message': "Invalid restaurant id. Must be integer.",
                'error_code': '107'
            }
        err = False
        
        if not name or not isinstance(name, str) or len(name.strip()) < 3:
            err = {
                'status': 'Fail',
                'message': "Invalid name. Name must be string and length must be greater than 3",
                'error_code': '101'
            }
        elif not phone or (isinstance(phone, str) and len(phone) < 5):
            err = {
                'status': 'Fail',
                'message': "Invalid phone number. Phone must be atleast 5 character long",
                'error_code': '102'
            }
        
        elif not senderid:
            err = {
                'status': 'Fail',
                'message': "Invalid sender id.",
                'error_code': '104'
            }
        else:

            cust_model = request.env['res.partner']
            customer = cust_model.sudo().search([('barcode', '=', str(senderid))])
            print('customer: %s, len=%s'%(customer, len(customer)))
            img_status = False
            try:
                if not customer:
                    if img_id and len(img_id.strip()) > 11 :
                        img_status, img = self.get_as_base64(kw.get('profile_pic_url', ''))
                    
                    cust_vals = {
                     'name': _(name),
                     'phone': str(phone), 
                     'mobile' : phone, 
                     'company_id': company.id,
                     'customer_rank': 1, 
                     'barcode': senderid,
                     'image_1920': img if img_status else '',
                     'restaurant_id': kw.get('restaurant_id', ''),
                     'is_company': False}
                    context = dict(request.env.context)
                    context['tracking_disable'] = True
                    context['force_company'] = company.id
                    request.env.context = context
                    print('custvals=%s, context=%s'%(cust_vals, request.env.context))

                    customer = cust_model.sudo().create(cust_vals)
                    print('customer=%s'%(customer))
                    return {
                        'status' : 'success',
                        'message' : 'Customer created successfully.',
                        'id' : customer.id
                    }

                else:
                    return {
                        'status' : 'success',
                        'message' : 'Customer already exist.',
                        'id' : customer.id
                    }
            except Exception as e:
                return {
                    'status': 'Fail',
                    'message': "Internal Server Error.",
                    'error_code': '110'
                }
        if err:
            return err

    def get_as_base64(self, url):
        try:
            img = requests.get(url, timeout=5)
            return True, base64.b64encode(img.content)
        except Exception as e:
            error = "Image conversion error: %s" % (str(e) or repr(e))
            return False, error

    @http.route('/pos/promotionlist', type='http', auth='none', methods=['GET'], cors="*")
    def promotion_list(self, **kw):
        """
        error_codes:
            103: Fetching promotion list success
            105: Invalid API Key
            
        Sample responses:
        {
            "result":{
            "message": "Promotion List Success.",
            "status": "success",
            "promotions": [{'id':1, 'name':'Promo1'},{'id':2, 'name':'Promo2'}],
            "error_code": 103
            },
            "jsonrpc": "2.0",
            "id": null
        }


        ODOO ERROR IF any EXCEPTION:
        {
            "error":{
                "data":{"message": "'int' object has no attribute 'strip'", "name": "builtins.AttributeError", "exception_type": "internal_error",…},
            "message": "Odoo Server Error",
            "code": 200
            },
            "jsonrpc": "2.0",
            "id": null
        }
        """

        headers = request.httprequest.headers
        company = request.env['res.company'].sudo().search([], order='id asc', limit=1)

        if headers.get('api_key') != company.api_key:
            return json.dumps({
                'status' : 'Fail',
                'message' : 'Invalid API Key.',
                'error_code' : '105'
            })
        else:
            promotion_env = request.env['pos.promotion']
            date = datetime.now()
            date = date.strftime(DEFAULT_SERVER_DATETIME_FORMAT)
            promotions = promotion_env.sudo().search([('end_date', '>=', date),('customer_specific','=',True)])
            promotion_list = []
            for promotion in promotions:
                temp = {
                    'name': promotion.name,
                    'id': promotion.id,
                    'type': promotion.type
                }
                promotion_list.append(temp)
            return json.dumps({
                'status' : 'success',
                'message' : 'Fetching promotion list success.',
                'error_code' : '103',
                'promotions' : promotion_list
            })

    @http.route('/pos/crypto_webhook', type='json', auth='none', methods=['POST'], cors="*")
    def web_hook_crypto(self, **post):
        print('****',post)
        headers = request.httprequest.headers
        company = request.env['res.company'].sudo().search([], order='id asc', limit=1)
        print('--',headers)
        if headers.get('api_key') != company.api_key:
            return {
                'status' : 'fail',
                'message' : 'Invalid API Key.',
                'error_code' : '105'
            }
        hook_model = request.env['foodbot.crypto.hook']
        hooks = hook_model.sudo().search([('invoice_ref','=',post['invoice_id'])])
        if hooks:
            hooks[0].sudo().write({
                'status': post['status'],
                'message': post['message']
                })
            return {
                'status' : 'success',
                'message' : 'Record already exist, update staus and message',
                'error_code' : '101'
            }
        else:
            hooks.sudo().create({
                'invoice_ref': post['invoice_id'],
                'status': post['status'],
                'message': post['message']
                })
            return {
                'status' : 'success',
                'message' : 'Record created.',
                'error_code' : '100'
            }

    @http.route('/pos/webhook_response', type='http', auth='none', methods=['GET'], cors="*")
    def web_hook_response(self, **kw):
        invoice_ref = kw.get('invoice_ref')
        hook_model = request.env['foodbot.crypto.hook']
        hooks = hook_model.sudo().search([('invoice_ref','=',invoice_ref)])
        if hooks:
            return json.dumps({
                'invoice_id': hooks[0].invoice_ref,
                'status' : hooks[0].status,
                'message': hooks[0].message
            })
        else:
            return json.dumps({
                'status': 'no'
            })



                


# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError


class pos_promotion(models.Model):
    _inherit = "pos.promotion"

    customer_specific = fields.Boolean('Customer')
    multi_days_ids = fields.Many2many('pos.promotion.days','pos_promotion_days_rel', 'promotion_day_id', 'day', string='Days')
    or_case = fields.Boolean('Is OR')
    start_time = fields.Float('Start Time')
    end_time = fields.Float('End Time')
    free_products = fields.Boolean('Free Products')
    free_product_qty = fields.Integer('Minimum Quantity')


    @api.constrains('start_time', 'end_time')
    def _check_start_end_time(self):
        print('-',self.start_time, type(self.start_time), self.end_time)
        if self.start_time < 0 or self.start_time > 23.99:
            raise ValidationError(_("Invalid start time"))
        if self.end_time < 0 or  self.end_time > 23.99:
            raise ValidationError(_("Invalid end time"))
        if self.start_time > self.end_time:
            raise ValidationError(_("Start time cannot be greater then end time"))


class pos_promotion_days(models.Model):
    """To store multiple days"""
    _name = "pos.promotion.days"
    # name = fields.Selection([
    #     ('monday','Monday'),
    #     ('tuesday', 'Tuesday'),
    #     ('wednesday', 'Wednesday'),
    #     ('thursday', 'Thursday'), 
    #     ('friday', 'Friday'),
    #     ('saturday', 'Saturday'),
    #     ('sunday', 'Sunday')], 'Days')
    name = fields.Char('Days')
    day_number = fields.Integer('Number')
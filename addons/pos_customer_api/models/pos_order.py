#!/usr/bin/python
# -*- coding: utf-8 -*-

import requests
import json
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo import fields, api, models, _


class PosConfig(models.Model):

    _inherit = 'pos.config'

    location_id = fields.Char('Location Key')

    # branch_id = fields.Integer('Branch ID', readonly=True)
    # rename label of branch id to location id and change data type of branch_id of int to char

    branch_id = fields.Char('Location ID')
    integrate_foodbot = fields.Boolean('Integrate to Foodbot')

    @api.model
    def create(self, values):
        pos_config = super(PosConfig, self).create(values)
        if 'location_id' in values and values['location_id']:
            self.enable_pos_location()

        return pos_config

    def write(self, values):
        pos_config = super(PosConfig, self).write(values)

        if 'location_id' in values and values['location_id']:
            self.enable_pos_location()

        return pos_config

    def enable_pos_location(self):

        end_point = '/pos/pos-settings'

        if self.company_id.integrate_foodbot:
            url = self.company_id.foodbot_url + end_point
            restaurant_id = self.company_id.restaurant_id
            location_id = self.location_id
            headers = {'api_key': self.company_id.foodbot_api_key,
                       'Content-Type': 'application/json'}
            params = {
                'restaurant_id': restaurant_id,
                'location_key': self.location_id,
                'pos_id': self.id,
                'pos_name': self.name,
                }
            r = requests.post(url, data=json.dumps(params),
                              headers=headers)
            print ('r=', r)
            if r.status_code == 200:
                response = r.json()

                if response.get('error_code', 0) == 309:
                    branch_id = response.get('branch_id', 0)

                    self.write({'branch_id': str(branch_id)})
                else:
                    self.write({'branch_id': ''})
                    print('Error in enabling pos location')
                    print('Error Code: %s' % response.get('error_code', 0))
                    print('Error Message: %s' % response.get('message', ''))
            else:
                print('something went wrong during location key generation')


class PosOrder(models.Model):

    _inherit = 'pos.order'

    store_value_used = fields.Float(string = 'Store Value Used')
    unique_id = fields.Char(string = 'Unique Id')

    @api.model
    def create(self, values):
        pos_order = super(PosOrder, self).create(values)

        foodbot_record = {'order_id': pos_order.id,
                          'user_id': pos_order.user_id.id,
                          'invoice_ticketid': values['pos_reference'],
                          }

        if 'partner_id' in values and values['partner_id']:
            foodbot_record['customer_id'] = values['partner_id']
        self.env['foodbot.invoice.data'].sudo().create(foodbot_record)

        return pos_order

    @api.model
    def create_from_ui(self, orders, draft=False):
        order_ids = super(PosOrder, self).create_from_ui(orders, draft)
        if order_ids:
            for order_id in order_ids:
                for order_rec in orders:
                    order = order_rec['data']
                    order_by_name = self.search([('pos_reference','=',order['name'])])
                    if order_by_name.id == order_id.get('id', False):
                        if 'promotion_ids' in order.keys() and order['promotion_ids']:
                            tmp = order['promotion_ids'][0]
                            rec_write = order_by_name.write({'promotion_ids': [tuple(tmp)]})
                        if 'unique_id' in order.keys():
                            order_by_name.write({'unique_id': order['unique_id']})

            self.env['foodbot.invoice.data'].upload_data_to_api(order_ids)
        return order_ids


class FoodBotInvoiceData(models.Model):

    _name = 'foodbot.invoice.data'
    _order = 'id desc'

    customer_id = fields.Many2one('res.partner', 'Customer Name')
    order_id = fields.Many2one('pos.order', 'POS Order')
    invoice_ticketid = fields.Char('Invoice Ticket')
    user_id = fields.Many2one('Salesman')
    status = fields.Selection([('pending', 'Pending'), ('fail', 'Failed'
                              ), ('success', 'Success')], 'Status',
                              default='pending')
    response_code = fields.Integer('Response Code')
    res_msg = fields.Char('Response Message')
    store_value_used = fields.Float('Store Value Used')
    rewarddetails = fields.Char('Reward Details')

    def format_date(self, record):
        """Required date format is 'DD:MM:YYYY HH:MM' """

        if record.order_id.date_order:
            o_date = record.order_id.date_order
            # o_date = datetime.strptime(o_date,
            #         DEFAULT_SERVER_DATETIME_FORMAT)
            return o_date.strftime('%d:%m:%Y %H:%M')
        else:
            o_date = datetime.strptime(record.create_date,
                    DEFAULT_SERVER_DATETIME_FORMAT)
            return o_date.strftime('%d:%m:%Y %H:%M')
    
    
    def fail_order_email(self,res):
        order_ref = self.order_id.pos_reference
        print("Sending email for fail order: %s"%(order_ref))
        config = self.order_id.session_id.config_id
        company = config.company_id
        sender = company.fail_order_senders
        recipients = company.fail_order_recipients
        template_obj = self.env['mail.mail']
        template_data = {
                        'subject': 'POS Invoice Update Failed for order: ' + order_ref,
                        'body_html': json.dumps(res),
                        'email_from': sender,
                        'email_to': recipients
                        }
        template_id = template_obj.create(template_data)
        template_obj.send(template_id)
        print("Email Sent.")

    def upload_data_to_api(self, pos_orders = []):
        """Function is used to push the pos order to foodbot"""

        print('pos_orders=',pos_orders)
        orders = [order.get('id', False) for order in pos_orders]
        print('orders=',orders)

        api_end_point = '/pos/payment'

        records = []
        if not orders:
            records = self.search([('status', 'in', ('pending', 'fail'))])
        else:
            records = self.search([('order_id', 'in', tuple(orders)), ('status', '=', 'pending')])

        for rec in records:
            print ("recccccccccccc",rec)
            api_data = {'sender_id': 'NA', 'phone': 'NA'}
            config = rec.order_id.session_id.config_id
            if not config.company_id.integrate_foodbot:
                rec.write({'res_msg': 'Foodbot Not Integrated',
                                 'response_code': 404, 'status': 'fail'
                                 })
                break
            if rec.customer_id:
                api_data['sender_id'] = rec.customer_id.barcode
                api_data['phone'] = rec.customer_id.phone

            payment_methods = []
            if rec.order_id:
                store_value_used = 0
                print('session_id=', rec.order_id.session_id)
                for acc_line in rec.order_id.payment_ids:
                    print('acc_line=', acc_line)
                    if acc_line.payment_method_id.name == 'FoodBot':
                        store_value_used += acc_line.amount
                    payment_methods.append({"method":acc_line.payment_method_id.name})

                rec.write({'store_value_used': store_value_used})
                rewards = []
                print('rec.order_id.unique_id',rec, rec.order_id, rec.order_id.unique_id )
                unique_id = rec.order_id.unique_id
                for pr_id in rec.order_id.promotion_ids._ids:
                    rewards.append({'reward_id':str(pr_id),'reward_type':'promotion'})

                purchased_points = 0;
                if config.company_id.is_sale_points:
                    for prd_line in rec.order_id.lines:
                        if(prd_line.product_id == config.company_id.point_product_id):
                            purchased_points += prd_line.price_subtotal_incl
                api_data.update({
                    'branch_id': config.branch_id,
                    'amount': round(rec.order_id.amount_total, 2),
                    'datetime': self.format_date(rec),
                    'invoice_id': rec.order_id.name,
                    'pos_userid': rec.user_id.id,
                    'invoice_ticketid': rec.order_id.pos_reference or False,
                    # 'invoice_ticketid': (rec.order_id.invoice_id.name if rec.order_id.invoice_id else rec.order_id.pos_reference),
                    'restaurant_id': config.company_id.restaurant_id,
                    'store_value_used': store_value_used,
                    'reward_redeemed': rewards,
                    'payment_method': payment_methods,
                    'unique_id': unique_id,
                    'lp_purchase_amount': purchased_points,
                    })

                # { "reward_id": "123", "reward_type":"point"}
                print('==api_data=', api_data);
                print ('rewards ids=', rec.order_id.promotion_ids._ids)
                print ('Store Value used for order ', rec.order_id,
                       ' is: ', store_value_used)
                print('api_data',api_data)
                url = config.company_id.foodbot_url + api_end_point
                headers = \
                    {'api_key': config.company_id.foodbot_api_key,
                     'Content-Type': 'application/json'}
                try:
                    r = requests.post(url,
                            data=json.dumps(api_data),
                            headers=headers, timeout=config.company_id.request_timeout)
                    print ('responce from foodBot invoice upload==', r)
                    if r.status_code == 200:
                        response = r.json()
                        
                        # print ("responce in invoice dataaaaaa",response)

                        message = response.get('message')
                        if isinstance(message, type([])):
                            if len(message) > 0:
                                message = message[0].get('msg', '')
                        elif isinstance(message, str):
                            message = message
                        else:
                            message = \
                                'Undefined message Please check log for detail'

                        rec.write({'res_msg': message,
                                'response_code': response.get('error_code', 0), 
                                'status': ('success' if response.get('status') == 'success' else 'fail')
                                })
                        if response.get('status') == 'fail':
                            email_body = {
                            'response_data': r.text,
                            'api_data': api_data
                            }
                            rec.fail_order_email(email_body)

                    else:
                        rec.write({'res_msg': 'Server Error',
                                'response_code': r.status_code,
                                'status': 'fail'})
                        email_body = {
                        'response_data': r.text,
                        'api_data': api_data
                        }
                        rec.fail_order_email(email_body)
                except requests.Timeout as err:
                    rec.write({'res_msg': 'Server Error: Timeout Exception'
                              , 'response_code': 500,
                              'status': 'fail'})
                    print('''Timeout exception while sending foodbot invoice data.\n
                        error: %s'''%(err))
                    email_body = {
                        'response_data': str(err),
                        'api_data': api_data
                        }
                    rec.fail_order_email(email_body)
                except requests.ConnectionError as err:
                    rec.write({'res_msg': 'Connection Error: Exception Connection Error'
                              , 'response_code': 500,
                              'status': 'fail'})
                    print('''Connection error while sending foodbot invoice data.\n
                        error: %s'''%(err))
                    email_body = {
                        'response_data': str(err),
                        'api_data': api_data
                        }
                    rec.fail_order_email(email_body)
                except requests.RequestException as err:
                    rec.write({'res_msg': 'Server Error: Exception Connection Error'
                              , 'response_code': 500,
                              'status': 'fail'})
                    print('''Something happens seriously wrong while sending foodbot invoice data.\n
                        error: %s'''%(err))
                    email_body = {
                        'response_data': str(err),
                        'api_data': api_data
                        }
                    rec.fail_order_email(email_body)

class FoodBotCryptoWebhook(models.Model):

    _name = 'foodbot.crypto.hook'
    _order = 'create_date desc'
    _name_rec = 'invoice_ref'
    
    invoice_ref = fields.Char("Order Ref")
    status = fields.Char("Status")
    message = fields.Char("Message")

# -*- coding: utf-8 -*-

##############################################################################
#
#    Cybrosys Technologies Pvt. Ltd.
#    Copyright (C) 2017-TODAY Cybrosys Technologies(<https://www.cybrosys.com>).
#    Author: Nikhil krishnan(<https://www.cybrosys.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import uuid
import requests
import json
from odoo import models, fields, api, _
from odoo.exceptions import Warning, ValidationError


class ResCompany(models.Model):
    _inherit = 'res.company'

    integrate_foodbot = fields.Boolean('Integrate to Foodbot',default=False)
    restaurant_key = fields.Char('Restaurant Key')
    restaurant_id = fields.Integer('Restaurant ID')
    foodbot_url = fields.Char('Foodbot URL')
    foodbot_api_key = fields.Char('Foodbot API Key')

    foodbot_uuid = fields.Char('FoodBot UUID')
    foodbot_auth_key = fields.Char('FoodBot Auth Key')
    foodbot_ably_key = fields.Char('FoodBot Ably Key')


    api_key = fields.Char('API Key')
    end_point_url = fields.Char('End Point URL')
    integration_status = fields.Selection([('pending','Pending'),('fail','Failed'),('success', 'Success')], 'Integration Status', default='pending', readonly= True)
    request_timeout = fields.Integer('Timeout', default=10, help="Foodbot API request timeout in seconds")
    fail_order_recipients = fields.Char("Recipient Emails", help="Insert e-mail ids to receive failed orders. Enter ',' seperated values in case of multiple recipients.")
    fail_order_senders = fields.Char("Sender Emails", help="Insert from e-mail id.")
    api_resp_code = fields.Integer('Status Code', readonly= True,\
        help="301: The action could not be completed \n \
        302: Invalid Restaurant Key \n \
        303: Restaurant not integrated yet. \n \
        308: Restaurant already integrated  \n \
        305: 1) No authorization token was  found \n \
        if auth token is not passed <br/>\
        2) Invalid Signature: if the token is incorrect \n \
        3) Action should be enabled/disable) \n \
        306: enabled successfully. \n \
        307: Unexpected end of JSON input: if JSON format is invalid. \n \
        309: Disabled successfully")
    api_resp_code_msg = fields.Selection(
        [
            ('301','The action could not be completed'),
            ('302','Invalid Restaurant Key'),
            ('303', 'Restaurant not integrated yet'),
            ('305', 'No authorization token was  found'),
            ('308', 'Restaurant already integrated'),
            ('306', 'enabled successfully'),
            ('307', 'Unexpected end of JSON input: if JSON format is invalid'),
            ('309', 'Disabled successfully'),
            ('0','Null')
        ], 
        'Status Code', readonly= True)
    is_crypto = fields.Boolean('Enable crypto', help="Enable crypto currency integration")
    # crypto_webhook_url = fields.Char('End Point URL', help="Crypto currency endpoint url(webhook)")
    crypto_expiration_time = fields.Integer('Max Retry', default="20", help="Maximum retry to to check webhook, each retry is of 5 seconds")
    qr_code_url = fields.Char('QR Code URl')

    point_product_id = fields.Many2one('product.product', 'Product')
    is_sale_points = fields.Boolean('Enable Points')

    @api.model
    def create(self, values):
        if 'integrate_foodbot' in values:
            if values['integrate_foodbot']:
                values['api_key'] = uuid.uuid4().hex
        company = super(ResCompany, self).create(values)
        if 'integrate_foodbot' in values:
            if values['integrate_foodbot']:
                url = company.foodbot_url
                rest_key = company.restaurant_key
                api_key = company.api_key
                foodbot_api_key = company.foodbot_api_key
                self.set_foodbot(url, rest_key, api_key, foodbot_api_key)
        if 'is_crypto' in values.keys():
            wallet_name = 'Cryptocurrency1'
            wallet_code = 'CRC1'
            enable = values.get('is_crypto', False)
            flag = False
            journal_obj = self.env['account.journal']
            journal_ids = journal_obj.search([('name', '=', wallet_name), '|', ('active','=', True),('active','=', False)])
            if not journal_ids:
                self.set_foodbot_wallet(enable, wallet_name, wallet_code)
            elif not journal_ids[0].active and enable:
                self.set_foodbot_wallet(enable, wallet_name, wallet_code)
        return company

    def write(self, values):
        # url = self.foodbot_url
        # rest_key = self.restaurant_key
        # api_key = self.api_key
        # foodbot_api_key = self.foodbot_api_key
        if 'foodbot_url' in values :
            url = values['foodbot_url']
        else :
            url = self.foodbot_url
        if 'restaurant_key' in values :
            rest_key = values['restaurant_key']
        else :
            rest_key = self.restaurant_key
        if 'foodbot_api_key' in values :
            foodbot_api_key = values['foodbot_api_key']
        else :
            foodbot_api_key = self.foodbot_api_key

        if 'integrate_foodbot' in values:
            if values['integrate_foodbot']:
                print ("in ifffffffff")
                values['api_key'] = uuid.uuid4().hex
                api_key = values['api_key']
                self.set_foodbot(url, rest_key, api_key, foodbot_api_key)
            else:
                values['api_key'] = ''
                api_key=''
                self.set_foodbot(url, rest_key, api_key, foodbot_api_key, 'disable')
        if 'is_crypto' in values.keys():
            wallet_name = 'Cryptocurrency'
            wallet_code = 'CRC'
            enable = values.get('is_crypto', False)
            flag = False
            journal_obj = self.env['account.journal']
            journal_ids = journal_obj.search([('name', '=', wallet_name), '|', ('active','=', True),('active','=', False)])
            if not journal_ids and enable:
                self.set_foodbot_wallet(enable, wallet_name, wallet_code)
            else:
                for journal in journal_ids:
                    if journal.active and not enable:
                        config_ids = self.env['pos.config'].search([('company_id','=',self.id)])
                        if len(config_ids) > 0 :
                            for config in config_ids :
                                if len(journal) > 0 and not values.get('is_crypto'):
                                    # print ("configs idddddddddd",config_ids)
                                    session_id = self.env['pos.session'].search([('config_id','=',config.id),('state','!=','closed')])
                                    # print ("session id",session_id)
                                    if len(session_id) > 0 :
                                        for session in session_id:
                                            bank_stmt_id = self.env['account.bank.statement'].search([('journal_id','=',journal.id),('pos_session_id','=',session.id)])
                                            if len(bank_stmt_id) > 0 :
                                                raise ValidationError(_('Cannot disable When Foodbot Payment have an open session !'))
                                            else:
                                                flag = True
                                elif len(journal) < 1 and values.get('is_crypto'):
                                    flag = True
                    elif not journal.active and enable:
                        journal.write({'active': True})
                    if flag:
                        self.set_foodbot_wallet(enable, wallet_name, wallet_code)
        return super(ResCompany, self).write(values)


    def set_foodbot(self, url, restaurant_key, api_key, foodbot_api_key, action="enable"):
        print ("set food bot")
        endpoint = '/pos/company-settings'
        url = url + endpoint
        config_url = self.env['ir.config_parameter'].search([('key','=','web.base.url')])
        if len(config_url) > 0 :
            end_point_url = config_url.value
        else :
            end_point_url =''
        headers = {'api_key' : foodbot_api_key,
                    'Content-Type': 'application/json'
                }
        params = {
            'restaurant_key': restaurant_key,
            'api_key': api_key,
            'action' : action,
            'end_point_url':end_point_url
        }
        # print ("beforeeeeeeee",url,params,headers)
        if action:
            config_ids = self.env['pos.config'].search([('company_id','=',self.id)])
            if len(config_ids) > 0 :
                for config in config_ids :
                    journal_obj = self.env['account.journal']
                    journal_ids = journal_obj.search([('name', '=', 'FoodBot'),('active','=', True)])
                    print('journal===',journal_ids)
                    for journal in journal_ids:
                        # print ("configs idddddddddd",config_ids)
                        session_id = self.env['pos.session'].search([('config_id','=',config.id),('state','!=','closed')])
                        print ("session id",session_id)
                        for session in session_id:
                            bank_stmt_id = self.env['account.bank.statement'].search([('journal_id','=',journal.id),('pos_session_id','=',session.id)])
                            if len(bank_stmt_id) > 0 :
                                raise ValidationError(_('Cannot disable When Foodbot Payment have an open session !'))
                if action == 'enable' :
                    for pos_config in config_ids :
                        pos_config.update({'integrate_foodbot':True})
                    self.set_foodbot_wallet(True)
                else :
                    for pos_config in config_ids :
                        pos_config.update({'integrate_foodbot':False})
                    self.set_foodbot_wallet(False)
        try:
            print("url headerrs",url,params,headers)
            r = requests.post(url, data=json.dumps(params), headers=headers, timeout=self.request_timeout)
            print('r=',r)
            if r.status_code == 200:
                response = r.json()
                # add condition for responce code message 
                resp_code = response.get('error_code', 0)
                # print ("resp codeeeeeeee",resp_code)
                if response.get('status') == 'success':
                    self.write({
                        'integration_status': 'success',
                        'api_resp_code' : response.get('error_code', 0),
                        'restaurant_id' : response.get('restaurant_id', 0),
                        'end_point_url' : end_point_url,
                        'api_resp_code_msg' : str(resp_code)
                        })
                else:
                    print('''Request failed while requesting to enable foodbot\n
                    error_code: %s, message: %s'''%(r.status_code, response.get('message','')))
                    raise ValidationError(_('Error: %s'%(response.get('message',''))))
            else:
                response = r.json()
                print('''Something went wrong while requesting to enable foodbot\n
                    error_code: %s, message: %s'''%(r.status_code, response.get('message','')))
                raise ValidationError(_('Error: %s'%(response.get('message',''))))
        except requests.Timeout as err:
            print('Company Enable Timeout Error %s'%(err))
            raise ValidationError(_('Timeout Error: %s'%(err)))
        except requests.ConnectionError as err:
            print('Company Enable connection Error %s'%(err))
            raise ValidationError(_('Connection Error: %s'%(err)))
        except requests.RequestException as err:
            print('Company Enable exception %s'%(err))
            raise ValidationError(_('General Exception: %s'%(err)))


    def set_foodbot_wallet(self, enable = True, f_name='FoodBot', f_code='FBW'):
        """ Enable disable foodbot wallet journal"""
        journal_obj = self.env['account.journal']
        pos_payment_obj = self.env['pos.payment.method']
        journal = journal_obj.search([('name', '=', f_name), '|',('active','=', False),('active','=', True)])
        if not journal and enable:
            journal_id = journal_obj.create({
                'name' : f_name, 
                'code' : f_code,
                'type': 'cash',
                })
            if journal_id:
                pos_payment_id = pos_payment_obj.create({
                    # 'receivable_account_id': rec_acc_id and rec_acc_id.id or False,
                    'name': f_name,
                    'is_cash_count': True,
                    'cash_journal_id': journal_id and journal_id.id or False,
                    'company_id': self.env.user.company_id and self.env.user.company_id.id or False,
                })
        elif len(journal) > 1:
            raise ValidationError(_('Multiple journals found with name %s'%(f_name)))
        elif journal and not journal.active and enable:
            journal.write({'active': True})
        elif journal and journal.active and not enable :
            journal.write({'active': False})










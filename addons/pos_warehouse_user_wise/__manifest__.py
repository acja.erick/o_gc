# -*- coding: utf-8 -*-

{
    "name" : "Pos Warehouse wise user access",
    "version" : "1.0",
    "depends" :["base","point_of_sale", "mrp", "stock","purchase"],
    "category" :  'Point of Sale',
    "description": """
Pos Warehouse wise user access.
""",
    "init_xml" : [],
    "demo_xml" : [],
    "data": [
           "security/security_views.xml",
            "views/pos_warehouse_user_wise_view.xml",
            ],
    "active": False,
    "installable": True
}

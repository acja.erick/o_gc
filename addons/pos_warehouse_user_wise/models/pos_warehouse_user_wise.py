# -*- coding: utf-8 -*-

##############################################################################
#
#    Cybrosys Technologies Pvt. Ltd.
#    Copyright (C) 2017-TODAY Cybrosys Technologies(<https://www.cybrosys.com>).
#    Author: Nikhil krishnan(<https://www.cybrosys.com>)
#    you can modify it under the terms of the GNU LESSER
#    GENERAL PUBLIC LICENSE (LGPL v3), Version 3.
#
#    It is forbidden to publish, distribute, sublicense, or sell copies
#    of the Software or modified copies of the Software.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU LESSER GENERAL PUBLIC LICENSE (LGPL v3) for more details.
#
#    You should have received a copy of the GNU LESSER GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (LGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import models, fields, api
from odoo.exceptions import Warning


class MrpProduction(models.Model):
    _inherit = 'mrp.production'

    warehouse_id = fields.Many2one('stock.warehouse','Warehouse',)

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    warehouse_id = fields.Many2one('stock.warehouse','Warehouse',)
    product_all = fields.Boolean('All Locationss',default=True) 
    pos_config_ids = fields.Many2many('pos.config', string='POS',)


class ProductProduct(models.Model):
    _inherit = 'product.product'

    warehouse_id = fields.Many2one('stock.warehouse','Warehouse',)
    # product_all = fields.Boolean('All Locationss') 
    # pos_config_ids = fields.Many2many('pos.config', string='POS',)

class ResUsers(models.Model):
    _inherit = 'res.users'

    warehouse_id = fields.Many2one('stock.warehouse','Warehouse',)
    pos_config_ids = fields.Many2many('pos.config', string='Available POS', help="Available POS for users. POS managers can view all POS configs.")

    @api.model
    def create(self,vals):
        # print ("aaaaaaaaaa",vals)
        res = super(ResUsers,self).create(vals)
        if res.pos_config_ids:
            if len(res.pos_config_ids) == 1:
                for val in res.pos_config_ids:
                    if val.picking_type_id:
                        warehouse_id = val.picking_type_id.warehouse_id
                        # print ("warehouseeeeee",warehouse_id)
                        if warehouse_id:
                            res.update({'warehouse_id':warehouse_id.id})
        return res

    def write(self,vals):
        # print ("aaaaaaaaaa",vals) 
        res= super(ResUsers,self).write(vals)
        if 'pos_config_ids' in vals and vals['pos_config_ids']:
            if self.pos_config_ids:
                if len(self.pos_config_ids) == 1:
                    for val in self.pos_config_ids:
                        if val.picking_type_id:
                            warehouse_id = val.picking_type_id.warehouse_id
                            # print ("warehouseeeeee",warehouse_id)
                            if warehouse_id:
                                self.update({'warehouse_id': warehouse_id.id})
            else:
                self.update({'warehouse_id': False})
        return res

    def get_user_pos_config(self):
        # print ("get user domain",self)
        domain = []
        if self:
            for val in self:
                if val.pos_config_ids:
                    if len(val.pos_config_ids) > 1:
                        pos_list=[]
                        for pos in val.pos_config_ids:
                            pos_list.append(pos.id)
                        domain=[('id','in',pos_list)]
                    elif len(val.pos_config_ids) == 1:
                        print ("val.pos_config_ids",val.pos_config_ids)
                        domain = [('id', '=', val.pos_config_ids.id)]
        # print ("domainnnnnnnnnn",domain)
        return domain




class PosOrder(models.Model):
    _inherit = 'pos.order'

    warehouse_id = fields.Many2one('stock.warehouse','Warehouse',)
    attribute_state = fields.Selection([('pending', 'Pending'), ('done', 'Done')], 'Attribute State', default='pending')

class PosSession(models.Model):
    _inherit = 'pos.session'

    warehouse_id = fields.Many2one('stock.warehouse','Warehouse',)

class PosConfig(models.Model):
    _inherit = 'pos.config'


class PosOrderReport(models.Model):
    _inherit = "report.pos.order"

class PurchaseOrder(models.Model):
    _inherit = "purchase.order"

    # @api.model
    # def default_get(self, fields):
    #     print ("default gettttttttt")
    #     user = self.env.user
    #     print ("userrrrrrrrrr",user,user.pos_config_ids,user.pos_config_ids.picking_type_id)
    #     res = super(PurchaseOrder, self).default_get(fields)
    #     return res

    @api.model
    def _default_picking_type(self):
        print ("userrrr in warehouse",self.env.user)
        type_obj = self.env['stock.picking.type']
        # company_id = self.env.context.get('company_id') or self.env.user.company_id.id
        # types = type_obj.search([('code', '=', 'incoming'), ('warehouse_id.company_id', '=', company_id)])
        # if not types:
        #     types = type_obj.search([('code', '=', 'incoming'), ('warehouse_id', '=', False)])
        # return types[:1]
        user_id =self.env.user
        types = type_obj.search([('code', '=', 'incoming'), ('warehouse_id', '=', user_id.warehouse_id.id)])
        print ("typesss",types)
        if not types:
            types = type_obj.search([('code', '=', 'incoming'),('warehouse_id', '=', 1)])
            # print ("typesssss in ware house id 1",types)
        return types[:1]

    # ajay
    @api.onchange('picking_type_id')
    def warehouse_id_change(self):
        if self.picking_type_id:
            self.warehouse_id = self.picking_type_id.warehouse_id

    READONLY_STATES = {
        'purchase': [('readonly', True)],
        'done': [('readonly', True)],
        'cancel': [('readonly', True)],
    }

    picking_type_id = fields.Many2one('stock.picking.type', 'Deliver To', states=READONLY_STATES, required=True, default=_default_picking_type,\
        help="This will determine operation type of incoming shipment")
   # warehouse_id = fields.Many2one('stock.warehouse','Warehouse',default=lambda self: self.env.user.warehouse_id.id)
    warehouse_id = fields.Many2one('stock.warehouse', 'Warehouse')

    
class StockPicking(models.Model):
    _inherit = "stock.picking"
    

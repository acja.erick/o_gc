# -*- coding: utf-8 -*-
##############################################################################
#
#    Cybrosys Technologies Pvt. Ltd.
#
#    Copyright (C) 2019-TODAY Cybrosys Technologies(<https://www.cybrosys.com>).
#    Author: Nikhil krishnan(odoo@cybrosys.com)
#    you can modify it under the terms of the GNU AFFERO
#    GENERAL PUBLIC LICENSE (AGPL v3), Version 3.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU AFFERO GENERAL PUBLIC LICENSE (AGPL v3) for more details.
#
#    You should have received a copy of the GNU AFFERO GENERAL PUBLIC LICENSE
#    GENERAL PUBLIC LICENSE (AGPL v3) along with this program.
#    If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from odoo import models, fields, api
from odoo.exceptions import Warning
import logging


_logger = logging.getLogger(__name__)


class StockMove(models.Model):
    _inherit = 'stock.move'

    def create_stock_move_line(self):
        sm_dict={
            'product_id' : self.product_id and self.product_id.id or False,
            'qty_done': self.product_uom_qty,
            'product_uom_id': self.product_uom and self.product_uom.id or False,
            'move_id' : self.id,
            'location_id' : self.location_id and self.location_id.id or False,
            'location_dest_id': self.location_dest_id and self.location_dest_id.id or False,
            # 'procure_method' : 'make_to_stock',
            'state': 'confirmed',
            'reference': self.name,
            'company_id' : self.company_id,
                 }
        return sm_dict



class MrpProduction(models.Model):
    _inherit = 'mrp.production'


    @api.model
    def _get_default_picking_type(self):
        company_id = self.env.context.get('default_company_id', self.env.company.id)
        user_id = self.env.user
        # print ("user idddddddd",user_id)
        # print ("warehouse_id",warehouse_id)
        if user_id and user_id.warehouse_id:
            print ("iffffffffff")
            pick_type_id = self.env['stock.picking.type'].search([
            ('code', '=', 'mrp_operation'),
            ('warehouse_id', '=', user_id.warehouse_id.id),
        ], limit=1).id
        else:
            # print ("elseeeeeeeeeee")
            pick_type_id =self.env['stock.picking.type'].search([
            ('code', '=', 'mrp_operation'),
            ('warehouse_id.company_id', '=', company_id),
        ], limit=1).id
        print ("pick_type_id1111111111",pick_type_id)
        return pick_type_id

    @api.model
    def get_picking_type(self):
        company_id = self.env.context.get('default_company_id', self.env.company.id)
        user_id = self.env.user
        # print ("user idddddddd",user_id)
        # print ("warehouse_id",warehouse_id)
        if user_id and user_id.warehouse_id:
            print ("iffffffffff")
            pick_type_id = self.env['stock.picking.type'].search([
            ('code', '=', 'mrp_operation'),
            ('warehouse_id', '=', user_id.warehouse_id.id),
        ], limit=1)
        else:
            # print ("elseeeeeeeeeee")
            pick_type_id =self.env['stock.picking.type'].search([
            ('code', '=', 'mrp_operation'),
            ('warehouse_id.company_id', '=', company_id),
        ], limit=1)
        print ("pick_type_id222222222",pick_type_id)
        return pick_type_id

    # @api.model
    # def _get_default_location_src_id(self):
    #     location = False
    #     company_id = self.env.context.get('default_company_id', self.env.company.id)
    #     user = self.env.user
    #     if user_id and user_id.warehouse_id:
    #         print ("iffffffffff")
    #         pick_type_id = self.env['stock.picking.type'].search([
    #         ('code', '=', 'mrp_operation'),
    #         ('warehouse_id', '=', user_id.warehouse_id.id)], limit=1)
    #         if pick_type_id:
    #             location = pick_type_id.default_location_src_id

    #     # if self.env.context.get('default_picking_type_id'):
    #     #     location = self.env['stock.picking.type'].browse(self.env.context['default_picking_type_id']).default_location_src_id
    #     if not location:
    #         location = self.env['stock.warehouse'].search([('company_id', '=', company_id)], limit=1).lot_stock_id
    #     return location and location.id or False

    # @api.model
    # def _get_default_location_dest_id(self):
    #     location = False
    #     company_id = self.env.context.get('default_company_id', self.env.company.id)
    #     user = self.env.user
    #     if user_id and user_id.warehouse_id:
    #         print ("iffffffffff")
    #         pick_type_id = self.env['stock.picking.type'].search([
    #         ('code', '=', 'mrp_operation'),
    #         ('warehouse_id', '=', user_id.warehouse_id.id)], limit=1)
    #         if pick_type_id:
    #             location = pick_type_id.default_location_dest_id
    #     # if self._context.get('default_picking_type_id'):
    #     #     location = self.env['stock.picking.type'].browse(self.env.context['default_picking_type_id']).default_location_dest_id
    #     if not location:
    #         location = self.env['stock.warehouse'].search([('company_id', '=', company_id)], limit=1).lot_stock_id
    #     return location and location.id or False


    def update_finish_qty(self):
        if self:
            for val in self:
                if val.move_raw_ids:
                    for raw in val.move_raw_ids:
                        dict1= raw.create_stock_move_line()
                        raw_mv_line = self.env['stock.move.line'].create(dict1)
                        # raw_mv_line._action_confirm()
                if val.move_finished_ids:
                    for finish in val.move_finished_ids:
                        dict2 = finish.create_stock_move_line()
                        finish_mv_line = self.env['stock.move.line'].create(dict2)
                        # finish_mv_line._action_confirm()

    def get_product_produce(self):
        # print ("selffffffff",self)
        fields = ['serial', 'production_id', 'product_id', 'product_qty', 'product_uom_id', 'product_tracking',
                  'lot_id', 'produce_line_ids','consumption','raw_workorder_line_ids']
        res = {}
        if self:
            production = self
            serial_finished = (production.product_id.tracking == 'serial')
            if serial_finished:
                todo_quantity = 1.0
            else:
                main_product_moves = production.move_finished_ids.filtered(
                    lambda x: x.product_id.id == production.product_id.id)
                todo_quantity = production.product_qty - sum(main_product_moves.mapped('quantity_done'))
                todo_quantity = todo_quantity if (todo_quantity > 0) else 0
                todo_uom = production.product_uom_id.id
                # todo_quantity = self._get_todo(production)
            if 'production_id' in fields:
                res['production_id'] = production.id
            if 'product_id' in fields:
                res['product_id'] = production.product_id.id
            if 'product_uom_id' in fields:
                res['product_uom_id'] = todo_uom
            if 'serial' in fields:
                res['serial'] = bool(serial_finished)
            if 'qty_producing' in fields:
                res['qty_producing'] = todo_quantity
            if 'consumption' in fields:
                res['consumption'] = production.bom_id.consumption
            if 'raw_workorder_line_ids' in fields:
                lines = []
                for move in production.move_raw_ids.filtered(
                        lambda x: (x.product_id.tracking != 'none') and x.state not in (
                        'done', 'cancel') and x.bom_line_id):
                    qty_to_consume = float_round(
                        todo_quantity / move.bom_line_id.bom_id.product_qty * move.bom_line_id.product_qty,
                        precision_rounding=move.product_uom.rounding, rounding_method="UP")
                    for move_line in move.move_line_ids:
                        if float_compare(qty_to_consume, 0.0, precision_rounding=move.product_uom.rounding) <= 0:
                            break
                        if move_line.lot_produced_id or float_compare(move_line.product_uom_qty, move_line.qty_done,
                                                                      precision_rounding=move.product_uom.rounding) <= 0:
                            continue
                        to_consume_in_line = min(qty_to_consume, move_line.product_uom_qty)
                        lines.append({
                            'move_id': move.id,
                            'qty_to_consume': to_consume_in_line,
                            'qty_done': 0.0,
                            'lot_id': move_line.lot_id.id,
                            'product_uom_id': move.product_uom.id,
                            'product_id': move.product_id.id,
                        })
                        qty_to_consume -= to_consume_in_line
                    if float_compare(qty_to_consume, 0.0, precision_rounding=move.product_uom.rounding) > 0:
                        if move.product_id.tracking == 'serial':
                            while float_compare(qty_to_consume, 0.0, precision_rounding=move.product_uom.rounding) > 0:
                                lines.append({
                                    'move_id': move.id,
                                    'qty_to_consume': 1,
                                    'qty_done': 0.0,
                                    'product_uom_id': move.product_uom.id,
                                    'product_id': move.product_id.id,
                                })
                                qty_to_consume -= 1
                        else:
                            lines.append({
                                'move_id': move.id,
                                'qty_to_consume': qty_to_consume,
                                'qty_done': 0.0,
                                'product_uom_id': move.product_uom.id,
                                'product_id': move.product_id.id,
                            })

                res['raw_workorder_line_ids'] = [(0, 0, x) for x in lines]
        return res


    def create_child_bom_mrp(self,bom_id,prod,qty):
        if qty > 0:
            bom_qty=qty
        else:
            bom_qty = 1.0
        if bom_id:
            if bom_id.product_id:
                route_name = [ route.name for route in bom_id.product_id.route_ids]
                if 'Manufacture' in route_name:
                    bom_temp = self.env['mrp.bom'].search([('product_tmpl_id', '=', bom_id.product_id.product_tmpl_id.id),
                                                           ('product_id', '=', False)])
                    bom_prod = self.env['mrp.bom'].search([('product_id', '=', bom_id.product_id.id)])
                    if bom_prod:
                        bom = bom_prod[0]
                    elif bom_temp:
                        bom = bom_temp[0]
                    else:
                        bom = []
                    picking_type_id = self.get_picking_type()
                    if bom:
                        if picking_type_id:
                            location_src_id= picking_type_id.default_location_src_id and picking_type_id.default_location_src_id or False
                            location_dest_id= picking_type_id.default_location_dest_id and picking_type_id.default_location_dest_id or False
                        else:
                            location_src_id=False
                            location_dest_id=False
                        vals = {
                            'origin': 'POS-' + prod['pos_reference'],
                            'state': 'confirmed',
                            'product_id': bom_id.product_id and bom_id.product_id.id or False,
                            'product_tmpl_id': bom_id.product_id.product_tmpl_id and bom_id.product_id.product_tmpl_id or False,
                            'product_uom_id': bom_id.product_id.uom_id and bom_id.product_id.uom_id.id or False,
                            'product_qty': prod['qty'] * bom_qty,
                            'bom_id': bom.id,
                            'picking_type_id' : picking_type_id and picking_type_id.id or False,
                            'location_src_id' : location_src_id and location_src_id.id or False,
                            'location_dest_id' : location_dest_id and location_dest_id.id or False,
                        }
                        # print ("valllllllsssssssss1111111111",vals)
                        mrp_order = self.sudo().create(vals)
                        # print ("mrp orderrrrrrrrrrrr1111111111",mrp_order)
                        list_value = []
                        for bom_line in mrp_order.bom_id.bom_line_ids:
                            self.create_child_bom_mrp(bom_line, prod,bom_line.product_qty)
                            # print ("bom ;lineeeeeeeeeeee",bom_line)
                            # print ("bom_line.product_qty",bom_line.product_qty)
                            # print ("bom_line.bom_id.product_qty",bom_line.bom_id.product_qty)
                            # print ("bom_line.product_qty/bom_line.bom_id.product_qty",bom_line.product_qty/bom_line.bom_id.product_qty)
                            # print ("mrp_order.product_qty,",mrp_order.product_qty)
                            # print ("tota;cali",(bom_line.product_qty/bom_line.bom_id.product_qty) * mrp_order.product_qty)
                            # print ("bom qtyyyyyyyyyy",bom_qty)
                            qty = 1.0
                            if ((bom_line.product_qty / bom_line.bom_id.product_qty) * mrp_order.product_qty) > 1.0:
                                qty = round(
                                    ((bom_line.product_qty / bom_line.bom_id.product_qty) * mrp_order.product_qty ), 0)
                            else:
                                qty = round(
                                    ((bom_line.product_qty / bom_line.bom_id.product_qty) * mrp_order.product_qty),
                                    5)
                            # print ("qtyyyyyyyyyyy",qty)
                            list_value.append((0, 0, {
                                'raw_material_production_id': mrp_order.id,
                                'name': mrp_order.name,
                                'product_id': bom_line.product_id.id,
                                'product_uom': bom_line.product_uom_id.id,
                                # 'product_uom_qty': (bom_line.product_qty/bom_line.bom_id.product_qty) * mrp_order.product_qty,
                                'product_uom_qty': qty,
                                'picking_type_id': mrp_order.picking_type_id.id,
                                'location_id': mrp_order.location_src_id.id,
                                'location_dest_id': bom_line.product_id.with_context(
                                    force_company=self.company_id.id).property_stock_production.id,
                                'company_id': mrp_order.company_id.id,
                            }))
                        # print ("mrp orderrrrrrrr",mrp_order)
                        mrp_order.update({'move_raw_ids': list_value})
                        if mrp_order:
                            mrp_order.action_confirm()
                            mrp_order.update_finish_qty()
                            # mrp_produce_dict = mrp_order.get_product_produce()
                            # # print ("mrp_produce_dictmrp_produce_dict",mrp_produce_dict)
                            # mrp_produce_product_id = self.env['mrp.product.produce'].create(mrp_produce_dict)
                            # # print (" mrp product produce iddd",mrp_produce_product_id)
                            # mrp_produce_product_id.do_produce()
                            # mrp_order.post_inventory()
                            # print ("mrp orderrrrrrrrbutton_mark_done",mrp_order)
                            mrp_order.button_mark_done()
                            # # mrp_order.button_mark_done()
                return True

    # @api.model
    # def create(self, values):
    #     print ("create valuessssssssssss",values)
    #     production = super(MrpProduction, self).create(values)
    #     print ("create production",production)
    #     return production

    # def write(self, values):
    #     print ("writeeeeeee valuessssssssssss",values)
    #     production = super(MrpProduction, self).write(values)
    #     print ("writeeeeeee production",self)
    #     return production


    def create_mrp_from_pos(self, products):
        product_ids = []
        if products:
            for product in products:
                flag = 1
                if product_ids:
                    for product_id in product_ids:
                        if product_id['id'] == product['id']:
                            product_id['qty'] += product['qty']
                            flag = 0
                if flag:
                    product_ids.append(product)
            for prod in product_ids:
                if prod['qty'] > 0:
                    product = self.env['product.product'].search([('id', '=', prod['id'])])
                    bom_count = self.env['mrp.bom'].search([('product_tmpl_id', '=', prod['product_tmpl_id'])])
                    if bom_count:
                        bom_temp = self.env['mrp.bom'].search([('product_tmpl_id', '=', prod['product_tmpl_id']),
                                                               ('product_id', '=', False)])
                        bom_prod = self.env['mrp.bom'].search([('product_id', '=', prod['id'])])
                        if bom_prod:
                            bom = bom_prod[0]
                        elif bom_temp:
                            bom = bom_temp[0]
                        else:
                            bom = []
                        picking_type_id = self.get_picking_type()
                        if bom:
                            if picking_type_id:
                                location_src_id= picking_type_id.default_location_src_id and picking_type_id.default_location_src_id or False
                                location_dest_id= picking_type_id.default_location_dest_id and picking_type_id.default_location_dest_id or False
                            else:
                                location_src_id=False
                                location_dest_id=False
                            vals = {
                                'origin': 'POS-' + prod['pos_reference'],
                                'state': 'confirmed',
                                'product_id': prod['id'],
                                'product_tmpl_id': prod['product_tmpl_id'],
                                'product_uom_id': prod['uom_id'],
                                'product_qty': prod['qty'],
                                'bom_id': bom.id,
                                'picking_type_id' : picking_type_id and picking_type_id.id or False,
                                'location_src_id' : location_src_id and location_src_id.id or False,
                                'location_dest_id' : location_dest_id and location_dest_id.id or False,
                            }
                            # print ("valllllllsssssssss1222222222222222222",vals)
                            mrp_order = self.sudo().create(vals)
                            # print ("mrp orderr222222222",mrp_order)
                            list_value = []
                            for bom_line in mrp_order.bom_id.bom_line_ids:
                                self.create_child_bom_mrp(bom_line, prod,bom_line.product_qty)
                                # print("bom ;lineeeeeeeeeeee mainnnnnnnn", bom_line)
                                # print("bom_line.product_qty mainnnnnnnn", bom_line.product_qty)
                                # print("bom_line.bom_id.product_qty mainnnnnnnn", bom_line.bom_id.product_qty)
                                # print("bom_line.product_qty/bom_line.bom_id.product_qty mainnnnnnnn",
                                #       bom_line.product_qty / bom_line.bom_id.product_qty)
                                # print("mrp_order.product_qty, mainnnnnnnn", mrp_order.product_qty)
                                # print("tota;cali mainnnnnnnn",
                                #       (bom_line.product_qty / bom_line.bom_id.product_qty) * mrp_order.product_qty)
                                # print("bom qtyyyyyyyyyy", bom_qty)
                                qty=1.0
                                if ((bom_line.product_qty / bom_line.bom_id.product_qty) * mrp_order.product_qty) > 1.0:
                                    qty = round(((bom_line.product_qty / bom_line.bom_id.product_qty) * mrp_order.product_qty),0)
                                else:
                                    qty=round(
                                        ((bom_line.product_qty / bom_line.bom_id.product_qty) * mrp_order.product_qty),
                                        5)
                                # print ("qtyyyyyyyyyyy mainnnnnnnnnnn",qty)
                                list_value.append((0, 0, {
                                    'raw_material_production_id': mrp_order.id,
                                    'name': mrp_order.name,
                                    'product_id': bom_line.product_id.id,
                                    'product_uom': bom_line.product_uom_id.id,
                                    # 'product_uom_qty': bom_line.product_qty * mrp_order.product_qty,
                                    'product_uom_qty': qty,
                                    'picking_type_id': mrp_order.picking_type_id.id,
                                    'location_id': mrp_order.location_src_id.id,
                                    'location_dest_id': bom_line.product_id.with_context(force_company=self.company_id.id).property_stock_production.id,
                                    'company_id': mrp_order.company_id.id,
                                }))
                            mrp_order.update({'move_raw_ids':list_value})
                            if mrp_order:
                                mrp_order.action_confirm()
                                mrp_order.update_finish_qty()
                                # mrp_produce_dict = mrp_order.get_product_produce()
                                # # print ("mrp_produce_dictmrp_produce_dict",mrp_produce_dict)
                                # mrp_produce_product_id = self.env['mrp.product.produce'].create(mrp_produce_dict)
                                # # print (" mrp product produce iddd",mrp_produce_product_id)
                                # mrp_produce_product_id.do_produce()
                                # mrp_order.post_inventory()
                                mrp_order.button_mark_done()
                                # # mrp_order.button_mark_done()
        return True


class ProductTemplate(models.Model):
    _inherit = 'product.template'

    to_make_mrp = fields.Boolean(string='To Create MRP Order',
                                 help="Check if the product should be make mrp order")

    @api.onchange('to_make_mrp')
    def onchange_to_make_mrp(self):
        if self.to_make_mrp:
            if not self.bom_count:
                raise Warning('Please set Bill of Material for this product.')


class ProductProduct(models.Model):
    _inherit = 'product.product'

    @api.onchange('to_make_mrp')
    def onchange_to_make_mrp(self):
        if self.to_make_mrp:
            if not self.bom_count:
                raise Warning('Please set Bill of Material for this product.')


class POSOrder(models.Model):
    _inherit = "pos.order"

    # @api.model
    # def create_from_ui(self, orders, draft=False):
    #     order_ref = super(POSOrder, self).create_from_ui(orders, draft)
    #     order_ids = [order.get('id', False) for order in order_ref]
    #     orders_object = self.browse(order_ids)
    #     for order in orders_object:
    #         self.create_bom_with_multi_variant(orders, order)
    #     return order_ref

    def create_bom_with_multi_variant(self, orders, order):
        _logger.info('create_bom_with_multi_variant inherit')
        for o in orders:
            # print("ooooooo in orders", o)
            move_object = self.env['stock.move']
            moves = move_object
            product_obj = self.env['product.product']
            variants = []
            if o['data']['name'] == order.pos_reference:
                if o['data'] and o['data'].get('lines', False):
                    for line in o['data']['lines']:
                        if line[2] and line[2].get('variants', False):
                            for var in line[2]['variants']:
                                if var.get('product_id'):
                                    variants.append(var)
                            # del line[2]['variants']
                if variants:
                    for variant in variants:
                        print("variantvariant", variant)
                        product = product_obj.browse(variant.get('product_id')[0])
                        bom_id = self.env['mrp.bom'].search(['|',('product_id','=',product.id),('product_tmpl_id','=',product.product_tmpl_id.id)])
                        if len(bom_id) > 0:
                            product_dict = {
                                'id': product.id,
                                'qty': abs(variant['quantity']),
                                'product_tmpl_id': product.product_tmpl_id and product.product_tmpl_id.id or False,
                                'pos_reference': order.name,
                                'uom_id': variant['uom_id'] and variant['uom_id'][0] if variant.get('uom_id', []) else product.uom_id.id,
                            }
                            print("product_dictproduct_dict", product_dict)
                            self.env['mrp.production'].create_mrp_from_pos([product_dict])

        return True
# -*- coding: utf-8 -*-

from odoo import models, fields, api


class PosConfigAsync(models.Model):

    _inherit = 'pos.config'
    accept_foodbot_orders = fields.Boolean("Accept Foodbot Orders")
    printer_ids = fields.Many2many('restaurant.printer', 'pos_config_async_printer_rel', 'config_id', 'printer_id',
                                   string='Order Printers')

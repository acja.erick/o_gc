# -*- coding: utf-8 -*-

from odoo import models, fields, api


class FoodBotPosOrder(models.Model):

    _inherit = 'pos.order'
    foodbot_order_id = fields.Char("foodbot_order_id", size = 10)


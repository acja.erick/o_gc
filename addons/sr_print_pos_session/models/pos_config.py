# -*- coding: utf-8 -*-
from odoo import fields, api, models, _


# class ResPartner(models.Model):
#     _inherit = "res.partner"
#
#     warehouse_config_id = fields.Boolean('Warehouse Config',default=False)
#
# class PosConfig(models.Model):
#     _inherit = "pos.config"
#
#     partner_id = fields.Many2one('res.partner','Partner')
#
#     @api.model
#     def create(self, values):
#         pos_config = super(PosConfig, self).create(values)
#         if 'partner_id' in values and values['partner_id'] :
#             pos_config.partner_id.write({'warehouse_config_id':True})
#         return pos_config
#
#     def write(self, vals):
#         print ("valssss",vals)
#         result = super(PosConfig, self).write(vals)
#         if 'partner_id' in  vals and vals['partner_id']:
#             partner_obj= self.env['res.partner'].browse([vals['partner_id']])
#             print ("partner objjjjjjjjjj",partner_obj)
#             if partner_obj :
#                 self.partner_id.write({'warehouse_config_id':False})
#                 partner_obj.write({'warehouse_config_id':True})
#
#         return result

    # def write(self, vals):
    #     if 'partner_id' in  vals and vals['partner_id']:
    #         partner_obj= self.env['res.partner'].browse([vals['partner_id']])
    #         # print ("partner objjjjjjjjjj",partner_obj)
    #         if self.partner_id :
    #             self.partner_id.write({'warehouse_config_id':False})
    #         if partner_obj :
    #             partner_obj.write({'warehouse_config_id':True})
    #     result = super(PosConfig, self).write(vals)
    #
    #     config_display = self.filtered(lambda c: c.is_posbox and c.iface_customer_facing_display and not (c.customer_facing_display_html or '').strip())
    #     if config_display:
    #         super(PosConfig, config_display).write({'customer_facing_display_html': self._compute_default_customer_html()})
    #
    #     self.sudo()._set_fiscal_position()
    #     self.sudo()._check_modules_to_install()
    #     self.sudo()._check_groups_implied()
    #     return result



class PosSession(models.Model):
    _inherit = "pos.session"

    @api.model
    def get_report_data(self, ids):
        acc_bnk_stmt = self.env['account.bank.statement']
        acc_bnk_stmt_lines = self.env['account.bank.statement.line']
        account_journal = self.env['account.journal']
        currency = self.env['res.currency']
        pos_order = self.env['pos.order']
        self._ids = ids
        sessions = []
        records = self.read([])
        for record in records:
            session = record
            pos_statement_ids = {}

            rec = self.browse(record['id'])
            
            session['is_pos_box'] = rec.config_id.is_posbox
            session['proxy_ip'] = rec.config_id.proxy_ip
            session['proxy_print'] = rec.config_id.iface_print_via_proxy
            session['company'] = rec.config_id.company_id.name
            session['logo'] = rec.config_id.company_id.logo

            currency._ids = [record['currency_id'][0]]
            session['symbol'] = currency.symbol
            acc_bnk_stmt._ids = record['statement_ids']
            statements = acc_bnk_stmt.read(['name', 'journal_id', 'journal_type', 'date', 'balance_start', 'balance_end_real', 'total_entry_encoding', 'line_ids'])
            stmts = []
            for statement in statements:
                
                acc_bnk_stmt_lines._ids = statement['line_ids']
                stmt_lines = []
                for line in acc_bnk_stmt_lines:
                    l = line.read(['name', 'date', 'amount', 'pos_statement_id'])[0]
                    journal = line.journal_id.read(['name', 'code'])
                    l['journal'] = len(journal) > 0 and journal[0] or False
                    
                    if l['amount'] > 0 and l['pos_statement_id'] and not l['pos_statement_id'][0] in pos_statement_ids.keys():
                        p_order = pos_order.browse([l['pos_statement_id'][0]])
                        l['amount'] = round(p_order.amount_total,2)
                        l['tax'] = round(p_order.amount_tax,2)
                        stmt_lines.append(l)
                        pos_statement_ids[l['pos_statement_id'][0]] = [l['journal']]
                    elif l['pos_statement_id'] and l['pos_statement_id'][0] in pos_statement_ids.keys():
                        temp_pos_ids = pos_statement_ids[l['pos_statement_id'][0]]
                        if temp_pos_ids:
                            temp_pos_ids.append(l['journal'])
                            pos_statement_ids[l['pos_statement_id'][0]] = temp_pos_ids
                
                statement['lines'] = stmt_lines
                stmts.append(statement)
            session['statements'] = stmts
            session['pos_statement_ids'] = pos_statement_ids
            sessions.append(session)
        return sessions

    @api.model
    def get_report_journal_data(self):
        session_list = []
        journal_list = []
        journal_new_list = []
        statement_list = []
        pos_order_list = []
        line_list1 = []
        line_list = []
        dict_journal = {}
        if self:
            for val in self:
                # print ("vallllllllllll",val)
                payment_ids = self.env['pos.payment'].search([('session_id','=',val.id)])
                print ("payment idsssssssss",payment_ids)
                if payment_ids:
                    for stat in payment_ids:
                        print ("dict journal 111111111111",dict_journal)
                        if stat.payment_method_id.name in journal_list:
                            tot = dict_journal[stat.payment_method_id.name]
                            new_tot = tot + stat.amount
                            dict_journal.update({stat.payment_method_id.name: new_tot})
                        else:
                            # amt = 0.0
                            dict_journal[stat.payment_method_id.name] = stat.amount
                            journal_list.append(stat.payment_method_id.name)
            # print ("session listttttttttttttt",session_list)
            # print ("statementtttttttttt liisttttttttttttt",statement_list)
            print ("dict journalllllllllllllll",dict_journal)
            if dict_journal:
                for k, v in dict_journal.items():
                    journal_new_list.append((k, v))
        print ("journal new list")
        return  journal_new_list


class AccountBankStatementLine(models.Model):
    _inherit = 'account.bank.statement.line'


    # def get_journal_code_report(self):
    #     #     code = ''
    #     #     account_statement_line_id = self.env['account.bank.statement.line'].search([('pos_statement_id','=',self.pos_statement_id.id)])
    #     #     if len(account_statement_line_id) > 0:
    #     #         for val in account_statement_line_id:
    #     #             if val.amount > 0:
    #     #                 code = code + '/' + val.journal_id.code
    #     #         code = code[1:]
    #     #     return code

    def get_journal_code_report(self):
        code = ''
        account_statement_line_id = self.env['account.bank.statement.line'].search([('pos_statement_id','=',self.pos_statement_id.id)])
        if len(account_statement_line_id) > 0:
            code_list=[]
            for val in account_statement_line_id:
                if val.amount > 0 and val.journal_id.code not in code_list:
                    code = code + '/' + val.journal_id.code
                    code_list.append(val.journal_id.code)
            code = code[1:]
        return code








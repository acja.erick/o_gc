odoo.define('sr_print_session', function (require) {
"use strict";
var core = require('web.core');

var rpc = require('web.rpc');
var Session = require('web.Session');
var ListController = require('web.ListController');
var Sidebar = require('web.Sidebar');
//var devices = require('point_of_sale.devices');

var _t = core._t;
var Qweb = core.qweb;

ListController.include({
  init: function (parent, model, renderer, params) {
    this._super.apply(this, arguments);
    this.connection = null;
    
    this.receipt_queue = [];

    this.notifications = [];
    this.bypass_proxy = false;

    this.connection = null;
    this.host       = '';
    this.keptalive  = false;

    this.set('status',{});

    this.set_connection_status('disconnected');

    this.on('change:status',this,function(eh,status){
        status = status.newValue;
        if(status.status === 'connected'){
            console.log(status.status);
        }
    });
    
  },
  renderSidebar: function ($node) {
      if (this.hasSidebar && !this.sidebar) {
          var other = [{
              label: _t("Export"),
              callback: this._onExportData.bind(this)
          }];
          if (this.archiveEnabled) {
              other.push({
                  label: _t("Archive"),
                  callback: this._onToggleArchiveState.bind(this, true)
              });
              other.push({
                  label: _t("Unarchive"),
                  callback: this._onToggleArchiveState.bind(this, false)
              });
          }
          if (this.is_action_enabled('delete')) {
              other.push({
                  label: _t('Delete'),
                  callback: this._onDeleteSelectedRecords.bind(this)
              });
          }
          if(this.modelName === 'pos.session'){
              other.push({
                  label: _t('Print Session'),
                  callback: this._printSessions.bind(this)
              });
          }
          this.sidebar = new Sidebar(this, {
              editable: this.is_action_enabled('edit'),
              env: {
                  context: this.model.get(this.handle, {raw: true}).getContext(),
                  activeIds: this.getSelectedIds(),
                  model: this.modelName,
              },
              actions: _.extend(this.toolbarActions, {other: other}),
          });
          this.sidebar.appendTo($node);

          this._toggleSidebar();
      }
  },
  get_format: function(val){
    var v = parseFloat(val);
    console.log('v');
    return v
  },
  // starts a loop that updates the connection status
    keepalive: function(){
        var self = this;

        function status(){
            self.connection.rpc('/hw_proxy/status_json',{},{timeout:2500})
                .then(function(driver_status){
                    self.set_connection_status('connected',driver_status);
                },function(){
                    if(self.get('status').status !== 'connecting'){
                        self.set_connection_status('disconnected');
                    }
                }).always(function(){
                    setTimeout(status,5000);
                });
        }

        if(!this.keptalive){
            this.keptalive = true;
            status();
        }
    },
  set_connection_status: function(status,drivers){
      var oldstatus = this.get('status') || {};
      var newstatus = {};
      newstatus.status = status;
      newstatus.drivers = status === 'disconnected' ? {} : oldstatus.drivers;
      newstatus.drivers = drivers ? drivers : newstatus.drivers;
      this.set('status',newstatus);
    },
  message : function(name,params){
        var callbacks = this.notifications[name] || [];
        for(var i = 0; i < callbacks.length; i++){
            callbacks[i](params);
        }
        if(this.get('status').status !== 'disconnected'){
            return this.connection.rpc('/hw_proxy/' + name, params || {});
        }else{
            return (new $.Deferred()).reject();
        }
    },
  disconnect: function(){
        if(this.get('status').status !== 'disconnected'){
            this.connection.destroy();
            this.set_connection_status('disconnected');
        }
    },
  connect: function(url){
    var self = this;
    this.connection = new Session(undefined,url, { use_cors: true});
    this.host   = url;
    this.set_connection_status('connecting',{});

    return self.message('handshake').then(function(response){
            if(response){
                self.set_connection_status('connected');
                localStorage.hw_proxy_url = url;
//                self.keepalive();
            }else{
                self.set_connection_status('disconnected');
                console.error('Connection refused by the Proxy');
            }
        },function(){
            self.set_connection_status('disconnected');
            console.error('Could not connect to the Proxy');
        });
    },

  _printSessions: function () {
      var self = this;
      var data = self.getSelectedRecords();
      var SessionScreenWidget;
      var res_ids = [];
      var is_pos_box = false;
      var proxy_print = false;
      var proxy_ip = '0.0.0.0';
      var result;
      _.each(data, function(row){
        res_ids.push(row.res_id);
      });
      self._rpc({
          model: 'pos.session',
          method: 'get_report_data',
          args: [res_ids],
        }).then(function(res){
          //console.log('Debug res==',res);
        is_pos_box = res[0]['is_pos_box']
        proxy_print = res[0]['proxy_print']
        proxy_ip = res[0]['proxy_ip']
        var reportTemplate = 'SessionDetailsReport_win';
        if(is_pos_box && proxy_print){
          reportTemplate = 'SessionDetailsReport';
	console.log('status=',self.get('status'));
          if(self.get('status') != 'connected'){
            console.log('trying to connect:','http://'+proxy_ip+':8069');
            self.connect('http://'+proxy_ip+':8069');
          }
        }
        SessionScreenWidget = Qweb.render(reportTemplate, {sessions: res})
      });
      function isScreenWidget(){
        if(SessionScreenWidget == undefined){
          setTimeout(function(){
	      isScreenWidget();
	  },2000);
	}
	else{
            //console.log('debug',SessionScreenWidget);
            if(!is_pos_box || !proxy_print){
		var w = window.open();
		if(w == null){
		   alert('Please check is popoup allowed for this url.');
		}else{
	           w.document.write(SessionScreenWidget);
            	   w.print();
               	   w.close();
		}
            }
            else{
               var params =  {receipt: SessionScreenWidget};
               self.message('print_xml_receipt',{ receipt: SessionScreenWidget },{ timeout: 5000 })
                 .then(function(){
//		     self.disconnect();
                     console.log('printed');
                 },function(error){
                  if (error) {
                      console.log('Printing Error: ',error.data.message)  
                      console.log(error.data.debug)
                      console.log('Suggestion: If printer available and connected, then try to restart posbox');
                      return;
                  }
              });
          }
	}
	};isScreenWidget()
    },
  })
});

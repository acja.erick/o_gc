# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from odoo import api, fields, models, tools, _

import math
import time
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT as DATETIME_FORMAT
from odoo.exceptions import UserError
#
# Use period and Journal for selection or resources
#


class PosSessionDetailReport(models.AbstractModel):
    _name = 'report.sr_print_pos_session.pos_session_detail_report_template'

    @api.model
    def _get_report_values(self, docids, data=None):
        print ("get report valuesssssssssssssssss thussssssssssssss",self,docids,data)
        doc = []
        session_ids = data.get('ids')
        print ("session idssssssssssssss",session_ids,type(session_ids))
        session_list=[]
        journal_list=[]
        journal_new_list=[]
        statement_list=[]
        pos_order_list=[]
        line_list1=[]
        line_list=[]
        dict_journal={}
        if session_ids :
            for val in self.env['pos.session'].browse(session_ids) :
                # print ("vallllllllllll",val)
                session_list.append(val)
                if val.statement_ids :
                    for stat in val.statement_ids :
                        for line1 in stat.line_ids:
                            line_list.append(line1)
                            if line1.amount > 0 and line1.pos_statement_id.id not in pos_order_list:
                                line_list1.append(line1)
                                pos_order_list.append(line1.pos_statement_id.id)
                        # print ("dict journal 111111111111",dict_journal)
                        if stat.journal_id.id in journal_list :
                            tot = dict_journal[stat.journal_id.name]
                            amt=0.0
                            if stat.line_ids:
                                for line in stat.line_ids:
                                    if line.amount > 0:
                                        amt = amt + line.amount
                            new_tot = tot  + amt
                            dict_journal.update({stat.journal_id.name:new_tot})
                        else :
                            amt=0.0
                            if stat.line_ids:
                                for line in stat.line_ids:
                                    if line.amount > 0:
                                        amt = amt + line.amount
                            dict_journal[stat.journal_id.name] = amt
                            journal_list.append(stat.journal_id.id)
                        if stat.line_ids :
                            for line in stat.line_ids :
                                statement_list.append(line)
            # print ("session listttttttttttttt",session_list)
            # print ("statementtttttttttt liisttttttttttttt",statement_list)
            # print ("dict journalllllllllllllll",dict_journal)
            if dict_journal :
                for k,v in dict_journal.items():
                    journal_new_list.append((k,v))
        else :
            raise UserError(_('No Data to Print'))
        return {
            'doc_model': data['model'],
            'doc_journal': journal_new_list,
            'doc_statement': statement_list,
            'doc_session': session_list,
            'doc_ticket_list': line_list1
        }


    # @api.model
    # def render_html(self, docids, data=None):
    #     Report = self.env['report']
    #     report = Report._get_report_from_name('stock_reports.gate_entry_register_report_template')
    #     docargs = {
    #         'doc_ids': self._ids,
    #         'doc_model': report.model,
    #         'docs': self,
    #         # 'get_report_values':self.get_report_values,
    #
    #     }
    #     return Report.render('stock_reports.gate_entry_register_report_template', docargs)


class PosSessionSummaryReport(models.AbstractModel):
    _name = 'report.sr_print_pos_session.pos_session_report_template'

    @api.model
    def _get_report_values(self, docids, data=None):
        # print ("get report valuesssssssssssssssss newwwwwwwwwwws",self,docids,data)
        doc = []
        session_ids = data.get('ids')
        # print ("session idssssssssssssss",session_ids,type(session_ids))
        session_list=[]
        journal_list=[]
        journal_new_list=[]
        statement_list=[]
        dict_journal={}
        line_list=[]
        line_list1=[]
        pos_order_list=[]
        if len(docids) > 0:
            session_ids = self.env['pos.session'].browse(docids)
            for val in  session_ids:
                # print ("vallllllllllll",val)
                session_list.append(val)
                if val.statement_ids :
                    for stat in val.statement_ids :
                        for line1 in stat.line_ids:
                            line_list.append(line1)
                            # print ("line1.pos_statement_id.id",line1,line1.pos_statement_id.id,line1.amount)
                            # print ("pos_order_list",pos_order_list)
                            if line1.amount > 0 and line1.ref and line1.pos_statement_id.id not in pos_order_list:
                                line_list1.append(line1)
                                pos_order_list.append(line1.pos_statement_id.id)

                        # print ("dict journal 111111111111",dict_journal)
                        # print ("stat journallllllll ",stat.journal_id,journal_list)
                        if stat.journal_id.id in journal_list :
                            tot = dict_journal[stat.journal_id.name]
                            amt=0.0
                            if stat.line_ids:
                                for line in stat.line_ids:
                                    # if line.amount > 0 and line.ref:
                                    if line.ref:
                                        amt = amt + line.amount
                            new_tot = tot  + amt
                            # print ("new toottttttttttt iffffffffff",tot,amt,new_tot)
                            dict_journal.update({stat.journal_id.name:new_tot})
                        else :
                            amt=0.0
                            if stat.line_ids:
                                for line in stat.line_ids:
                                    # if line.amount > 0 and line.ref:
                                    if line.ref:
                                        amt = amt + line.amount
                            # print("new toottttttttttt elseee", stat.journal_id.name, amt)
                            dict_journal[stat.journal_id.name] = amt
                            journal_list.append(stat.journal_id.id)
                        # print ("dict journal",dict_journal)
                        # print ("journal list",journal_list)
                        if stat.line_ids :
                            for line in stat.line_ids :
                                statement_list.append(line)
            # print ("session listttttttttttttt",session_list)
            # print ("statementtttttttttt liisttttttttttttt",statement_list)
            # print ("dict journalllllllllllllll",dict_journal)
            if dict_journal :
                for k,v in dict_journal.items():
                    journal_new_list.append((k,v))
        else :
            raise UserError(_('No Data to Print'))
        return {
            # 'doc_model': data['model'],
            'doc_journal': journal_new_list,
            'doc_statement': statement_list,
            'doc_session': session_list,
            'docs': session_ids,
            'doc_ticket_list': line_list1
        }



# class Accounbankstatementline(models.Model):
#     inherit="account.bank.statement.line"

#     def my_function(self):
#         print ("my functionnnnnnnnnnnnnn")

# code commented by Ajay 7jan
# class PosOrderReport(models.Model):
#     """
#     abhishek
#     Used to add "Product Attribute" in Point of Sale/Reporting/Order pivot view
#     """
#     _inherit = "report.pos.order"
    
#     product_link_stock_id = fields.Many2one('product.product', string='Product Extras', readonly=True)
    
#     def _select(self):
#         return """
#             SELECT
#                 MIN(l.id) AS id,
#                 COUNT(*) AS nbr_lines,
#                 s.date_order AS date,
#                 SUM(l.qty) AS product_qty,
#                 SUM(l.qty * l.price_unit) AS price_sub_total,
#                 SUM((l.qty * l.price_unit) * (100 - l.discount) / 100) AS price_total,
#                 SUM((l.qty * l.price_unit) * (l.discount / 100)) AS total_discount,
#                 (SUM(l.qty*l.price_unit)/SUM(l.qty * u.factor))::decimal AS average_price,
#                 SUM(cast(to_char(date_trunc('day',s.date_order) - date_trunc('day',s.create_date),'DD') AS INT)) AS delay_validation,
#                 s.id as order_id,
#                 s.partner_id AS partner_id,
#                 s.state AS state,
#                 s.user_id AS user_id,
#                 s.location_id AS location_id,
#                 s.company_id AS company_id,
#                 s.sale_journal AS journal_id,
#                 l.product_id AS product_id,
#                 pt.categ_id AS product_categ_id,
#                 p.product_tmpl_id,
#                 ps.config_id,
#                 pt.pos_categ_id,
#                 pc.stock_location_id,
#                 s.pricelist_id,
#                 s.session_id,
#                 s.invoice_id IS NOT NULL AS invoiced,
#                 pv.product_id as product_link_stock_id
#         """

#     def _from(self):
#         return """
#             FROM pos_order_line AS l
#                 LEFT JOIN pos_order s ON (s.id=l.order_id)
#                 LEFT JOIN product_product p ON (l.product_id=p.id)
#                 LEFT JOIN product_template pt ON (p.product_tmpl_id=pt.id)
#                 LEFT JOIN product_uom u ON (u.id=pt.uom_id)
#                 LEFT JOIN pos_session ps ON (s.session_id=ps.id)
#                 LEFT JOIN pos_config pc ON (ps.config_id=pc.id)
#                 LEFT JOIN product_variant pv ON (pv.product_tmpl_id=pt.id)
#                 LEFT JOIN product_product pp ON (pp.id=pv.product_id)
#         """

#     def _group_by(self):
#         return """
#             GROUP BY
#                 s.id, s.date_order, s.partner_id,s.state, pt.categ_id,
#                 s.user_id, s.location_id, s.company_id, s.sale_journal,
#                 s.pricelist_id, s.invoice_id, s.create_date, s.session_id,
#                 l.product_id,
#                 pt.categ_id, pt.pos_categ_id,
#                 p.product_tmpl_id,
#                 ps.config_id,
#                 pc.stock_location_id,
#                 pv.product_id
                
#         """

#     def _having(self):
#         return """
#             HAVING
#                 SUM(l.qty * u.factor) != 0
#         """

#     @api.model_cr
#     def init(self):
#         tools.drop_view_if_exists(self._cr, self._table)
#         self._cr.execute("""
#             CREATE OR REPLACE VIEW %s AS (
#                 %s
#                 %s
#                 %s
#                 %s
#             )
#         """ % (self._table, self._select(), self._from(), self._group_by(),self._having())
#         )


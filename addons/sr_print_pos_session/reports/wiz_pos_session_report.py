# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

import itertools
import time
from odoo import api, fields, models, tools, _
from odoo.addons import decimal_precision as dp
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT, DEFAULT_SERVER_DATE_FORMAT
from odoo.exceptions import ValidationError, RedirectWarning, except_orm




# class PosSessionReportWizard(models.TransientModel):
#     _name = "pos.session.report.wizard"
#     _description = "Requisition Create Wizard"
#
#
#     name = fields.Char('Name')
#     pos_session_ids = fields.Many2many('pos.session','pos_sesion_rel','pos_wiz_id','session_id', string='Pos Session')
#     session_date = fields.Date('Session Date')
#     pos_config_id = fields.Many2one('pos.config','POS')
#     # type = fields.Selection([('direct', 'Direct'), ('arc', 'ARC'), ('open_order', 'Open Order')], string="Type",
#     #                         help='ARC: When flow running from MRS to PO.\n'
#     #                              ' Direct: When creted for ARC Item and Flow running from MRS to PR and then PO created directly from PR')
#     # session_id = fields.Many2one('pos.session', 'Vendor')
#     session_ids = fields.Many2many('pos.session','pos_sesion_wiz_rel','pos_wizard_id','session_new_id',string='Session ids')
#     # pr_or_po = fields.Selection([('pr','PR'), ('po', 'PO')], string='Create PR or PO', help="PR: Pr created, PO: Adhoc Order created if type is Direct or ARC Order created if type is ARC")
#
#     # @api.model
#     # def default_get(self, fields):
#     #     active_ids = self._context['active_ids']
#     #     # pr_or_po = self._context['pr_or_po']
#     #
#     #     type = ''
#     #
#     #     res = super(PosSessionReportWizard, self).default_get(fields)
#     #     if active_ids:
#     #         browseable_active_ids = self.env['pos.session'].browse(active_ids)
#     #
#     #
#     #
#     #         if 'pos_session_ids' in fields and 'pos_session_ids' not in res:
#     #             res['pos_session_ids'] = [(6,0,active_ids)]
#     #
#     #
#     #     return res
#     #
#
#     def get_report(self):
#         """Call when button 'Print' clicked.
#         """
#         # if self.till_date < self.from_date:
#         #     raise UserError(_('Till Date Can\'t be smaller the from date !'))
#         session_obj = self.env['pos.session']
#         print ("selfffffffffffffffff",self,self.session_date,self.pos_config_id)
#         session_list=[]
#         if self.session_date and self.pos_config_id :
#             self.env.cr.execute(
#                 """select id from pos_session as sess where sess.config_id=%s and TO_CHAR(sess.start_at, 'YYYY-MM-DD') = %s""",
#                 (self.pos_config_id.id,str(self.session_date)))
#             match_recs_session = self.env.cr.dictfetchall()
#             # session_ids = session_obj.search([('start_at','=',self.session_date),('config_id','=',self.pos_config_id.id)])
#             # print ("match_recs_sessionmatch_recs_session",match_recs_session)
#             if len(match_recs_session) > 0:
#                 for val in match_recs_session :
#                     # print ("valllllllllll",val)
#                     session_list.append(val.get('id'))
#             else :
#                 raise ValidationError(_('No data to Print !'))
#         # print ("session listttttttttttt",session_list)
#         data = {
#             'ids': session_list,
#             # 'category': self.category.id,
#             # 'from_date': self.from_date,
#             # 'till_date': self.till_date,
#             # 'type': self.type,
#             # 'group_by_id': self.group_by_id,
#             'model': self._name,
#
#         }
#         return self.env.ref('sr_print_pos_session.sr_pos_session_detail_report').report_action(self,
#                                                                                                  data=data)

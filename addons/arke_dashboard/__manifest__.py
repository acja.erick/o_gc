# -*- coding: utf-8 -*-

{
    "name" : "Arke Dashboard",
    "version" : "1.0",
    "depends" : ['base','purchase','stock','purchase_extension','web'],
    "category" : "Purchase",
    "description": """
 This module adds the Dashboard for user wise and activity wise.
  * 
""",
    "init_xml" : [],
    "demo_xml" : [],
    "data": [
            "views/arke_dashboard_view.xml",
            "views/arke_dashboard.xml",
            "views/purchase_dashboard_value_line_view.xml",
            "menu.xml",
            "security/ir.model.access.csv",
            ],
    'qweb': [
        "static/src/xml/template.xml",
    ],
    "active": False,
    "installable": True
}

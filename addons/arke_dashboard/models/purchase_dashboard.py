import itertools
import psycopg2
from odoo import api, fields, models, tools, _
from odoo.exceptions import ValidationError, RedirectWarning, except_orm
from odoo.tools import pycompat
from datetime import datetime
from lxml import etree


class PurchaseDashBoard(models.Model):
    _description = "Purchase Dashboard"
    _name= "purchase.dashboard"

    name = fields.Char('Name',size=256,required="1",default=fields.Datetime.now)
    company_id = fields.Many2one('res.company','Company',default=lambda self: self.env.user.company_id.id)

    def get_product_list(self,all_mrs):
        print ("get prod listttttttt",all_mrs)
        product_list=[]
        if all_mrs :
            for val in all_mrs:
                for line in val.material_req_slip_line_ids:
                    print ("lineeeeeeee",line,line.template_id)
                    if line.template_id.id not in product_list:
                        product_list.append(line.template_id.id)
        return product_list

    def get_available_product(self,all_mrs):
        print("get avaliable listttttttt", all_mrs)
        avail_prod_list=[]
        prod_list = self.get_product_list(all_mrs)
        if len(prod_list) > 0 :
            for val in self.env['product.template'].browse(prod_list) :
                print ("vall in get availaable",val,val.name,val.qty_available_product)
                if val.qty_available_product > 0 :
                    avail_prod_list.append(val)
        return avail_prod_list



    def get_widget_master(self,company_id):
        # print ("get widget masterrrrr",self,self._context)
        widget_master_id = self.env['dashboard.widget.master'].search([('company_id','=',company_id.id)],order='widget_no')
        # print ("widget master id",widget_master_id)
        return widget_master_id

    def get_mrs_details(self,widget,company_id):
        print ("get mrs detailssssssssssssssssss",self,self._context,self.env.user,self.env.user.warehouse_id)
        res = {}
        mrs_list=[]
        if widget.widget_master_lines :
            for new in widget.widget_master_lines :
                print ("user groupppppppppppppp",self.env.user.has_group('stock.group_stock_manager'))
                if self.env.user.has_group('stock.group_stock_manager'):
                    if new.query_type == 'query':
                        # self.env.cr.execute(
                        #     "select id from material_req_slip where state='draft' "
                        #     " and CURRENT_TIMESTAMP >= create_date + interval '2' day and create_uid = %s and company_id = %s",
                        #     [employee_id1.user_id.id, company_id.id])
                        mrs_query= new.widget_query
                        # print ("mrs queryyyyyy",mrs_query)
                        mrs_query = mrs_query + ' and company_id = %s'
                        print ("mrs queryyyyyyyyyyyyy",mrs_query)
                        try:
                            self.env.cr.execute(mrs_query,[company_id.id])
                            match_recs = self.env.cr.dictfetchall()
                            if match_recs:
                                print ("match reccccccc",match_recs,len(match_recs))
                                mrs_tup = (new.name, len(match_recs),new.id,widget.id,'MRS')
                                mrs_list.append(mrs_tup)
                                # for dict in match_recs:
                                #     ids.append(dict.get('id'))
                            else :
                                mrs_tup = (new.name, 0,new.id,widget.id,'MRS')
                                mrs_list.append(mrs_tup)
                        except:
                            raise ValidationError(_('Query does not exist !'))
                    elif new.query_type == 'filter' :
                        filter_list=[]
                        if new.widget_filter_lines :
                            for wid_filter in new.widget_filter_lines :
                                # print ("wid_filterwid_filter",wid_filter)
                                filter = self.get_filter_condition(wid_filter)
                                filter_list.append(filter)
                            filter_list.append(('company_id', '=', company_id.id))
                            # filter_list=[('partner_id','=',14),('id','>',0)]
                            print("rfq filter_list", filter_list)
                            mrs_ids= self.env['material.req.slip'].search(filter_list)
                            # print ("rfq idsssssss",rfq_ids,len(rfq_ids))
                            if len(mrs_ids) > 0 :
                                mrs_tup = (new.name, len(mrs_ids), new.id,widget.id,'MRS')
                                mrs_list.append(mrs_tup)
                            else :
                                mrs_tup = (new.name, 0, new.id,widget.id,'MRS')
                                mrs_list.append(mrs_tup)
                else :
                    if new.query_type == 'query':
                        # self.env.cr.execute(
                        #     "select id from material_req_slip where state='draft' "
                        #     " and CURRENT_TIMESTAMP >= create_date + interval '2' day and create_uid = %s and company_id = %s",
                        #     [employee_id1.user_id.id, company_id.id])
                        mrs_query= new.widget_query
                        # print ("mrs queryyyyyy",mrs_query)
                        mrs_query = mrs_query + ' and company_id = %s' + ' and warehouse_id = %s'
                        print ("mrs queryyyyyyyyyyyyy",mrs_query)
                        try:
                            self.env.cr.execute(mrs_query,[company_id.id,self.env.user.warehouse_id.id])
                            match_recs = self.env.cr.dictfetchall()
                            if match_recs:
                                print ("match reccccccc",match_recs,len(match_recs))
                                mrs_tup = (new.name, len(match_recs),new.id,widget.id,'MRS')
                                mrs_list.append(mrs_tup)
                                # for dict in match_recs:
                                #     ids.append(dict.get('id'))
                            else :
                                mrs_tup = (new.name, 0,new.id,widget.id,'MRS')
                                mrs_list.append(mrs_tup)
                        except:
                            raise ValidationError(_('Query does not exist !'))
                    elif new.query_type == 'filter' :
                        filter_list=[]
                        if new.widget_filter_lines :
                            for wid_filter in new.widget_filter_lines :
                                # print ("wid_filterwid_filter",wid_filter)
                                filter = self.get_filter_condition(wid_filter)
                                filter_list.append(filter)
                            filter_list.append(('company_id', '=', company_id.id))
                            # filter_list=[('partner_id','=',14),('id','>',0)]
                            print("rfq filter_list", filter_list)
                            mrs_ids= self.env['material.req.slip'].search(filter_list)
                            # print ("rfq idsssssss",rfq_ids,len(rfq_ids))
                            if len(mrs_ids) > 0 :
                                mrs_tup = (new.name, len(mrs_ids), new.id,widget.id,'MRS')
                                mrs_list.append(mrs_tup)
                            else :
                                mrs_tup = (new.name, 0, new.id,widget.id,'MRS')
                                mrs_list.append(mrs_tup)


        # print ("mrs listttttt",mrs_list)
        return mrs_list

    def get_rfq_details(self, widget,company_id):
        res = {}
        rfq_list = []
        rfq_ids = self.env['request.for.quotation'].search([('id', '>', 0),('company_id','=',company_id.id)])
        if len(rfq_ids) > 0:
            total_rfq = len(rfq_ids)
        else:
            total_rfq = 0
        # rfq_tup = ('Total RFQ', total_rfq,'',widget.id)
        # rfq_list.append(rfq_tup)
        if widget.widget_master_lines:
            for new in widget.widget_master_lines:
                if new.query_type == 'query' :
                    rfq_query = new.widget_query
                    # print("rfq queryyyyyy", rfq_query)
                    try:
                        rfq_query = rfq_query + ' and rfq.company_id = %s'
                        self.env.cr.execute(rfq_query,[company_id.id])
                        match_recs = self.env.cr.dictfetchall()
                        if match_recs:
                            # print("match reccccccc", match_recs, len(match_recs))
                            rfq_tup = (new.name, len(match_recs), new.id,widget.id,'RFQ')
                            rfq_list.append(rfq_tup)
                            # for dict in match_recs:
                            #     ids.append(dict.get('id'))
                        else:
                            rfq_tup = (new.name, 0, new.id,widget.id,'RFQ')
                            rfq_list.append(rfq_tup)
                    except:
                        raise ValidationError(_('Query does not exist !'))
                elif new.query_type == 'filter' :
                    filter_list=[]
                    if new.widget_filter_lines :
                        for wid_filter in new.widget_filter_lines :
                            # print ("wid_filterwid_filter",wid_filter)
                            filter = self.get_filter_condition(wid_filter)
                            filter_list.append(filter)
                        filter_list.append(('company_id', '=', company_id.id))
                        # filter_list=[('partner_id','=',14),('id','>',0)]
                        print("rfq filter_list", filter_list)
                        rfq_ids= self.env['request.for.quotation'].search(filter_list)
                        # print ("rfq idsssssss",rfq_ids,len(rfq_ids))
                        if len(rfq_ids) > 0 :
                            rfq_tup = (new.name, len(rfq_ids), new.id,widget.id,'RFQ')
                            rfq_list.append(rfq_tup)
                        else :
                            rfq_tup = (new.name, 0, new.id,widget.id,'RFQ')
                            rfq_list.append(rfq_tup)

        print("rfq_list listttttt", rfq_list)
        return rfq_list

    def get_pr_details(self, widget,company_id):
        res = {}
        pr_list = []
        pr_ids = self.env['purchase.req'].search([('id', '>', 0),('company_id','=',company_id.id)])
        if len(pr_ids) > 0:
            total_pr = len(pr_ids)
        else:
            total_pr = 0
        # pr_tup = ('Total PR', total_pr, '', widget.id)
        # pr_list.append(pr_tup)
        if widget.widget_master_lines:
            for new in widget.widget_master_lines:
                if new.query_type == 'query':
                    pr_query = new.widget_query
                    # print("rfq queryyyyyy", rfq_query)
                    try:
                        pr_query = pr_query + ' and pr.company_id = %s'
                        self.env.cr.execute(pr_query,[company_id.id])
                        match_recs = self.env.cr.dictfetchall()
                        if match_recs:
                            # print("match reccccccc", match_recs, len(match_recs))
                            pr_tup = (new.name, len(match_recs), new.id, widget.id,'PR')
                            pr_list.append(pr_tup)
                            # for dict in match_recs:
                            #     ids.append(dict.get('id'))
                        else:
                            pr_tup = (new.name, 0, new.id, widget.id,'PR')
                            pr_list.append(pr_tup)
                    except:
                        raise ValidationError(_('Query does not exist !'))
                elif new.query_type == 'filter':
                    filter_list = []
                    if new.widget_filter_lines:
                        for wid_filter in new.widget_filter_lines:
                            # print ("wid_filterwid_filter",wid_filter)
                            filter = self.get_filter_condition(wid_filter)
                            filter_list.append(filter)
                        filter_list.append(('company_id', '=', company_id.id))
                        # filter_list=[('partner_id','=',14),('id','>',0)]
                        print("rfq filter_list", filter_list)
                        pr_ids = self.env['purchase.req'].search(filter_list)
                        # print ("rfq idsssssss",rfq_ids,len(rfq_ids))
                        if len(pr_ids) > 0:
                            pr_tup = (new.name, len(pr_ids), new.id, widget.id,'PR')
                            pr_list.append(pr_tup)
                        else:
                            pr_tup = (new.name, 0, new.id, widget.id,'PR')
                            pr_list.append(pr_tup)

        print("pr list listttttt", pr_list)
        return pr_list

    def get_po_details(self, widget,company_id):
        res = {}
        pr_list = []
        pr_ids = self.env['purchase.order'].search([('id', '>', 0),('company_id','=',company_id.id)])
        if len(pr_ids) > 0:
            total_pr = len(pr_ids)
        else:
            total_pr = 0
        # pr_tup = ('Total PO', total_pr, '', widget.id)
        # pr_list.append(pr_tup)
        if widget.widget_master_lines:
            for new in widget.widget_master_lines:
                if new.query_type == 'query':
                    pr_query = new.widget_query
                    # print("rfq queryyyyyy", rfq_query)
                    try:
                        pr_query = pr_query + ' and po.company_id = %s'
                        self.env.cr.execute(pr_query,[company_id.id])
                        match_recs = self.env.cr.dictfetchall()
                        if match_recs:
                            # print("match reccccccc", match_recs, len(match_recs))
                            pr_tup = (new.name, len(match_recs), new.id, widget.id,'PO')
                            pr_list.append(pr_tup)
                            # for dict in match_recs:
                            #     ids.append(dict.get('id'))
                        else:
                            pr_tup = (new.name, 0, new.id, widget.id,'PO')
                            pr_list.append(pr_tup)
                    except:
                        raise ValidationError(_('Query does not exist !'))
                elif new.query_type == 'filter':
                    filter_list = []
                    if new.widget_filter_lines:
                        for wid_filter in new.widget_filter_lines:
                            # print ("wid_filterwid_filter",wid_filter)
                            filter = self.get_filter_condition(wid_filter)
                            filter_list.append(filter)
                        filter_list.append(('company_id', '=', company_id.id))
                        # filter_list=[('partner_id','=',14),('id','>',0)]
                        print("rfq filter_list", filter_list)
                        pr_ids = self.env['purchase.order'].search(filter_list)
                        # print ("rfq idsssssss",rfq_ids,len(rfq_ids))
                        if len(pr_ids) > 0:
                            pr_tup = (new.name, len(pr_ids), new.id, widget.id,'PO')
                            pr_list.append(pr_tup)
                        else:
                            pr_tup = (new.name, 0, new.id, widget.id,'PO')
                            pr_list.append(pr_tup)

        print("pr list listttttt", pr_list)
        return pr_list

    def get_material_issue_details(self, widget,company_id):
        res = {}
        pr_list = []
        pr_ids = self.env['material.issue'].search([('id', '>', 0),('company_id','=',company_id.id)])
        if len(pr_ids) > 0:
            total_pr = len(pr_ids)
        else:
            total_pr = 0
        # pr_tup = ('Total PO', total_pr, '', widget.id)
        # pr_list.append(pr_tup)
        if widget.widget_master_lines:
            for new in widget.widget_master_lines:
                if new.query_type == 'query':
                    pr_query = new.widget_query
                    # print("rfq queryyyyyy", rfq_query)
                    try:
                        pr_query = pr_query + ' and company_id = %s'
                        self.env.cr.execute(pr_query,[company_id.id])
                        match_recs = self.env.cr.dictfetchall()
                        if match_recs:
                            print("match reccccccc", match_recs, len(match_recs))
                            pr_tup = (new.name, len(match_recs), new.id, widget.id,'Issue')
                            pr_list.append(pr_tup)
                            # for dict in match_recs:
                            #     ids.append(dict.get('id'))
                        else:
                            pr_tup = (new.name, 0, new.id, widget.id,'Issue')
                            pr_list.append(pr_tup)
                    except:
                        raise ValidationError(_('Query does not exist !'))
                elif new.query_type == 'filter':
                    filter_list = []
                    if new.widget_filter_lines:
                        for wid_filter in new.widget_filter_lines:
                            # print ("wid_filterwid_filter",wid_filter)
                            filter = self.get_filter_condition(wid_filter)
                            filter_list.append(filter)
                        filter_list.append(('company_id', '=', company_id.id))
                        # filter_list=[('partner_id','=',14),('id','>',0)]
                        print("rfq filter_list", filter_list)
                        pr_ids = self.env['purchase.order'].search(filter_list)
                        # print ("rfq idsssssss",rfq_ids,len(rfq_ids))
                        if len(pr_ids) > 0:
                            pr_tup = (new.name, len(pr_ids), new.id, widget.id,'Issue')
                            pr_list.append(pr_tup)
                        else:
                            pr_tup = (new.name, 0, new.id, widget.id,'Issue')
                            pr_list.append(pr_tup)

        print("pr list listttttt", pr_list)
        return pr_list

    def get_quotation_details(self, widget,company_id):
        res = {}
        pr_list = []
        pr_ids = self.env['supplier.quotation'].search([('id', '>', 0),('company_id','=',company_id.id)])
        if len(pr_ids) > 0:
            total_pr = len(pr_ids)
        else:
            total_pr = 0
        # pr_tup = ('Total Quotation', total_pr, '', widget.id)
        # pr_list.append(pr_tup)
        if widget.widget_master_lines:
            for new in widget.widget_master_lines:
                if new.query_type == 'query':
                    pr_query = new.widget_query
                    # print("rfq queryyyyyy", rfq_query)
                    try:
                        pr_query = pr_query + ' and quot.company_id = %s'
                        self.env.cr.execute(pr_query,[company_id.id])
                        match_recs = self.env.cr.dictfetchall()
                        if match_recs:
                            # print("match reccccccc", match_recs, len(match_recs))
                            pr_tup = (new.name, len(match_recs), new.id, widget.id,'Quotation')
                            pr_list.append(pr_tup)
                            # for dict in match_recs:
                            #     ids.append(dict.get('id'))
                        else:
                            pr_tup = (new.name, 0, new.id, widget.id,'Quotation')
                            pr_list.append(pr_tup)
                    except:
                        raise ValidationError(_('Query does not exist !'))
                elif new.query_type == 'filter':
                    filter_list = []
                    if new.widget_filter_lines:
                        filter_list = [('company_id', '=', company_id.id)]
                        for wid_filter in new.widget_filter_lines:
                            # print ("wid_filterwid_filter",wid_filter)
                            filter = self.get_filter_condition(wid_filter)
                            filter_list.append(filter)
                        filter_list.append(('company_id', '=', company_id.id))
                        # filter_list=[('partner_id','=',14),('id','>',0)]
                        print("rfq filter_list", filter_list)
                        pr_ids = self.env['supplier.quotation'].search(filter_list)
                        # print ("rfq idsssssss",rfq_ids,len(rfq_ids))
                        if len(pr_ids) > 0:
                            pr_tup = (new.name, len(pr_ids), new.id, widget.id,'Quotation')
                            pr_list.append(pr_tup)
                        else:
                            pr_tup = (new.name, 0, new.id, widget.id,'Quotation')
                            pr_list.append(pr_tup)

        print("pr list listttttt", pr_list)
        return pr_list



    def get_purchase_dashboard_value(self):
        print("get purchase dashboard value function",self,self._context)
        res = {}
        details=''
        # if self :
        #     user_id = self.get_widget_master()
        user_id = self.env.user
        company_id = user_id.company_id
        # print ("user idddddddd",user_id,user_id.company_id)
        # print ("aaaaaa",a)
        widget_ids = self.get_widget_master(company_id)
        if len(widget_ids) > 0 :
            for val in widget_ids :
                # print ("valllllll",val.widget_name)
                if val.widget_name == 'mrs':
                    details = self.get_mrs_details(val,company_id)
                elif val.widget_name == 'rfq':
                    details = self.get_rfq_details(val,company_id)
                elif val.widget_name == 'pr':
                    details = self.get_pr_details(val,company_id)
                elif val.widget_name == 'quotation':
                    details = self.get_quotation_details(val,company_id)
                elif val.widget_name == 'purchase_order':
                    details = self.get_po_details(val,company_id)
                elif val.widget_name == 'material_issue':
                    details = self.get_material_issue_details(val,company_id)
                else :
                    details=[]
                res[val.widget_no]= details
                # res={1 : details }

        print("res in get purchase dash board", res)
        return res


    def get_pending_mrs(self,all_mrs):
        """
        Return PR from button in Control Panel of Purchase Requisition
        """
        result = {}
        all_mrs_ids = []
        all_mrs = self.env['material.req.slip'].search([('state', '=', 'draft')])
        if all_mrs:
            all_mrs_ids = all_mrs.ids
        # print("SSSSSSSSSSSSSSSSSS", all_mrs)
        action = self.env.ref('purchase_extension.action_material_req_slip')
        result = action.read()[0]
        res = self.env.ref('purchase_extension.view_material_req_slip_tree', False)
        res_form = self.env.ref('purchase_extension.view_material_req_slip_form', False)
        # print ("KKKKKKKKKKKKKK",res,res.id)
        result['views'] = [(res and res.id or False, 'list'), (res_form and res_form.id or False, 'form')]
        result['domain'] = [('id', 'in', tuple(all_mrs_ids))]
        result['target']='new'
        return result

    def get_confirmed_mrs(self,all_mrs):
        """
        Return PR from button in Control Panel of Purchase Requisition
        """
        result = {}
        all_mrs_ids = []
        all_mrs = self.env['material.req.slip'].search([('state', 'in', ('confirm','in_progress'))])
        if all_mrs:
            all_mrs_ids = all_mrs.ids
        # print("SSSSSSSSSSSSSSSSSS", all_mrs_ids)
        action = self.env.ref('purchase_extension.action_material_req_slip')
        result = action.read()[0]
        res = self.env.ref('purchase_extension.view_material_req_slip_tree', False)
        res_form = self.env.ref('purchase_extension.view_material_req_slip_form', False)
        # print ("KKKKKKKKKKKKKK",res,res.id)
        result['views'] = [(res and res.id or False, 'list'), (res_form and res_form.id or False, 'form')]
        result['domain'] = [('id', 'in', tuple(all_mrs_ids))]
        result['target']='new'
        return result

    def get_completed_mrs(self,all_mrs):
        """
        Return PR from button in Control Panel of Purchase Requisition
        """
        result = {}
        all_mrs_ids = []
        all_mrs = self.env['material.req.slip'].search([('state', '=', 'done')])
        if all_mrs:
            all_mrs_ids = all_mrs.ids
        # print("SSSSSSSSSSSSSSSSSS", all_mrs_ids)
        action = self.env.ref('purchase_extension.action_material_req_slip')
        result = action.read()[0]
        res = self.env.ref('purchase_extension.view_material_req_slip_tree', False)
        res_form = self.env.ref('purchase_extension.view_material_req_slip_form', False)
        # print ("KKKKKKKKKKKKKK",res,res.id)
        result['views'] = [(res and res.id or False, 'list'), (res_form and res_form.id or False, 'form')]
        result['domain'] = [('id', 'in', tuple(all_mrs_ids))]
        result['target']='new'
        return result

    def get_issue_mrs(self,all_mrs):
        """
        Return PR from button in Control Panel of Purchase Requisition
        """
        print ("get issue mrssssssssssssssss")
        result = {}
        all_mrs_ids = []
        all_mrs = self.env['material.req.slip'].search([('state', '=', 'issue')])
        if all_mrs:
            all_mrs_ids = all_mrs.ids
        print("SSSSSSSSSSSSSSSSSS", all_mrs_ids)
        action = self.env.ref('purchase_extension.action_material_req_slip')
        result = action.read()[0]
        res = self.env.ref('purchase_extension.view_material_req_slip_tree', False)
        res_form = self.env.ref('purchase_extension.view_material_req_slip_form', False)
        # print ("KKKKKKKKKKKKKK",res,res.id)
        result['views'] = [(res and res.id or False, 'list'), (res_form and res_form.id or False, 'form')]
        result['domain'] = [('id', 'in', tuple(all_mrs_ids))]
        result['target']='new'
        return result

    def get_pending_issue(self,all_mrs):
        """
        Return PR from button in Control Panel of Purchase Requisition
        """
        result = {}
        all_mrs_ids = []
        all_mrs = self.env['material.issue'].search([('state', '=', 'draft')])
        if all_mrs:
            all_mrs_ids = all_mrs.ids
        # print("SSSSSSSSSSSSSSSSSS", all_mrs_ids)
        action = self.env.ref('purchase_extension.action_material_issue')
        result = action.read()[0]
        res = self.env.ref('purchase_extension.view_material_issue_tree', False)
        res_form = self.env.ref('purchase_extension.view_material_issue_form', False)
        # print ("KKKKKKKKKKKKKK",res,res.id)
        result['views'] = [(res and res.id or False, 'list'), (res_form and res_form.id or False, 'form')]
        result['domain'] = [('id', 'in', tuple(all_mrs_ids))]
        result['target']='new'
        return result

    def get_issued_mrs(self,all_mrs):
        """
        Return PR from button in Control Panel of Purchase Requisition
        """
        result = {}
        all_mrs_ids = []
        all_mrs = self.env['material.issue'].search([('state', '=', 'done')])
        if all_mrs:
            all_mrs_ids = all_mrs.ids
        # print("SSSSSSSSSSSSSSSSSS", all_mrs_ids)
        action = self.env.ref('purchase_extension.action_material_issue')
        result = action.read()[0]
        res = self.env.ref('purchase_extension.view_material_issue_tree', False)
        res_form = self.env.ref('purchase_extension.view_material_issue_form', False)
        # print ("KKKKKKKKKKKKKK",res,res.id)
        result['views'] = [(res and res.id or False, 'list'), (res_form and res_form.id or False, 'form')]
        result['domain'] = [('id', 'in', tuple(all_mrs_ids))]
        result['target']='new'
        return result

    def get_action_rfq(self, rfq_id):
        """
        Return RFQ from Quotation Comparision Javascript form
        """
        result = {}
        if rfq_id :
            rfq_ids= rfq_id.ids
        action = self.env.ref('purchase_extension.action_request_for_quotation')
        result = action.read()[0]
        res = self.env.ref('purchase_extension.view_request_for_quotation_tree', False)
        res_form = self.env.ref('purchase_extension.view_request_for_quotation_form', False)
        result['views'] = [(res and res.id or False, 'list'),(res_form and res_form.id or False, 'form')]
        result['domain'] = [('id', 'in', tuple(rfq_ids))]
        result['target'] = 'new'
        return result

    def get_action_mrs(self, rfq_id):
        """
        Return RFQ from Quotation Comparision Javascript form
        """
        result = {}
        if rfq_id :
            rfq_ids= rfq_id.ids
        action = self.env.ref('purchase_extension.action_material_req_slip')
        result = action.read()[0]
        res = self.env.ref('purchase_extension.view_material_req_slip_tree', False)
        res_form = self.env.ref('purchase_extension.view_material_req_slip_form', False)
        result['views'] = [(res and res.id or False, 'list'),(res_form and res_form.id or False, 'form')]
        result['domain'] = [('id', 'in', tuple(rfq_ids))]
        result['target'] = 'new'
        return result

    def get_action_pr(self, rfq_id):
        """
        Return RFQ from Quotation Comparision Javascript form
        """
        result = {}
        if rfq_id :
            rfq_ids= rfq_id.ids
        action = self.env.ref('purchase_extension.action_purchase_req')
        result = action.read()[0]
        res = self.env.ref('purchase_extension.view_purchase_req_tree', False)
        res_form = self.env.ref('purchase_extension.view_purchase_req_form', False)
        result['views'] = [(res and res.id or False, 'list'),(res_form and res_form.id or False, 'form')]
        result['domain'] = [('id', 'in', tuple(rfq_ids))]
        result['target'] = 'new'
        return result

    def get_action_po(self, rfq_id):
        """
        Return RFQ from Quotation Comparision Javascript form
        """
        result = {}
        if rfq_id :
            rfq_ids= rfq_id.ids
        action = self.env.ref('purchase_extension.purchase_order_action_inherit_btn')
        result = action.read()[0]
        res = self.env.ref('purchase_extension.purchase_order_ext_inherit_tree', False)
        res_form = self.env.ref('purchase_extension.purchase_order_form_inherit_ext', False)
        result['views'] = [(res and res.id or False, 'list'),(res_form and res_form.id or False, 'form')]
        result['domain'] = [('id', 'in', tuple(rfq_ids))]
        result['target'] = 'new'
        return result

    def get_action_quotation(self, rfq_id):
        """
        Return RFQ from Quotation Comparision Javascript form
        """
        result = {}
        if rfq_id :
            rfq_ids= rfq_id.ids
        action = self.env.ref('purchase_extension.action_supplier_quotation')
        result = action.read()[0]
        res = self.env.ref('purchase_extension.view_supplier_quotation_tree', False)
        res_form = self.env.ref('purchase_extension.view_supplier_quotation_form', False)
        result['views'] = [(res and res.id or False, 'list'),(res_form and res_form.id or False, 'form')]
        result['domain'] = [('id', 'in', tuple(rfq_ids))]
        result['target'] = 'new'
        return result

    def get_purchase_dashboard_report(self, all_mrs,field_list):
        """
        Return PR from button in Control Panel of Purchase Requisition
        """
        result = {}
        approved_pr_ids = []
        # print("SSSSSSSSSSSSSSSSSS", all_mrs)
        action = self.env.ref('arke_dashboard.purchase_dashboard_report_action')
        result = action.read()[0]
        res = self.env.ref('arke_dashboard.view_purchase_dashboard_report_tree', False)
        print ("res in get purchse",res)
        # context= self._context
        # context.update({'field_list':field_list})
        # self.with_context(field_list=field_list)
        # self = self.with_context(field_list=field_list)
        self = self.with_context(field_list=field_list).create({'field_list':field_list})
        print ("selffffffff",self)
        result1 = self.env['purchase.dashboard.report'].fields_view_get(view_id=None, view_type='tree', toolbar=field_list,
                                                                        submenu=field_list)
        print ("resulttttttt111 in purchase dashboard btn",result1)
        print ("res in purchase dahboard btn",res)
        # res_form = self.env.ref('purchase_extension.view_material_req_slip_form', False)
        # print ("KKKKKKKKKKKKKK",res,res.id)
        result['views'] = [(res and res.id or False, 'list')]
        result['domain'] = [('id', 'in', tuple(all_mrs))]
        result['target'] = 'new'
        result1['domain'] = [('id', 'in', tuple(all_mrs))]
        result1['target'] = 'new'
        return result

    def get_field_values(self,no,value):
        # print ("get field valueee",no,value,type(no))
        if type(value)== str and 'MRS' in str(value) and 'mrs_date' not in str(value):
            field_generated = 'mrs_id'
            mrs_id = self.env['material.req.slip'].search([('name', '=', value)])
            # value=mrs_id.id
            if mrs_id :
                dict1 = {field_generated: mrs_id.id}
            else :
                dict1 = {field_generated: ''}
        else :
            field_generated = 'field_' + str(no +1)
        # print ("field generated",field_generated)
            dict1={field_generated:value}
        print ("dict1111111",dict1)
        return dict1

    def get_field_values_rfq(self,no,value):
        print ("get field valueee",no,value,type(no))
        if type(value)== str and 'RFQ' in str(value) and 'rfq_date' not in str(value):
            field_generated = 'rfq_id'
            rfq_id = self.env['request.for.quotation'].search([('name', '=', value)])
            # value=mrs_id.id
            if rfq_id :
                dict1 = {field_generated: rfq_id.id}
            else :
                dict1 = {field_generated: ''}
        else :
            field_generated = 'field_' + str(no +1)
        # print ("field generated",field_generated)
            dict1={field_generated:value}
        print ("dict1111111",dict1)
        return dict1

    def get_filter_symbol(self,condi):
        symbol=''
        if condi == 'contain':
            symbol = 'in'
        elif condi == 'not_contain' :
            symbol = 'not in'
        elif condi == 'is_equal' :
            symbol = '='
        elif condi == 'not_equal' :
            symbol = '!='
        elif condi == 'greater_then' :
            symbol = '>'
        elif condi == 'greater_equal_to' :
            symbol = '>='
        elif condi == 'less_then' :
            symbol = '<'
        elif condi == 'less_equal_to' :
            symbol = '<='
        return symbol


    def get_filter_condition(self,widget_filter):
        fil_field= widget_filter.filter_field
        fil_condi = self.get_filter_symbol(widget_filter.filter_selection)
        print ("get filter field",fil_field)
        if fil_field.ttype =='many2one' :
            search_id = self.env[str(fil_field.relation)].search([('name','=',widget_filter.value)])
            print ("search idddddd",search_id)
            filter_tupple = (fil_field.name,fil_condi, search_id.id)
        elif fil_field.ttype in ('datetime','date'):
            print("field date timeeeeee",widget_filter.value,type(widget_filter.value))
            filter_tupple = (fil_field.name, fil_condi, widget_filter.value)
            # print ("aaaaaaa",a)
        elif fil_field.ttype == 'selection' :
            filter_tupple= (fil_field.name,'=',widget_filter.value)
        elif fil_field.ttype in ('datetime','date','text','integer','float','monetary','boolean','char','selection'):
            filter_tupple= (fil_field.name,fil_condi,widget_filter.value)
        print ("filter tupple",filter_tupple)
        return filter_tupple

    def get_widget_click_value(self,widget_id):
        """
        Return PR from button in Control Panel of Purchase Requisition
        """
        result = {}
        approved_pr_ids = []
        print ("widget iddddddddddddddd",widget_id)
        if widget_id :
            widget_id = self.env['widget.master.line'].browse([widget_id])
            if widget_id.widget_model.name == 'Material Requisition Slip' :
                if widget_id.name == 'MRS Created':
                    result= self.get_pending_mrs([])
                elif widget_id.name == 'Pending For Issue' :
                    result= self.get_confirmed_mrs([])
                elif widget_id.name == 'Pending For Receive' :
                    result= self.get_issue_mrs([])
                elif widget_id.name == 'MRS Completed' :
                    result= self.get_completed_mrs([])
            elif widget_id.widget_model.name == 'Material issue' :
                if widget_id.name == 'Issue Created':
                    result= self.get_pending_issue([])
                elif widget_id.name == 'Issued' :
                    result= self.get_issued_mrs([])
        print ("resultttttttttttttt",result)
        return result


    def get_widget_click_value11(self,widget_id):
        """
        Return PR from button in Control Panel of Purchase Requisition
        """
        result = {}
        approved_pr_ids = []
        result_list=[]
        value_list=[]
        dict_list=[]
        pb_list=[]
        field_list=[]
        user_id= self.env.user
        company_id= user_id.company_id
        print ("get widget click value function",widget_id)
        if widget_id :
            widget_id = self.env['widget.master.line'].browse([widget_id])
            print ("widget iddddd",widget_id,widget_id.widget_model.name)
            if widget_id.widget_model.name == 'Material Requisition Slip' :
                if widget_id.query_type == 'query':
                    mrs_query = widget_id.widget_query
                    print("mrs queryyyyyy", mrs_query)
                    self.env.cr.execute(mrs_query)
                    match_recs = self.env.cr.dictfetchall()
                    # print ("match recsssss",match_recs)
                    for val in match_recs :
                        field_list= list(val.keys())
                        value_list.append(list(val.values()))
                    print ("keyssssssss",field_list,len(field_list))
                    for vat in value_list:
                        print ("vatttttttttt",vat)
                    for value in value_list :
                        dict2 = {}
                        for i in range(0,len(field_list)):
                            print ("valueeeeeeee",value)
                            dict1= self.get_field_values(i,value[i])
                            # print ("dict1 returnnnn",dict1)
                            dict2.update(dict1)
                            print ("dict2 updateeee",dict2)
                        dict_list.append(dict2)
                    print ("dict listttttttt",dict_list)
                    if dict_list:
                        for db_report in dict_list:
                            print ("db reportttttt",db_report)
                            pd_id1=self.env['purchase.dashboard.report'].create(db_report)
                            print ("pd_id1pd_id1",pd_id1)
                            pb_list.append(pd_id1.id)
                    print ("values listttt",value_list)
                    print("pb_list listttt", pb_list)
                    result=self.get_purchase_dashboard_report(pb_list,field_list)
                else :
                    if widget_id:
                        filter_list=[]
                        if widget_id.widget_filter_lines :
                            for wid_filter in widget_id.widget_filter_lines :
                                print ("wid_filterwid_filter",wid_filter)
                                filter = self.get_filter_condition(wid_filter)
                                filter_list.append(filter)
                            filter_list = [('company_id', '=', company_id.id)]
                            # filter_list=[('partner_id','=',14),('id','>',0)]
                            print("rfq filter_list", filter_list)
                            rfq_ids= self.env['request.for.quotation'].search(filter_list)
                            print ("rfq idsssssss",rfq_ids)
                            result=self.get_action_mrs(rfq_ids)
            elif widget_id.widget_model.name == 'Issue' :
                if widget_id.query_type == 'query' :
                    rfq_query = widget_id.widget_query
                    print("mrs queryyyyyy", rfq_query)
                    self.env.cr.execute(rfq_query)
                    match_recs = self.env.cr.dictfetchall()
                    # print ("match recsssss",match_recs)
                    for val in match_recs :
                        field_list= list(val.keys())
                        value_list.append(list(val.values()))
                    print ("keyssssssss",field_list,len(field_list))
                    for vat in value_list:
                        print ("vatttttttttt",vat)
                    for value in value_list :
                        dict2 = {}
                        for i in range(0,len(field_list)):
                            print ("valueeeeeeee",value)
                            dict1= self.get_field_values_rfq(i,value[i])
                            # print ("dict1 returnnnn",dict1)
                            dict2.update(dict1)
                            print ("dict2 updateeee",dict2)
                        dict_list.append(dict2)
                    print ("dict listttttttt",dict_list)
                    if dict_list:
                        for db_report in dict_list:
                            print ("db reportttttt",db_report)
                            pd_id1=self.env['purchase.dashboard.report'].create(db_report)
                            print ("pd_id1pd_id1",pd_id1)
                            pb_list.append(pd_id1.id)
                    print ("values listttt",value_list)
                    print("pb_list listttt", pb_list)
                    result=self.get_purchase_dashboard_report(pb_list,field_list)
                else :
                    if widget_id:
                        filter_list=[]
                        if widget_id.widget_filter_lines :
                            for wid_filter in widget_id.widget_filter_lines :
                                print ("wid_filterwid_filter",wid_filter)
                                filter = self.get_filter_condition(wid_filter)
                                filter_list.append(filter)
                            filter_list = [('company_id', '=', company_id.id)]
                            # filter_list=[('partner_id','=',14),('id','>',0)]
                            print("rfq filter_list", filter_list)
                            rfq_ids= self.env['request.for.quotation'].search(filter_list)
                            print ("rfq idsssssss",rfq_ids)
                            result=self.get_action_rfq(rfq_ids)
            elif widget_id.widget_model.name == 'RFQ' :
                if widget_id.query_type == 'query' :
                    rfq_query = widget_id.widget_query
                    print("mrs queryyyyyy", rfq_query)
                    self.env.cr.execute(rfq_query)
                    match_recs = self.env.cr.dictfetchall()
                    # print ("match recsssss",match_recs)
                    for val in match_recs :
                        field_list= list(val.keys())
                        value_list.append(list(val.values()))
                    print ("keyssssssss",field_list,len(field_list))
                    for vat in value_list:
                        print ("vatttttttttt",vat)
                    for value in value_list :
                        dict2 = {}
                        for i in range(0,len(field_list)):
                            print ("valueeeeeeee",value)
                            dict1= self.get_field_values_rfq(i,value[i])
                            # print ("dict1 returnnnn",dict1)
                            dict2.update(dict1)
                            print ("dict2 updateeee",dict2)
                        dict_list.append(dict2)
                    print ("dict listttttttt",dict_list)
                    if dict_list:
                        for db_report in dict_list:
                            print ("db reportttttt",db_report)
                            pd_id1=self.env['purchase.dashboard.report'].create(db_report)
                            print ("pd_id1pd_id1",pd_id1)
                            pb_list.append(pd_id1.id)
                    print ("values listttt",value_list)
                    print("pb_list listttt", pb_list)
                    result=self.get_purchase_dashboard_report(pb_list,field_list)
                else :
                    if widget_id:
                        filter_list=[]
                        if widget_id.widget_filter_lines :
                            for wid_filter in widget_id.widget_filter_lines :
                                print ("wid_filterwid_filter",wid_filter)
                                filter = self.get_filter_condition(wid_filter)
                                filter_list.append(filter)
                            filter_list = [('company_id', '=', company_id.id)]
                            # filter_list=[('partner_id','=',14),('id','>',0)]
                            print("rfq filter_list", filter_list)
                            rfq_ids= self.env['request.for.quotation'].search(filter_list)
                            print ("rfq idsssssss",rfq_ids)
                            result=self.get_action_rfq(rfq_ids)
            elif widget_id.widget_model.name == 'Purchase Requisition' :
                if widget_id.query_type == 'query' :
                    pr_query = widget_id.widget_query
                    print("mrs queryyyyyy", pr_query)
                    self.env.cr.execute(pr_query)
                    match_recs = self.env.cr.dictfetchall()
                    # print ("match recsssss",match_recs)
                    for val in match_recs :
                        field_list= list(val.keys())
                        value_list.append(list(val.values()))
                    print ("keyssssssss",field_list,len(field_list))
                    for vat in value_list:
                        print ("vatttttttttt",vat)
                    for value in value_list :
                        dict2 = {}
                        for i in range(0,len(field_list)):
                            print ("valueeeeeeee",value)
                            dict1= self.get_field_values_rfq(i,value[i])
                            # print ("dict1 returnnnn",dict1)
                            dict2.update(dict1)
                            print ("dict2 updateeee",dict2)
                        dict_list.append(dict2)
                    print ("dict listttttttt",dict_list)
                    if dict_list:
                        for db_report in dict_list:
                            print ("db reportttttt",db_report)
                            pd_id1=self.env['purchase.dashboard.report'].create(db_report)
                            print ("pd_id1pd_id1",pd_id1)
                            pb_list.append(pd_id1.id)
                    print ("values listttt",value_list)
                    print("pb_list listttt", pb_list)
                    result=self.get_purchase_dashboard_report(pb_list,field_list)
                else :
                    if widget_id:
                        filter_list=[]
                        if widget_id.widget_filter_lines :
                            for wid_filter in widget_id.widget_filter_lines :
                                print ("wid_filterwid_filter",wid_filter)
                                filter = self.get_filter_condition(wid_filter)
                                filter_list.append(filter)
                            filter_list = [('company_id', '=', company_id.id)]
                            # filter_list=[('partner_id','=',14),('id','>',0)]
                            print("rfq filter_list", filter_list)
                            rfq_ids= self.env['request.for.quotation'].search(filter_list)
                            print ("rfq idsssssss",rfq_ids)
                            result=self.get_action_pr(rfq_ids)
            elif widget_id.widget_model.name == 'Purchase Order' :
                if widget_id.query_type == 'query' :
                    rfq_query = widget_id.widget_query
                    print("mrs queryyyyyy", rfq_query)
                    self.env.cr.execute(rfq_query)
                    match_recs = self.env.cr.dictfetchall()
                    # print ("match recsssss",match_recs)
                    for val in match_recs :
                        field_list= list(val.keys())
                        value_list.append(list(val.values()))
                    print ("keyssssssss",field_list,len(field_list))
                    for vat in value_list:
                        print ("vatttttttttt",vat)
                    for value in value_list :
                        dict2 = {}
                        for i in range(0,len(field_list)):
                            print ("valueeeeeeee",value)
                            dict1= self.get_field_values_rfq(i,value[i])
                            # print ("dict1 returnnnn",dict1)
                            dict2.update(dict1)
                            print ("dict2 updateeee",dict2)
                        dict_list.append(dict2)
                    print ("dict listttttttt",dict_list)
                    if dict_list:
                        for db_report in dict_list:
                            print ("db reportttttt",db_report)
                            pd_id1=self.env['purchase.dashboard.report'].create(db_report)
                            print ("pd_id1pd_id1",pd_id1)
                            pb_list.append(pd_id1.id)
                    print ("values listttt",value_list)
                    print("pb_list listttt", pb_list)
                    result=self.get_purchase_dashboard_report(pb_list,field_list)
                else :
                    if widget_id:
                        filter_list=[]
                        if widget_id.widget_filter_lines :
                            for wid_filter in widget_id.widget_filter_lines :
                                print ("wid_filterwid_filter",wid_filter)
                                filter = self.get_filter_condition(wid_filter)
                                filter_list.append(filter)
                            filter_list = [('company_id', '=', company_id.id)]
                            # filter_list=[('partner_id','=',14),('id','>',0)]
                            print("rfq filter_list", filter_list)
                            rfq_ids= self.env['purchase.order'].search(filter_list)
                            print ("rfq idsssssss",rfq_ids)
                            result=self.get_action_po(rfq_ids)
            elif widget_id.widget_model.name == 'Supplier Quotation' :
                if widget_id.query_type == 'query' :
                    rfq_query = widget_id.widget_query
                    print("mrs queryyyyyy", rfq_query)
                    self.env.cr.execute(rfq_query)
                    match_recs = self.env.cr.dictfetchall()
                    # print ("match recsssss",match_recs)
                    for val in match_recs :
                        field_list= list(val.keys())
                        value_list.append(list(val.values()))
                    print ("keyssssssss",field_list,len(field_list))
                    for vat in value_list:
                        print ("vatttttttttt",vat)
                    for value in value_list :
                        dict2 = {}
                        for i in range(0,len(field_list)):
                            print ("valueeeeeeee",value)
                            dict1= self.get_field_values_rfq(i,value[i])
                            # print ("dict1 returnnnn",dict1)
                            dict2.update(dict1)
                            print ("dict2 updateeee",dict2)
                        dict_list.append(dict2)
                    print ("dict listttttttt",dict_list)
                    if dict_list:
                        for db_report in dict_list:
                            print ("db reportttttt",db_report)
                            pd_id1=self.env['purchase.dashboard.report'].create(db_report)
                            print ("pd_id1pd_id1",pd_id1)
                            pb_list.append(pd_id1.id)
                    print ("values listttt",value_list)
                    print("pb_list listttt", pb_list)
                    result=self.get_purchase_dashboard_report(pb_list,field_list)
                else :
                    if widget_id:
                        filter_list=[]
                        if widget_id.widget_filter_lines :
                            for wid_filter in widget_id.widget_filter_lines :
                                print ("wid_filterwid_filter",wid_filter)
                                filter = self.get_filter_condition(wid_filter)
                                filter_list.append(filter)
                            filter_list.append(('company_id', '=', company_id.id))
                            # filter_list=[('partner_id','=',14),('id','>',0)]
                            print("rfq filter_list", filter_list)
                            # if len(filter_list) > 1 :
                            #     filter_string=''
                            #     for filters_loop in filter_list :
                            #         print ("in looop",filters_loop)
                            #         filter_string = filter_string + ',' + filters_loop
                            #     print ("filter string before slice",filter_string)
                            #     filter_string = filter_string[1:]
                            # else :
                            #     filter_string = filter_list
                            # print ("filter string",filter_string)
                            rfq_ids= self.env['supplier.quotation'].search(filter_list)
                            print ("rfq idsssssss",rfq_ids)
                            result=self.get_action_quotation(rfq_ids)

        # context = self._context
        # context.update({'field_list': field_list})
        # self.with_context(field_list=field_list)
        if len(field_list) > 0:
            self = self.with_context(field_list=field_list)
            print ("dataaaaa  context",self,self._context)
            print ("result after context",result)
            result['field_list']=field_list
            print ("result after field listtt",result)
        return result

    def get_item_request_mrs(self):
        result = {}
        approved_pr_ids = []
        slip_line_list = []
        print ("get item request mrsssssss",self)
        all_mrs = self.env['material.req.slip'].search([('state', 'not in', ('close','reject'))])
        print ("all mrsssss",all_mrs)
        if all_mrs:
            if all_mrs :
                for val in all_mrs :
                    for line in val.material_req_slip_line_ids :
                        slip_line_list.append(line.id)
            # all_mrs_ids = all_mrs.ids
        # print("SSSSSSSSSSSSSSSSSS", slip_line_list)
        action = self.env.ref('purchase_extension.action_material_req_slip_line')
        result = action.read()[0]
        res = self.env.ref('purchase_extension.view_material_req_slip_line_tree', False)
        res_form = self.env.ref('purchase_extension.view_material_req_slip__line_form', False)
        res_search = self.env.ref('purchase_extension.view_material_req_slip__line_search', False)
        # print ("KKKKKKKKKKKKKK",res,res.id)
        result['views'] = [(res and res.id or False, 'list'), (res_form and res_form.id or False, 'form'),(res_search and res_search.id or False,'search')]
        result['domain'] = [('id', 'in', tuple(slip_line_list))]
        result['target']='new'
        result['context'] = {'group_by': 'template_id'}
        return result

    def get_item_available_mrs(self):
        result = {}
        approved_pr_ids = []
        template_prod_list = []
        print ("get item request mrsssssss",self)
        all_mrs = self.env['material.req.slip'].search([('state', 'not in', ('close','reject'))])
        print ("all mrsssss",all_mrs)
        if all_mrs:
            all_mrs_ids = all_mrs.ids
            avail_prod_list = self.get_available_product(all_mrs)
            if len(avail_prod_list) > 0 :
                for new in avail_prod_list:
                    template_prod_list.append(new.id)
            slip_line_id = self.env['material.req.slip.line'].search([('template_id','in',template_prod_list),('material_req_slip_id','in',all_mrs_ids)])
            if len(slip_line_id) > 0 :
                slip_line_ids = slip_line_id.ids
        # print("SSSSSSSSSSSSSSSSSS", template_prod_list)
        action = self.env.ref('purchase_extension.action_material_req_slip_line')
        result = action.read()[0]
        res = self.env.ref('purchase_extension.view_material_req_slip_line_tree', False)
        res_form = self.env.ref('purchase_extension.view_material_req_slip__line_form', False)
        res_search = self.env.ref('purchase_extension.view_material_req_slip__line_search', False)
        # print ("KKKKKKKKKKKKKK",res,res.id)
        result['views'] = [(res and res.id or False, 'list'), (res_form and res_form.id or False, 'form'),(res_search and res_search.id or False,'search')]
        result['domain'] = [('id', 'in', tuple(slip_line_ids))]
        result['target']='new'
        result['context'] = {'group_by': 'template_id'}
        return result

    def get_item_procure_mrs(self):
        result = {}
        approved_pr_ids = []
        template_prod_list = []
        print ("get item request mrsssssss",self)
        all_mrs = self.env['material.req.slip'].search([('state', 'not in', ('close','reject'))])
        print ("all mrsssss",all_mrs)
        if all_mrs:
            all_mrs_ids = all_mrs.ids
            avail_prod_list = self.get_available_product(all_mrs)
            if len(avail_prod_list) > 0 :
                for new in avail_prod_list:
                    template_prod_list.append(new.id)
            slip_line_id = self.env['material.req.slip.line'].search([('template_id','not in',template_prod_list),('material_req_slip_id','in',all_mrs_ids)])
            if len(slip_line_id) > 0 :
                slip_line_ids = slip_line_id.ids
        # print("SSSSSSSSSSSSSSSSSS", template_prod_list)
        action = self.env.ref('purchase_extension.action_material_req_slip_line')
        result = action.read()[0]
        res = self.env.ref('purchase_extension.view_material_req_slip_line_tree', False)
        res_form = self.env.ref('purchase_extension.view_material_req_slip__line_form', False)
        res_search = self.env.ref('purchase_extension.view_material_req_slip__line_search', False)
        # print ("KKKKKKKKKKKKKK",res,res.id)
        result['views'] = [(res and res.id or False, 'list'), (res_form and res_form.id or False, 'form'),(res_search and res_search.id or False,'search')]
        result['domain'] = [('id', 'in', tuple(slip_line_ids))]
        result['target']='new'
        result['context'] = {'group_by': 'template_id'}
        return result

    # def get_user_wise_mrs(self):
    #     result = {}
    #     approved_pr_ids = []
    #     template_prod_list = []
    #     employe_obj= self.env['hr.employee']
    #     pb_value_line_obj = self.env['purchase.dashboard.value.line']
    #     mrs_obj= self.env['material.req.slip']
    #     print ("get item request mrsssssss",self,self._context)
    #     # user_id = self._context.get('uid')
    #     user_id = 1
    #     user_id = self.env['res.users'].browse(user_id)
    #     print ("user iddddd",user_id)
    #     # employee_id = self.env['hr.employee'].search[('user_id','=',user_id.id)]
    #     employee_id = self.env['hr.employee'].search([('user_id', '=', user_id.id)])
    #     print ("employee id",user_id,employee_id,)
    #     pbl_level1_id =pb_value_line_obj.create({'user_id':user_id.id,'name':user_id.name})
    #     print ("pbl level 1 id",)
    #     if len(employee_id) > 0 :
    #         employee_level1 = self.env['hr.employee'].search([('parent_id','=',employee_id.id)])
    #         if len(employee_level1) > 0 :
    #             for val in employee_level1 :
    #                 pbl_level2_id = pb_value_line_obj.create({'user_id': val.user_id.id, 'parent_id': pbl_level1_id.id})
    #                 # mrs_id = self.env['material.req.slip'].search[('assigned_id','=',val.user_id.id),('state','=','sent_for_approval')]
    #                 mrs_id = self.env['material.req.slip'].search([('state', '=', 'sent_for_approval'),('assigned_id','=',val.user_id.id)])
    #                 if len(mrs_id) > 0 :
    #                     for level2 in mrs_id :
    #                         pbl_level2_id = pb_value_line_obj.create(
    #                                                             {'user_id': val.user_id.id,
    #                                                              'mrs_id': level2.id,
    #                                                              'mrs_name': level2.name,
    #                                                              'parent_id': pbl_level2_id.id})
    #                 else :
    #                     # pbl_level2_id = pb_value_line_obj.create({'user_id':val.user_id.id,'parent_id':pbl_level1_id})
    #                     employee_level2 = self.env['hr.employee'].search([('parent_id', '=', val.id)])
    #                     if len(employee_level2) > 0:
    #                         for val1 in employee_level2 :
    #                             pbl_level3_id = pb_value_line_obj.create(
    #                                 {'user_id': val1.user_id.id, 'parent_id': pbl_level2_id.id})
    #                             # mrs_id1 = self.env['material.req.slip'].search[
    #                             #     ('assigned_id', '=', val1.user_id.id), ('state', '=', 'sent_for_approval')]
    #                             mrs_id1 = self.env['material.req.slip'].search(
    #                                 [('state', '=', 'sent_for_approval'), ('assigned_id', '=', val1.user_id.id)])
    #                             if len(mrs_id1) > 0:
    #                                 for level2 in mrs_id:
    #                                     pbl_level2_id = pb_value_line_obj.create(
    #                                         {'user_id': val1.user_id.id,
    #                                          'mrs_id': level2.id,
    #                                          'mrs_name': level2.name,
    #                                          'parent_id': pbl_level3_id.id})
    #
    #
    #     return True

    # @api.model
    # def get_children(self, dep):
    #     data = []
    #     if dep.user_id.id:
    #         dep_data={'employee_id':dep.name,'user_id':dep.user_id.id}
    #     else:
    #         dep_data = {'employee_id': dep.name, 'user_id': ''}
    #     print ("dep data before searchhhhh",dep_data)
    #     childrens = self.env['hr.employee'].search([('parent_id', '=', dep.id)])
    #     print("childersss", childrens, dep.name)
    #     for child in childrens:
    #         sub_child = self.env['hr.employee'].search([('parent_id', '=', child.id)])
    #         print("sub childdddd", child.name, sub_child)
    #         # next_style = self._get_style(style)
    #         if not sub_child:
    #             print("if not sub childdddddd", child.user_id)
    #             if child.user_id.id:
    #                 data.append({'employee_id': child.name, 'user_id': child.user_id.id})
    #             else:
    #                 data.append({'employee_id': child.name, 'user_id': ''})
    #         # data.append({'name': child.name, 'title': 'Emps', 'className': next_style})
    #         else:
    #             data.append(self.get_children(child))
    #
    #     if childrens:
    #         dep_data['children'] = data
    #
    #     return dep_data

    @api.model
    def get_children(self, dep,data_list):
        data_list=data_list
        data = []
        if dep.user_id.id:
            # dep_data = {dep.id :dep.user_id.id}
            if dep.id not in data_list:
                data_list.append(dep.id)
        # else:
        #     dep_data = {dep.id: ''}
        print("dep data before searchhhhh", data_list)
        childrens = self.env['hr.employee'].search([('parent_id', '=', dep.id)])
        print("childersss", childrens, dep.name)
        for child in childrens:
            sub_child = self.env['hr.employee'].search([('parent_id', '=', child.id)])
            print("sub childdddd", child.name, sub_child)
            # next_style = self._get_style(style)
            if not sub_child:
                print("if not sub childdddddd", child.user_id)
                if child.user_id.id:
                    # data.append({child.id :child.user_id.id})
                    if child.id not in data_list :
                        data_list.append(child.id)
                else:
                    # data.append({child.id: ''})
                    if child.id not in data_list :
                        data_list.append(child.id)
            # data.append({'name': child.name, 'title': 'Emps', 'className': next_style})
            else:
                # data.append(self.get_children(child))
                data=self.get_children(child,data_list)
                if child.id not in data_list:
                    data_list.append(child.id)

        # if childrens:
        #     dep_data['children'] = data
        print ("return data listttt",data_list)
        return data_list

    def get_user_wise_mrs(self,get_hierachy_data,company_id):
        result = {}
        approved_pr_ids = []
        emp_details_list = []
        employe_obj = self.env['hr.employee']
        if len(get_hierachy_data) > 0 :
            for val in get_hierachy_data :
                employee_id1 = employe_obj.browse(val)
                print ("employee id",employee_id1,employee_id1.name,employee_id1.user_id.name)
                mrs_id = self.env['material.req.slip'].search([('create_uid','=',employee_id1.user_id.id),('company_id','=',company_id.id)])
                # mrs_delay_id = self.env['material.req.slip'].search([('create_uid', '=', employee_id1.user_id.id),('state','=','draft'),])
                print ("mrs idddddd",mrs_id)

                if len(mrs_id) >0 :
                    # mrs_delay_query = "select id,create_date,create_date + interval '2' day,CURRENT_TIMESTAMP from material_req_slip where state='draft' and CURRENT_TIMESTAMP >= create_date + interval '2' day and create_uid = "
                    self.env.cr.execute(
                        "select id from material_req_slip where state not in ('approve','reject') "
                        " and CURRENT_TIMESTAMP >= create_date + interval '2' day and assigned_id = %s and company_id = %s",
                        [employee_id1.id,company_id.id])

                    mrs_delay_query = self.env.cr.dictfetchall()
                    print("mrs delay queryyyyyyyyy", mrs_delay_query,len(mrs_delay_query))
                    details_tupple= (employee_id1.name,len(mrs_id),'Delay',len(mrs_delay_query),employee_id1.user_id.id,)
                    emp_details_list.append(details_tupple)
                # else :
                #     details_tupple = (employee_id1.name, 0,'Delay',0,employee_id1.user_id.id)
                #     emp_details_list.append(details_tupple)

        print ("employee detailssss",emp_details_list)
        return emp_details_list

    def get_user_wise_rfq(self,get_hierachy_data,company_id):
        result = {}
        approved_pr_ids = []
        emp_details_list = []
        employe_obj = self.env['hr.employee']
        if len(get_hierachy_data) > 0 :
            for val in get_hierachy_data :
                employee_id1 = employe_obj.browse(val)
                print ("employee id",employee_id1,employee_id1.name,employee_id1.user_id.name)
                rfq_id = self.env['request.for.quotation'].search([('create_uid','=',employee_id1.user_id.id),('company_id','=',company_id.id)])
                # mrs_delay_id = self.env['material.req.slip'].search([('create_uid', '=', employee_id1.user_id.id),('state','=','draft'),])
                print ("mrs idddddd",rfq_id)
                if len(rfq_id) >0 :
                    # mrs_delay_query = "select id,create_date,create_date + interval '2' day,CURRENT_TIMESTAMP from material_req_slip where state='draft' and CURRENT_TIMESTAMP >= create_date + interval '2' day and create_uid = "
                    self.env.cr.execute(
                        "select id from request_for_quotation where  "
                        " CURRENT_TIMESTAMP >= create_date + interval '2' day and create_uid = %s and company_id = %s",
                        [employee_id1.user_id.id,company_id.id])

                    rfq_delay_query = self.env.cr.dictfetchall()
                    print("rfq delay queryyyyyyyyy", rfq_delay_query,len(rfq_delay_query))
                    details_tupple= (employee_id1.name,len(rfq_id),'Delay',len(rfq_delay_query),employee_id1.user_id.id,)
                    emp_details_list.append(details_tupple)
                # else :
                #     details_tupple = (employee_id1.name, 0,'Delay',0,employee_id1.user_id.id)
                #     emp_details_list.append(details_tupple)

        print ("employee detailssss",emp_details_list)
        return emp_details_list

    def get_user_wise_pr(self,get_hierachy_data,company_id):
        result = {}
        approved_pr_ids = []
        emp_details_list = []
        employe_obj = self.env['hr.employee']
        if len(get_hierachy_data) > 0 :
            for val in get_hierachy_data :
                employee_id1 = employe_obj.browse(val)
                print ("employee id",employee_id1,employee_id1.name,employee_id1.user_id.name)
                pr_id = self.env['purchase.req'].search([('create_uid','=',employee_id1.user_id.id),('company_id','=',company_id.id)])
                # mrs_delay_id = self.env['material.req.slip'].search([('create_uid', '=', employee_id1.user_id.id),('state','=','draft'),])
                print ("mrs idddddd",pr_id)
                if len(pr_id) >0 :
                    # mrs_delay_query = "select id,create_date,create_date + interval '2' day,CURRENT_TIMESTAMP from material_req_slip where state='draft' and CURRENT_TIMESTAMP >= create_date + interval '2' day and create_uid = "
                    if employee_id1.user_id.id :
                        self.env.cr.execute(
                            "select id from purchase_req where state not in ('approve','cancel','reject','done') "
                            " and CURRENT_TIMESTAMP >= create_date + interval '2' day and assign_to = %s and company_id = %s ",
                            [employee_id1.user_id.id,company_id.id])
                    else :
                        self.env.cr.execute(
                        "select id from purchase_req where state not in ('approve','cancel','reject','done') "
                        " and CURRENT_TIMESTAMP >= create_date + interval '2' day and create_uid = %s and company_id = %s ",
                        [employee_id1.user_id.id, company_id.id])

                    pr_delay_query = self.env.cr.dictfetchall()
                    print("pr delay queryyyyyyyyy", pr_delay_query,len(pr_delay_query))
                    details_tupple= (employee_id1.name,len(pr_id),'Delay',len(pr_delay_query),employee_id1.user_id.id,)
                    emp_details_list.append(details_tupple)
                # else :
                #     details_tupple = (employee_id1.name, 0,'Delay',0,employee_id1.user_id.id)
                #     emp_details_list.append(details_tupple)

        print ("employee detailssss",emp_details_list)
        return emp_details_list

    def get_user_wise_quotation(self,get_hierachy_data,company_id):
        result = {}
        approved_pr_ids = []
        emp_details_list = []
        employe_obj = self.env['hr.employee']
        if len(get_hierachy_data) > 0 :
            for val in get_hierachy_data :
                employee_id1 = employe_obj.browse(val)
                print ("employee id",employee_id1,employee_id1.name,employee_id1.user_id.name)
                pr_id = self.env['supplier.quotation'].search([('create_uid','=',employee_id1.user_id.id),('company_id','=',company_id.id)])
                # mrs_delay_id = self.env['material.req.slip'].search([('create_uid', '=', employee_id1.user_id.id),('state','=','draft'),])
                print ("mrs idddddd",pr_id)
                if len(pr_id) >0 :
                    # mrs_delay_query = "select id,create_date,create_date + interval '2' day,CURRENT_TIMESTAMP from material_req_slip where state='draft' and CURRENT_TIMESTAMP >= create_date + interval '2' day and create_uid = "
                    self.env.cr.execute(
                        "select id from supplier_quotation where "
                        " CURRENT_TIMESTAMP >= create_date + interval '2' day and create_uid = %s and company_id = %s ",
                        [employee_id1.user_id.id,company_id.id])

                    pr_delay_query = self.env.cr.dictfetchall()
                    print("pr delay queryyyyyyyyy", pr_delay_query,len(pr_delay_query))
                    details_tupple= (employee_id1.name,len(pr_id),'Delay',len(pr_delay_query),employee_id1.user_id.id,)
                    emp_details_list.append(details_tupple)
                # else :
                #     details_tupple = (employee_id1.name, 0,'Delay',0,employee_id1.user_id.id)
                #     emp_details_list.append(details_tupple)

        print ("employee detailssss",emp_details_list)
        return emp_details_list

    def get_user_wise_po(self,get_hierachy_data,company_id):
        result = {}
        approved_pr_ids = []
        emp_details_list = []
        employe_obj = self.env['hr.employee']
        if len(get_hierachy_data) > 0 :
            for val in get_hierachy_data :
                employee_id1 = employe_obj.browse(val)
                print ("employee id",employee_id1,employee_id1.name,employee_id1.user_id.name)
                pr_id = self.env['purchase.order'].search([('create_uid','=',employee_id1.user_id.id),('company_id','=',company_id.id)])
                # mrs_delay_id = self.env['material.req.slip'].search([('create_uid', '=', employee_id1.user_id.id),('state','=','draft'),])
                print ("mrs idddddd",pr_id)
                if len(pr_id) >0 :
                    # mrs_delay_query = "select id,create_date,create_date + interval '2' day,CURRENT_TIMESTAMP from material_req_slip where state='draft' and CURRENT_TIMESTAMP >= create_date + interval '2' day and create_uid = "
                    self.env.cr.execute(
                        "select id from purchase_order where state='draft' "
                        " and CURRENT_TIMESTAMP >= create_date + interval '2' day and create_uid = %s and company_id = %s ",
                        [employee_id1.user_id.id,company_id.id])

                    pr_delay_query = self.env.cr.dictfetchall()
                    print("pr delay queryyyyyyyyy", pr_delay_query,len(pr_delay_query))
                    details_tupple= (employee_id1.name,len(pr_id),'Delay',len(pr_delay_query),employee_id1.user_id.id,)
                    emp_details_list.append(details_tupple)
                # else :
                #     details_tupple = (employee_id1.name, 0,'Delay',0,employee_id1.user_id.id)
                #     emp_details_list.append(details_tupple)

        print ("employee detailssss",emp_details_list)
        return emp_details_list

    def get_user_wise_activity(self,widget_id):
        result = {}
        details_dict={}
        print ("self in user wiseeeee",self,self.env.user)
        dashboad_widget = self.env['dashboard.widget.master'].browse(widget_id)
        print ("dashboard widget",dashboad_widget,dashboad_widget.widget_name)
        print ("get item request mrsssssss",self,self._context)
        # user_id = self._context.get('uid')
        user_id = self.env.user
        company_id = user_id.company_id
        # user_id = self.env['res.users'].browse(user_id)
        print ("user iddddd",user_id)
        employee_id = self.env['hr.employee'].search([('user_id', '=', user_id.id)])
        print ("employee id",user_id,employee_id,)
        get_hierachy_data = self.get_children(employee_id,[])
        print ("get hierachy dataaaaa",get_hierachy_data)
        if len(get_hierachy_data) > 0 :
            if dashboad_widget.widget_name == 'mrs' :
                details_list = self.get_user_wise_mrs(get_hierachy_data,company_id)
            elif dashboad_widget.widget_name == 'rfq' :
                details_list = self.get_user_wise_rfq(get_hierachy_data,company_id)
            elif dashboad_widget.widget_name == 'pr' :
                details_list = self.get_user_wise_pr(get_hierachy_data,company_id)
            elif dashboad_widget.widget_name == 'purchase_order' :
                details_list = self.get_user_wise_po(get_hierachy_data,company_id)
            elif dashboad_widget.widget_name == 'quotation' :
                details_list = self.get_user_wise_quotation(get_hierachy_data,company_id)

        print ("details_list detailssss",details_list)
        # print ("aaaaaaa",a)
        if len(details_list) > 0 :
            details_dict={dashboad_widget.widget_no : details_list}
            # details_dict ={'widget_seq' :dashboad_widget.widget_no,'widget_value': details_list}
        else :
            # details_dict = {'widget_seq': dashboad_widget.widget_no, 'widget_value': []}
            details_dict = {dashboad_widget.widget_no: details_list}
        print ("return detail dictttttt",details_dict)
        return details_dict

    def get_user_pending_mrs(self,user,company_id):
        result = {}
        all_mrs_ids = []
        template_prod_list = []
        print ("get item request mrsssssss",user)
        if user :
            emp_id = self.env['hr.employee'].search([('user_id','=',user)])
            if len(emp_id) > 0 :
                self.env.cr.execute(
                    "select id from material_req_slip where state not in ('approve','reject') "
                    " and CURRENT_TIMESTAMP >= create_date + interval '2' day and assigned_id = %s and company_id = %s ",
                    [emp_id.id,company_id.id])
            else :
                self.env.cr.execute(
                    "select id from material_req_slip where state not in ('approve','reject') "
                    " and CURRENT_TIMESTAMP >= create_date + interval '2' day and create_uid = %s and company_id = %s ",
                    [user, company_id.id])

            mrs_delay_query = self.env.cr.dictfetchall()
        print ("mrs delay queryyyyyyy user wise",mrs_delay_query)
        if len(mrs_delay_query):
            for mrs_loop in mrs_delay_query :
                for val in mrs_loop.values() :
                    all_mrs_ids.append(val)
        print("SSSSSSSSSSSSSSSSSS", all_mrs_ids)
        # action = self.env.ref('purchase_extension.action_material_req_slip')
        # result = action.read()[0]
        # res = self.env.ref('purchase_extension.view_material_req_slip_tree', False)
        # res_form = self.env.ref('purchase_extension.view_material_req_slip_form', False)
        # # print ("KKKKKKKKKKKKKK",res,res.id)
        # result['views'] = [(res and res.id or False, 'list'), (res_form and res_form.id or False, 'form')]
        # result['domain'] = [('id', 'in', tuple(all_mrs_ids))]
        action = self.env.ref('purchase_extension.action_material_req_slip')
        result = action.read()[0]
        res = self.env.ref('purchase_extension.view_material_req_slip_tree', False)
        res_form = self.env.ref('purchase_extension.view_material_req_slip_form', False)
        # print ("KKKKKKKKKKKKKK",res,res.id)
        result['views'] = [(res and res.id or False, 'list'), (res_form and res_form.id or False, 'form')]
        result['domain'] = [('id', 'in', tuple(all_mrs_ids))]
        result['target'] = 'new'
        print ("result in mrs delay qiuery",result)
        return result

    def get_user_mrs(self,user,company_id):
        result = {}
        approved_pr_ids = []
        template_prod_list = []
        print ("get item request mrsssssss",user)
        all_mrs = self.env['material.req.slip'].search([('create_uid', '=', user),('company_id','=',company_id.id)])
        if all_mrs:
            all_mrs_ids = all_mrs.ids
        print("SSSSSSSSSSSSSSSSSS", all_mrs_ids)
        action = self.env.ref('purchase_extension.action_material_req_slip')
        result = action.read()[0]
        res = self.env.ref('purchase_extension.view_material_req_slip_tree', False)
        res_form = self.env.ref('purchase_extension.view_material_req_slip_form', False)
        # print ("KKKKKKKKKKKKKK",res,res.id)
        result['views'] = [(res and res.id or False, 'list'), (res_form and res_form.id or False, 'form')]
        result['domain'] = [('id', 'in', tuple(all_mrs_ids))]
        result['target'] = 'new'
        return result

    def get_user_material_issue(self,user,company_id):
        result = {}
        approved_pr_ids = []
        template_prod_list = []
        print ("get item request mrsssssss",user)
        all_mrs = self.env['material.issue'].search([('create_uid', '=', user),('company_id','=',company_id.id)])
        if all_mrs:
            all_mrs_ids = all_mrs.ids
        print("user rfq", all_mrs_ids)
        action = self.env.ref('purchase_extension.action_material_issue')
        result = action.read()[0]
        res = self.env.ref('purchase_extension.view_material_issue_tree', False)
        res_form = self.env.ref('purchase_extension.view_material_issue_form', False)
        # print ("KKKKKKKKKKKKKK",res,res.id)
        result['views'] = [(res and res.id or False, 'list'), (res_form and res_form.id or False, 'form')]
        result['domain'] = [('id', 'in', tuple(all_mrs_ids))]
        result['target'] = 'new'
        return result

    def get_user_pending_material_issue(self,user,company_id):
        result = {}
        approved_pr_ids = []
        all_mrs_ids = []
        print ("get item request mrsssssss",user)
        # self.env.cr.execute(
        #     "select id from purchase_req where state='draft' "
        #     " and CURRENT_TIMESTAMP >= create_date + interval '2' day and create_uid = %s and company_id = %s ",
        #     [user,company_id.id])

        self.env.cr.execute(
            "select id from material_issue where state not in ('approve','cancel','reject','done') "
            " and CURRENT_TIMESTAMP >= create_date + interval '2' day and assign_to = %s and company_id = %s ",
            [user, company_id.id])


        mrs_delay_query = self.env.cr.dictfetchall()
        print("mrs delay queryyyyyyy user wise", mrs_delay_query)
        if len(mrs_delay_query):
            for mrs_loop in mrs_delay_query:
                for val in mrs_loop.values():
                    all_mrs_ids.append(val)
        print("user rfq", all_mrs_ids)
        action = self.env.ref('purchase_extension.action_purchase_req')
        result = action.read()[0]
        res = self.env.ref('purchase_extension.view_purchase_req_tree', False)
        res_form = self.env.ref('purchase_extension.view_purchase_req_form', False)
        # print ("KKKKKKKKKKKKKK",res,res.id)
        result['views'] = [(res and res.id or False, 'list'), (res_form and res_form.id or False, 'form')]
        result['domain'] = [('id', 'in', tuple(all_mrs_ids))]
        result['target'] = 'new'
        return result

    def get_user_rfq(self,user,company_id):
        result = {}
        approved_pr_ids = []
        template_prod_list = []
        print ("get item request mrsssssss",user)
        all_mrs = self.env['request.for.quotation'].search([('create_uid', '=', user),('company_id','=',company_id.id)])
        if all_mrs:
            all_mrs_ids = all_mrs.ids
        print("user rfq", all_mrs_ids)
        action = self.env.ref('purchase_extension.action_request_for_quotation')
        result = action.read()[0]
        res = self.env.ref('purchase_extension.view_request_for_quotation_tree', False)
        res_form = self.env.ref('purchase_extension.view_request_for_quotation_form', False)
        # print ("KKKKKKKKKKKKKK",res,res.id)
        result['views'] = [(res and res.id or False, 'list'), (res_form and res_form.id or False, 'form')]
        result['domain'] = [('id', 'in', tuple(all_mrs_ids))]
        result['target'] = 'new'
        return result

    def get_user_pending_rfq(self,user,company_id):
        result = {}
        approved_pr_ids = []
        all_mrs_ids = []
        print ("get item request mrsssssss",user)
        self.env.cr.execute(
            "select id from request_for_quotation where "
            " CURRENT_TIMESTAMP >= create_date + interval '2' day and create_uid = %s and company_id = %s ",
            [user,company_id.id])

        mrs_delay_query = self.env.cr.dictfetchall()
        print("mrs delay queryyyyyyy user wise", mrs_delay_query)
        if len(mrs_delay_query):
            for mrs_loop in mrs_delay_query:
                for val in mrs_loop.values():
                    all_mrs_ids.append(val)
        print("user rfq", all_mrs_ids)
        action = self.env.ref('purchase_extension.action_request_for_quotation')
        result = action.read()[0]
        res = self.env.ref('purchase_extension.view_request_for_quotation_tree', False)
        res_form = self.env.ref('purchase_extension.view_request_for_quotation_form', False)
        # print ("KKKKKKKKKKKKKK",res,res.id)
        result['views'] = [(res and res.id or False, 'list'), (res_form and res_form.id or False, 'form')]
        result['domain'] = [('id', 'in', tuple(all_mrs_ids))]
        result['target'] = 'new'
        return result

    def get_user_pr(self,user,company_id):
        result = {}
        approved_pr_ids = []
        template_prod_list = []
        print ("get item request mrsssssss",user)
        all_mrs = self.env['purchase.req'].search([('create_uid', '=', user),('company_id','=',company_id.id)])
        if all_mrs:
            all_mrs_ids = all_mrs.ids
        print("user rfq", all_mrs_ids)
        action = self.env.ref('purchase_extension.action_purchase_req')
        result = action.read()[0]
        res = self.env.ref('purchase_extension.view_purchase_req_tree', False)
        res_form = self.env.ref('purchase_extension.view_purchase_req_form', False)
        # print ("KKKKKKKKKKKKKK",res,res.id)
        result['views'] = [(res and res.id or False, 'list'), (res_form and res_form.id or False, 'form')]
        result['domain'] = [('id', 'in', tuple(all_mrs_ids))]
        result['target'] = 'new'
        return result

    def get_user_pending_pr(self,user,company_id):
        result = {}
        approved_pr_ids = []
        all_mrs_ids = []
        print ("get item request mrsssssss",user)
        # self.env.cr.execute(
        #     "select id from purchase_req where state='draft' "
        #     " and CURRENT_TIMESTAMP >= create_date + interval '2' day and create_uid = %s and company_id = %s ",
        #     [user,company_id.id])

        self.env.cr.execute(
            "select id from purchase_req where state not in ('approve','cancel','reject','done') "
            " and CURRENT_TIMESTAMP >= create_date + interval '2' day and assign_to = %s and company_id = %s ",
            [user, company_id.id])


        mrs_delay_query = self.env.cr.dictfetchall()
        print("mrs delay queryyyyyyy user wise", mrs_delay_query)
        if len(mrs_delay_query):
            for mrs_loop in mrs_delay_query:
                for val in mrs_loop.values():
                    all_mrs_ids.append(val)
        print("user rfq", all_mrs_ids)
        action = self.env.ref('purchase_extension.action_purchase_req')
        result = action.read()[0]
        res = self.env.ref('purchase_extension.view_purchase_req_tree', False)
        res_form = self.env.ref('purchase_extension.view_purchase_req_form', False)
        # print ("KKKKKKKKKKKKKK",res,res.id)
        result['views'] = [(res and res.id or False, 'list'), (res_form and res_form.id or False, 'form')]
        result['domain'] = [('id', 'in', tuple(all_mrs_ids))]
        result['target'] = 'new'
        return result

    def get_user_po(self,user,company_id):
        result = {}
        approved_pr_ids = []
        template_prod_list = []
        print ("get item request mrsssssss",user)
        all_mrs = self.env['purchase.order'].search([('create_uid', '=', user),('company_id','=',company_id.id)])
        if all_mrs:
            all_mrs_ids = all_mrs.ids
        print("user rfq", all_mrs_ids)
        action = self.env.ref('purchase_extension.purchase_order_action_inherit_btn')
        result = action.read()[0]
        res = self.env.ref('purchase_extension.purchase_order_ext_inherit_tree', False)
        res_form = self.env.ref('purchase_extension.purchase_order_form_inherit_ext', False)
        result['views'] = [(res and res.id or False, 'list'), (res_form and res_form.id or False, 'form')]
        # print ("KKKKKKKKKKKKKK",res,res.id)
        result['domain'] = [('id', 'in', tuple(all_mrs_ids))]
        result['target'] = 'new'
        return result

    def get_user_pending_po(self,user,company_id):
        result = {}
        approved_pr_ids = []
        all_mrs_ids = []
        print ("get item request mrsssssss",user)
        self.env.cr.execute(
            "select id from purchase_order where state='draft' "
            " and CURRENT_TIMESTAMP >= create_date + interval '2' day and create_uid = %s and company_id = %s ",
            [user,company_id.id])

        mrs_delay_query = self.env.cr.dictfetchall()
        print("mrs delay queryyyyyyy user wise", mrs_delay_query)
        if len(mrs_delay_query):
            for mrs_loop in mrs_delay_query:
                for val in mrs_loop.values():
                    all_mrs_ids.append(val)
        print("user rfq", all_mrs_ids)
        action = self.env.ref('purchase_extension.purchase_order_action_inherit_btn')
        result = action.read()[0]
        res = self.env.ref('purchase_extension.purchase_order_ext_inherit_tree', False)
        res_form = self.env.ref('purchase_extension.purchase_order_form_inherit_ext', False)
        result['views'] = [(res and res.id or False, 'list'), (res_form and res_form.id or False, 'form')]
        # print ("KKKKKKKKKKKKKK",res,res.id)
        result['domain'] = [('id', 'in', tuple(all_mrs_ids))]
        result['target'] = 'new'
        return result

    def get_user_quotation(self,user,company_id):
        result = {}
        approved_pr_ids = []
        template_prod_list = []
        print ("get item request mrsssssss",user)
        all_mrs = self.env['supplier.quotation'].search([('create_uid', '=', user),('company_id','=',company_id.id)])
        if all_mrs:
            all_mrs_ids = all_mrs.ids
        print("user rfq", all_mrs_ids)
        action = self.env.ref('purchase_extension.action_supplier_quotation')
        result = action.read()[0]
        res = self.env.ref('purchase_extension.view_supplier_quotation_tree', False)
        res_form = self.env.ref('purchase_extension.view_supplier_quotation_form', False)
        # print ("KKKKKKKKKKKKKK",res,res.id)
        result['views'] = [(res and res.id or False, 'list'), (res_form and res_form.id or False, 'form')]
        result['domain'] = [('id', 'in', tuple(all_mrs_ids))]
        result['target'] = 'new'
        return result

    def get_user_pending_quotation(self,user,company_id):
        result = {}
        approved_pr_ids = []
        all_mrs_ids = []
        print ("get item request mrsssssss",user)
        self.env.cr.execute(
            "select id from supplier_quotation where  "
            "  CURRENT_TIMESTAMP >= create_date + interval '2' day and create_uid = %s and company_id = %s ",
            [user,company_id.id])

        mrs_delay_query = self.env.cr.dictfetchall()
        print("mrs delay queryyyyyyy user wise", mrs_delay_query)
        if len(mrs_delay_query):
            for mrs_loop in mrs_delay_query:
                for val in mrs_loop.values():
                    all_mrs_ids.append(val)
        print("user rfq", all_mrs_ids)
        action = self.env.ref('purchase_extension.action_supplier_quotation')
        result = action.read()[0]
        res = self.env.ref('purchase_extension.view_supplier_quotation_tree', False)
        res_form = self.env.ref('purchase_extension.view_supplier_quotation_form', False)
        # print ("KKKKKKKKKKKKKK",res,res.id)
        result['views'] = [(res and res.id or False, 'list'), (res_form and res_form.id or False, 'form')]
        result['domain'] = [('id', 'in', tuple(all_mrs_ids))]
        result['target'] = 'new'
        return result


    def get_user_widget_wise_data(self,user,widget):
        result = {}
        approved_pr_ids = []
        template_prod_list = []
        user_id= self.env.user
        company_id= user_id.company_id
        print ("user wise widget data",user,widget)
        if widget:
            widget_master= self.env['dashboard.widget.master'].browse(widget)
            print ("widget nameee",widget_master.widget_name)
            if widget_master.widget_name == 'mrs' :
                widget_wise_detail = self.get_user_mrs(user,company_id)
            elif widget_master.widget_name == 'material_issue' :
                widget_wise_detail = self.get_user_material_issue(user,company_id)
            elif widget_master.widget_name == 'rfq' :
                widget_wise_detail = self.get_user_rfq(user,company_id)
            elif widget_master.widget_name == 'pr':
                widget_wise_detail = self.get_user_pr( user,company_id)
            elif widget_master.widget_name == 'purchase_order':
                widget_wise_detail = self.get_user_po( user,company_id)
            elif widget_master.widget_name == 'quotation':
                widget_wise_detail = self.get_user_quotation( user,company_id)
            else :
                widget_wise_detail=[]
        print ("get item request mrsssssss",widget_wise_detail)
        # print ("aaaaaaa",a)

        return widget_wise_detail

    def get_user_widget_wise_pending_data(self,user,widget):
        result = {}
        approved_pr_ids = []
        template_prod_list = []
        user_id = self.env.user
        company_id = user_id.company_id
        if widget:
            widget_master= self.env['dashboard.widget.master'].browse(widget)
            print ("dashboard widget name",user,widget,widget_master)
            # print ("aaaaaa",a)
            if widget_master.widget_name == 'mrs' :
                widget_wise_detail = self.get_user_pending_mrs(user,company_id)
            elif widget_master.widget_name == 'material_issue' :
                widget_wise_detail = self.get_user_pending_material_issue(user,company_id)
            elif widget_master.widget_name == 'rfq' :
                widget_wise_detail = self.get_user_pending_rfq(user,company_id)
            elif widget_master.widget_name == 'pr':
                widget_wise_detail = self.get_user_pending_pr(user,company_id)
            elif widget_master.widget_name == 'purchase_order':
                widget_wise_detail = self.get_user_pending_po(user,company_id)
            elif widget_master.widget_name == 'quotation':
                widget_wise_detail = self.get_user_pending_quotation(user,company_id)
            else:
                widget_wise_detail = []
        print ("get item request mrsssssss",widget_wise_detail)

        return widget_wise_detail


class PurchaseDashBoardValueLine(models.Model):
    _description = "Purchase Dashboard Lines"
    _name= "purchase.dashboard.value.line"


    name = fields.Char('Name')
    user_id = fields.Many2one('res.users','User')
    parent_id = fields.Many2one('purchase.dashboard.value.line')
    child_ids = fields.One2many('purchase.dashboard.value.line','parent_id','Children')
    mrs_name = fields.Char('MRS')
    mrs_id = fields.Many2one('material.req.slip','MRS ID')


class DashBoardWidgetMaster(models.Model):
    _description = "Dashboard Widget Master"
    _name= "dashboard.widget.master"

    MOD_SEL = [
        ('purchase','Purchase'),
        ('inventory','Inventory')
            ]
    WIDGET_NAME = [
        ('mrs', 'MRS'),
        ('material_issue', 'Material Issue'),
        # ('pr', 'PR'),
        # ('rfq', 'RFQ'),
        # ('quotation', 'Quotation'),
        # ('purchase_order', 'PO'),
    ]

    name = fields.Char('Name')
    widget_no = fields.Integer('Widget No')
    widget_name = fields.Selection(WIDGET_NAME,'Widget Name')
    widget_module = fields.Selection(MOD_SEL ,'Module')
    widget_master_lines = fields.One2many('widget.master.line','widget_master_id','Widget Lines')
    widget_obj= fields.Many2one('ir.model','Widget Model')
    company_id = fields.Many2one('res.company','Company')
    widget_model = fields.Many2one('ir.model', 'Model')

    @api.onchange('widget_name')
    def widget_module_change(self):
        line_list=[]
        list1=['draft','confirm','issue','done']
        count= 0
        if self.widget_name :
            if self.widget_name == 'mrs':
                model_id = self.env['ir.model'].search([('model','=','material.req.slip')])
                model = 'material_req_slip'
            elif self.widget_name == 'material_issue':
                model_id = self.env['ir.model'].search([('model','=','material.issue')])
                model = 'material.issue'
            elif self.widget_name == 'pr':
                model_id = self.env['ir.model'].search([('model','=','purchase.req')])
                model = 'purchase_req'
            elif self.widget_name == 'rfq':
                model_id = self.env['ir.model'].search([('model','=','request.for.quotation')])
                model = 'request_for_quotation'
            elif self.widget_name == 'purchase_order':
                model_id = self.env['ir.model'].search([('model', '=', 'purchase.order')])
                model = 'purchase_order'
            elif self.widget_name == 'quotation':
                model_id = self.env['ir.model'].search([('model', '=', 'supplier.quotation')])
                model = 'supplier_quotation'
            else :
                model =''
            name = self.widget_name
            for val in list1 :
                count = count+ 1
                if val == 'draft' :
                    lablel = 'MRS Created'
                    label= "MRS Created"
                    query = "select * from material_req_slip where state='draft'"
                elif val == 'confirm' :
                    lablel = 'Pending For Issue'
                    label = "Pending For Issue"
                    query = "select * from material_req_slip where state in ('in_progress','confirm')"
                elif val == 'issue' :
                    lablel = 'Pending For Receive'
                    label = "Pending For Receive"
                    query = "select * from material_req_slip where state in ('issue')"
                elif val == 'done' :
                    lablel = 'MRS Completed'
                    label = "MRS Completed"
                    query = "select * from material_req_slip where state in ('done')"

                # if len(model) > 0 :
                #     query = 'select * from ' + model + ' where state = '+'\''+label +'\''
                line_tupple=(0,False,{
                    'widget_sequence' : count,
                    'enable':True,
                    'name':lablel,
                    'name11': lablel,
                    'state': val,
                    'widget_query': query or False,
                    'widget_model' : model_id.id or False,
                    'query_type':'query',
                    'widget_name' : self.widget_name,
                })
                line_list.append(line_tupple)
            print ("line listttttt",line_list)

            self.widget_master_lines= line_list
            self.name = name
            self.widget_model = model_id.id or False



class WidgetMasterLine(models.Model):
    _description = "Dashboard Widget Master Line"
    _name= "widget.master.line"
    _order = 'widget_sequence'


    @api.model
    def default_get(self, fields):
        tup_list = []
        res = {}
        widget_model=''
        print("self fielddd", self,self._context)
        if 'default_widget_model' in self._context :
            widget_model = self._context.get('default_widget_model')
        print ("widget model",widget_model)
        # if len(centralized_purchase2) > 0:
        #     centralize_purchase = True
        res = super(WidgetMasterLine, self).default_get(fields)
        res = {'widget_model': widget_model}
        # self.widget_model = widget_model
        # print("ressssss", res)
        return res

    STATE_SEL = [
        ('draft', 'Draft'),
        ('sent_for_approval', 'Sent For Approval'),
        ('in_progress', 'In Progress'),
        ('confirm', 'Confirm'),
        ('approve', 'Approved'),
        ('issue', 'Issue'),
        ('cancel', 'Cancelled'),
        ('done', 'Done')
    ]

    WIDGET_NAME = [
        ('mrs', "MRS"),
        # ('pr', "PR"),
        # ('rfq', "RFQ"),
        # ('quotation', "Quotation"),
        # ('purchase_order', "PO"),
        ('material_issue', "Material Issue"),
    ]

    name = fields.Char('Label')
    name11 = fields.Char('Label')
    widget_sequence = fields.Integer('Widget sequence')
    widget_filter = fields.Char('Filter')
    widget_query = fields.Char('Query',size=1024)
    state = fields.Selection(STATE_SEL,'State')
    enable = fields.Boolean('Enable')
    widget_model = fields.Many2one('ir.model','Model')
    widget_master_id = fields.Many2one('dashboard.widget.master','Dashboard Widget Master')
    widget_filter_lines = fields.One2many('widget.master.filter.line','widget_master_line_id','Filter Lines')
    # query_type = fields.Selection([('pre_defined', 'Pre-Defined'), ('user_defined', 'User-Defined')], "Type",
    #                                  default="user_defined")
    query_type = fields.Selection([('filter', 'Filter'), ('query', 'Query')], "Query Type",
                                  default="query")
    widget_name = fields.Selection(WIDGET_NAME, 'Widget Name')

class WidgetMasterFilterLine(models.Model):
    _description = "Dashboard Widget Master Filter Line"
    _name= "widget.master.filter.line"

    FILTER_SEL = [
        ('contain','Contain'),
        ('not_contain','doesn\'t contain'),
        ('is_equal','Is equal to'),
        ('not_equal', 'Not equal to'),
        ('greater_then','Greater then'),
        ('greater_equal_to','Greater then Equal to'),
        ('less_then','Less then'),
        ('less_equal_to','Lesser then Equal to')

    ]


    filter_field = fields.Many2one('ir.model.fields','Field')
    filter_selection = fields.Selection(FILTER_SEL,'Condition')
    value = fields.Char('Value')
    filter_model = fields.Many2one('ir.model', 'Model')
    widget_master_line_id = fields.Many2one('widget.master.line','Master Line id')

    @api.onchange('filter_model')
    def onchange_filter_model(self):
        model_list=[]
        print("filter model", self, self._context)
        if 'default_widget_name' in self._context:
            widget_name = self._context.get('default_widget_name')
            model_ids = self.env['purchase.dashboard.link.model'].search([('dashboard_widget', '=', widget_name)])
            if len(model_ids) > 0:
                for val in model_ids:
                    print("vallllll", val, val.link_model)
                    model_list.append(val.link_model.id)
        print("widget model", model_list)
        if 'default_filter_model' in self._context:
            widget_model = self._context.get('default_filter_model')
            print("default widget modell", widget_model)
            if widget_model:
                model_list.append(widget_model)
        print("model list final", model_list)
        return {'domain': {'filter_model': [('id', 'in', model_list)]}}

    @api.onchange('value')
    def value_change(self):
        """
        Values automatic flows from the product form onchange of Product
        """
        if self.value:
            if self.filter_field :
                print ("self fliter fielddddd",self.filter_field,self.filter_field.ttype)
                if self.filter_field.ttype in ('datetime','date') :
                    try:
                        if len(self.value) == 10:
                            date_val=datetime.strptime(self.value, '%Y-%m-%d')
                            # print ("date valllllllll",date_val)
                            # return True
                        else:
                            raise ValidationError(_('Please enter date in \'YYYY-MM-DD\' format!'))

                    except ValueError:
                        raise ValidationError(_('Please enter date in \'YYYY-MM-DD\' format!'))






class PurchaseDashboardReport(models.Model):
    _name = "purchase.dashboard.report"
    _description = "Purchase Dashboard Statistics"
    # _auto = False
    # _rec_name = 'mrs_date'
    # _order = 'mrs_date desc'

    field_1 = fields.Char('Field 1')
    field_2 = fields.Char('Field 2')
    field_3 = fields.Char('Field 3')
    field_4 = fields.Char('Field 4')
    field_5 = fields.Char('Field 5')
    field_6 = fields.Char('Field 6')
    field_7 = fields.Char('Field 7')
    field_8 = fields.Char('Field 8')
    field_9 = fields.Char('Field 9')
    name = fields.Char('Name', size=256, required="1", default=fields.Datetime.now)
    company_id = fields.Many2one('res.company', 'Company', default=lambda self: self.env.user.company_id.id)
    mrs_no = fields.Many2one('material.req.slip','MRS' )
    mrs_id = fields.Many2one('material.req.slip', 'MRS')
    rfq_id = fields.Many2one('request.for.quotation', 'MRS')
    department_id = fields.Many2one('hr.department', 'Department', )
    employee_id = fields.Many2one('hr.employee', 'Assigned To')
    mrs_date = fields.Datetime('MRS Date')
    require_date = fields.Datetime('MRS Require Date')

    def get_field_header(self,field,no):
        field_generated = 'field_' + str(no + 1)
        header = field.upper()
        if '_' in header :
            header= header.replace('_',' ')
        field_generated = (field_generated,header)
        print("field generated", field_generated)
        return field_generated


    @api.model
    def fields_view_get(self, view_id=None, view_type='tree', toolbar=False, submenu=False):
        print("self in field view get111111", self, self._context, self._context.get('field_list'))
        # print ("toolbar before gsuperrrrrr",toolbar)

        result = super(PurchaseDashboardReport, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar,
                                                         submenu=submenu)

        doc = etree.XML(result['arch'])
        print ("self in field view get22222222222",self,self._context,self._context.get('field_list'))
        new_string='R Company'
        submenu1=['mrs_no', 'department_id', 'employee_id', 'mrs_date', 'require_date']
        submenu=['rfq', 'pr', 'supplier', 'rfq_date', 'date_order']
        submenu_rev= submenu[::-1]
        # print ("sub menu reverseeeeeee",submenu_rev)
        # print ("sub menu valeeeeeeee",submenu)
        # print("sub menu valeeeeeeee",  toolbar)
        if submenu:
            for node in doc.xpath("// field[@ name='name']"):
                for val in range(len(submenu)-1,-1,-1):
                    if type(submenu[val])== str and 'mrs' in submenu[val] and 'mrs_date' not in str(submenu[val]):
                        field_return = ('mrs_id','MRS')
                        print ("field returnnn",field_return)
                        if field_return :
                            node.addnext(etree.Element('field', {'name': field_return[0],
                                                                    'string': field_return[1],

                                                                    }))
                            result['arch'] = etree.tostring(doc)
                            print ("archhhh",result['arch'])
                    elif type(submenu[val])== str and 'rfq' in submenu[val] and 'rfq_date' not in str(submenu[val]):
                        field_return = ('rfq_id','RFQ')
                        print ("field returnnn",field_return)
                        if field_return :
                            node.addnext(etree.Element('field', {'name': field_return[0],
                                                                    'string': field_return[1],

                                                                    }))
                            result['arch'] = etree.tostring(doc)
                            # print ("archhhh",result['arch'])
                    else :
                        field_return = self.get_field_header(submenu[val], val)
                        print("field returnnn", field_return)
                        if field_return:
                            node.addnext(etree.Element('field', {'name': field_return[0],
                                                                 'string': field_return[1],
                                                                 'nolabel': '0',
                                                                 }))
                            result['arch'] = etree.tostring(doc)
                            # print("archhhh", result['arch'])
        # else :
        #     for node in doc.xpath("// field[@ name='name']"):
        #         result['arch'] = etree.tostring(doc)
        return result

        # if new_string:
        #     for node in doc.xpath("// field[@ name='name']"):
        #
        #         node.set('string', new_string)
        #         node.addnext(etree.Element('field', {'name': 'field_first',
        #                                                 'string': 'Currency',
        #                                                 'nolabel': '0',
        #                                                 }))
        #         node.addnext(etree.Element('field', {'name': 'field_second',
        #                                              'string': 'C Date',
        #                                              'nolabel': '0',
        #                                              }))
        #         node.addnext(etree.Element('field', {'name': 'field_three',
        #                                              'string': 'C ID',
        #                                              'nolabel': '0',
        #                                              }))
        #         node.addnext(etree.Element('field', {'name': 'field_four',
        #                                              'string': 'W Date',
        #                                              'nolabel': '0',
        #                                              }))
        #         node.addnext(etree.Element('field', {'name': 'field_five',
        #                                              'string': 'W ID',
        #                                              'nolabel': '0',
        #                                              }))
        #         result['arch'] = etree.tostring(doc)
        #         print ("archhhh",result['arch'])
        #     return result

    # @api.model_cr
    # def init(self):
    #     # self._table = sale_report
    #     tools.drop_view_if_exists(self.env.cr, self._table)
    #     self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
    #             %s
    #             FROM ( %s )
    #             %s
    #             )""" % (self._table, self._select(), self._from(), self._group_by()))

    # @api.model_cr
    # def get_sql_view_custon(self, query):
    #     # self._table = sale_report
    #     print("sql view custom", self.env.cr, self._table, query)
    #     tools.drop_view_if_exists(self.env.cr, self._table)
    #     self.env.cr.execute("""CREATE or REPLACE VIEW %s as (
    #                 %s
    #                 )""" % (self._table, query))





class PurchaseDashboardLinkModel(models.Model):
    _name = "purchase.dashboard.link.model"
    _description = "Purchase Dashboard Report Table"

    WIDGET_NAME = [
        ('mrs', 'MRS'),
        ('pr', 'PR'),
        ('rfq', 'RFQ'),
        ('quotation', 'Quotation'),
        ('purchase_order', 'PO'),
        ('material_issue', 'Material Issue'),
    ]

    name = fields.Char('Name')
    link_model = fields.Many2one('ir.model', 'Model')
    dashboard_widget = fields.Selection(WIDGET_NAME,'Dashboard Widget')









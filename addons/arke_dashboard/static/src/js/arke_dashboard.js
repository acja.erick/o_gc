odoo.define('arke_dashboard.explore', function (require) {
"use strict";
var core = require('web.core');
var dialogs = require('web.view_dialogs');
var Dialog = require('web.Dialog');
var Widget = require('web.Widget');
var rpc = require('web.rpc');
var ActionManager = require('web.ActionManager');
var AbstractAction = require('web.AbstractAction');
var _t = core._t;
var qweb = core.qweb;

var arkeDashboardUI = AbstractAction.extend({
    title: core._t("New Dialog"),
    template: 'arkeDashboardUI',
    init: function(parent, action, options) {
        this._super.apply(this, arguments);
        this.action = action;
        this.context = action.context;
        this.actionManager = parent;
        this.options = options || {};
    },
    events: {
            'click #getPendingMRS': function(event){this.getPendingMRS(event)},
            'click #getItemrequestedMRS': function(event){this.getItemrequestedMRS(event)},
            'click #getItemAvailableMRS': function(event){this.getItemAvailableMRS(event)},
            'click #getItemProcureMRS': function(event){this.getItemProcureMRS(event)},
            'click #getUserWise': function(event){this.getUserWise(event)},
            'click #getUserWiseSecond': function(event){this.getUserWiseSecond(event)},
            'click #getUserWiseThird': function(event){this.getUserWiseThird(event)},
            'click #getUserWiseFourth': function(event){this.getUserWiseFourth(event)},
            'click #getUserWiseFifeth': function(event){this.getUserWiseFifeth(event)},
            'click #getUserWiseSixth': function(event){this.getUserWiseSixth(event)},
            'click #getUserFirst': function(event){this.getUserFirst(event)},
            'click #getUserPendingFirst': function(event){this.getUserPendingSecond(event)},
            'click #getUserSecond': function(event){this.getUserSecond(event)},
            'click #getUserPendingSecond': function(event){this.getUserPendingSecond(event)},
            'click #getUserThird': function(event){this.getUserThird(event)},
            'click #getUserPendingThird': function(event){this.getUserPendingThird(event)},
            'click #getUserFourth': function(event){this.getUserFourth(event)},
            'click #getUserPendingFourth': function(event){this.getUserPendingFourth(event)},
            'click #getUserFifeth': function(event){this.getUserFifeth(event)},
            'click #getUserPendingFifeth': function(event){this.getUserPendingFifeth(event)},
            'click #getUserSixth': function(event){this.getUserSixth(event)},
            'click #getUserPendingFirstNew' : function(event){this.getUserPendingFirstNew(event)},
            'click #getUserPendingSixth': function(event){this.getUserPendingSixth(event)},
            'click #getQuot': function(event){this.getQuot(event)},
            'click #getQuotNegotiate': function(event){this.getQuotNegotiate(event)},
            'click #getPO': function(event){this.getPO(event)},
            'click [id^=CHECK]' : function(event){this.CHECK(event)},
            'click #historyback': function(event){this.HistoryBack(event)},
            //'click #CHECK': 'CHECK',
//            'click #AccessionLink' : 'loadAccession'
        },

//    init:function(parent,options){
////        this.pr_id = options.context.pr_id;
//        this._super(parent);
//    },
    start: function(){
        var self = this;
        self._super();
        console.log("self in jssss")
        rpc.query({
            model: 'purchase.dashboard',
            method: 'get_purchase_dashboard_value',
            args: [[]]
        }).then(function (returned_value) {
            var prs = {};
            prs = returned_value;
            self.setProductValues(self, prs);

    //        var products = {};
    //        $.getJSON('/purchase_extension/static/src/js/data.json',function(data){
    //            products = prs;
    //        });

//            self.$el.find('#hero-demo').autoComplete({
//                minChars: 1,
//                source: function(term, suggest){
//                    term = term.toLowerCase();
//                    var choices = prs;
//                    var suggestions = [];
//                    for (var i=0;i<choices.length;i++){
//    //                    if (~(choices[i].name+' '+choices[i].code).toLowerCase().indexOf(term)){
//                        if (~(choices[i].name).toLowerCase().indexOf(term)){
//                            suggestions.push(choices[i]);
//                        }
//                    }
//
//                    suggest(suggestions);
//                },
//                renderItem: function(item, search){
//                    search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
//                    var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
//    //                return '<div class="autocomplete-suggestion" data-id="'+item.id+'" data-langname="'+item.name+'" data-lang="'+item.code+'" data-val="'+search+'"><img width=20 height=30 src="/product_explorer/static/src/img/products/'+item.id+'.png"> '+item.name.replace(re, "<b>$1</b>")+'</div>';
//                    return '<div class="autocomplete-suggestion" data-id="'+item.id+'" data-langname="'+item.name+'" data-val="'+search+'"> '+item.name.replace(re, "<b>$1</b>")+'</div>';
//                },
//                onSelect: function(e, term, item){
//                    self.$el.find('#hero-demo').val(item.data('langname'));
//                    self.setProductValues(item.data('id'), prs);
//                }
//            });
        });

    },
    setProductValues: function(pr_id, prs){
        var self=this;
//        console.log('prs in set product values',prs)
//        console.log('prs in set product values222',prs['1'])
        var count=0
        var header=''
        console.log("prs vall value in set product value",prs)
        _.each(prs, function(pr){
            if(pr){
                var list = [
                        { activity1: 'Activity Wise', activity2: 'User Wise', widget_id: pr[0][3] }
                    ];
                console.log('prrrr in each',pr,pr[0][0],count)
                count=count+1
                if (pr[0][4] == 'MRS') {
                    header = 'MRS'
                    }
                else if (pr[0][4] == 'RFQ') {
                    header='RFQ'
                    }
                else if (pr[0][4] == 'PR') {
                    header='PR'
                    }
                else if (pr[0][4] == 'Quotation') {
                    header='Quotation'
                    }
                else if (pr[0][4] == 'PO') {
                    header='Purchase Order'
                    }
                else if (pr[0][4] == 'Issue') {
                    header='Material Issue'
                    }
                else {
                    header ='ND'
                    }

                console.log('countttttttt',list)
                if (count == 1) {
                    self.$el.find('.First').append(qweb.render('First', {allprods :pr,'first_header' :header,allactivities:list,alldetails:[]}))
                    }
                else if (count == 2){
                    self.$el.find('.Second').append(qweb.render('Second', {allprods :pr,'second_header' :header,allactivities:list,alldetails:[]}))
                    }
                else if (count == 3){
                    self.$el.find('.Third').append(qweb.render('Third', {allprods :pr,'third_header' :header,allactivities:list,alldetails:[]}))
                    }
                else if (count == 4){
                    self.$el.find('.Fourth').append(qweb.render('Fourth', {allprods :pr,'fourth_header' :header,allactivities:list,alldetails:[]}))
                    }
                else if (count == 5){
                    self.$el.find('.Fifeth').append(qweb.render('Fifeth', {allprods :pr,'fifeth_header' :header,allactivities:list,alldetails:[]}))
                    }
                else if (count == 6){
                    self.$el.find('.Sixth').append(qweb.render('Sixth', {allprods :pr,'sixth_header' :header,allactivities:list,alldetails:[]}))
                    }
                else {
                    console.log("elseeeeeeeeeeee")
                    }
            }
        });


    },

    getPendingMRS: function(event){
        var self = this;
        var actionManager = new ActionManager(self);
        var widget_id = parseInt(event.target.getAttribute('data-id'));
//        var widget_name = parseInt(event.target.getAttribute('data-name'));
        console.log("widget iddddpending mrsss",widget_id)
//        console.log("widget iddddpending widget nameee",widget_name)
//        var rfq_id = parseInt(self.$el.find('#getRFQ').attr('data-id'));

        rpc.query({
            model: 'purchase.dashboard',
            method: 'get_widget_click_value',
            args: [[],widget_id]
        }).then(function (result) {
            console.log("RRRRRRRR get pending mrs",result)
            console.log("field listttt",result['field_list'])
            actionManager.do_action(result);
        });
    },

    getUserAllMRS: function(event){
        var self = this;
        var actionManager = new ActionManager(self);
        var widget_id = parseInt(event.target.getAttribute('data-id'));
        console.log("user idddd",widget_id)
//        var rfq_id = parseInt(self.$el.find('#getRFQ').attr('data-id'));

        rpc.query({
            model: 'purchase.dashboard',
            method: 'get_widget_click_value',
            args: [[],widget_id]
        }).then(function (result) {
//            console.log("RRRRRRRR",result)
            actionManager.do_action(result);
        });
    },

    getItemrequestedMRS: function(event){
        var self = this;
        var actionManager = new ActionManager(self);
        console.log("get item requestttt")
        var user_id = parseInt(event.target.getAttribute('data-id'));
        var user_name = parseInt(event.target.getAttribute('data-name'));
        console.log('user if item request data user',user_id,user_name)
//        var rfq_id = parseInt(self.$el.find('#getRFQ').attr('data-id'));

        rpc.query({
            model: 'purchase.dashboard',
            method: 'get_user_pending_mrs',
            args: [[],user_id]
        }).then(function (result) {
            console.log("RRRRRRRR in return for mrs user wise",result)
            actionManager.do_action(result);
        });
    },

    getItemAvailableMRS: function(event){
        var self = this;
        var actionManager = new ActionManager(self);
        var user_id = parseInt(event.target.getAttribute('data-id'));
        var json_value = parseInt(event.target.getAttribute('data-name'));
        console.log("data jsonnnn",json_value)
        console.log('user if item request avalibale',user_id)
//        var rfq_id = parseInt(event.target.getAttribute('data-id'));
//        var rfq_id = parseInt(self.$el.find('#getRFQ').attr('data-id'));

        rpc.query({
            model: 'purchase.dashboard',
            method: 'get_user_mrs',
            args: [[],user_id]
        }).then(function (result) {
//            console.log("RRRRRRRR",result)
            actionManager.do_action(result);
        });
    },

    getItemProcureMRS : function(event){
        var self = this;
        var actionManager = new ActionManager(self);
        console.log("get item procure")
//        var rfq_id = parseInt(event.target.getAttribute('data-id'));
//        var rfq_id = parseInt(self.$el.find('#getRFQ').attr('data-id'));

        rpc.query({
            model: 'purchase.dashboard',
            method: 'get_item_procure_mrs',
            args: [[]]
        }).then(function (result) {
            console.log("RRRRRRRR",result)
            actionManager.do_action(result);
        });
    },

    getUserWise : function(event){
        var self = this;
        var actionManager = new ActionManager(self);
        console.log("get User Wise")
        var widget_id = parseInt(self.$el.find('#getUserWise').attr('data-id'));
        console.log("user wise id for get user wise function",widget_id)

        rpc.query({
            model: 'purchase.dashboard',
            method: 'get_user_wise_activity',
            args: [[],widget_id]
        }).then(function (result) {
            console.log("RRRRRRRR",result)
//            var user_vals = ();
//            user_vals = result;
            self.setUserValues(self, result);
            actionManager.do_action(result);
        });
    },

    getUserWiseSecond : function(event){
        var self = this;
        var actionManager = new ActionManager(self);
        console.log("get User Wise")
        var widget_id = parseInt(self.$el.find('#getUserWiseSecond').attr('data-id'));
        console.log("user wise id for get user wise function",widget_id)

        rpc.query({
            model: 'purchase.dashboard',
            method: 'get_user_wise_activity',
            args: [[],widget_id]
        }).then(function (result) {
            console.log("RRRRRRRR",result)
//            var user_vals = ();
//            user_vals = result;
            self.setUserValues(self, result);
            actionManager.do_action(result);
        });
    },

    getUserWiseThird : function(event){
        var self = this;
        var actionManager = new ActionManager(self);
        console.log("get User Wise")
        var widget_id = parseInt(self.$el.find('#getUserWiseThird').attr('data-id'));
        console.log("user wise id for get user wise function",widget_id)

        rpc.query({
            model: 'purchase.dashboard',
            method: 'get_user_wise_activity',
            args: [[],widget_id]
        }).then(function (result) {
            console.log("RRRRRRRR",result)
//            var user_vals = ();
//            user_vals = result;
            self.setUserValues(self, result);
            actionManager.do_action(result);
        });
    },

     getUserWiseFourth : function(event){
        var self = this;
        var actionManager = new ActionManager(self);
        console.log("get User Wise")
        var widget_id = parseInt(self.$el.find('#getUserWiseFourth').attr('data-id'));
        console.log("user wise id for get user wise function",widget_id)

        rpc.query({
            model: 'purchase.dashboard',
            method: 'get_user_wise_activity',
            args: [[],widget_id]
        }).then(function (result) {
            console.log("RRRRRRRR",result)
//            var user_vals = ();
//            user_vals = result;
            self.setUserValues(self, result);
            actionManager.do_action(result);
        });
    },

     getUserWiseFifeth : function(event){
        var self = this;
        var actionManager = new ActionManager(self);
        console.log("get User Wise")
        var widget_id = parseInt(self.$el.find('#getUserWiseFifeth').attr('data-id'));
        console.log("user wise id for get user wise function",widget_id)

        rpc.query({
            model: 'purchase.dashboard',
            method: 'get_user_wise_activity',
            args: [[],widget_id]
        }).then(function (result) {
            console.log("RRRRRRRR",result)
//            var user_vals = ();
//            user_vals = result;
            self.setUserValues(self, result);
            actionManager.do_action(result);
        });
    },

    getUserWiseSixth : function(event){
        var self = this;
        var actionManager = new ActionManager(self);
        console.log("get User Wise")
        var widget_id = parseInt(self.$el.find('#getUserWiseSixth').attr('data-id'));
        console.log("user wise id for get user wise function",widget_id)

        rpc.query({
            model: 'purchase.dashboard',
            method: 'get_user_wise_activity',
            args: [[],widget_id]
        }).then(function (result) {
            console.log("RRRRRRRR",result)
//            var user_vals = ();
//            user_vals = result;
            self.setUserValues(self, result);
            actionManager.do_action(result);
        });
    },



    getUserFirst: function(event){
        console.log("get user wise mrsssss")
        var self = this;
        var actionManager = new ActionManager(self);
        var user_id = parseInt(event.target.getAttribute('data-id'));
        console.log("widget idddd",user_id)
        var widget_id = parseInt(self.$el.find('#getUserWise').attr('data-id'));
        console.log("widget id",widget_id)
//        var rfq_id = parseInt(self.$el.find('#getRFQ').attr('data-id'));

        rpc.query({
            model: 'purchase.dashboard',
            method: 'get_user_widget_wise_data',
            args: [[],user_id,widget_id]
        }).then(function (result) {
//            console.log("RRRRRRRR",result)
            actionManager.do_action(result);
        });
    },

    getUserPendingFirstNew: function(event){
        console.log("get user wise mrsssss")
        var self = this;
        var actionManager = new ActionManager(self);
        var user_id = parseInt(event.target.getAttribute('data-id'));
        console.log("widget idddd",user_id)
        var widget_id = parseInt(self.$el.find('#getUserWise').attr('data-id'));
        console.log("widget id",widget_id)
//        var rfq_id = parseInt(self.$el.find('#getRFQ').attr('data-id'));

        rpc.query({
            model: 'purchase.dashboard',
            method: 'get_user_widget_wise_pending_data',
            args: [[],user_id,widget_id]
        }).then(function (result) {
//            console.log("RRRRRRRR",result)
            actionManager.do_action(result);
        });
    },

    getUserPendingFirst: function(event){
        var self = this;
        var actionManager = new ActionManager(self);
        var user_id = parseInt(event.target.getAttribute('data-id'));
        console.log("widget idddd",user_id)
        var widget_id = parseInt(self.$el.find('#getUserWise').attr('data-id'));
        console.log("widget id",widget_id)
//        var rfq_id = parseInt(self.$el.find('#getRFQ').attr('data-id'));

        rpc.query({
            model: 'purchase.dashboard',
            method: 'get_user_widget_wise_pending_data',
            args: [[],user_id,widget_id]
        }).then(function (result) {
//            console.log("RRRRRRRR",result)
            actionManager.do_action(result);
        });
    },

    getUserSecond: function(event){
        console.log("get user wise mrsssss")
        var self = this;
        var actionManager = new ActionManager(self);
        var user_id = parseInt(event.target.getAttribute('data-id'));
        console.log("user idddd",user_id)
        var widget_id = parseInt(self.$el.find('#getUserWiseSecond').attr('data-id'));
        console.log("widget id",widget_id)

        rpc.query({
            model: 'purchase.dashboard',
            method: 'get_user_widget_wise_data',
            args: [[],user_id,widget_id]
        }).then(function (result) {
//            console.log("RRRRRRRR",result)
            actionManager.do_action(result);
        });
    },

    getUserPendingSecond: function(event){
        var self = this;
        var actionManager = new ActionManager(self);
        var user_id = parseInt(event.target.getAttribute('data-id'));
        console.log("widget idddd",user_id)
        var widget_id = parseInt(self.$el.find('#getUserWiseSecond').attr('data-id'));
        console.log("widget id",widget_id)
//        var rfq_id = parseInt(self.$el.find('#getRFQ').attr('data-id'));

        rpc.query({
            model: 'purchase.dashboard',
            method: 'get_user_widget_wise_pending_data',
            args: [[],user_id,widget_id]
        }).then(function (result) {
//            console.log("RRRRRRRR",result)
            actionManager.do_action(result);
        });
    },

    getUserThird: function(event){
        console.log("get user wise mrsssss")
        var self = this;
        var actionManager = new ActionManager(self);
        var user_id = parseInt(event.target.getAttribute('data-id'));
        console.log("user idddd",user_id)
        var widget_id = parseInt(self.$el.find('#getUserWiseThird').attr('data-id'));
        console.log("widget id",widget_id)

        rpc.query({
            model: 'purchase.dashboard',
            method: 'get_user_widget_wise_data',
            args: [[],user_id,widget_id]
        }).then(function (result) {
//            console.log("RRRRRRRR",result)
            actionManager.do_action(result);
        });
    },

    getUserPendingThird: function(event){
        var self = this;
        var actionManager = new ActionManager(self);
        var user_id = parseInt(event.target.getAttribute('data-id'));
        console.log("widget idddd",user_id)
        var widget_id = parseInt(self.$el.find('#getUserWiseThird').attr('data-id'));
        console.log("widget id",widget_id)
//        var rfq_id = parseInt(self.$el.find('#getRFQ').attr('data-id'));

        rpc.query({
            model: 'purchase.dashboard',
            method: 'get_user_widget_wise_pending_data',
            args: [[],user_id,widget_id]
        }).then(function (result) {
//            console.log("RRRRRRRR",result)
            actionManager.do_action(result);
        });
    },

    getUserFourth: function(event){
        console.log("get user wise mrsssss")
        var self = this;
        var actionManager = new ActionManager(self);
        var user_id = parseInt(event.target.getAttribute('data-id'));
        console.log("user idddd",user_id)
        var widget_id = parseInt(self.$el.find('#getUserWiseFourth').attr('data-id'));
        console.log("widget id",widget_id)

        rpc.query({
            model: 'purchase.dashboard',
            method: 'get_user_widget_wise_data',
            args: [[],user_id,widget_id]
        }).then(function (result) {
//            console.log("RRRRRRRR",result)
            actionManager.do_action(result);
        });
    },

    getUserPendingFourth: function(event){
        var self = this;
        var actionManager = new ActionManager(self);
        var user_id = parseInt(event.target.getAttribute('data-id'));
        console.log("widget idddd",user_id)
        var widget_id = parseInt(self.$el.find('#getUserWiseFourth').attr('data-id'));
        console.log("widget id",widget_id)
//        var rfq_id = parseInt(self.$el.find('#getRFQ').attr('data-id'));

        rpc.query({
            model: 'purchase.dashboard',
            method: 'get_user_widget_wise_pending_data',
            args: [[],user_id,widget_id]
        }).then(function (result) {
//            console.log("RRRRRRRR",result)
            actionManager.do_action(result);
        });
    },

    getUserFifeth: function(event){
        console.log("get user wise mrsssss")
        var self = this;
        var actionManager = new ActionManager(self);
        var user_id = parseInt(event.target.getAttribute('data-id'));
        console.log("user idddd",user_id)
        var widget_id = parseInt(self.$el.find('#getUserWiseFifeth').attr('data-id'));
        console.log("widget id",widget_id)

        rpc.query({
            model: 'purchase.dashboard',
            method: 'get_user_widget_wise_data',
            args: [[],user_id,widget_id]
        }).then(function (result) {
//            console.log("RRRRRRRR",result)
            actionManager.do_action(result);
        });
    },

    getUserPendingFifeth: function(event){
        var self = this;
        var actionManager = new ActionManager(self);
        var user_id = parseInt(event.target.getAttribute('data-id'));
        console.log("widget idddd",user_id)
        var widget_id = parseInt(self.$el.find('#getUserWiseFifeth').attr('data-id'));
        console.log("widget id",widget_id)
//        var rfq_id = parseInt(self.$el.find('#getRFQ').attr('data-id'));

        rpc.query({
            model: 'purchase.dashboard',
            method: 'get_user_widget_wise_pending_data',
            args: [[],user_id,widget_id]
        }).then(function (result) {
//            console.log("RRRRRRRR",result)
            actionManager.do_action(result);
        });
    },

    getUserSixth: function(event){
        console.log("get user wise mrsssss")
        var self = this;
        var actionManager = new ActionManager(self);
        var user_id = parseInt(event.target.getAttribute('data-id'));
        console.log("user idddd",user_id)
        var widget_id = parseInt(self.$el.find('#getUserWiseSixth').attr('data-id'));
        console.log("widget id",widget_id)

        rpc.query({
            model: 'purchase.dashboard',
            method: 'get_user_widget_wise_data',
            args: [[],user_id,widget_id]
        }).then(function (result) {
//            console.log("RRRRRRRR",result)
            actionManager.do_action(result);
        });
    },

    getUserPendingSixth: function(event){
        var self = this;
        var actionManager = new ActionManager(self);
        var user_id = parseInt(event.target.getAttribute('data-id'));
        console.log("widget idddd",user_id)
        var widget_id = parseInt(self.$el.find('#getUserWiseSixth').attr('data-id'));
        console.log("widget id",widget_id)
//        var rfq_id = parseInt(self.$el.find('#getRFQ').attr('data-id'));

        rpc.query({
            model: 'purchase.dashboard',
            method: 'get_user_widget_wise_pending_data',
            args: [[],user_id,widget_id]
        }).then(function (result) {
//            console.log("RRRRRRRR",result)
            actionManager.do_action(result);
        });
    },





    setUserValues: function(pr_id, user_vals){
        var self=this;
        console.log('set user vaue prrrrr',pr_id)
        console.log('set user vaue',user_vals)
////        console.log('prs in set product values',prs)
////        console.log('prs in set product values222',prs['1'])
//        var count=0
//        var header=''
        for (var key in user_vals) {
            console.log("for var keyyyyyyy",key)
            console.log("for var valueeeee",user_vals[key])
            if ( key == 1) {
//                   self.$el.find('.First').removeChild(qweb.render('First', {allprods :[],alldetails :user_vals[key],allactivities:[]}))
                   self.$el.find('.First').append(qweb.render('First', {allprods :[],alldetails :user_vals[key],allactivities:[]}))
            }
            else if ( key == 2) {
                   self.$el.find('.Second').append(qweb.render('Second', {allprods :[],alldetails :user_vals[key],allactivities:[]}))
            }
            else if ( key == 3) {
                   self.$el.find('.Third').append(qweb.render('Third', {allprods :[],alldetails :user_vals[key],allactivities:[]}))
            }
            else if ( key == 4) {
                   self.$el.find('.Fourth').append(qweb.render('Fourth', {allprods :[],alldetails :user_vals[key],allactivities:[]}))
            }
            else if ( key == 5) {
                   self.$el.find('.Fifeth').append(qweb.render('Fifeth', {allprods :[],alldetails :user_vals[key],allactivities:[]}))
            }
            else if ( key == 6) {
                   self.$el.find('.Sixth').append(qweb.render('Sixth', {allprods :[],alldetails :user_vals[key],allactivities:[]}))
            }
        }
//         _.each(user_vals, function(vals){
//            if(vals){
//                console.log('each prssssss vals0000000',vals[0])
//                console.log('vals11111111111',vals[1])
////                if (user_seq == 1) {
////                self.$el.find('.First').append(qweb.render('First', {allprods :[],alldetails :user_vals,allactivities:[]}))
////                }
//            }
//        });
//        self.$el.find('.First').append(qweb.render('First', {allprods :[],alldetails :user_vals,allactivities:[]}))



    },

    getRFQ: function(event){
        var self = this;
        var actionManager = new ActionManager(self);
        var rfq_id = parseInt(event.target.getAttribute('data-id'));
//        var rfq_id = parseInt(self.$el.find('#getRFQ').attr('data-id'));

        rpc.query({
            model: 'quotation.comparision',
            method: 'get_rfq',
            args: [[], rfq_id]
        }).then(function (result) {
            console.log("RRRRRRRR",result)
            actionManager.do_action(result);
        });
    },

    getQuot: function(event){
        var self = this;
        var actionManager = new ActionManager(self);
        var quot_id = parseInt(event.target.getAttribute('data-id'));
        console.log("self=QUOTTTTID==",quot_id);

        rpc.query({
            model: 'quotation.comparision',
            method: 'get_quot',
            args: [[],quot_id]
        }).then(function (result) {
            actionManager.do_action(result);
        });
    },

    getQuotNegotiate: function(event){
        var self = this;
        var actionManager = new ActionManager(self);
        var quot_id = parseInt(event.target.getAttribute('data-id'));

        rpc.query({
            model: 'quotation.comparision',
            method: 'get_quot_negotiate',
            args: [[],quot_id]
        }).then(function (result) {
            actionManager.do_action(result);
        });
    },

    getPO: function(event){
        var self = this;
        var actionManager = new ActionManager(self);
        var quot_id = parseInt(event.target.getAttribute('data-id'));
//        var context = event.target.getAttribute('data-context');
//        console.log("self=QUOTTAAAAAAAAAAAAAATTID==",context, typeof context);
//        var mycontext = JSON.parse(context);
//        console.log("self=Q1111111111==",mycontext, typeof mycontext);
//        var quot_id = mycontext.quot_id;
        var product_ids = []
        var class_name = event.target.getAttribute('class');
//        var tableName = document.getElementsByName("AAAAAAA");
//        console.log("TTTTTTTTTTTTTTTTTTTTTTT",tableName)
        var all_cl = document.getElementsByClassName(class_name);
        console.log("GGGGGGGGGGGGGGGGGG");
        if (all_cl){
            console.log("HHHHHHHHHHHHHHHHHHHHH",all_cl);
            for (var i = 0; i < all_cl.length; i++) {
                console.log("IIIIIIIIIIIIIII",all_cl.length,all_cl[i],all_cl[i].checked)
                if (all_cl[i].type == 'checkbox' && all_cl[i].checked==true){
//                    items[i].checked = true;
//                    console.log("PPPPPPPPPPPPPPP",all_cl[i].parentElement.parentElement.cells[3])
//                    console.log("PPPPPPPPPPPPPPP",all_cl[i].parentElement.parentElement.parentElement.rows[1].cells[0])
                    var product_id = parseInt(all_cl[i].getAttribute('product-id'));
                    product_ids.push(product_id);
                    }
            }
            if (product_ids){
                rpc.query({
                    model: 'quotation.comparision',
                    method: 'generate_po',
                    args: [[], quot_id, product_ids]
                    }).then(function (result) {
                        actionManager.do_action(result);
                        });
            }
        }
    },

    CHECK: function(event) {
        var self = this;
        var status = event.target.checked;
        var check_id = 'tbody #'+event.target.id;
        var check_name = event.target.name;
        if (status == true && check_name == 'thead_check') {
            $(check_id).prop('checked', true);
        }else if (status == false && check_name == 'thead_check') {
            $(check_id).prop('checked', false);
        }else if (status == false && check_name == 'tbody_check'){
            var check_id = document.getElementsByName('thead_check');
            if (check_id){
                for (var i = 0; i < check_id.length; i++){
                    if (check_id[i].id==event.target.id){
                        check_id[i].checked = false;
                    }
                }
            }
        }
    },

    HistoryBack: function(event){
        var self = this;

        this.trigger_up('history_back');
    },


});

core.action_registry.add('arkeDashboard.ui', arkeDashboardUI);
return {
    arkeDashboardUI:arkeDashboardUI
};
});
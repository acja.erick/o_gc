odoo.define('pos_label_print.chrome', function (require) {
"use strict";

var chrome = require('point_of_sale.chrome');
var core = require('web.core');

var _t = core._t;
var _lt = core._lt;
var QWeb = core.qweb;

chrome.ProxyStatusWidget.include({

    set_smart_status: function(status){
        console.log('status==', status);
        if(status.status === 'connected'){
            var warning = false;
            var msg = '';
            if(this.pos.config.iface_scan_via_proxy){
                var scanner = status.drivers.scanner ? status.drivers.scanner.status : false;
                if( scanner != 'connected' && scanner != 'connecting'){
                    warning = true;
                    msg += _t('Scanner');
                }
            }
            if( this.pos.config.iface_print_via_proxy || 
                this.pos.config.iface_cashdrawer ){
                var printer = status.drivers.printer ? status.drivers.printer.status : false;
                if(!printer){
                    printer = status.drivers.escpos ? status.drivers.escpos.status : false;
                }
                if (printer != 'connected' && printer != 'connecting') {
                    warning = true;
                    msg = msg ? msg + ' & ' : msg;
                    msg += _t('Printer');
                }
            }
            if( this.pos.config.iface_electronic_scale ){
                var scale = status.drivers.scale ? status.drivers.scale.status : false;
                if( scale != 'connected' && scale != 'connecting' ){
                    warning = true;
                    msg = msg ? msg + ' & ' : msg;
                    msg += _t('Scale');
                }
            }

            msg = msg ? msg + ' ' + _t('Offline') : msg;
            this.set_status(warning ? 'warning' : 'connected', msg);
        }else{
            this.set_status(status.status, status.msg || '');
        }
    }

});

var LabelPrinterStatusWidget = chrome.StatusWidget.extend({
    template: 'LabelPrinterStatusWidget',
    set_smart_status: function(status){
	
        if(status.status === 'connected'){
            var warning = false;
            var msg = '';
            
            if( this.pos.config.label_proxy_ip){
                var printer = status.drivers.printer ? status.drivers.printer.status : false;
		        if(!printer){
                    printer = status.drivers.escpos ? status.drivers.escpos.status : false
                }
                if (printer != 'connected' && printer != 'connecting') {
                    warning = true;
                    msg = msg ? msg + ' & ' : msg;
                    msg += _t('Printer');
                }
            }
            msg = msg ? msg + ' ' + _t('Offline') : msg;
            this.set_status(warning ? 'warning' : 'connected', msg);
        }else{
            this.set_status(status.status, status.msg || '');
        }
    },
    start: function(){
        var self = this;   
        this.set_smart_status(this.pos.labelProxy.get('status'));

        this.pos.labelProxy.on('change:status',this,function(eh,status){ //FIXME remove duplicate changes 
            self.set_smart_status(status.newValue);
        });

        this.$el.click(function(){
            self.pos.connect_to_label_proxy();
        });
    }
});

chrome.Chrome.include({
	build_widgets: function(){
	    this.widgets.push(
    	    {
            	'name':   'label_printer_status',
            	'widget': LabelPrinterStatusWidget,
	        'append':  '.pos-rightheader',
        	'condition': function(){ return this.pos.config.label_proxy_ip; },
            });
		this._super();
	}
});
return {
    LabelPrinterStatusWidget:LabelPrinterStatusWidget
};
});

odoo.define('pos_label_print.labelScreens', function (require) {
"use strict";

var core = require('web.core');

var QWeb = core.qweb;
var _t = core._t;

var screens = require('point_of_sale.screens');

    
    

    screens.ReceiptScreenWidget.include({
//        print_html: functiion () {
//            this._super();
//            var receipt = QWeb.render('OrderReceipt', this.get_receipt_render_env());
//            this.pos.labelProxy.printer.print_label(receipt);
//	    this._super();
//        },
        getTotalLines: function(order){
            let lineTotal = 0;
            _.each(order.orderlines.models, function(line){
                if(!line.promotion){
                    lineTotal+=line.quantity;
                }
            });
            return lineTotal;

        },
        getLabelData: function(){

            var self = this;
            var order = self.pos.get_order();
            let totalLines = self.getTotalLines(order),
            lineIndex = 0,
            client = '';
            if(order.changed && order.changed.client){
                client = order.changed.client.name;
            }
            
            _.each(order.orderlines.models, function(line){
                let labelData = {};
                if(!line.promotion){
                    if(!self.pos.config.pos_product_categ_ids || (self.pos.config.pos_product_categ_ids && self.pos.config.pos_product_categ_ids.indexOf(line.product.pos_categ_id[0]) < 0)){
                        return
                    }
                    labelData.ordern = order.uid;
                    labelData.lineTotal = totalLines;
                    labelData.product_name = line.product.display_name;
                    labelData.variants = line.variants;
                    labelData.client = client;
                    let i = 0,
                    labels = '';
                    for(i; i< line.variants.length; i++){
                        let variant = line.variants[i];
                        if(variant && variant.value_id){
                            let variantValue = variant.value_id[1];
                            if(!labels.length){
                                labels = (variantValue.split(':')[1]).trim();
                            }else{
                                labels += ' >> ';
                                labels += (variantValue.split(':')[1]).trim();
                            }
                        }
                    }
                    labelData.labels = labels;
                    for(let j=0; j< line.quantity; j++){
                        lineIndex +=1;
                        labelData.lineIndex = lineIndex;
                        var printData = QWeb.render('AttributeLables', labelData);
                        self.pos.labelProxy.printer.print_label(printData);
                    }
                }
            });

        },
        print_html: function () {
            var receipt = "";
            if(!this.pos.config.iot_proxy_version || this.pos.config.iot_proxy_version > 17){
                receipt = QWeb.render('OrderReceipt', this.get_receipt_render_env());
            }else{
                receipt = QWeb.render('XmlReceipt', this.get_receipt_render_env());
            }
            
            if(this.pos.config.label_proxy_ip){
                this.getLabelData();
            }
            
            var self = this;
    		setTimeout(function(){
    	    self.pos.proxy.printer.print_receipt(receipt);
                self.pos.get_order()._printed = true;
    		}, 3000);
        },
    });
});

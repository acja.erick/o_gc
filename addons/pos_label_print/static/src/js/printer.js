odoo.define('pos_label_print.printer', function (require) {
"use strict";

var Session = require('web.Session');
var Printer = require('point_of_sale.Printer').Printer;
var core = require('web.core');
var _t = core._t;


Printer.include({
    print_receipt: function (receipt) {
        var self = this;
        if (receipt) {
            this.receipt_queue.push(receipt);
        }
        function process_next_job() {
            if (self.receipt_queue.length > 0) {
                var r = self.receipt_queue.shift();
                return self.htmlToImg(r)
                    .then(self.send_printing_job.bind(self))
                    .then(self._onIoTActionResult.bind(self))
                    .then(process_next_job)
                    .guardedCatch(self._onIoTActionFail.bind(self));
            }
        }
        if(!self.pos.config.iot_proxy_version || self.pos.config.iot_proxy_version > 17){
            return process_next_job();
        }else{
            self.print_receipt_old();
            return true
        }
    },
    print_receipt_old: function () {
        var self = this;
        
        function process_next_job() {
            if (self.receipt_queue.length > 0) {
                var r = self.receipt_queue.shift();
                let  res = self.send_printing_job_old(r);
            }
        }
        return process_next_job();
    },

    open_cashbox: function () {
        var self = this;
        if(!self.pos.config.iot_proxy_version || self.pos.config.iot_proxy_version > 17){
            return this.connection.rpc('/hw_proxy/default_printer_action', {
                data: {
                    action: 'cashbox'
                }
            }).then(self._onIoTActionResult.bind(self))
                .guardedCatch(self._onIoTActionFail.bind(self));
        }else{
            self.open_cashbox_old();
        }
    },
    open_cashbox_old: function () {
        var self = this;
        this.connection.rpc('/hw_proxy/open_cashbox');
    },

    send_printing_job_old: function (receipt) {
        this.connection.rpc('/hw_proxy/print_xml_receipt', {
            
            receipt: receipt,
        }).then(function(res){
            console.log('');
        });
    },
});
});

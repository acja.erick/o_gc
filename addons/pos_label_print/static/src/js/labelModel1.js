odoo.define('pos_label_print.labelModel1', function (require) {
"use strict";

var models = require('point_of_sale.models');
var labelDevices = require('pos_label_print.labelDevices');
var core = require('web.core');

var _t = core._t;

var posmodel_super = models.PosModel.prototype;
models.PosModel = models.PosModel.extend({
    after_load_server_data: function () {
        var self = this;
	
        return posmodel_super.after_load_server_data.apply(this, arguments).then(function () {
            self.labelProxy = new labelDevices.LabelProxyDevice(self);
            self.labelJobQueue = new labelDevices.LabelJobQueue();
	
            self.connect_to_label_proxy();
        });
    },
    destroy: function(){
        this._super();
        this.labelProxy.disconnect();
        
    },

   connect_to_label_proxy: function () {
            var self = this;
	    
            return new Promise(function (resolve, reject) {
                self.chrome.loading_message(_t('Connecting to the abel print IoT Box'), 0);
                self.chrome.loading_skip(function () {
                    self.labelProxy.stop_searching();
                });
                self.labelProxy.autoconnect({
                    force_ip: self.config.label_proxy_ip || undefined,
                    progress: function(prog){
                        self.chrome.loading_progress(prog);
                    },
                }).then(
                    function (statusText, url) {
                        var show_loading_error = (self.gui.current_screen === null);
                        resolve();
                        if (show_loading_error && statusText == 'error' && window.location.protocol == 'https:') {
                            self.gui.show_popup('alert', {
                                title: _t('HTTPS connection to label print IoT Box failed'),
                                body: _.str.sprintf(
                                  _t('Make sure you are using label print IoT Box v18.12 or higher. Navigate to %s to accept the certificate of your IoT Box.'),
                                  url
                                ),
                            });
                        }
                    }
                );
            });
        }

});
});

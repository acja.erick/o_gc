odoo.define('pos_label_print.labelPrinter', function (require) {
"use strict";

var Session = require('web.Session');
var core = require('web.core');
var _t = core._t;

var LabelPrinterMixin = {
    init: function () {
        this.label_queue = [];
    },

    /**
     * Add the receipt to the queue of receipts to be printed and process it.
     * @param {String} receipt: The receipt to be printed, in HTML
     */
    print_label: function (label) {
        var self = this;
        if (label) {
            this.label_queue.push(label);
        }
        function process_next_job() {
		//console.log('self process job request=', self);
          if (self.label_queue.length > 0) {
                var r = self.label_queue.shift();
             //   return self.htmlToImg(r)
             let  res = self.send_printing_job(r);
		   // console.log('res===', res);
		    return res
//                    .then(self._onIoTActionResult.bind(self))
  //                  .then(process_next_job)
    //                .guardedCatch(self._onIoTActionFail.bind(self));
            }
        }
        return process_next_job();
    },

    /**
     * Generate a jpeg image from a canvas
     * @param {DOMElement} canvas 
     */
    process_canvas: function (canvas) {
        return canvas.toDataURL('image/jpeg').replace('data:image/jpeg;base64,','');
    },

    /**
     * Renders the html as an image to print it
     * @param {String} receipt: The receipt to be printed, in HTML
     */
/*    htmlToImg: function (label) {
        var self = this;
        $('.pos-label-print').html(label);
        var promise = new Promise(function (resolve, reject) {
            self.label = $('.pos-label-print>.pos-label');
            html2canvas(self.label[0], {
                onparsed: function(queue) {
                    queue.stack.ctx.height = Math.ceil(self.label.outerHeight() + self.label.offset().top);
                },
                onrendered: function (canvas) {
                    $('.pos-label-print').empty();
                    resolve(self.process_canvas(canvas));
                } 
            })
        });
        return promise;
    },*/

   htmlToImg: function (receipt) {
        var self = this;
	   //console.log('htmltoimg start=', self);
        $('.pos-receipt-print').html(receipt);
        var promise = new Promise(function (resolve, reject) {
            //let rr = "<div class='pos-receipt' style='height:30px; width:20px'><span>You label will be print here</span>";
            self.receipt = $('.pos-receipt-print>.pos-receipt');
	    //self.receipt = $(rr);
	    //console.log('receipt=', self.receipt, self.receipt.outerHeight() + self.receipt.offset().top);
            html2canvas(self.receipt[0], {
                onparsed: function(queue) {
                    queue.stack.ctx.height = Math.ceil(self.receipt.outerHeight() + self.receipt.offset().top);
                },
                onrendered: function (canvas) {
                    //$('.pos-receipt-print').empty();
                    resolve(self.process_canvas(canvas));
                } 
            })
        });
        return promise;
    },

    _onIoTActionResult: function (data){
        if (this.pos && (data === false || data.result === false)) {
            this.pos.gui.show_popup('error',{
                'title': _t('Connection to the label printer failed'),
                'body':  _t('Please check if the label printer is still connected.'),
            });
        }
    },

    _onIoTActionFail: function () {
        if (this.pos) {
            this.pos.gui.show_popup('error',{
                'title': _t('Connection to label IoT Box failed'),
                'body':  _t('Please check if the label IoT Box is still connected.'),
            });
        }
    },
}

var LabelPrinter = core.Class.extend(LabelPrinterMixin, {
    init: function (url, pos) {
        LabelPrinterMixin.init.call(this, arguments);
        this.pos = pos;
        this.connection = new Session(undefined, url || 'http://localhost:8069', { use_cors: true});
	    //console.log('printer initialized', this);
    },

    /**
     * Sends the printing command the connected proxy
     * @param {String} img : The receipt to be printed, as an image
     */
    send_printing_job: function (img) {
	   // console.log('send_printing_job=', this, this.connection);
//        
	  this.connection.rpc('/hw_proxy/print_xml_receipt', {
            //data: {
              //  action: 'print_receipt',
                receipt: img,
            //}
        }).then(function(res){
		console.log('res===', res);
	});
    },
});

return {
    LabelPrinterMixin: LabelPrinterMixin,
    LabelPrinter: LabelPrinter,
}
});

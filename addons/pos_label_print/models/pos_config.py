#!/usr/bin/python
# -*- coding: utf-8 -*-

import requests
import json
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo import fields, api, models, _


class PosConfig(models.Model):

    _inherit = 'pos.config'

    label_proxy_ip = fields.Char('Label Printer IP')
    iot_proxy_version = fields.Integer('IOT Version')
    pos_product_categ_ids = fields.Many2many('pos.category', relation="pos_product_categ_ids_rel", string='POS Product Category',
        help='The label printer will print labels only for under mentioned categories.')

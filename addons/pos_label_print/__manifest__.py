# -*- coding: utf-8 -*-
{
    'name': 'POS Label Print',
    'version': '1.0.1',
    'category': 'hardware',
    'sequence': 30,
    'summary': 'To print labels alongwith receipt on different printer',
    'description': "",
    'depends': ['web','point_of_sale'],
    'data': [
        'views/pos_label_print.xml',
        'views/pos_config_view.xml'
    ],
    'demo': [
    ],
    'installable': True,
    'application': False,
    'qweb': ['static/src/xml/*'],
    'website': ''
}
